
naken_util - by Michael Kohn
                Joe Davisson
    Web: http://www.mikekohn.net/
  Email: mike@mikekohn.net

Version: April 11, 2018

Loaded bin ./TSDZ2_original_firmware.bin from 0x8000 to 0xbfff
Type help for a list of commands.

Addr    Opcode Instruction                              Cycles
------- ------ ----------------------------------       ------
0x8000:  82 00 80 90    int $8090                                cycles=2
0x8004:  82 00 bb 49    int $bb49                                cycles=2
0x8008:  82 00 bc 00    int $bc00                                cycles=2
0x800c:  82 00 bc 00    int $bc00                                cycles=2
0x8010:  82 00 bc 00    int $bc00                                cycles=2
0x8014:  82 00 bc 00    int $bc00                                cycles=2
0x8018:  82 00 bc 00    int $bc00                                cycles=2
0x801c:  82 00 ac 5c    int $ac5c                                cycles=2
0x8020:  82 00 ac 5c    int $ac5c                                cycles=2
0x8024:  82 00 ac 5c    int $ac5c                                cycles=2
0x8028:  82 00 bc 00    int $bc00                                cycles=2
0x802c:  82 00 bc 00    int $bc00                                cycles=2
0x8030:  82 00 bc 00    int $bc00                                cycles=2
0x8034:  82 00 ac fe    int $acfe                                cycles=2
0x8038:  82 00 ad 2f    int $ad2f                                cycles=2
0x803c:  82 00 bc 00    int $bc00                                cycles=2
0x8040:  82 00 bc 00    int $bc00                                cycles=2
0x8044:  82 00 af cb    int $afcb                                cycles=2
0x8048:  82 00 af ae    int $afae                                cycles=2
0x804c:  82 00 bc 00    int $bc00                                cycles=2
0x8050:  82 00 bc 00    int $bc00                                cycles=2
0x8054:  82 00 bc 00    int $bc00                                cycles=2
0x8058:  82 00 bc 00    int $bc00                                cycles=2
0x805c:  82 00 ba f3    int $baf3                                cycles=2
0x8060:  82 00 bc 00    int $bc00                                cycles=2
0x8064:  82 00 af e7    int $afe7                                cycles=2
0x8068:  82 00 bc 00    int $bc00                                cycles=2
0x806c:  00 00          neg ($00,SP)                             cycles=1
0x806e:  00 00          neg ($00,SP)                             cycles=1
0x8070:  00 00          neg ($00,SP)                             cycles=1
0x8072:  00 00          neg ($00,SP)                             cycles=1
0x8074:  00 00          neg ($00,SP)                             cycles=1
0x8076:  00 00          neg ($00,SP)                             cycles=1
0x8078:  00 00          neg ($00,SP)                             cycles=1
0x807a:  00 00          neg ($00,SP)                             cycles=1
0x807c:  00 00          neg ($00,SP)                             cycles=1
0x807e:  00 00          neg ($00,SP)                             cycles=1
0x8080:  00 7a          neg ($7a,SP)                             cycles=1
0x8082:  80             iret                                     cycles=11
0x8083:  5c             incw X                                   cycles=1
0x8084:  60 8c          neg ($8c,X)                              cycles=1
0x8086:  01             rrwa X, A                                cycles=1
0x8087:  01             rrwa X, A                                cycles=1
0x8088:  02             rlwa X, A                                cycles=1
0x8089:  04 06          srl ($06,SP)                             cycles=1
0x808b:  01             rrwa X, A                                cycles=1
0x808c:  03 05          cpl ($05,SP)                             cycles=1
0x808e:  00 01          neg ($01,SP)                             cycles=1
0x8090:  9b             sim                                      cycles=1
0x8091:  72 00 50 b3 19 btjt $50b3, #0, $80af  (offset=25)       cycles=2-3
0x8096:  72 02 50 b3 14 btjt $50b3, #1, $80af  (offset=20)       cycles=2-3
0x809b:  72 04 50 b3 0f btjt $50b3, #2, $80af  (offset=15)       cycles=2-3
0x80a0:  72 06 50 b3 0a btjt $50b3, #3, $80af  (offset=10)       cycles=2-3
0x80a5:  72 08 50 b3 05 btjt $50b3, #4, $80af  (offset=5)        cycles=2-3
0x80aa:  cd 9c 26       call $9c26                               cycles=4
0x80ad:  20 07          jra $80b6  (offset=7)                    cycles=2
0x80af:  cd 9c 26       call $9c26                               cycles=4
0x80b2:  72 10 00 97    bset $97, #0                             cycles=1
0x80b6:  5f             clrw X                                   cycles=1
0x80b7:  a6 00          ld A, #$00                               cycles=1
0x80b9:  97             ld XL, A                                 cycles=1
0x80ba:  d6 40 00       ld A, ($4000,X)                          cycles=1
0x80bd:  a1 78          cp A, #$78                               cycles=1
0x80bf:  25 04          jrc $80c5  (offset=4)                    cycles=1-2
0x80c1:  a6 78          ld A, #$78                               cycles=1
0x80c3:  20 06          jra $80cb  (offset=6)                    cycles=2
0x80c5:  a1 20          cp A, #$20                               cycles=1
0x80c7:  24 02          jrnc $80cb  (offset=2)                   cycles=1-2
0x80c9:  a6 20          ld A, #$20                               cycles=1
0x80cb:  b7 9c          ld $9c,A                                 cycles=1
0x80cd:  b7 9f          ld $9f,A                                 cycles=1
0x80cf:  b6 9f          ld A, $9f                                cycles=1
0x80d1:  5f             clrw X                                   cycles=1
0x80d2:  97             ld XL, A                                 cycles=1
0x80d3:  a6 11          ld A, #$11                               cycles=1
0x80d5:  42             mul X, A                                 cycles=4
0x80d6:  a6 1a          ld A, #$1a                               cycles=1
0x80d8:  62             div X, A                                 cycles=2-17
0x80d9:  9f             ld A, XL                                 cycles=1
0x80da:  b7 a1          ld $a1,A                                 cycles=1
0x80dc:  b6 9f          ld A, $9f                                cycles=1
0x80de:  5f             clrw X                                   cycles=1
0x80df:  97             ld XL, A                                 cycles=1
0x80e0:  a6 20          ld A, #$20                               cycles=1
0x80e2:  42             mul X, A                                 cycles=4
0x80e3:  a6 28          ld A, #$28                               cycles=1
0x80e5:  62             div X, A                                 cycles=2-17
0x80e6:  9f             ld A, XL                                 cycles=1
0x80e7:  b7 a0          ld $a0,A                                 cycles=1
0x80e9:  b6 9c          ld A, $9c                                cycles=1
0x80eb:  5f             clrw X                                   cycles=1
0x80ec:  97             ld XL, A                                 cycles=1
0x80ed:  a6 0b          ld A, #$0b                               cycles=1
0x80ef:  42             mul X, A                                 cycles=4
0x80f0:  a6 1a          ld A, #$1a                               cycles=1
0x80f2:  62             div X, A                                 cycles=2-17
0x80f3:  9f             ld A, XL                                 cycles=1
0x80f4:  b6 9c          ld A, $9c                                cycles=1
0x80f6:  5f             clrw X                                   cycles=1
0x80f7:  97             ld XL, A                                 cycles=1
0x80f8:  a6 02          ld A, #$02                               cycles=1
0x80fa:  42             mul X, A                                 cycles=4
0x80fb:  a6 13          ld A, #$13                               cycles=1
0x80fd:  62             div X, A                                 cycles=2-17
0x80fe:  9f             ld A, XL                                 cycles=1
0x80ff:  b6 9c          ld A, $9c                                cycles=1
0x8101:  5f             clrw X                                   cycles=1
0x8102:  97             ld XL, A                                 cycles=1
0x8103:  a6 29          ld A, #$29                               cycles=1
0x8105:  42             mul X, A                                 cycles=4
0x8106:  a6 28          ld A, #$28                               cycles=1
0x8108:  62             div X, A                                 cycles=2-17
0x8109:  a3 00 ff       cpw X, #$ff                              cycles=2
0x810c:  24 05          jrnc $8113  (offset=5)                   cycles=1-2
0x810e:  9f             ld A, XL                                 cycles=1
0x810f:  b7 9b          ld $9b,A                                 cycles=1
0x8111:  20 04          jra $8117  (offset=4)                    cycles=2
0x8113:  a6 ff          ld A, #$ff                               cycles=1
0x8115:  b7 9b          ld $9b,A                                 cycles=1
0x8117:  b6 a0          ld A, $a0                                cycles=1
0x8119:  b6 9c          ld A, $9c                                cycles=1
0x811b:  5f             clrw X                                   cycles=1
0x811c:  97             ld XL, A                                 cycles=1
0x811d:  a6 20          ld A, #$20                               cycles=1
0x811f:  42             mul X, A                                 cycles=4
0x8120:  a6 28          ld A, #$28                               cycles=1
0x8122:  62             div X, A                                 cycles=2-17
0x8123:  9f             ld A, XL                                 cycles=1
0x8124:  b7 38          ld $38,A                                 cycles=1
0x8126:  b6 9f          ld A, $9f                                cycles=1
0x8128:  5f             clrw X                                   cycles=1
0x8129:  97             ld XL, A                                 cycles=1
0x812a:  a6 50          ld A, #$50                               cycles=1
0x812c:  42             mul X, A                                 cycles=4
0x812d:  a6 64          ld A, #$64                               cycles=1
0x812f:  62             div X, A                                 cycles=2-17
0x8130:  9f             ld A, XL                                 cycles=1
0x8131:  b7 9f          ld $9f,A                                 cycles=1
0x8133:  5f             clrw X                                   cycles=1
0x8134:  97             ld XL, A                                 cycles=1
0x8135:  a6 04          ld A, #$04                               cycles=1
0x8137:  42             mul X, A                                 cycles=4
0x8138:  a6 05          ld A, #$05                               cycles=1
0x813a:  62             div X, A                                 cycles=2-17
0x813b:  9f             ld A, XL                                 cycles=1
0x813c:  b7 9e          ld $9e,A                                 cycles=1
0x813e:  5f             clrw X                                   cycles=1
0x813f:  a6 00          ld A, #$00                               cycles=1
0x8141:  97             ld XL, A                                 cycles=1
0x8142:  d6 40 00       ld A, ($4000,X)                          cycles=1
0x8145:  a1 c8          cp A, #$c8                               cycles=1
0x8147:  25 04          jrc $814d  (offset=4)                    cycles=1-2
0x8149:  a6 c8          ld A, #$c8                               cycles=1
0x814b:  20 06          jra $8153  (offset=6)                    cycles=2
0x814d:  a1 0a          cp A, #$0a                               cycles=1
0x814f:  24 02          jrnc $8153  (offset=2)                   cycles=1-2
0x8151:  a6 0a          ld A, #$0a                               cycles=1
0x8153:  b7 9c          ld $9c,A                                 cycles=1
0x8155:  b6 9c          ld A, $9c                                cycles=1
0x8157:  5f             clrw X                                   cycles=1
0x8158:  97             ld XL, A                                 cycles=1
0x8159:  a6 08          ld A, #$08                               cycles=1
0x815b:  42             mul X, A                                 cycles=4
0x815c:  a6 64          ld A, #$64                               cycles=1
0x815e:  62             div X, A                                 cycles=2-17
0x815f:  9f             ld A, XL                                 cycles=1
0x8160:  b7 b0          ld $b0,A                                 cycles=1
0x8162:  b6 9c          ld A, $9c                                cycles=1
0x8164:  5f             clrw X                                   cycles=1
0x8165:  97             ld XL, A                                 cycles=1
0x8166:  a6 23          ld A, #$23                               cycles=1
0x8168:  42             mul X, A                                 cycles=4
0x8169:  a6 0a          ld A, #$0a                               cycles=1
0x816b:  62             div X, A                                 cycles=2-17
0x816c:  a3 00 ff       cpw X, #$ff                              cycles=2
0x816f:  25 03          jrc $8174  (offset=3)                    cycles=1-2
0x8171:  a6 ff          ld A, #$ff                               cycles=1
0x8173:  97             ld XL, A                                 cycles=1
0x8174:  9f             ld A, XL                                 cycles=1
0x8175:  b7 9d          ld $9d,A                                 cycles=1
0x8177:  5f             clrw X                                   cycles=1
0x8178:  a6 01          ld A, #$01                               cycles=1
0x817a:  97             ld XL, A                                 cycles=1
0x817b:  d6 40 00       ld A, ($4000,X)                          cycles=1
0x817e:  a1 c8          cp A, #$c8                               cycles=1
0x8180:  25 04          jrc $8186  (offset=4)                    cycles=1-2
0x8182:  a6 c8          ld A, #$c8                               cycles=1
0x8184:  20 06          jra $818c  (offset=6)                    cycles=2
0x8186:  a1 21          cp A, #$21                               cycles=1
0x8188:  24 02          jrnc $818c  (offset=2)                   cycles=1-2
0x818a:  a6 21          ld A, #$21                               cycles=1
0x818c:  b7 a2          ld $a2,A                                 cycles=1
0x818e:  b6 a2          ld A, $a2                                cycles=1
0x8190:  44             srl A                                    cycles=1
0x8191:  97             ld XL, A                                 cycles=1
0x8192:  a6 03          ld A, #$03                               cycles=1
0x8194:  42             mul X, A                                 cycles=4
0x8195:  9f             ld A, XL                                 cycles=1
0x8196:  b7 6d          ld $6d,A                                 cycles=1
0x8198:  b6 a2          ld A, $a2                                cycles=1
0x819a:  a1 4c          cp A, #$4c                               cycles=1
0x819c:  24 3a          jrnc $81d8  (offset=58)                  cycles=1-2
0x819e:  a6 73          ld A, #$73                               cycles=1
0x81a0:  b7 ad          ld $ad,A                                 cycles=1
0x81a2:  a6 64          ld A, #$64                               cycles=1
0x81a4:  b7 ae          ld $ae,A                                 cycles=1
0x81a6:  b6 a2          ld A, $a2                                cycles=1
0x81a8:  ab 09          add A, #$09                              cycles=1
0x81aa:  b7 a3          ld $a3,A                                 cycles=1
0x81ac:  a6 03          ld A, #$03                               cycles=1
0x81ae:  b7 af          ld $af,A                                 cycles=1
0x81b0:  a6 2d          ld A, #$2d                               cycles=1
0x81b2:  b7 d8          ld $d8,A                                 cycles=1
0x81b4:  b6 9c          ld A, $9c                                cycles=1
0x81b6:  5f             clrw X                                   cycles=1
0x81b7:  97             ld XL, A                                 cycles=1
0x81b8:  a6 a0          ld A, #$a0                               cycles=1
0x81ba:  42             mul X, A                                 cycles=4
0x81bb:  a6 64          ld A, #$64                               cycles=1
0x81bd:  62             div X, A                                 cycles=2-17
0x81be:  9f             ld A, XL                                 cycles=1
0x81bf:  b7 a7          ld $a7,A                                 cycles=1
0x81c1:  b6 9c          ld A, $9c                                cycles=1
0x81c3:  5f             clrw X                                   cycles=1
0x81c4:  97             ld XL, A                                 cycles=1
0x81c5:  a6 1d          ld A, #$1d                               cycles=1
0x81c7:  42             mul X, A                                 cycles=4
0x81c8:  a6 0a          ld A, #$0a                               cycles=1
0x81ca:  62             div X, A                                 cycles=2-17
0x81cb:  a3 00 ff       cpw X, #$ff                              cycles=2
0x81ce:  25 03          jrc $81d3  (offset=3)                    cycles=1-2
0x81d0:  a6 ff          ld A, #$ff                               cycles=1
0x81d2:  97             ld XL, A                                 cycles=1
0x81d3:  9f             ld A, XL                                 cycles=1
0x81d4:  b7 f5          ld $f5,A                                 cycles=1
0x81d6:  20 38          jra $8210  (offset=56)                   cycles=2
0x81d8:  a6 4f          ld A, #$4f                               cycles=1
0x81da:  b7 ad          ld $ad,A                                 cycles=1
0x81dc:  a6 64          ld A, #$64                               cycles=1
0x81de:  b7 ae          ld $ae,A                                 cycles=1
0x81e0:  b6 a2          ld A, $a2                                cycles=1
0x81e2:  ab 06          add A, #$06                              cycles=1
0x81e4:  b7 a3          ld $a3,A                                 cycles=1
0x81e6:  a6 01          ld A, #$01                               cycles=1
0x81e8:  b7 af          ld $af,A                                 cycles=1
0x81ea:  a6 3f          ld A, #$3f                               cycles=1
0x81ec:  b7 d8          ld $d8,A                                 cycles=1
0x81ee:  b6 9c          ld A, $9c                                cycles=1
0x81f0:  5f             clrw X                                   cycles=1
0x81f1:  97             ld XL, A                                 cycles=1
0x81f2:  a6 76          ld A, #$76                               cycles=1
0x81f4:  42             mul X, A                                 cycles=4
0x81f5:  a6 64          ld A, #$64                               cycles=1
0x81f7:  62             div X, A                                 cycles=2-17
0x81f8:  9f             ld A, XL                                 cycles=1
0x81f9:  b7 a7          ld $a7,A                                 cycles=1
0x81fb:  b6 9c          ld A, $9c                                cycles=1
0x81fd:  5f             clrw X                                   cycles=1
0x81fe:  97             ld XL, A                                 cycles=1
0x81ff:  a6 15          ld A, #$15                               cycles=1
0x8201:  42             mul X, A                                 cycles=4
0x8202:  a6 0a          ld A, #$0a                               cycles=1
0x8204:  62             div X, A                                 cycles=2-17
0x8205:  a3 00 ff       cpw X, #$ff                              cycles=2
0x8208:  25 03          jrc $820d  (offset=3)                    cycles=1-2
0x820a:  a6 ff          ld A, #$ff                               cycles=1
0x820c:  97             ld XL, A                                 cycles=1
0x820d:  9f             ld A, XL                                 cycles=1
0x820e:  b7 f5          ld $f5,A                                 cycles=1
0x8210:  a6 19          ld A, #$19                               cycles=1
0x8212:  b7 f2          ld $f2,A                                 cycles=1
0x8214:  a6 1a          ld A, #$1a                               cycles=1
0x8216:  b7 f1          ld $f1,A                                 cycles=1
0x8218:  a6 95          ld A, #$95                               cycles=1
0x821a:  b7 f3          ld $f3,A                                 cycles=1
0x821c:  a6 5f          ld A, #$5f                               cycles=1
0x821e:  b7 a5          ld $a5,A                                 cycles=1
0x8220:  a6 64          ld A, #$64                               cycles=1
0x8222:  b7 fc          ld $fc,A                                 cycles=1
0x8224:  72 00 00 97 06 btjt $97, #0, $822f  (offset=6)          cycles=2-3
0x8229:  cd 9e b4       call $9eb4                               cycles=4
0x822c:  cd a1 aa       call $a1aa                               cycles=4
0x822f:  cd a4 8a       call $a48a                               cycles=4
0x8232:  cd a4 42       call $a442                               cycles=4
0x8235:  cd a4 43       call $a443                               cycles=4
0x8238:  cd a4 b2       call $a4b2                               cycles=4
0x823b:  cd a4 6b       call $a46b                               cycles=4
0x823e:  cd ab 28       call $ab28                               cycles=4
0x8241:  cd ab 98       call $ab98                               cycles=4
0x8244:  72 14 00 8f    bset $8f, #2                             cycles=1
0x8248:  9a             rim                                      cycles=1
0x8249:  72 14 52 45    bset $5245, #2                           cycles=1
0x824d:  72 00 00 97 2f btjt $97, #0, $8281  (offset=47)         cycles=2-3
0x8252:  cd 9e 94       call $9e94                               cycles=4
0x8255:  cd 9e 94       call $9e94                               cycles=4
0x8258:  cd 9e 94       call $9e94                               cycles=4
0x825b:  cd 9e 94       call $9e94                               cycles=4
0x825e:  cd 9e 94       call $9e94                               cycles=4
0x8261:  cd 9e 94       call $9e94                               cycles=4
0x8264:  cd 9e 94       call $9e94                               cycles=4
0x8267:  cd 9e 94       call $9e94                               cycles=4
0x826a:  cd 9e 94       call $9e94                               cycles=4
0x826d:  cd 9e 94       call $9e94                               cycles=4
0x8270:  cd 9e 94       call $9e94                               cycles=4
0x8273:  cd 9e 94       call $9e94                               cycles=4
0x8276:  cd 9e 94       call $9e94                               cycles=4
0x8279:  cd 9e 94       call $9e94                               cycles=4
0x827c:  cd a2 cd       call $a2cd                               cycles=4
0x827f:  20 00          jra $8281  (offset=0)                    cycles=2
0x8281:  72 15 00 8f    bres $8f, #2                             cycles=1
0x8285:  cd 9c 06       call $9c06                               cycles=4
0x8288:  cd 89 91       call $8991                               cycles=4
0x828b:  cd ac 33       call $ac33                               cycles=4
0x828e:  cd 85 bd       call $85bd                               cycles=4
0x8291:  72 18 00 97    bset $97, #4                             cycles=1
0x8295:  cd a3 8c       call $a38c                               cycles=4
0x8298:  cd a0 30       call $a030                               cycles=4
0x829b:  cd a0 80       call $a080                               cycles=4
0x829e:  cd a7 c1       call $a7c1                               cycles=4
0x82a1:  cd a7 fd       call $a7fd                               cycles=4
0x82a4:  cd 9e cd       call $9ecd                               cycles=4
0x82a7:  cd 9f 0f       call $9f0f                               cycles=4
0x82aa:  cd 82 e0       call $82e0                               cycles=4
0x82ad:  cd 82 f5       call $82f5                               cycles=4
0x82b0:  cd 82 c9       call $82c9                               cycles=4
0x82b3:  72 12 00 8f    bset $8f, #1                             cycles=1
0x82b7:  72 08 00 8d 07 btjt $8d, #4, $82c3  (offset=7)          cycles=2-3
0x82bc:  72 02 00 8f 02 btjt $8f, #1, $82c3  (offset=2)          cycles=2-3
0x82c1:  20 00          jra $82c3  (offset=0)                    cycles=2
0x82c3:  cd 9e 8a       call $9e8a                               cycles=4
0x82c6:  cc 82 91       jp $8291                                 cycles=1
0x82c9:  b6 7c          ld A, $7c                                cycles=1
0x82cb:  a1 06          cp A, #$06                               cycles=1
0x82cd:  25 04          jrc $82d3  (offset=4)                    cycles=1-2
0x82cf:  a6 15          ld A, #$15                               cycles=1
0x82d1:  20 02          jra $82d5  (offset=2)                    cycles=2
0x82d3:  a6 bc          ld A, #$bc                               cycles=1
0x82d5:  b7 0d          ld $0d,A                                 cycles=1
0x82d7:  b6 e1          ld A, $e1                                cycles=1
0x82d9:  b7 78          ld $78,A                                 cycles=1
0x82db:  b6 20          ld A, $20                                cycles=1
0x82dd:  b7 79          ld $79,A                                 cycles=1
0x82df:  81             ret                                      cycles=4
0x82e0:  b6 1f          ld A, $1f                                cycles=1
0x82e2:  b1 6d          cp A, $6d                                cycles=1
0x82e4:  24 06          jrnc $82ec  (offset=6)                   cycles=1-2
0x82e6:  72 1d 00 90    bres $90, #6                             cycles=1
0x82ea:  20 08          jra $82f4  (offset=8)                    cycles=2
0x82ec:  72 1c 00 90    bset $90, #6                             cycles=1
0x82f0:  72 18 00 8d    bset $8d, #4                             cycles=1
0x82f4:  81             ret                                      cycles=4
0x82f5:  a6 85          ld A, #$85                               cycles=1
0x82f7:  b1 fb          cp A, $fb                                cycles=1
0x82f9:  25 25          jrc $8320  (offset=37)                   cycles=1-2
0x82fb:  a6 76          ld A, #$76                               cycles=1
0x82fd:  b1 fb          cp A, $fb                                cycles=1
0x82ff:  25 2c          jrc $832d  (offset=44)                   cycles=1-2
0x8301:  b6 fb          ld A, $fb                                cycles=1
0x8303:  a1 2d          cp A, #$2d                               cycles=1
0x8305:  25 10          jrc $8317  (offset=16)                   cycles=1-2
0x8307:  a0 2d          sub A, #$2d                              cycles=1
0x8309:  5f             clrw X                                   cycles=1
0x830a:  97             ld XL, A                                 cycles=1
0x830b:  d6 83 37       ld A, ($8337,X)                          cycles=1
0x830e:  b1 fc          cp A, $fc                                cycles=1
0x8310:  27 1b          jreq $832d  (offset=27)                  cycles=1-2
0x8312:  24 0c          jrnc $8320  (offset=12)                  cycles=1-2
0x8314:  3a fc          dec $fc                                  cycles=1
0x8316:  81             ret                                      cycles=4
0x8317:  a6 0a          ld A, #$0a                               cycles=1
0x8319:  b1 fc          cp A, $fc                                cycles=1
0x831b:  24 11          jrnc $832e  (offset=17)                  cycles=1-2
0x831d:  3a fc          dec $fc                                  cycles=1
0x831f:  81             ret                                      cycles=4
0x8320:  b6 fc          ld A, $fc                                cycles=1
0x8322:  a1 64          cp A, #$64                               cycles=1
0x8324:  24 03          jrnc $8329  (offset=3)                   cycles=1-2
0x8326:  3c fc          inc $fc                                  cycles=1
0x8328:  81             ret                                      cycles=4
0x8329:  a6 64          ld A, #$64                               cycles=1
0x832b:  b7 fc          ld $fc,A                                 cycles=1
0x832d:  81             ret                                      cycles=4
0x832e:  72 18 00 94    bset $94, #4                             cycles=1
0x8332:  72 18 00 8d    bset $8d, #4                             cycles=1
0x8336:  81             ret                                      cycles=4
0x8337:  01             rrwa X, A                                cycles=1
0x8338:  03 05          cpl ($05,SP)                             cycles=1
0x833a:  06 08          rrc ($08,SP)                             cycles=1
0x833c:  09 0a          rlc ($0a,SP)                             cycles=1
0x833e:  0c 0d          inc ($0d,SP)                             cycles=1
0x8340:  0f 12          clr ($12,SP)                             cycles=1
0x8342:  14 15          and A, ($15,SP)                          cycles=1
0x8344:  17 18          ldw ($18,SP),Y                           cycles=2
0x8346:  1a 1b          or A, ($1b,SP)                           cycles=1
0x8348:  1c 1e 20       addw X, #$1e20                           cycles=2
0x834b:  21 22          jrf $836f  (offset=34)                   cycles=1-2
0x834d:  23 24          jrule $8373  (offset=36)                 cycles=1-2
0x834f:  25 26          jrc $8377  (offset=38)                   cycles=1-2
0x8351:  27 28          jreq $837b  (offset=40)                  cycles=1-2
0x8353:  29 2b          jrv $8380  (offset=43)                   cycles=1-2
0x8355:  2c 2d          jrsgt $8384  (offset=45)                 cycles=1-2
0x8357:  2f 32          jrslt $838b  (offset=50)                 cycles=1-2
0x8359:  33 34          cpl $34                                  cycles=1
0x835b:  35 37 38 39    mov $3839, #$37                          cycles=1
0x835f:  3a 3b          dec $3b                                  cycles=1
0x8361:  3c 3d          inc $3d                                  cycles=1
0x8363:  3e 3f          swap $3f                                 cycles=1
0x8365:  40             neg A                                    cycles=1
0x8366:  41             exg A, XL                                cycles=1
0x8367:  42             mul X, A                                 cycles=4
0x8368:  44             srl A                                    cycles=1
0x8369:  45 47 48       mov $48, $47                             cycles=1
0x836c:  49             rlc A                                    cycles=1
0x836d:  4b 4c          push #$4c                                cycles=1
0x836f:  4d             tnz A                                    cycles=1
0x8370:  4e             swap A                                   cycles=1
0x8371:  4f             clr A                                    cycles=1
0x8372:  50             negw X                                   cycles=1
0x8373:  51             exgw X, Y                                cycles=1
0x8374:  52 53          sub SP, #$53                             cycles=1
0x8376:  55 56 57 58 59 mov $5859, $5657                         cycles=1
0x837b:  5a             decw X                                   cycles=1
0x837c:  5b 5c          addw SP, #$5c                            cycles=2
0x837e:  5d             tnzw X                                   cycles=2
0x837f:  5f             clrw X                                   cycles=1
0x8380:  60 60          neg ($60,X)                              cycles=1
0x8382:  61             exg A, YL                                cycles=1
0x8383:  61             exg A, YL                                cycles=1
0x8384:  62             div X, A                                 cycles=2-17
0x8385:  62             div X, A                                 cycles=2-17
0x8386:  62             div X, A                                 cycles=2-17
0x8387:  c6 53 0f       ld A, $530f                              cycles=1
0x838a:  4d             tnz A                                    cycles=1
0x838b:  27 0c          jreq $8399  (offset=12)                  cycles=1-2
0x838d:  72 01 00 85 1c btjf $85, #0, $83ae  (offset=28)         cycles=2-3
0x8392:  72 05 50 15 20 btjf $5015, #2, $83b7  (offset=32)       cycles=2-3
0x8397:  3f fe          clr $fe                                  cycles=1
0x8399:  72 0e 00 91 10 btjt $91, #7, $83ae  (offset=16)         cycles=2-3
0x839e:  72 01 00 85 0b btjf $85, #0, $83ae  (offset=11)         cycles=2-3
0x83a3:  a6 01          ld A, #$01                               cycles=1
0x83a5:  c7 53 0f       ld $530f,A                               cycles=1
0x83a8:  a6 f4          ld A, #$f4                               cycles=1
0x83aa:  c7 53 10       ld $5310,A                               cycles=1
0x83ad:  81             ret                                      cycles=4
0x83ae:  a6 00          ld A, #$00                               cycles=1
0x83b0:  c7 53 0f       ld $530f,A                               cycles=1
0x83b3:  c7 53 10       ld $5310,A                               cycles=1
0x83b6:  81             ret                                      cycles=4
0x83b7:  3c fe          inc $fe                                  cycles=1
0x83b9:  b6 fe          ld A, $fe                                cycles=1
0x83bb:  a1 1b          cp A, #$1b                               cycles=1
0x83bd:  25 da          jrc $8399  (offset=-38)                  cycles=1-2
0x83bf:  3f fe          clr $fe                                  cycles=1
0x83c1:  72 1e 00 91    bset $91, #7                             cycles=1
0x83c5:  3f fd          clr $fd                                  cycles=1
0x83c7:  a6 00          ld A, #$00                               cycles=1
0x83c9:  c7 53 0f       ld $530f,A                               cycles=1
0x83cc:  c7 53 10       ld $5310,A                               cycles=1
0x83cf:  81             ret                                      cycles=4
0x83d0:  72 0f 00 91 0e btjf $91, #7, $83e3  (offset=14)         cycles=2-3
0x83d5:  3c fd          inc $fd                                  cycles=1
0x83d7:  b6 fd          ld A, $fd                                cycles=1
0x83d9:  a1 fc          cp A, #$fc                               cycles=1
0x83db:  25 06          jrc $83e3  (offset=6)                    cycles=1-2
0x83dd:  3f fd          clr $fd                                  cycles=1
0x83df:  72 1f 00 91    bres $91, #7                             cycles=1
0x83e3:  81             ret                                      cycles=4
0x83e4:  a6 16          ld A, #$16                               cycles=1
0x83e6:  b7 31          ld $31,A                                 cycles=1
0x83e8:  cd b9 90       call $b990                               cycles=4
0x83eb:  a6 09          ld A, #$09                               cycles=1
0x83ed:  cd b9 83       call $b983                               cycles=4
0x83f0:  c6 54 04       ld A, $5404                              cycles=1
0x83f3:  97             ld XL, A                                 cycles=1
0x83f4:  b6 31          ld A, $31                                cycles=1
0x83f6:  a1 16          cp A, #$16                               cycles=1
0x83f8:  26 ea          jrne $83e4  (offset=-22)                 cycles=1-2
0x83fa:  9f             ld A, XL                                 cycles=1
0x83fb:  a1 7f          cp A, #$7f                               cycles=1
0x83fd:  24 0b          jrnc $840a  (offset=11)                  cycles=1-2
0x83ff:  48             sll A                                    cycles=1
0x8400:  b7 df          ld $df,A                                 cycles=1
0x8402:  c6 54 05       ld A, $5405                              cycles=1
0x8405:  44             srl A                                    cycles=1
0x8406:  ba df          or A, $df                                cycles=1
0x8408:  20 02          jra $840c  (offset=2)                    cycles=2
0x840a:  a6 ff          ld A, #$ff                               cycles=1
0x840c:  b1 fb          cp A, $fb                                cycles=1
0x840e:  27 08          jreq $8418  (offset=8)                   cycles=1-2
0x8410:  25 04          jrc $8416  (offset=4)                    cycles=1-2
0x8412:  3c fb          inc $fb                                  cycles=1
0x8414:  20 02          jra $8418  (offset=2)                    cycles=2
0x8416:  3a fb          dec $fb                                  cycles=1
0x8418:  81             ret                                      cycles=4
0x8419:  b6 71          ld A, $71                                cycles=1
0x841b:  a1 43          cp A, #$43                               cycles=1
0x841d:  25 0c          jrc $842b  (offset=12)                   cycles=1-2
0x841f:  72 1b 00 85    bres $85, #5                             cycles=1
0x8423:  72 1f 00 96    bres $96, #7                             cycles=1
0x8427:  3f fa          clr $fa                                  cycles=1
0x8429:  20 02          jra $842d  (offset=2)                    cycles=2
0x842b:  3c 71          inc $71                                  cycles=1
0x842d:  81             ret                                      cycles=4
0x842e:  b6 87          ld A, $87                                cycles=1
0x8430:  72 5d 84 4c    tnz $844c                                cycles=1
0x8434:  a1 aa          cp A, #$aa                               cycles=1
0x8436:  26 14          jrne $844c  (offset=20)                  cycles=1-2
0x8438:  3c fa          inc $fa                                  cycles=1
0x843a:  b6 fa          ld A, $fa                                cycles=1
0x843c:  a1 64          cp A, #$64                               cycles=1
0x843e:  25 12          jrc $8452  (offset=18)                   cycles=1-2
0x8440:  3f fa          clr $fa                                  cycles=1
0x8442:  72 1e 00 96    bset $96, #7                             cycles=1
0x8446:  72 17 00 97    bres $97, #3                             cycles=1
0x844a:  20 06          jra $8452  (offset=6)                    cycles=2
0x844c:  72 1f 00 96    bres $96, #7                             cycles=1
0x8450:  3f fa          clr $fa                                  cycles=1
0x8452:  81             ret                                      cycles=4
0x8453:  5f             clrw X                                   cycles=1
0x8454:  b6 f1          ld A, $f1                                cycles=1
0x8456:  a1 06          cp A, #$06                               cycles=1
0x8458:  25 23          jrc $847d  (offset=35)                   cycles=1-2
0x845a:  a1 1e          cp A, #$1e                               cycles=1
0x845c:  24 1f          jrnc $847d  (offset=31)                  cycles=1-2
0x845e:  97             ld XL, A                                 cycles=1
0x845f:  a6 95          ld A, #$95                               cycles=1
0x8461:  42             mul X, A                                 cycles=4
0x8462:  a6 1a          ld A, #$1a                               cycles=1
0x8464:  62             div X, A                                 cycles=2-17
0x8465:  a6 19          ld A, #$19                               cycles=1
0x8467:  42             mul X, A                                 cycles=4
0x8468:  b6 f2          ld A, $f2                                cycles=1
0x846a:  62             div X, A                                 cycles=2-17
0x846b:  9e             ld A, XH                                 cycles=1
0x846c:  a1 01          cp A, #$01                               cycles=1
0x846e:  24 09          jrnc $8479  (offset=9)                   cycles=1-2
0x8470:  9f             ld A, XL                                 cycles=1
0x8471:  a1 ff          cp A, #$ff                               cycles=1
0x8473:  24 04          jrnc $8479  (offset=4)                   cycles=1-2
0x8475:  b7 f3          ld $f3,A                                 cycles=1
0x8477:  20 04          jra $847d  (offset=4)                    cycles=2
0x8479:  a6 ff          ld A, #$ff                               cycles=1
0x847b:  b7 f3          ld $f3,A                                 cycles=1
0x847d:  81             ret                                      cycles=4
0x847e:  5f             clrw X                                   cycles=1
0x847f:  ae 00 64       ldw X, #$64                              cycles=2
0x8482:  b6 f3          ld A, $f3                                cycles=1
0x8484:  42             mul X, A                                 cycles=4
0x8485:  90 5f          clrw Y                                   cycles=1
0x8487:  b6 7c          ld A, $7c                                cycles=1
0x8489:  90 95          ld YH, A                                 cycles=1
0x848b:  b6 7b          ld A, $7b                                cycles=1
0x848d:  90 97          ld YL, A                                 cycles=1
0x848f:  65             divw X, Y                                cycles=2-17
0x8490:  a3 00 66       cpw X, #$66                              cycles=2
0x8493:  25 03          jrc $8498  (offset=3)                    cycles=1-2
0x8495:  ae 00 66       ldw X, #$66                              cycles=2
0x8498:  9f             ld A, XL                                 cycles=1
0x8499:  b7 f9          ld $f9,A                                 cycles=1
0x849b:  a6 67          ld A, #$67                               cycles=1
0x849d:  b0 f9          sub A, $f9                               cycles=1
0x849f:  b7 f9          ld $f9,A                                 cycles=1
0x84a1:  81             ret                                      cycles=4
0x84a2:  72 0f 00 8a 1f btjf $8a, #7, $84c6  (offset=31)         cycles=2-3
0x84a7:  3c ed          inc $ed                                  cycles=1
0x84a9:  b6 db          ld A, $db                                cycles=1
0x84ab:  a1 23          cp A, #$23                               cycles=1
0x84ad:  24 06          jrnc $84b5  (offset=6)                   cycles=1-2
0x84af:  b6 ed          ld A, $ed                                cycles=1
0x84b1:  a1 03          cp A, #$03                               cycles=1
0x84b3:  25 0d          jrc $84c2  (offset=13)                   cycles=1-2
0x84b5:  3f ed          clr $ed                                  cycles=1
0x84b7:  b6 ec          ld A, $ec                                cycles=1
0x84b9:  b1 eb          cp A, $eb                                cycles=1
0x84bb:  27 05          jreq $84c2  (offset=5)                   cycles=1-2
0x84bd:  25 04          jrc $84c3  (offset=4)                    cycles=1-2
0x84bf:  3c eb          inc $eb                                  cycles=1
0x84c1:  81             ret                                      cycles=4
0x84c2:  81             ret                                      cycles=4
0x84c3:  b7 eb          ld $eb,A                                 cycles=1
0x84c5:  81             ret                                      cycles=4
0x84c6:  3f eb          clr $eb                                  cycles=1
0x84c8:  81             ret                                      cycles=4
0x84c9:  b6 d7          ld A, $d7                                cycles=1
0x84cb:  a1 01          cp A, #$01                               cycles=1
0x84cd:  24 04          jrnc $84d3  (offset=4)                   cycles=1-2
0x84cf:  a6 01          ld A, #$01                               cycles=1
0x84d1:  b7 d7          ld $d7,A                                 cycles=1
0x84d3:  81             ret                                      cycles=4
0x84d4:  a6 01          ld A, #$01                               cycles=1
0x84d6:  b7 d7          ld $d7,A                                 cycles=1
0x84d8:  b7 d6          ld $d6,A                                 cycles=1
0x84da:  3f db          clr $db                                  cycles=1
0x84dc:  81             ret                                      cycles=4
0x84dd:  cd 84 7e       call $847e                               cycles=4
0x84e0:  72 0f 00 8a ef btjf $8a, #7, $84d4  (offset=-17)        cycles=2-3
0x84e5:  72 07 00 97 e9 btjf $97, #3, $84d3  (offset=-23)        cycles=2-3
0x84ea:  72 0c 00 96 03 btjt $96, #6, $84f2  (offset=3)          cycles=2-3
0x84ef:  cd 85 e6       call $85e6                               cycles=4
0x84f2:  3c e8          inc $e8                                  cycles=1
0x84f4:  b6 e8          ld A, $e8                                cycles=1
0x84f6:  b1 e9          cp A, $e9                                cycles=1
0x84f8:  25 5b          jrc $8555  (offset=91)                   cycles=1-2
0x84fa:  3f e8          clr $e8                                  cycles=1
0x84fc:  b6 d6          ld A, $d6                                cycles=1
0x84fe:  5f             clrw X                                   cycles=1
0x84ff:  97             ld XL, A                                 cycles=1
0x8500:  b6 c0          ld A, $c0                                cycles=1
0x8502:  42             mul X, A                                 cycles=4
0x8503:  a6 64          ld A, #$64                               cycles=1
0x8505:  62             div X, A                                 cycles=2-17
0x8506:  b6 f9          ld A, $f9                                cycles=1
0x8508:  a1 0d          cp A, #$0d                               cycles=1
0x850a:  24 04          jrnc $8510  (offset=4)                   cycles=1-2
0x850c:  42             mul X, A                                 cycles=4
0x850d:  a6 0d          ld A, #$0d                               cycles=1
0x850f:  62             div X, A                                 cycles=2-17
0x8510:  9f             ld A, XL                                 cycles=1
0x8511:  b7 d6          ld $d6,A                                 cycles=1
0x8513:  b1 d7          cp A, $d7                                cycles=1
0x8515:  27 3e          jreq $8555  (offset=62)                  cycles=1-2
0x8517:  24 20          jrnc $8539  (offset=32)                  cycles=1-2
0x8519:  b6 d7          ld A, $d7                                cycles=1
0x851b:  a1 01          cp A, #$01                               cycles=1
0x851d:  25 b0          jrc $84cf  (offset=-80)                  cycles=1-2
0x851f:  b0 d6          sub A, $d6                               cycles=1
0x8521:  b7 ea          ld $ea,A                                 cycles=1
0x8523:  a1 05          cp A, #$05                               cycles=1
0x8525:  24 09          jrnc $8530  (offset=9)                   cycles=1-2
0x8527:  a6 05          ld A, #$05                               cycles=1
0x8529:  b0 ea          sub A, $ea                               cycles=1
0x852b:  b7 e9          ld $e9,A                                 cycles=1
0x852d:  3a d7          dec $d7                                  cycles=1
0x852f:  81             ret                                      cycles=4
0x8530:  a6 01          ld A, #$01                               cycles=1
0x8532:  b7 e9          ld $e9,A                                 cycles=1
0x8534:  3a d7          dec $d7                                  cycles=1
0x8536:  3a d7          dec $d7                                  cycles=1
0x8538:  81             ret                                      cycles=4
0x8539:  b0 d7          sub A, $d7                               cycles=1
0x853b:  a1 0a          cp A, #$0a                               cycles=1
0x853d:  b7 ea          ld $ea,A                                 cycles=1
0x853f:  24 0d          jrnc $854e  (offset=13)                  cycles=1-2
0x8541:  a6 0a          ld A, #$0a                               cycles=1
0x8543:  b0 ea          sub A, $ea                               cycles=1
0x8545:  a1 02          cp A, #$02                               cycles=1
0x8547:  25 05          jrc $854e  (offset=5)                    cycles=1-2
0x8549:  b7 e9          ld $e9,A                                 cycles=1
0x854b:  3c d7          inc $d7                                  cycles=1
0x854d:  81             ret                                      cycles=4
0x854e:  a6 02          ld A, #$02                               cycles=1
0x8550:  b7 e9          ld $e9,A                                 cycles=1
0x8552:  3c d7          inc $d7                                  cycles=1
0x8554:  81             ret                                      cycles=4
0x8555:  81             ret                                      cycles=4
0x8556:  3c ee          inc $ee                                  cycles=1
0x8558:  b6 ee          ld A, $ee                                cycles=1
0x855a:  72 0e 00 8c 1b btjt $8c, #7, $857a  (offset=27)         cycles=2-3
0x855f:  72 07 00 97 16 btjf $97, #3, $857a  (offset=22)         cycles=2-3
0x8564:  b6 20          ld A, $20                                cycles=1
0x8566:  b1 e4          cp A, $e4                                cycles=1
0x8568:  25 08          jrc $8572  (offset=8)                    cycles=1-2
0x856a:  b6 ee          ld A, $ee                                cycles=1
0x856c:  a1 06          cp A, #$06                               cycles=1
0x856e:  25 3d          jrc $85ad  (offset=61)                   cycles=1-2
0x8570:  20 0c          jra $857e  (offset=12)                   cycles=2
0x8572:  b6 ee          ld A, $ee                                cycles=1
0x8574:  a1 02          cp A, #$02                               cycles=1
0x8576:  25 35          jrc $85ad  (offset=53)                   cycles=1-2
0x8578:  20 04          jra $857e  (offset=4)                    cycles=2
0x857a:  a1 05          cp A, #$05                               cycles=1
0x857c:  25 2f          jrc $85ad  (offset=47)                   cycles=1-2
0x857e:  3f ee          clr $ee                                  cycles=1
0x8580:  72 0d 00 96 29 btjf $96, #6, $85ae  (offset=41)         cycles=2-3
0x8585:  72 0e 00 8c 05 btjt $8c, #7, $858f  (offset=5)          cycles=2-3
0x858a:  72 0b 00 8a 1e btjf $8a, #5, $85ad  (offset=30)         cycles=2-3
0x858f:  72 0f 00 8c 06 btjf $8c, #7, $859a  (offset=6)          cycles=2-3
0x8594:  b6 1d          ld A, $1d                                cycles=1
0x8596:  a1 05          cp A, #$05                               cycles=1
0x8598:  25 0a          jrc $85a4  (offset=10)                   cycles=1-2
0x859a:  3d c0          tnz $c0                                  cycles=1
0x859c:  27 06          jreq $85a4  (offset=6)                   cycles=1-2
0x859e:  3a c0          dec $c0                                  cycles=1
0x85a0:  3f de          clr $de                                  cycles=1
0x85a2:  20 09          jra $85ad  (offset=9)                    cycles=2
0x85a4:  72 1b 00 8a    bres $8a, #5                             cycles=1
0x85a8:  72 1f 00 8c    bres $8c, #7                             cycles=1
0x85ac:  83             trap                                     cycles=9
0x85ad:  81             ret                                      cycles=4
0x85ae:  72 0f 00 8c fa btjf $8c, #7, $85ad  (offset=-6)         cycles=2-3
0x85b3:  72 1f 00 8c    bres $8c, #7                             cycles=1
0x85b7:  72 1b 00 8a    bres $8a, #5                             cycles=1
0x85bb:  83             trap                                     cycles=9
0x85bc:  81             ret                                      cycles=4
0x85bd:  a6 12          ld A, #$12                               cycles=1
0x85bf:  b7 31          ld $31,A                                 cycles=1
0x85c1:  cd b9 90       call $b990                               cycles=4
0x85c4:  a6 05          ld A, #$05                               cycles=1
0x85c6:  cd b9 83       call $b983                               cycles=4
0x85c9:  c6 54 04       ld A, $5404                              cycles=1
0x85cc:  a1 3f          cp A, #$3f                               cycles=1
0x85ce:  24 07          jrnc $85d7  (offset=7)                   cycles=1-2
0x85d0:  48             sll A                                    cycles=1
0x85d1:  48             sll A                                    cycles=1
0x85d2:  ca 54 05       or A, $5405                              cycles=1
0x85d5:  20 02          jra $85d9  (offset=2)                    cycles=2
0x85d7:  a6 ff          ld A, #$ff                               cycles=1
0x85d9:  97             ld XL, A                                 cycles=1
0x85da:  b6 31          ld A, $31                                cycles=1
0x85dc:  a1 12          cp A, #$12                               cycles=1
0x85de:  26 dd          jrne $85bd  (offset=-35)                 cycles=1-2
0x85e0:  9f             ld A, XL                                 cycles=1
0x85e1:  b7 d0          ld $d0,A                                 cycles=1
0x85e3:  b7 d1          ld $d1,A                                 cycles=1
0x85e5:  81             ret                                      cycles=4
0x85e6:  5f             clrw X                                   cycles=1
0x85e7:  b6 32          ld A, $32                                cycles=1
0x85e9:  95             ld XH, A                                 cycles=1
0x85ea:  b6 33          ld A, $33                                cycles=1
0x85ec:  97             ld XL, A                                 cycles=1
0x85ed:  a6 05          ld A, #$05                               cycles=1
0x85ef:  62             div X, A                                 cycles=2-17
0x85f0:  9f             ld A, XL                                 cycles=1
0x85f1:  b7 c2          ld $c2,A                                 cycles=1
0x85f3:  b7 c0          ld $c0,A                                 cycles=1
0x85f5:  81             ret                                      cycles=4
0x85f6:  72 0f 00 8a 20 btjf $8a, #7, $861b  (offset=32)         cycles=2-3
0x85fb:  72 0e 00 98 1c btjt $98, #7, $861c  (offset=28)         cycles=2-3
0x8600:  72 1e 00 98    bset $98, #7                             cycles=1
0x8604:  b6 3e          ld A, $3e                                cycles=1
0x8606:  4d             tnz A                                    cycles=1
0x8607:  27 0e          jreq $8617  (offset=14)                  cycles=1-2
0x8609:  ae 01 0a       ldw X, #$10a                             cycles=2
0x860c:  62             div X, A                                 cycles=2-17
0x860d:  a3 00 1e       cpw X, #$1e                              cycles=2
0x8610:  24 05          jrnc $8617  (offset=5)                   cycles=1-2
0x8612:  9f             ld A, XL                                 cycles=1
0x8613:  b7 c9          ld $c9,A                                 cycles=1
0x8615:  20 04          jra $861b  (offset=4)                    cycles=2
0x8617:  a6 1e          ld A, #$1e                               cycles=1
0x8619:  b7 c9          ld $c9,A                                 cycles=1
0x861b:  81             ret                                      cycles=4
0x861c:  3f c9          clr $c9                                  cycles=1
0x861e:  81             ret                                      cycles=4
0x861f:  72 03 00 8f 57 btjf $8f, #1, $867b  (offset=87)         cycles=2-3
0x8624:  72 0c 00 98 52 btjt $98, #6, $867b  (offset=82)         cycles=2-3
0x8629:  72 0c 00 96 68 btjt $96, #6, $8696  (offset=104)        cycles=2-3
0x862e:  72 0b 50 18 63 btjf $5018, #5, $8696  (offset=99)       cycles=2-3
0x8633:  3c c4          inc $c4                                  cycles=1
0x8635:  b6 c4          ld A, $c4                                cycles=1
0x8637:  a1 07          cp A, #$07                               cycles=1
0x8639:  25 3f          jrc $867a  (offset=63)                   cycles=1-2
0x863b:  3f c4          clr $c4                                  cycles=1
0x863d:  cd 85 e6       call $85e6                               cycles=4
0x8640:  cd 87 91       call $8791                               cycles=4
0x8643:  9e             ld A, XH                                 cycles=1
0x8644:  c7 52 65       ld $5265,A                               cycles=1
0x8647:  9f             ld A, XL                                 cycles=1
0x8648:  c7 52 66       ld $5266,A                               cycles=1
0x864b:  cd 87 ef       call $87ef                               cycles=4
0x864e:  9e             ld A, XH                                 cycles=1
0x864f:  c7 52 67       ld $5267,A                               cycles=1
0x8652:  9f             ld A, XL                                 cycles=1
0x8653:  c7 52 68       ld $5268,A                               cycles=1
0x8656:  cd 88 4d       call $884d                               cycles=4
0x8659:  9e             ld A, XH                                 cycles=1
0x865a:  c7 52 69       ld $5269,A                               cycles=1
0x865d:  9f             ld A, XL                                 cycles=1
0x865e:  c7 52 6a       ld $526a,A                               cycles=1
0x8661:  72 10 52 57    bset $5257, #0                           cycles=1
0x8665:  cd 9a 47       call $9a47                               cycles=4
0x8668:  9d             nop                                      cycles=1
0x8669:  9d             nop                                      cycles=1
0x866a:  72 1c 00 96    bset $96, #6                             cycles=1
0x866e:  72 11 50 05    bres $5005, #0                           cycles=1
0x8672:  72 13 50 05    bres $5005, #1                           cycles=1
0x8676:  72 15 50 05    bres $5005, #2                           cycles=1
0x867a:  81             ret                                      cycles=4
0x867b:  3f c4          clr $c4                                  cycles=1
0x867d:  72 1d 00 96    bres $96, #6                             cycles=1
0x8681:  72 1f 00 98    bres $98, #7                             cycles=1
0x8685:  72 1b 52 50    bres $5250, #5                           cycles=1
0x8689:  72 17 52 53    bres $5253, #3                           cycles=1
0x868d:  72 15 52 53    bres $5253, #2                           cycles=1
0x8691:  72 13 52 53    bres $5253, #1                           cycles=1
0x8695:  81             ret                                      cycles=4
0x8696:  3f c4          clr $c4                                  cycles=1
0x8698:  81             ret                                      cycles=4
0x8699:  b6 fc          ld A, $fc                                cycles=1
0x869b:  a1 64          cp A, #$64                               cycles=1
0x869d:  24 0b          jrnc $86aa  (offset=11)                  cycles=1-2
0x869f:  5f             clrw X                                   cycles=1
0x86a0:  97             ld XL, A                                 cycles=1
0x86a1:  b6 9c          ld A, $9c                                cycles=1
0x86a3:  42             mul X, A                                 cycles=4
0x86a4:  a6 64          ld A, #$64                               cycles=1
0x86a6:  62             div X, A                                 cycles=2-17
0x86a7:  9f             ld A, XL                                 cycles=1
0x86a8:  b7 9c          ld $9c,A                                 cycles=1
0x86aa:  81             ret                                      cycles=4
0x86ab:  5f             clrw X                                   cycles=1
0x86ac:  b6 1d          ld A, $1d                                cycles=1
0x86ae:  97             ld XL, A                                 cycles=1
0x86af:  a6 0a          ld A, #$0a                               cycles=1
0x86b1:  42             mul X, A                                 cycles=4
0x86b2:  b6 d5          ld A, $d5                                cycles=1
0x86b4:  62             div X, A                                 cycles=2-17
0x86b5:  9f             ld A, XL                                 cycles=1
0x86b6:  5f             clrw X                                   cycles=1
0x86b7:  97             ld XL, A                                 cycles=1
0x86b8:  a3 00 14       cpw X, #$14                              cycles=2
0x86bb:  25 02          jrc $86bf  (offset=2)                    cycles=1-2
0x86bd:  20 0f          jra $86ce  (offset=15)                   cycles=2
0x86bf:  d6 8b 08       ld A, ($8b08,X)                          cycles=1
0x86c2:  b7 c5          ld $c5,A                                 cycles=1
0x86c4:  d6 8b 1d       ld A, ($8b1d,X)                          cycles=1
0x86c7:  b7 cc          ld $cc,A                                 cycles=1
0x86c9:  3d 1d          tnz $1d                                  cycles=1
0x86cb:  27 0a          jreq $86d7  (offset=10)                  cycles=1-2
0x86cd:  81             ret                                      cycles=4
0x86ce:  a6 01          ld A, #$01                               cycles=1
0x86d0:  b7 c5          ld $c5,A                                 cycles=1
0x86d2:  a6 03          ld A, #$03                               cycles=1
0x86d4:  b7 cc          ld $cc,A                                 cycles=1
0x86d6:  81             ret                                      cycles=4
0x86d7:  a6 01          ld A, #$01                               cycles=1
0x86d9:  b7 c5          ld $c5,A                                 cycles=1
0x86db:  81             ret                                      cycles=4
0x86dc:  72 04 00 8b 13 btjt $8b, #2, $86f4  (offset=19)         cycles=2-3
0x86e1:  72 04 00 8a 0e btjt $8a, #2, $86f4  (offset=14)         cycles=2-3
0x86e6:  b6 1c          ld A, $1c                                cycles=1
0x86e8:  a1 4e          cp A, #$4e                               cycles=1
0x86ea:  24 08          jrnc $86f4  (offset=8)                   cycles=1-2
0x86ec:  a6 01          ld A, #$01                               cycles=1
0x86ee:  b7 cc          ld $cc,A                                 cycles=1
0x86f0:  3f c5          clr $c5                                  cycles=1
0x86f2:  20 03          jra $86f7  (offset=3)                    cycles=2
0x86f4:  cd 86 ab       call $86ab                               cycles=4
0x86f7:  b6 66          ld A, $66                                cycles=1
0x86f9:  a1 a0          cp A, #$a0                               cycles=1
0x86fb:  25 0a          jrc $8707  (offset=10)                   cycles=1-2
0x86fd:  3c d3          inc $d3                                  cycles=1
0x86ff:  b6 d3          ld A, $d3                                cycles=1
0x8701:  a1 50          cp A, #$50                               cycles=1
0x8703:  25 04          jrc $8709  (offset=4)                    cycles=1-2
0x8705:  20 4b          jra $8752  (offset=75)                   cycles=2
0x8707:  3f d3          clr $d3                                  cycles=1
0x8709:  3c c6          inc $c6                                  cycles=1
0x870b:  b6 c6          ld A, $c6                                cycles=1
0x870d:  b1 c5          cp A, $c5                                cycles=1
0x870f:  25 1d          jrc $872e  (offset=29)                   cycles=1-2
0x8711:  3f c6          clr $c6                                  cycles=1
0x8713:  b6 1d          ld A, $1d                                cycles=1
0x8715:  72 0b 00 8a 04 btjf $8a, #5, $871e  (offset=4)          cycles=2-3
0x871a:  a1 01          cp A, #$01                               cycles=1
0x871c:  20 02          jra $8720  (offset=2)                    cycles=2
0x871e:  b1 9c          cp A, $9c                                cycles=1
0x8720:  27 02          jreq $8724  (offset=2)                   cycles=1-2
0x8722:  24 21          jrnc $8745  (offset=33)                  cycles=1-2
0x8724:  b6 c0          ld A, $c0                                cycles=1
0x8726:  b1 c2          cp A, $c2                                cycles=1
0x8728:  27 04          jreq $872e  (offset=4)                   cycles=1-2
0x872a:  24 12          jrnc $873e  (offset=18)                  cycles=1-2
0x872c:  3c c0          inc $c0                                  cycles=1
0x872e:  a6 01          ld A, #$01                               cycles=1
0x8730:  b1 c0          cp A, $c0                                cycles=1
0x8732:  25 02          jrc $8736  (offset=2)                    cycles=1-2
0x8734:  b7 c0          ld $c0,A                                 cycles=1
0x8736:  b6 cf          ld A, $cf                                cycles=1
0x8738:  4d             tnz A                                    cycles=1
0x8739:  26 19          jrne $8754  (offset=25)                  cycles=1-2
0x873b:  3f c8          clr $c8                                  cycles=1
0x873d:  81             ret                                      cycles=4
0x873e:  b6 cf          ld A, $cf                                cycles=1
0x8740:  4d             tnz A                                    cycles=1
0x8741:  26 11          jrne $8754  (offset=17)                  cycles=1-2
0x8743:  3f c8          clr $c8                                  cycles=1
0x8745:  3d c0          tnz $c0                                  cycles=1
0x8747:  27 0a          jreq $8753  (offset=10)                  cycles=1-2
0x8749:  3a c0          dec $c0                                  cycles=1
0x874b:  3a cc          dec $cc                                  cycles=1
0x874d:  3d cc          tnz $cc                                  cycles=1
0x874f:  26 c2          jrne $8713  (offset=-62)                 cycles=1-2
0x8751:  81             ret                                      cycles=4
0x8752:  83             trap                                     cycles=9
0x8753:  81             ret                                      cycles=4
0x8754:  72 0a 00 8a fa btjt $8a, #5, $8753  (offset=-6)         cycles=2-3
0x8759:  3d c0          tnz $c0                                  cycles=1
0x875b:  27 f6          jreq $8753  (offset=-10)                 cycles=1-2
0x875d:  3c c8          inc $c8                                  cycles=1
0x875f:  b6 c8          ld A, $c8                                cycles=1
0x8761:  a1 5a          cp A, #$5a                               cycles=1
0x8763:  25 0c          jrc $8771  (offset=12)                   cycles=1-2
0x8765:  3f c8          clr $c8                                  cycles=1
0x8767:  b6 c0          ld A, $c0                                cycles=1
0x8769:  a1 64          cp A, #$64                               cycles=1
0x876b:  25 05          jrc $8772  (offset=5)                    cycles=1-2
0x876d:  a6 64          ld A, #$64                               cycles=1
0x876f:  b7 c0          ld $c0,A                                 cycles=1
0x8771:  81             ret                                      cycles=4
0x8772:  b1 95          cp A, $95                                cycles=1
0x8774:  24 fb          jrnc $8771  (offset=-5)                  cycles=1-2
0x8776:  72 06 00 91 03 btjt $91, #3, $877e  (offset=3)          cycles=2-3
0x877b:  3c c0          inc $c0                                  cycles=1
0x877d:  81             ret                                      cycles=4
0x877e:  a1 5a          cp A, #$5a                               cycles=1
0x8780:  24 ef          jrnc $8771  (offset=-17)                 cycles=1-2
0x8782:  3c c0          inc $c0                                  cycles=1
0x8784:  81             ret                                      cycles=4
0x8785:  cd 85 f6       call $85f6                               cycles=4
0x8788:  b6 c9          ld A, $c9                                cycles=1
0x878a:  b1 c0          cp A, $c0                                cycles=1
0x878c:  25 02          jrc $8790  (offset=2)                    cycles=1-2
0x878e:  b7 c0          ld $c0,A                                 cycles=1
0x8790:  81             ret                                      cycles=4
0x8791:  5f             clrw X                                   cycles=1
0x8792:  b6 bf          ld A, $bf                                cycles=1
0x8794:  95             ld XH, A                                 cycles=1
0x8795:  b6 bc          ld A, $bc                                cycles=1
0x8797:  97             ld XL, A                                 cycles=1
0x8798:  d6 94 9a       ld A, ($949a,X)                          cycles=1
0x879b:  20 0c          jra $87a9  (offset=12)                   cycles=2
0x879d:  a3 03 c0       cpw X, #$3c0                             cycles=2
0x87a0:  24 05          jrnc $87a7  (offset=5)                   cycles=1-2
0x87a2:  d6 8b 32       ld A, ($8b32,X)                          cycles=1
0x87a5:  20 02          jra $87a9  (offset=2)                    cycles=2
0x87a7:  a6 00          ld A, #$00                               cycles=1
0x87a9:  a1 7d          cp A, #$7d                               cycles=1
0x87ab:  25 29          jrc $87d6  (offset=41)                   cycles=1-2
0x87ad:  a0 7d          sub A, #$7d                              cycles=1
0x87af:  5f             clrw X                                   cycles=1
0x87b0:  97             ld XL, A                                 cycles=1
0x87b1:  b6 c0          ld A, $c0                                cycles=1
0x87b3:  42             mul X, A                                 cycles=4
0x87b4:  a6 64          ld A, #$64                               cycles=1
0x87b6:  62             div X, A                                 cycles=2-17
0x87b7:  1c 00 7d       addw X, #$7d                             cycles=2
0x87ba:  a6 02          ld A, #$02                               cycles=1
0x87bc:  42             mul X, A                                 cycles=4
0x87bd:  a3 01 f4       cpw X, #$1f4                             cycles=2
0x87c0:  25 03          jrc $87c5  (offset=3)                    cycles=1-2
0x87c2:  ae 01 f4       ldw X, #$1f4                             cycles=2
0x87c5:  a3 00 12       cpw X, #$12                              cycles=2
0x87c8:  24 02          jrnc $87cc  (offset=2)                   cycles=1-2
0x87ca:  5f             clrw X                                   cycles=1
0x87cb:  81             ret                                      cycles=4
0x87cc:  a3 01 e2       cpw X, #$1e2                             cycles=2
0x87cf:  24 01          jrnc $87d2  (offset=1)                   cycles=1-2
0x87d1:  81             ret                                      cycles=4
0x87d2:  ae 01 f4       ldw X, #$1f4                             cycles=2
0x87d5:  81             ret                                      cycles=4
0x87d6:  b7 f4          ld $f4,A                                 cycles=1
0x87d8:  a6 7d          ld A, #$7d                               cycles=1
0x87da:  b0 f4          sub A, $f4                               cycles=1
0x87dc:  5f             clrw X                                   cycles=1
0x87dd:  97             ld XL, A                                 cycles=1
0x87de:  b6 c0          ld A, $c0                                cycles=1
0x87e0:  42             mul X, A                                 cycles=4
0x87e1:  a6 64          ld A, #$64                               cycles=1
0x87e3:  62             div X, A                                 cycles=2-17
0x87e4:  9f             ld A, XL                                 cycles=1
0x87e5:  b7 f4          ld $f4,A                                 cycles=1
0x87e7:  a6 7d          ld A, #$7d                               cycles=1
0x87e9:  b0 f4          sub A, $f4                               cycles=1
0x87eb:  5f             clrw X                                   cycles=1
0x87ec:  97             ld XL, A                                 cycles=1
0x87ed:  20 cb          jra $87ba  (offset=-53)                  cycles=2
0x87ef:  5f             clrw X                                   cycles=1
0x87f0:  b6 bd          ld A, $bd                                cycles=1
0x87f2:  95             ld XH, A                                 cycles=1
0x87f3:  b6 ba          ld A, $ba                                cycles=1
0x87f5:  97             ld XL, A                                 cycles=1
0x87f6:  d6 94 9a       ld A, ($949a,X)                          cycles=1
0x87f9:  20 0c          jra $8807  (offset=12)                   cycles=2
0x87fb:  a3 03 c0       cpw X, #$3c0                             cycles=2
0x87fe:  24 05          jrnc $8805  (offset=5)                   cycles=1-2
0x8800:  d6 8b 32       ld A, ($8b32,X)                          cycles=1
0x8803:  20 02          jra $8807  (offset=2)                    cycles=2
0x8805:  a6 00          ld A, #$00                               cycles=1
0x8807:  a1 7d          cp A, #$7d                               cycles=1
0x8809:  25 29          jrc $8834  (offset=41)                   cycles=1-2
0x880b:  a0 7d          sub A, #$7d                              cycles=1
0x880d:  5f             clrw X                                   cycles=1
0x880e:  97             ld XL, A                                 cycles=1
0x880f:  b6 c0          ld A, $c0                                cycles=1
0x8811:  42             mul X, A                                 cycles=4
0x8812:  a6 64          ld A, #$64                               cycles=1
0x8814:  62             div X, A                                 cycles=2-17
0x8815:  1c 00 7d       addw X, #$7d                             cycles=2
0x8818:  a6 02          ld A, #$02                               cycles=1
0x881a:  42             mul X, A                                 cycles=4
0x881b:  a3 01 f4       cpw X, #$1f4                             cycles=2
0x881e:  25 03          jrc $8823  (offset=3)                    cycles=1-2
0x8820:  ae 01 f4       ldw X, #$1f4                             cycles=2
0x8823:  a3 00 12       cpw X, #$12                              cycles=2
0x8826:  24 02          jrnc $882a  (offset=2)                   cycles=1-2
0x8828:  5f             clrw X                                   cycles=1
0x8829:  81             ret                                      cycles=4
0x882a:  a3 01 e2       cpw X, #$1e2                             cycles=2
0x882d:  24 01          jrnc $8830  (offset=1)                   cycles=1-2
0x882f:  81             ret                                      cycles=4
0x8830:  ae 01 f4       ldw X, #$1f4                             cycles=2
0x8833:  81             ret                                      cycles=4
0x8834:  b7 f4          ld $f4,A                                 cycles=1
0x8836:  a6 7d          ld A, #$7d                               cycles=1
0x8838:  b0 f4          sub A, $f4                               cycles=1
0x883a:  5f             clrw X                                   cycles=1
0x883b:  97             ld XL, A                                 cycles=1
0x883c:  b6 c0          ld A, $c0                                cycles=1
0x883e:  42             mul X, A                                 cycles=4
0x883f:  a6 64          ld A, #$64                               cycles=1
0x8841:  62             div X, A                                 cycles=2-17
0x8842:  9f             ld A, XL                                 cycles=1
0x8843:  b7 f4          ld $f4,A                                 cycles=1
0x8845:  a6 7d          ld A, #$7d                               cycles=1
0x8847:  b0 f4          sub A, $f4                               cycles=1
0x8849:  5f             clrw X                                   cycles=1
0x884a:  97             ld XL, A                                 cycles=1
0x884b:  20 cb          jra $8818  (offset=-53)                  cycles=2
0x884d:  5f             clrw X                                   cycles=1
0x884e:  b6 be          ld A, $be                                cycles=1
0x8850:  95             ld XH, A                                 cycles=1
0x8851:  b6 bb          ld A, $bb                                cycles=1
0x8853:  97             ld XL, A                                 cycles=1
0x8854:  d6 94 9a       ld A, ($949a,X)                          cycles=1
0x8857:  20 0c          jra $8865  (offset=12)                   cycles=2
0x8859:  a3 03 c0       cpw X, #$3c0                             cycles=2
0x885c:  24 05          jrnc $8863  (offset=5)                   cycles=1-2
0x885e:  d6 8b 32       ld A, ($8b32,X)                          cycles=1
0x8861:  20 02          jra $8865  (offset=2)                    cycles=2
0x8863:  a6 00          ld A, #$00                               cycles=1
0x8865:  a1 7d          cp A, #$7d                               cycles=1
0x8867:  25 29          jrc $8892  (offset=41)                   cycles=1-2
0x8869:  5f             clrw X                                   cycles=1
0x886a:  a0 7d          sub A, #$7d                              cycles=1
0x886c:  97             ld XL, A                                 cycles=1
0x886d:  b6 c0          ld A, $c0                                cycles=1
0x886f:  42             mul X, A                                 cycles=4
0x8870:  a6 64          ld A, #$64                               cycles=1
0x8872:  62             div X, A                                 cycles=2-17
0x8873:  1c 00 7d       addw X, #$7d                             cycles=2
0x8876:  a6 02          ld A, #$02                               cycles=1
0x8878:  42             mul X, A                                 cycles=4
0x8879:  a3 01 f4       cpw X, #$1f4                             cycles=2
0x887c:  25 03          jrc $8881  (offset=3)                    cycles=1-2
0x887e:  ae 01 f4       ldw X, #$1f4                             cycles=2
0x8881:  a3 00 12       cpw X, #$12                              cycles=2
0x8884:  24 02          jrnc $8888  (offset=2)                   cycles=1-2
0x8886:  5f             clrw X                                   cycles=1
0x8887:  81             ret                                      cycles=4
0x8888:  a3 01 e2       cpw X, #$1e2                             cycles=2
0x888b:  24 01          jrnc $888e  (offset=1)                   cycles=1-2
0x888d:  81             ret                                      cycles=4
0x888e:  ae 01 f4       ldw X, #$1f4                             cycles=2
0x8891:  81             ret                                      cycles=4
0x8892:  b7 f4          ld $f4,A                                 cycles=1
0x8894:  a6 7d          ld A, #$7d                               cycles=1
0x8896:  b0 f4          sub A, $f4                               cycles=1
0x8898:  5f             clrw X                                   cycles=1
0x8899:  97             ld XL, A                                 cycles=1
0x889a:  b6 c0          ld A, $c0                                cycles=1
0x889c:  42             mul X, A                                 cycles=4
0x889d:  a6 64          ld A, #$64                               cycles=1
0x889f:  62             div X, A                                 cycles=2-17
0x88a0:  9f             ld A, XL                                 cycles=1
0x88a1:  b7 f4          ld $f4,A                                 cycles=1
0x88a3:  a6 7d          ld A, #$7d                               cycles=1
0x88a5:  b0 f4          sub A, $f4                               cycles=1
0x88a7:  5f             clrw X                                   cycles=1
0x88a8:  97             ld XL, A                                 cycles=1
0x88a9:  20 cb          jra $8876  (offset=-53)                  cycles=2
0x88ab:  3f 40          clr $40                                  cycles=1
0x88ad:  3f 41          clr $41                                  cycles=1
0x88af:  3f 42          clr $42                                  cycles=1
0x88b1:  3f 43          clr $43                                  cycles=1
0x88b3:  3f 44          clr $44                                  cycles=1
0x88b5:  3f 45          clr $45                                  cycles=1
0x88b7:  3f 46          clr $46                                  cycles=1
0x88b9:  3f 47          clr $47                                  cycles=1
0x88bb:  81             ret                                      cycles=4
0x88bc:  3f 48          clr $48                                  cycles=1
0x88be:  3f 49          clr $49                                  cycles=1
0x88c0:  3f 4a          clr $4a                                  cycles=1
0x88c2:  b6 44          ld A, $44                                cycles=1
0x88c4:  b7 46          ld $46,A                                 cycles=1
0x88c6:  b6 45          ld A, $45                                cycles=1
0x88c8:  b7 47          ld $47,A                                 cycles=1
0x88ca:  b6 42          ld A, $42                                cycles=1
0x88cc:  b7 44          ld $44,A                                 cycles=1
0x88ce:  b6 43          ld A, $43                                cycles=1
0x88d0:  b7 45          ld $45,A                                 cycles=1
0x88d2:  b6 40          ld A, $40                                cycles=1
0x88d4:  b7 42          ld $42,A                                 cycles=1
0x88d6:  b6 41          ld A, $41                                cycles=1
0x88d8:  b7 43          ld $43,A                                 cycles=1
0x88da:  3d 46          tnz $46                                  cycles=1
0x88dc:  26 05          jrne $88e3  (offset=5)                   cycles=1-2
0x88de:  3d 47          tnz $47                                  cycles=1
0x88e0:  26 01          jrne $88e3  (offset=1)                   cycles=1-2
0x88e2:  81             ret                                      cycles=4
0x88e3:  b6 46          ld A, $46                                cycles=1
0x88e5:  bb 44          add A, $44                               cycles=1
0x88e7:  bb 42          add A, $42                               cycles=1
0x88e9:  bb 40          add A, $40                               cycles=1
0x88eb:  b7 48          ld $48,A                                 cycles=1
0x88ed:  b6 47          ld A, $47                                cycles=1
0x88ef:  bb 45          add A, $45                               cycles=1
0x88f1:  24 11          jrnc $8904  (offset=17)                  cycles=1-2
0x88f3:  3c 4a          inc $4a                                  cycles=1
0x88f5:  bb 43          add A, $43                               cycles=1
0x88f7:  24 11          jrnc $890a  (offset=17)                  cycles=1-2
0x88f9:  3c 4a          inc $4a                                  cycles=1
0x88fb:  bb 41          add A, $41                               cycles=1
0x88fd:  24 11          jrnc $8910  (offset=17)                  cycles=1-2
0x88ff:  3c 4a          inc $4a                                  cycles=1
0x8901:  20 0d          jra $8910  (offset=13)                   cycles=2
0x8903:  81             ret                                      cycles=4
0x8904:  bb 43          add A, $43                               cycles=1
0x8906:  24 02          jrnc $890a  (offset=2)                   cycles=1-2
0x8908:  3c 4a          inc $4a                                  cycles=1
0x890a:  bb 41          add A, $41                               cycles=1
0x890c:  24 02          jrnc $8910  (offset=2)                   cycles=1-2
0x890e:  3c 4a          inc $4a                                  cycles=1
0x8910:  b7 49          ld $49,A                                 cycles=1
0x8912:  b6 48          ld A, $48                                cycles=1
0x8914:  bb 4a          add A, $4a                               cycles=1
0x8916:  25 07          jrc $891f  (offset=7)                    cycles=1-2
0x8918:  b7 3e          ld $3e,A                                 cycles=1
0x891a:  b6 49          ld A, $49                                cycles=1
0x891c:  b7 3f          ld $3f,A                                 cycles=1
0x891e:  81             ret                                      cycles=4
0x891f:  81             ret                                      cycles=4
0x8920:  cd 89 4c       call $894c                               cycles=4
0x8923:  90 5f          clrw Y                                   cycles=1
0x8925:  5f             clrw X                                   cycles=1
0x8926:  b6 3e          ld A, $3e                                cycles=1
0x8928:  95             ld XH, A                                 cycles=1
0x8929:  b6 3f          ld A, $3f                                cycles=1
0x892b:  97             ld XL, A                                 cycles=1
0x892c:  a3 17 e8       cpw X, #$17e8                            cycles=2
0x892f:  24 0a          jrnc $893b  (offset=10)                  cycles=1-2
0x8931:  72 12 00 98    bset $98, #1                             cycles=1
0x8935:  90 ae 00 18    ldw Y, #$18                              cycles=2
0x8939:  20 0a          jra $8945  (offset=10)                   cycles=2
0x893b:  90 ae 00 1e    ldw Y, #$1e                              cycles=2
0x893f:  72 13 00 98    bres $98, #1                             cycles=1
0x8943:  20 00          jra $8945  (offset=0)                    cycles=2
0x8945:  65             divw X, Y                                cycles=2-17
0x8946:  5d             tnzw X                                   cycles=2
0x8947:  27 02          jreq $894b  (offset=2)                   cycles=1-2
0x8949:  bf cd          ldw $cd,X                                cycles=2
0x894b:  81             ret                                      cycles=4
0x894c:  5f             clrw X                                   cycles=1
0x894d:  b6 3e          ld A, $3e                                cycles=1
0x894f:  95             ld XH, A                                 cycles=1
0x8950:  b6 3f          ld A, $3f                                cycles=1
0x8952:  97             ld XL, A                                 cycles=1
0x8953:  a3 05 fa       cpw X, #$5fa                             cycles=2
0x8956:  25 10          jrc $8968  (offset=16)                   cycles=1-2
0x8958:  a6 01          ld A, #$01                               cycles=1
0x895a:  b1 ca          cp A, $ca                                cycles=1
0x895c:  25 03          jrc $8961  (offset=3)                    cycles=1-2
0x895e:  b7 ca          ld $ca,A                                 cycles=1
0x8960:  81             ret                                      cycles=4
0x8961:  3d ca          tnz $ca                                  cycles=1
0x8963:  27 02          jreq $8967  (offset=2)                   cycles=1-2
0x8965:  3a ca          dec $ca                                  cycles=1
0x8967:  81             ret                                      cycles=4
0x8968:  ae 05 fa       ldw X, #$5fa                             cycles=2
0x896b:  72 b0 00 3e    subw X, $3e                              cycles=2
0x896f:  a6 05          ld A, #$05                               cycles=1
0x8971:  62             div X, A                                 cycles=2-17
0x8972:  a3 00 80       cpw X, #$80                              cycles=2
0x8975:  24 0c          jrnc $8983  (offset=12)                  cycles=1-2
0x8977:  9f             ld A, XL                                 cycles=1
0x8978:  ab 01          add A, #$01                              cycles=1
0x897a:  b1 ca          cp A, $ca                                cycles=1
0x897c:  27 04          jreq $8982  (offset=4)                   cycles=1-2
0x897e:  24 0e          jrnc $898e  (offset=14)                  cycles=1-2
0x8980:  3a ca          dec $ca                                  cycles=1
0x8982:  81             ret                                      cycles=4
0x8983:  a6 81          ld A, #$81                               cycles=1
0x8985:  b1 ca          cp A, $ca                                cycles=1
0x8987:  27 f9          jreq $8982  (offset=-7)                  cycles=1-2
0x8989:  24 03          jrnc $898e  (offset=3)                   cycles=1-2
0x898b:  b7 ca          ld $ca,A                                 cycles=1
0x898d:  81             ret                                      cycles=4
0x898e:  3c ca          inc $ca                                  cycles=1
0x8990:  81             ret                                      cycles=4
0x8991:  b6 08          ld A, $08                                cycles=1
0x8993:  a1 01          cp A, #$01                               cycles=1
0x8995:  27 23          jreq $89ba  (offset=35)                  cycles=1-2
0x8997:  a1 05          cp A, #$05                               cycles=1
0x8999:  27 52          jreq $89ed  (offset=82)                  cycles=1-2
0x899b:  a1 04          cp A, #$04                               cycles=1
0x899d:  27 10          jreq $89af  (offset=16)                  cycles=1-2
0x899f:  a1 06          cp A, #$06                               cycles=1
0x89a1:  27 09          jreq $89ac  (offset=9)                   cycles=1-2
0x89a3:  a1 02          cp A, #$02                               cycles=1
0x89a5:  27 0d          jreq $89b4  (offset=13)                  cycles=1-2
0x89a7:  a1 03          cp A, #$03                               cycles=1
0x89a9:  27 0c          jreq $89b7  (offset=12)                  cycles=1-2
0x89ab:  81             ret                                      cycles=4
0x89ac:  cc 8a 5d       jp $8a5d                                 cycles=1
0x89af:  cc 8a 2a       jp $8a2a                                 cycles=1
0x89b2:  20 76          jra $8a2a  (offset=118)                  cycles=2
0x89b4:  cc 8a 9a       jp $8a9a                                 cycles=1
0x89b7:  cc 8a cb       jp $8acb                                 cycles=1
0x89ba:  a6 00          ld A, #$00                               cycles=1
0x89bc:  bb ca          add A, $ca                               cycles=1
0x89be:  b7 b6          ld $b6,A                                 cycles=1
0x89c0:  a6 00          ld A, #$00                               cycles=1
0x89c2:  b7 b9          ld $b9,A                                 cycles=1
0x89c4:  a6 e0          ld A, #$e0                               cycles=1
0x89c6:  bb ca          add A, $ca                               cycles=1
0x89c8:  25 08          jrc $89d2  (offset=8)                    cycles=1-2
0x89ca:  b7 b5          ld $b5,A                                 cycles=1
0x89cc:  a6 01          ld A, #$01                               cycles=1
0x89ce:  b7 b8          ld $b8,A                                 cycles=1
0x89d0:  20 06          jra $89d8  (offset=6)                    cycles=2
0x89d2:  b7 b5          ld $b5,A                                 cycles=1
0x89d4:  a6 02          ld A, #$02                               cycles=1
0x89d6:  b7 b8          ld $b8,A                                 cycles=1
0x89d8:  a6 c0          ld A, #$c0                               cycles=1
0x89da:  bb ca          add A, $ca                               cycles=1
0x89dc:  25 08          jrc $89e6  (offset=8)                    cycles=1-2
0x89de:  b7 b4          ld $b4,A                                 cycles=1
0x89e0:  a6 03          ld A, #$03                               cycles=1
0x89e2:  b7 b7          ld $b7,A                                 cycles=1
0x89e4:  20 06          jra $89ec  (offset=6)                    cycles=2
0x89e6:  b7 b4          ld $b4,A                                 cycles=1
0x89e8:  a6 04          ld A, #$04                               cycles=1
0x89ea:  b7 b7          ld $b7,A                                 cycles=1
0x89ec:  81             ret                                      cycles=4
0x89ed:  a6 f0          ld A, #$f0                               cycles=1
0x89ef:  bb ca          add A, $ca                               cycles=1
0x89f1:  25 08          jrc $89fb  (offset=8)                    cycles=1-2
0x89f3:  b7 b6          ld $b6,A                                 cycles=1
0x89f5:  a6 00          ld A, #$00                               cycles=1
0x89f7:  b7 b9          ld $b9,A                                 cycles=1
0x89f9:  20 06          jra $8a01  (offset=6)                    cycles=2
0x89fb:  b7 b6          ld $b6,A                                 cycles=1
0x89fd:  a6 01          ld A, #$01                               cycles=1
0x89ff:  b7 b9          ld $b9,A                                 cycles=1
0x8a01:  a6 d0          ld A, #$d0                               cycles=1
0x8a03:  bb ca          add A, $ca                               cycles=1
0x8a05:  25 08          jrc $8a0f  (offset=8)                    cycles=1-2
0x8a07:  b7 b5          ld $b5,A                                 cycles=1
0x8a09:  a6 02          ld A, #$02                               cycles=1
0x8a0b:  b7 b8          ld $b8,A                                 cycles=1
0x8a0d:  20 06          jra $8a15  (offset=6)                    cycles=2
0x8a0f:  b7 b5          ld $b5,A                                 cycles=1
0x8a11:  a6 03          ld A, #$03                               cycles=1
0x8a13:  b7 b8          ld $b8,A                                 cycles=1
0x8a15:  a6 b0          ld A, #$b0                               cycles=1
0x8a17:  bb ca          add A, $ca                               cycles=1
0x8a19:  25 08          jrc $8a23  (offset=8)                    cycles=1-2
0x8a1b:  b7 b4          ld $b4,A                                 cycles=1
0x8a1d:  a6 04          ld A, #$04                               cycles=1
0x8a1f:  b7 b7          ld $b7,A                                 cycles=1
0x8a21:  20 06          jra $8a29  (offset=6)                    cycles=2
0x8a23:  b7 b4          ld $b4,A                                 cycles=1
0x8a25:  a6 05          ld A, #$05                               cycles=1
0x8a27:  b7 b7          ld $b7,A                                 cycles=1
0x8a29:  81             ret                                      cycles=4
0x8a2a:  a6 e0          ld A, #$e0                               cycles=1
0x8a2c:  bb ca          add A, $ca                               cycles=1
0x8a2e:  25 08          jrc $8a38  (offset=8)                    cycles=1-2
0x8a30:  b7 b6          ld $b6,A                                 cycles=1
0x8a32:  a6 01          ld A, #$01                               cycles=1
0x8a34:  b7 b9          ld $b9,A                                 cycles=1
0x8a36:  20 06          jra $8a3e  (offset=6)                    cycles=2
0x8a38:  b7 b6          ld $b6,A                                 cycles=1
0x8a3a:  a6 02          ld A, #$02                               cycles=1
0x8a3c:  b7 b9          ld $b9,A                                 cycles=1
0x8a3e:  a6 c0          ld A, #$c0                               cycles=1
0x8a40:  bb ca          add A, $ca                               cycles=1
0x8a42:  25 08          jrc $8a4c  (offset=8)                    cycles=1-2
0x8a44:  b7 b5          ld $b5,A                                 cycles=1
0x8a46:  a6 03          ld A, #$03                               cycles=1
0x8a48:  b7 b8          ld $b8,A                                 cycles=1
0x8a4a:  20 06          jra $8a52  (offset=6)                    cycles=2
0x8a4c:  b7 b5          ld $b5,A                                 cycles=1
0x8a4e:  a6 04          ld A, #$04                               cycles=1
0x8a50:  b7 b8          ld $b8,A                                 cycles=1
0x8a52:  a6 00          ld A, #$00                               cycles=1
0x8a54:  bb ca          add A, $ca                               cycles=1
0x8a56:  b7 b4          ld $b4,A                                 cycles=1
0x8a58:  a6 00          ld A, #$00                               cycles=1
0x8a5a:  b7 b7          ld $b7,A                                 cycles=1
0x8a5c:  81             ret                                      cycles=4
0x8a5d:  a6 d0          ld A, #$d0                               cycles=1
0x8a5f:  bb ca          add A, $ca                               cycles=1
0x8a61:  25 08          jrc $8a6b  (offset=8)                    cycles=1-2
0x8a63:  b7 b6          ld $b6,A                                 cycles=1
0x8a65:  a6 02          ld A, #$02                               cycles=1
0x8a67:  b7 b9          ld $b9,A                                 cycles=1
0x8a69:  20 06          jra $8a71  (offset=6)                    cycles=2
0x8a6b:  b7 b6          ld $b6,A                                 cycles=1
0x8a6d:  a6 03          ld A, #$03                               cycles=1
0x8a6f:  b7 b9          ld $b9,A                                 cycles=1
0x8a71:  a6 b0          ld A, #$b0                               cycles=1
0x8a73:  bb ca          add A, $ca                               cycles=1
0x8a75:  25 08          jrc $8a7f  (offset=8)                    cycles=1-2
0x8a77:  b7 b5          ld $b5,A                                 cycles=1
0x8a79:  a6 04          ld A, #$04                               cycles=1
0x8a7b:  b7 b8          ld $b8,A                                 cycles=1
0x8a7d:  20 06          jra $8a85  (offset=6)                    cycles=2
0x8a7f:  b7 b5          ld $b5,A                                 cycles=1
0x8a81:  a6 05          ld A, #$05                               cycles=1
0x8a83:  b7 b8          ld $b8,A                                 cycles=1
0x8a85:  a6 f0          ld A, #$f0                               cycles=1
0x8a87:  bb ca          add A, $ca                               cycles=1
0x8a89:  25 08          jrc $8a93  (offset=8)                    cycles=1-2
0x8a8b:  b7 b4          ld $b4,A                                 cycles=1
0x8a8d:  a6 00          ld A, #$00                               cycles=1
0x8a8f:  b7 b7          ld $b7,A                                 cycles=1
0x8a91:  20 06          jra $8a99  (offset=6)                    cycles=2
0x8a93:  b7 b4          ld $b4,A                                 cycles=1
0x8a95:  a6 01          ld A, #$01                               cycles=1
0x8a97:  b7 b7          ld $b7,A                                 cycles=1
0x8a99:  81             ret                                      cycles=4
0x8a9a:  a6 c0          ld A, #$c0                               cycles=1
0x8a9c:  bb ca          add A, $ca                               cycles=1
0x8a9e:  25 08          jrc $8aa8  (offset=8)                    cycles=1-2
0x8aa0:  b7 b6          ld $b6,A                                 cycles=1
0x8aa2:  a6 03          ld A, #$03                               cycles=1
0x8aa4:  b7 b9          ld $b9,A                                 cycles=1
0x8aa6:  20 06          jra $8aae  (offset=6)                    cycles=2
0x8aa8:  b7 b6          ld $b6,A                                 cycles=1
0x8aaa:  a6 04          ld A, #$04                               cycles=1
0x8aac:  b7 b9          ld $b9,A                                 cycles=1
0x8aae:  a6 00          ld A, #$00                               cycles=1
0x8ab0:  bb ca          add A, $ca                               cycles=1
0x8ab2:  b7 b5          ld $b5,A                                 cycles=1
0x8ab4:  a6 00          ld A, #$00                               cycles=1
0x8ab6:  b7 b8          ld $b8,A                                 cycles=1
0x8ab8:  a6 e0          ld A, #$e0                               cycles=1
0x8aba:  bb ca          add A, $ca                               cycles=1
0x8abc:  25 06          jrc $8ac4  (offset=6)                    cycles=1-2
0x8abe:  b7 b4          ld $b4,A                                 cycles=1
0x8ac0:  a6 01          ld A, #$01                               cycles=1
0x8ac2:  b7 b7          ld $b7,A                                 cycles=1
0x8ac4:  b7 b4          ld $b4,A                                 cycles=1
0x8ac6:  a6 02          ld A, #$02                               cycles=1
0x8ac8:  b7 b7          ld $b7,A                                 cycles=1
0x8aca:  81             ret                                      cycles=4
0x8acb:  a6 b0          ld A, #$b0                               cycles=1
0x8acd:  bb ca          add A, $ca                               cycles=1
0x8acf:  25 08          jrc $8ad9  (offset=8)                    cycles=1-2
0x8ad1:  b7 b6          ld $b6,A                                 cycles=1
0x8ad3:  a6 04          ld A, #$04                               cycles=1
0x8ad5:  b7 b9          ld $b9,A                                 cycles=1
0x8ad7:  20 06          jra $8adf  (offset=6)                    cycles=2
0x8ad9:  b7 b6          ld $b6,A                                 cycles=1
0x8adb:  a6 05          ld A, #$05                               cycles=1
0x8add:  b7 b9          ld $b9,A                                 cycles=1
0x8adf:  a6 f0          ld A, #$f0                               cycles=1
0x8ae1:  bb ca          add A, $ca                               cycles=1
0x8ae3:  25 08          jrc $8aed  (offset=8)                    cycles=1-2
0x8ae5:  b7 b5          ld $b5,A                                 cycles=1
0x8ae7:  a6 00          ld A, #$00                               cycles=1
0x8ae9:  b7 b8          ld $b8,A                                 cycles=1
0x8aeb:  20 06          jra $8af3  (offset=6)                    cycles=2
0x8aed:  b7 b5          ld $b5,A                                 cycles=1
0x8aef:  a6 01          ld A, #$01                               cycles=1
0x8af1:  b7 b8          ld $b8,A                                 cycles=1
0x8af3:  a6 d0          ld A, #$d0                               cycles=1
0x8af5:  bb ca          add A, $ca                               cycles=1
0x8af7:  25 08          jrc $8b01  (offset=8)                    cycles=1-2
0x8af9:  b7 b4          ld $b4,A                                 cycles=1
0x8afb:  a6 02          ld A, #$02                               cycles=1
0x8afd:  b7 b7          ld $b7,A                                 cycles=1
0x8aff:  20 06          jra $8b07  (offset=6)                    cycles=2
0x8b01:  b7 b4          ld $b4,A                                 cycles=1
0x8b03:  a6 03          ld A, #$03                               cycles=1
0x8b05:  b7 b7          ld $b7,A                                 cycles=1
0x8b07:  81             ret                                      cycles=4
0x8b08:  06 06          rrc ($06,SP)                             cycles=1
0x8b0a:  06 06          rrc ($06,SP)                             cycles=1
0x8b0c:  06 06          rrc ($06,SP)                             cycles=1
0x8b0e:  06 06          rrc ($06,SP)                             cycles=1
0x8b10:  06 06          rrc ($06,SP)                             cycles=1
0x8b12:  06 06          rrc ($06,SP)                             cycles=1
0x8b14:  06 05          rrc ($05,SP)                             cycles=1
0x8b16:  04 03          srl ($03,SP)                             cycles=1
0x8b18:  02             rlwa X, A                                cycles=1
0x8b19:  01             rrwa X, A                                cycles=1
0x8b1a:  01             rrwa X, A                                cycles=1
0x8b1b:  00 00          neg ($00,SP)                             cycles=1
0x8b1d:  01             rrwa X, A                                cycles=1
0x8b1e:  01             rrwa X, A                                cycles=1
0x8b1f:  01             rrwa X, A                                cycles=1
0x8b20:  01             rrwa X, A                                cycles=1
0x8b21:  01             rrwa X, A                                cycles=1
0x8b22:  01             rrwa X, A                                cycles=1
0x8b23:  01             rrwa X, A                                cycles=1
0x8b24:  01             rrwa X, A                                cycles=1
0x8b25:  02             rlwa X, A                                cycles=1
0x8b26:  02             rlwa X, A                                cycles=1
0x8b27:  02             rlwa X, A                                cycles=1
0x8b28:  02             rlwa X, A                                cycles=1
0x8b29:  02             rlwa X, A                                cycles=1
0x8b2a:  02             rlwa X, A                                cycles=1
0x8b2b:  02             rlwa X, A                                cycles=1
0x8b2c:  03 03          cpl ($03,SP)                             cycles=1
0x8b2e:  03 03          cpl ($03,SP)                             cycles=1
0x8b30:  03 03          cpl ($03,SP)                             cycles=1
0x8b32:  00 01          neg ($01,SP)                             cycles=1
0x8b34:  03 04          cpl ($04,SP)                             cycles=1
0x8b36:  05             ???                                      cycles=?
0x8b37:  06 07          rrc ($07,SP)                             cycles=1
0x8b39:  09 0a          rlc ($0a,SP)                             cycles=1
0x8b3b:  0b             ???                                      cycles=?
0x8b3c:  0c 0d          inc ($0d,SP)                             cycles=1
0x8b3e:  0f 10          clr ($10,SP)                             cycles=1
0x8b40:  11 12          cp A, ($12,SP)                           cycles=1
0x8b42:  13 15          cpw X, ($15,SP)                          cycles=2
0x8b44:  16 17          ldw Y, ($17,SP)                          cycles=2
0x8b46:  18 19          xor A, ($19,SP)                          cycles=1
0x8b48:  1a 1b          or A, ($1b,SP)                           cycles=1
0x8b4a:  1d 1e 1f       subw X, #$1e1f                           cycles=2
0x8b4d:  20 21          jra $8b70  (offset=33)                   cycles=2
0x8b4f:  22 23          jrugt $8b74  (offset=35)                 cycles=1-2
0x8b51:  24 26          jrnc $8b79  (offset=38)                  cycles=1-2
0x8b53:  27 28          jreq $8b7d  (offset=40)                  cycles=1-2
0x8b55:  29 2a          jrv $8b81  (offset=42)                   cycles=1-2
0x8b57:  2b 2c          jrmi $8b85  (offset=44)                  cycles=1-2
0x8b59:  2d 2e          jrsle $8b89  (offset=46)                 cycles=1-2
0x8b5b:  2f 30          jrslt $8b8d  (offset=48)                 cycles=1-2
0x8b5d:  31 32 34       exg A, $3234                             cycles=3
0x8b60:  35 36 37 38    mov $3738, #$36                          cycles=1
0x8b64:  39 3a          rlc $3a                                  cycles=1
0x8b66:  3b 3c 3d       push $3c3d                               cycles=1
0x8b69:  3e 3f          swap $3f                                 cycles=1
0x8b6b:  40             neg A                                    cycles=1
0x8b6c:  41             exg A, XL                                cycles=1
0x8b6d:  42             mul X, A                                 cycles=4
0x8b6e:  43             cpl A                                    cycles=1
0x8b6f:  44             srl A                                    cycles=1
0x8b70:  45 46 47       mov $47, $46                             cycles=1
0x8b73:  48             sll A                                    cycles=1
0x8b74:  49             rlc A                                    cycles=1
0x8b75:  4a             dec A                                    cycles=1
0x8b76:  4b 4c          push #$4c                                cycles=1
0x8b78:  4d             tnz A                                    cycles=1
0x8b79:  4e             swap A                                   cycles=1
0x8b7a:  4f             clr A                                    cycles=1
0x8b7b:  50             negw X                                   cycles=1
0x8b7c:  51             exgw X, Y                                cycles=1
0x8b7d:  52 53          sub SP, #$53                             cycles=1
0x8b7f:  54             srlw X                                   cycles=2
0x8b80:  55 56 57 58 59 mov $5859, $5657                         cycles=1
0x8b85:  5a             decw X                                   cycles=1
0x8b86:  5b 5c          addw SP, #$5c                            cycles=2
0x8b88:  5d             tnzw X                                   cycles=2
0x8b89:  5e             swapw X                                  cycles=1
0x8b8a:  5f             clrw X                                   cycles=1
0x8b8b:  60 60          neg ($60,X)                              cycles=1
0x8b8d:  61             exg A, YL                                cycles=1
0x8b8e:  62             div X, A                                 cycles=2-17
0x8b8f:  63 64          cpl ($64,X)                              cycles=1
0x8b91:  65             divw X, Y                                cycles=2-17
0x8b92:  66 67          rrc ($67,X)                              cycles=1
0x8b94:  68 69          sll ($69,X)                              cycles=1
0x8b96:  6a 6b          dec ($6b,X)                              cycles=1
0x8b98:  6c 6d          inc ($6d,X)                              cycles=1
0x8b9a:  6e 6f          swap ($6f,X)                             cycles=1
0x8b9c:  70             neg (X)                                  cycles=1
0x8b9d:  71             ???                                      cycles=?
0x8b9e:  72 73          ???                                      cycles=?
0x8ba0:  74             srl (X)                                  cycles=1
0x8ba1:  74             srl (X)                                  cycles=1
0x8ba2:  75             ???                                      cycles=?
0x8ba3:  76             rrc (X)                                  cycles=1
0x8ba4:  77             sra (X)                                  cycles=1
0x8ba5:  78             sll (X)                                  cycles=1
0x8ba6:  79             rlc (X)                                  cycles=1
0x8ba7:  7a             dec (X)                                  cycles=1
0x8ba8:  7b 7c          ld A, ($7c,SP)                           cycles=1
0x8baa:  7d             tnz (X)                                  cycles=1
0x8bab:  7e             swap (X)                                 cycles=1
0x8bac:  7f             clr (X)                                  cycles=1
0x8bad:  80             iret                                     cycles=11
0x8bae:  81             ret                                      cycles=4
0x8baf:  82 83 84 85    int $838485                              cycles=2
0x8bb3:  86             pop CC                                   cycles=1
0x8bb4:  86             pop CC                                   cycles=1
0x8bb5:  87             retf                                     cycles=5
0x8bb6:  88             push A                                   cycles=1
0x8bb7:  89             pushw X                                  cycles=2
0x8bb8:  8a             push CC                                  cycles=1
0x8bb9:  8b             break                                    cycles=1
0x8bba:  8c             ccf                                      cycles=1
0x8bbb:  8d 8e 8f 90    callf $8e8f90                            cycles=5
0x8bbf:  91 92          ???                                      cycles=?
0x8bc1:  93             ldw X, Y                                 cycles=1
0x8bc2:  94             ldw SP, X                                cycles=1
0x8bc3:  95             ld XH, A                                 cycles=1
0x8bc4:  96             ldw X, SP                                cycles=1
0x8bc5:  97             ld XL, A                                 cycles=1
0x8bc6:  98             rcf                                      cycles=1
0x8bc7:  99             scf                                      cycles=1
0x8bc8:  9a             rim                                      cycles=1
0x8bc9:  9a             rim                                      cycles=1
0x8bca:  9b             sim                                      cycles=1
0x8bcb:  9c             rvf                                      cycles=5
0x8bcc:  9d             nop                                      cycles=1
0x8bcd:  9e             ld A, XH                                 cycles=1
0x8bce:  9f             ld A, XL                                 cycles=1
0x8bcf:  a0 a1          sub A, #$a1                              cycles=1
0x8bd1:  a2 a3          sbc A, #$a3                              cycles=1
0x8bd3:  a4 a5          and A, #$a5                              cycles=1
0x8bd5:  a6 a7          ld A, #$a7                               cycles=1
0x8bd7:  a8 a9          xor A, #$a9                              cycles=1
0x8bd9:  aa ab          or A, #$ab                               cycles=1
0x8bdb:  ac ad ae af    jpf $adaeaf                              cycles=2
0x8bdf:  b0 b1          sub A, $b1                               cycles=1
0x8be1:  b2 b3          sbc A, $b3                               cycles=1
0x8be3:  b4 b5          and A, $b5                               cycles=1
0x8be5:  b6 b7          ld A, $b7                                cycles=1
0x8be7:  b8 b9          xor A, $b9                               cycles=1
0x8be9:  ba bb          or A, $bb                                cycles=1
0x8beb:  bc bd be bf    ldf A, $bdbebf                           cycles=1
0x8bef:  c0 c1 c2       sub A, $c1c2                             cycles=1
0x8bf2:  c3 c4 c5       cpw X, $c4c5                             cycles=2
0x8bf5:  c6 c8 c9       ld A, $c8c9                              cycles=1
0x8bf8:  ca cb cc       or A, $cbcc                              cycles=1
0x8bfb:  cd ce cf       call $cecf                               cycles=4
0x8bfe:  d0 d1 d2       sub A, ($d1d2,X)                         cycles=1
0x8c01:  d3 d4 d6       cpw Y, ($d4d6,X)                         cycles=2
0x8c04:  d7 d8 d9       ld ($d8d9,X),A                           cycles=1
0x8c07:  da db dc       or A, ($dbdc,X)                          cycles=1
0x8c0a:  dd df e0       call ($dfe0,X)                           cycles=4
0x8c0d:  e1 e2          cp A, ($e2,X)                            cycles=1
0x8c0f:  e3 e4          cpw Y, ($e4,X)                           cycles=2
0x8c11:  e5 e7          bcp A, ($e7,X)                           cycles=1
0x8c13:  e8 e9          xor A, ($e9,X)                           cycles=1
0x8c15:  ea eb          or A, ($eb,X)                            cycles=1
0x8c17:  ed ee          call ($ee,X)                             cycles=4
0x8c19:  ef f0          ldw ($f0,X),Y                            cycles=2
0x8c1b:  f1             cp A, (X)                                cycles=1
0x8c1c:  f3             cpw Y, (X)                               cycles=2
0x8c1d:  f4             and A, (X)                               cycles=1
0x8c1e:  f5             bcp A, (X)                               cycles=1
0x8c1f:  f6             ld A, (X)                                cycles=1
0x8c20:  f7             ld (X),A                                 cycles=1
0x8c21:  f9             adc A, (X)                               cycles=1
0x8c22:  fa             or A, (X)                                cycles=1
0x8c23:  fa             or A, (X)                                cycles=1
0x8c24:  fa             or A, (X)                                cycles=1
0x8c25:  fa             or A, (X)                                cycles=1
0x8c26:  fa             or A, (X)                                cycles=1
0x8c27:  fa             or A, (X)                                cycles=1
0x8c28:  fa             or A, (X)                                cycles=1
0x8c29:  fa             or A, (X)                                cycles=1
0x8c2a:  fa             or A, (X)                                cycles=1
0x8c2b:  fa             or A, (X)                                cycles=1
0x8c2c:  fa             or A, (X)                                cycles=1
0x8c2d:  fa             or A, (X)                                cycles=1
0x8c2e:  fa             or A, (X)                                cycles=1
0x8c2f:  fa             or A, (X)                                cycles=1
0x8c30:  fa             or A, (X)                                cycles=1
0x8c31:  fa             or A, (X)                                cycles=1
0x8c32:  fa             or A, (X)                                cycles=1
0x8c33:  fa             or A, (X)                                cycles=1
0x8c34:  fa             or A, (X)                                cycles=1
0x8c35:  fa             or A, (X)                                cycles=1
0x8c36:  fa             or A, (X)                                cycles=1
0x8c37:  fa             or A, (X)                                cycles=1
0x8c38:  fa             or A, (X)                                cycles=1
0x8c39:  fa             or A, (X)                                cycles=1
0x8c3a:  fa             or A, (X)                                cycles=1
0x8c3b:  fa             or A, (X)                                cycles=1
0x8c3c:  fa             or A, (X)                                cycles=1
0x8c3d:  fa             or A, (X)                                cycles=1
0x8c3e:  fa             or A, (X)                                cycles=1
0x8c3f:  fa             or A, (X)                                cycles=1
0x8c40:  fa             or A, (X)                                cycles=1
0x8c41:  fa             or A, (X)                                cycles=1
0x8c42:  fa             or A, (X)                                cycles=1
0x8c43:  fa             or A, (X)                                cycles=1
0x8c44:  fa             or A, (X)                                cycles=1
0x8c45:  fa             or A, (X)                                cycles=1
0x8c46:  fa             or A, (X)                                cycles=1
0x8c47:  fa             or A, (X)                                cycles=1
0x8c48:  fa             or A, (X)                                cycles=1
0x8c49:  fa             or A, (X)                                cycles=1
0x8c4a:  fa             or A, (X)                                cycles=1
0x8c4b:  fa             or A, (X)                                cycles=1
0x8c4c:  fa             or A, (X)                                cycles=1
0x8c4d:  fa             or A, (X)                                cycles=1
0x8c4e:  fa             or A, (X)                                cycles=1
0x8c4f:  fa             or A, (X)                                cycles=1
0x8c50:  fa             or A, (X)                                cycles=1
0x8c51:  fa             or A, (X)                                cycles=1
0x8c52:  fa             or A, (X)                                cycles=1
0x8c53:  fa             or A, (X)                                cycles=1
0x8c54:  fa             or A, (X)                                cycles=1
0x8c55:  fa             or A, (X)                                cycles=1
0x8c56:  fa             or A, (X)                                cycles=1
0x8c57:  fa             or A, (X)                                cycles=1
0x8c58:  fa             or A, (X)                                cycles=1
0x8c59:  fa             or A, (X)                                cycles=1
0x8c5a:  fa             or A, (X)                                cycles=1
0x8c5b:  fa             or A, (X)                                cycles=1
0x8c5c:  fa             or A, (X)                                cycles=1
0x8c5d:  fa             or A, (X)                                cycles=1
0x8c5e:  fa             or A, (X)                                cycles=1
0x8c5f:  fa             or A, (X)                                cycles=1
0x8c60:  fa             or A, (X)                                cycles=1
0x8c61:  fa             or A, (X)                                cycles=1
0x8c62:  fa             or A, (X)                                cycles=1
0x8c63:  fa             or A, (X)                                cycles=1
0x8c64:  fa             or A, (X)                                cycles=1
0x8c65:  fa             or A, (X)                                cycles=1
0x8c66:  fa             or A, (X)                                cycles=1
0x8c67:  fa             or A, (X)                                cycles=1
0x8c68:  fa             or A, (X)                                cycles=1
0x8c69:  fa             or A, (X)                                cycles=1
0x8c6a:  fa             or A, (X)                                cycles=1
0x8c6b:  fa             or A, (X)                                cycles=1
0x8c6c:  fa             or A, (X)                                cycles=1
0x8c6d:  fa             or A, (X)                                cycles=1
0x8c6e:  fa             or A, (X)                                cycles=1
0x8c6f:  fa             or A, (X)                                cycles=1
0x8c70:  fa             or A, (X)                                cycles=1
0x8c71:  fa             or A, (X)                                cycles=1
0x8c72:  fa             or A, (X)                                cycles=1
0x8c73:  fa             or A, (X)                                cycles=1
0x8c74:  fa             or A, (X)                                cycles=1
0x8c75:  fa             or A, (X)                                cycles=1
0x8c76:  fa             or A, (X)                                cycles=1
0x8c77:  fa             or A, (X)                                cycles=1
0x8c78:  fa             or A, (X)                                cycles=1
0x8c79:  fa             or A, (X)                                cycles=1
0x8c7a:  fa             or A, (X)                                cycles=1
0x8c7b:  fa             or A, (X)                                cycles=1
0x8c7c:  fa             or A, (X)                                cycles=1
0x8c7d:  fa             or A, (X)                                cycles=1
0x8c7e:  fa             or A, (X)                                cycles=1
0x8c7f:  fa             or A, (X)                                cycles=1
0x8c80:  fa             or A, (X)                                cycles=1
0x8c81:  fa             or A, (X)                                cycles=1
0x8c82:  fa             or A, (X)                                cycles=1
0x8c83:  fa             or A, (X)                                cycles=1
0x8c84:  fa             or A, (X)                                cycles=1
0x8c85:  fa             or A, (X)                                cycles=1
0x8c86:  fa             or A, (X)                                cycles=1
0x8c87:  fa             or A, (X)                                cycles=1
0x8c88:  fa             or A, (X)                                cycles=1
0x8c89:  fa             or A, (X)                                cycles=1
0x8c8a:  fa             or A, (X)                                cycles=1
0x8c8b:  fa             or A, (X)                                cycles=1
0x8c8c:  fa             or A, (X)                                cycles=1
0x8c8d:  fa             or A, (X)                                cycles=1
0x8c8e:  fa             or A, (X)                                cycles=1
0x8c8f:  fa             or A, (X)                                cycles=1
0x8c90:  fa             or A, (X)                                cycles=1
0x8c91:  fa             or A, (X)                                cycles=1
0x8c92:  fa             or A, (X)                                cycles=1
0x8c93:  fa             or A, (X)                                cycles=1
0x8c94:  fa             or A, (X)                                cycles=1
0x8c95:  fa             or A, (X)                                cycles=1
0x8c96:  fa             or A, (X)                                cycles=1
0x8c97:  fa             or A, (X)                                cycles=1
0x8c98:  fa             or A, (X)                                cycles=1
0x8c99:  fa             or A, (X)                                cycles=1
0x8c9a:  fa             or A, (X)                                cycles=1
0x8c9b:  fa             or A, (X)                                cycles=1
0x8c9c:  fa             or A, (X)                                cycles=1
0x8c9d:  fa             or A, (X)                                cycles=1
0x8c9e:  fa             or A, (X)                                cycles=1
0x8c9f:  fa             or A, (X)                                cycles=1
0x8ca0:  fa             or A, (X)                                cycles=1
0x8ca1:  fa             or A, (X)                                cycles=1
0x8ca2:  fa             or A, (X)                                cycles=1
0x8ca3:  fa             or A, (X)                                cycles=1
0x8ca4:  fa             or A, (X)                                cycles=1
0x8ca5:  fa             or A, (X)                                cycles=1
0x8ca6:  fa             or A, (X)                                cycles=1
0x8ca7:  fa             or A, (X)                                cycles=1
0x8ca8:  fa             or A, (X)                                cycles=1
0x8ca9:  fa             or A, (X)                                cycles=1
0x8caa:  fa             or A, (X)                                cycles=1
0x8cab:  fa             or A, (X)                                cycles=1
0x8cac:  fa             or A, (X)                                cycles=1
0x8cad:  fa             or A, (X)                                cycles=1
0x8cae:  fa             or A, (X)                                cycles=1
0x8caf:  fa             or A, (X)                                cycles=1
0x8cb0:  fa             or A, (X)                                cycles=1
0x8cb1:  fa             or A, (X)                                cycles=1
0x8cb2:  fa             or A, (X)                                cycles=1
0x8cb3:  fa             or A, (X)                                cycles=1
0x8cb4:  fa             or A, (X)                                cycles=1
0x8cb5:  fa             or A, (X)                                cycles=1
0x8cb6:  fa             or A, (X)                                cycles=1
0x8cb7:  fa             or A, (X)                                cycles=1
0x8cb8:  fa             or A, (X)                                cycles=1
0x8cb9:  fa             or A, (X)                                cycles=1
0x8cba:  fa             or A, (X)                                cycles=1
0x8cbb:  fa             or A, (X)                                cycles=1
0x8cbc:  fa             or A, (X)                                cycles=1
0x8cbd:  fa             or A, (X)                                cycles=1
0x8cbe:  fa             or A, (X)                                cycles=1
0x8cbf:  fa             or A, (X)                                cycles=1
0x8cc0:  fa             or A, (X)                                cycles=1
0x8cc1:  fa             or A, (X)                                cycles=1
0x8cc2:  fa             or A, (X)                                cycles=1
0x8cc3:  fa             or A, (X)                                cycles=1
0x8cc4:  fa             or A, (X)                                cycles=1
0x8cc5:  fa             or A, (X)                                cycles=1
0x8cc6:  fa             or A, (X)                                cycles=1
0x8cc7:  fa             or A, (X)                                cycles=1
0x8cc8:  fa             or A, (X)                                cycles=1
0x8cc9:  fa             or A, (X)                                cycles=1
0x8cca:  fa             or A, (X)                                cycles=1
0x8ccb:  fa             or A, (X)                                cycles=1
0x8ccc:  fa             or A, (X)                                cycles=1
0x8ccd:  fa             or A, (X)                                cycles=1
0x8cce:  fa             or A, (X)                                cycles=1
0x8ccf:  fa             or A, (X)                                cycles=1
0x8cd0:  fa             or A, (X)                                cycles=1
0x8cd1:  fa             or A, (X)                                cycles=1
0x8cd2:  fa             or A, (X)                                cycles=1
0x8cd3:  fa             or A, (X)                                cycles=1
0x8cd4:  fa             or A, (X)                                cycles=1
0x8cd5:  fa             or A, (X)                                cycles=1
0x8cd6:  fa             or A, (X)                                cycles=1
0x8cd7:  fa             or A, (X)                                cycles=1
0x8cd8:  fa             or A, (X)                                cycles=1
0x8cd9:  fa             or A, (X)                                cycles=1
0x8cda:  fa             or A, (X)                                cycles=1
0x8cdb:  fa             or A, (X)                                cycles=1
0x8cdc:  fa             or A, (X)                                cycles=1
0x8cdd:  fa             or A, (X)                                cycles=1
0x8cde:  fa             or A, (X)                                cycles=1
0x8cdf:  fa             or A, (X)                                cycles=1
0x8ce0:  fa             or A, (X)                                cycles=1
0x8ce1:  fa             or A, (X)                                cycles=1
0x8ce2:  fa             or A, (X)                                cycles=1
0x8ce3:  fa             or A, (X)                                cycles=1
0x8ce4:  fa             or A, (X)                                cycles=1
0x8ce5:  fa             or A, (X)                                cycles=1
0x8ce6:  fa             or A, (X)                                cycles=1
0x8ce7:  fa             or A, (X)                                cycles=1
0x8ce8:  fa             or A, (X)                                cycles=1
0x8ce9:  fa             or A, (X)                                cycles=1
0x8cea:  fa             or A, (X)                                cycles=1
0x8ceb:  fa             or A, (X)                                cycles=1
0x8cec:  fa             or A, (X)                                cycles=1
0x8ced:  fa             or A, (X)                                cycles=1
0x8cee:  fa             or A, (X)                                cycles=1
0x8cef:  fa             or A, (X)                                cycles=1
0x8cf0:  fa             or A, (X)                                cycles=1
0x8cf1:  fa             or A, (X)                                cycles=1
0x8cf2:  fa             or A, (X)                                cycles=1
0x8cf3:  fa             or A, (X)                                cycles=1
0x8cf4:  fa             or A, (X)                                cycles=1
0x8cf5:  fa             or A, (X)                                cycles=1
0x8cf6:  fa             or A, (X)                                cycles=1
0x8cf7:  fa             or A, (X)                                cycles=1
0x8cf8:  fa             or A, (X)                                cycles=1
0x8cf9:  fa             or A, (X)                                cycles=1
0x8cfa:  fa             or A, (X)                                cycles=1
0x8cfb:  fa             or A, (X)                                cycles=1
0x8cfc:  fa             or A, (X)                                cycles=1
0x8cfd:  fa             or A, (X)                                cycles=1
0x8cfe:  fa             or A, (X)                                cycles=1
0x8cff:  fa             or A, (X)                                cycles=1
0x8d00:  fa             or A, (X)                                cycles=1
0x8d01:  fa             or A, (X)                                cycles=1
0x8d02:  fa             or A, (X)                                cycles=1
0x8d03:  fa             or A, (X)                                cycles=1
0x8d04:  fa             or A, (X)                                cycles=1
0x8d05:  fa             or A, (X)                                cycles=1
0x8d06:  fa             or A, (X)                                cycles=1
0x8d07:  fa             or A, (X)                                cycles=1
0x8d08:  fa             or A, (X)                                cycles=1
0x8d09:  fa             or A, (X)                                cycles=1
0x8d0a:  fa             or A, (X)                                cycles=1
0x8d0b:  fa             or A, (X)                                cycles=1
0x8d0c:  fa             or A, (X)                                cycles=1
0x8d0d:  fa             or A, (X)                                cycles=1
0x8d0e:  fa             or A, (X)                                cycles=1
0x8d0f:  fa             or A, (X)                                cycles=1
0x8d10:  fa             or A, (X)                                cycles=1
0x8d11:  fa             or A, (X)                                cycles=1
0x8d12:  fa             or A, (X)                                cycles=1
0x8d13:  fa             or A, (X)                                cycles=1
0x8d14:  fa             or A, (X)                                cycles=1
0x8d15:  fa             or A, (X)                                cycles=1
0x8d16:  fa             or A, (X)                                cycles=1
0x8d17:  fa             or A, (X)                                cycles=1
0x8d18:  fa             or A, (X)                                cycles=1
0x8d19:  fa             or A, (X)                                cycles=1
0x8d1a:  fa             or A, (X)                                cycles=1
0x8d1b:  fa             or A, (X)                                cycles=1
0x8d1c:  fa             or A, (X)                                cycles=1
0x8d1d:  fa             or A, (X)                                cycles=1
0x8d1e:  fa             or A, (X)                                cycles=1
0x8d1f:  fa             or A, (X)                                cycles=1
0x8d20:  fa             or A, (X)                                cycles=1
0x8d21:  fa             or A, (X)                                cycles=1
0x8d22:  fa             or A, (X)                                cycles=1
0x8d23:  fa             or A, (X)                                cycles=1
0x8d24:  fa             or A, (X)                                cycles=1
0x8d25:  fa             or A, (X)                                cycles=1
0x8d26:  fa             or A, (X)                                cycles=1
0x8d27:  fa             or A, (X)                                cycles=1
0x8d28:  fa             or A, (X)                                cycles=1
0x8d29:  fa             or A, (X)                                cycles=1
0x8d2a:  fa             or A, (X)                                cycles=1
0x8d2b:  fa             or A, (X)                                cycles=1
0x8d2c:  fa             or A, (X)                                cycles=1
0x8d2d:  fa             or A, (X)                                cycles=1
0x8d2e:  fa             or A, (X)                                cycles=1
0x8d2f:  fa             or A, (X)                                cycles=1
0x8d30:  fa             or A, (X)                                cycles=1
0x8d31:  fa             or A, (X)                                cycles=1
0x8d32:  fa             or A, (X)                                cycles=1
0x8d33:  fa             or A, (X)                                cycles=1
0x8d34:  fa             or A, (X)                                cycles=1
0x8d35:  fa             or A, (X)                                cycles=1
0x8d36:  fa             or A, (X)                                cycles=1
0x8d37:  fa             or A, (X)                                cycles=1
0x8d38:  fa             or A, (X)                                cycles=1
0x8d39:  fa             or A, (X)                                cycles=1
0x8d3a:  fa             or A, (X)                                cycles=1
0x8d3b:  fa             or A, (X)                                cycles=1
0x8d3c:  fa             or A, (X)                                cycles=1
0x8d3d:  fa             or A, (X)                                cycles=1
0x8d3e:  fa             or A, (X)                                cycles=1
0x8d3f:  fa             or A, (X)                                cycles=1
0x8d40:  fa             or A, (X)                                cycles=1
0x8d41:  fa             or A, (X)                                cycles=1
0x8d42:  fa             or A, (X)                                cycles=1
0x8d43:  fa             or A, (X)                                cycles=1
0x8d44:  fa             or A, (X)                                cycles=1
0x8d45:  fa             or A, (X)                                cycles=1
0x8d46:  fa             or A, (X)                                cycles=1
0x8d47:  fa             or A, (X)                                cycles=1
0x8d48:  fa             or A, (X)                                cycles=1
0x8d49:  fa             or A, (X)                                cycles=1
0x8d4a:  fa             or A, (X)                                cycles=1
0x8d4b:  fa             or A, (X)                                cycles=1
0x8d4c:  fa             or A, (X)                                cycles=1
0x8d4d:  fa             or A, (X)                                cycles=1
0x8d4e:  fa             or A, (X)                                cycles=1
0x8d4f:  fa             or A, (X)                                cycles=1
0x8d50:  fa             or A, (X)                                cycles=1
0x8d51:  fa             or A, (X)                                cycles=1
0x8d52:  fa             or A, (X)                                cycles=1
0x8d53:  fa             or A, (X)                                cycles=1
0x8d54:  fa             or A, (X)                                cycles=1
0x8d55:  fa             or A, (X)                                cycles=1
0x8d56:  fa             or A, (X)                                cycles=1
0x8d57:  fa             or A, (X)                                cycles=1
0x8d58:  fa             or A, (X)                                cycles=1
0x8d59:  fa             or A, (X)                                cycles=1
0x8d5a:  fa             or A, (X)                                cycles=1
0x8d5b:  fa             or A, (X)                                cycles=1
0x8d5c:  fa             or A, (X)                                cycles=1
0x8d5d:  fa             or A, (X)                                cycles=1
0x8d5e:  fa             or A, (X)                                cycles=1
0x8d5f:  fa             or A, (X)                                cycles=1
0x8d60:  fa             or A, (X)                                cycles=1
0x8d61:  fa             or A, (X)                                cycles=1
0x8d62:  fa             or A, (X)                                cycles=1
0x8d63:  fa             or A, (X)                                cycles=1
0x8d64:  fa             or A, (X)                                cycles=1
0x8d65:  fa             or A, (X)                                cycles=1
0x8d66:  fa             or A, (X)                                cycles=1
0x8d67:  fa             or A, (X)                                cycles=1
0x8d68:  fa             or A, (X)                                cycles=1
0x8d69:  fa             or A, (X)                                cycles=1
0x8d6a:  fa             or A, (X)                                cycles=1
0x8d6b:  fa             or A, (X)                                cycles=1
0x8d6c:  fa             or A, (X)                                cycles=1
0x8d6d:  fa             or A, (X)                                cycles=1
0x8d6e:  fa             or A, (X)                                cycles=1
0x8d6f:  fa             or A, (X)                                cycles=1
0x8d70:  fa             or A, (X)                                cycles=1
0x8d71:  fa             or A, (X)                                cycles=1
0x8d72:  fa             or A, (X)                                cycles=1
0x8d73:  fa             or A, (X)                                cycles=1
0x8d74:  fa             or A, (X)                                cycles=1
0x8d75:  fa             or A, (X)                                cycles=1
0x8d76:  fa             or A, (X)                                cycles=1
0x8d77:  fa             or A, (X)                                cycles=1
0x8d78:  fa             or A, (X)                                cycles=1
0x8d79:  fa             or A, (X)                                cycles=1
0x8d7a:  fa             or A, (X)                                cycles=1
0x8d7b:  fa             or A, (X)                                cycles=1
0x8d7c:  fa             or A, (X)                                cycles=1
0x8d7d:  fa             or A, (X)                                cycles=1
0x8d7e:  fa             or A, (X)                                cycles=1
0x8d7f:  fa             or A, (X)                                cycles=1
0x8d80:  fa             or A, (X)                                cycles=1
0x8d81:  fa             or A, (X)                                cycles=1
0x8d82:  fa             or A, (X)                                cycles=1
0x8d83:  fa             or A, (X)                                cycles=1
0x8d84:  fa             or A, (X)                                cycles=1
0x8d85:  fa             or A, (X)                                cycles=1
0x8d86:  fa             or A, (X)                                cycles=1
0x8d87:  fa             or A, (X)                                cycles=1
0x8d88:  fa             or A, (X)                                cycles=1
0x8d89:  fa             or A, (X)                                cycles=1
0x8d8a:  fa             or A, (X)                                cycles=1
0x8d8b:  fa             or A, (X)                                cycles=1
0x8d8c:  fa             or A, (X)                                cycles=1
0x8d8d:  fa             or A, (X)                                cycles=1
0x8d8e:  fa             or A, (X)                                cycles=1
0x8d8f:  fa             or A, (X)                                cycles=1
0x8d90:  fa             or A, (X)                                cycles=1
0x8d91:  fa             or A, (X)                                cycles=1
0x8d92:  fa             or A, (X)                                cycles=1
0x8d93:  fa             or A, (X)                                cycles=1
0x8d94:  fa             or A, (X)                                cycles=1
0x8d95:  fa             or A, (X)                                cycles=1
0x8d96:  fa             or A, (X)                                cycles=1
0x8d97:  fa             or A, (X)                                cycles=1
0x8d98:  fa             or A, (X)                                cycles=1
0x8d99:  fa             or A, (X)                                cycles=1
0x8d9a:  fa             or A, (X)                                cycles=1
0x8d9b:  fa             or A, (X)                                cycles=1
0x8d9c:  fa             or A, (X)                                cycles=1
0x8d9d:  fa             or A, (X)                                cycles=1
0x8d9e:  fa             or A, (X)                                cycles=1
0x8d9f:  fa             or A, (X)                                cycles=1
0x8da0:  fa             or A, (X)                                cycles=1
0x8da1:  fa             or A, (X)                                cycles=1
0x8da2:  fa             or A, (X)                                cycles=1
0x8da3:  fa             or A, (X)                                cycles=1
0x8da4:  fa             or A, (X)                                cycles=1
0x8da5:  fa             or A, (X)                                cycles=1
0x8da6:  fa             or A, (X)                                cycles=1
0x8da7:  fa             or A, (X)                                cycles=1
0x8da8:  fa             or A, (X)                                cycles=1
0x8da9:  fa             or A, (X)                                cycles=1
0x8daa:  fa             or A, (X)                                cycles=1
0x8dab:  fa             or A, (X)                                cycles=1
0x8dac:  fa             or A, (X)                                cycles=1
0x8dad:  fa             or A, (X)                                cycles=1
0x8dae:  fa             or A, (X)                                cycles=1
0x8daf:  fa             or A, (X)                                cycles=1
0x8db0:  fa             or A, (X)                                cycles=1
0x8db1:  fa             or A, (X)                                cycles=1
0x8db2:  fa             or A, (X)                                cycles=1
0x8db3:  fa             or A, (X)                                cycles=1
0x8db4:  fa             or A, (X)                                cycles=1
0x8db5:  fa             or A, (X)                                cycles=1
0x8db6:  fa             or A, (X)                                cycles=1
0x8db7:  fa             or A, (X)                                cycles=1
0x8db8:  fa             or A, (X)                                cycles=1
0x8db9:  fa             or A, (X)                                cycles=1
0x8dba:  fa             or A, (X)                                cycles=1
0x8dbb:  fa             or A, (X)                                cycles=1
0x8dbc:  fa             or A, (X)                                cycles=1
0x8dbd:  fa             or A, (X)                                cycles=1
0x8dbe:  fa             or A, (X)                                cycles=1
0x8dbf:  fa             or A, (X)                                cycles=1
0x8dc0:  fa             or A, (X)                                cycles=1
0x8dc1:  fa             or A, (X)                                cycles=1
0x8dc2:  fa             or A, (X)                                cycles=1
0x8dc3:  fa             or A, (X)                                cycles=1
0x8dc4:  fa             or A, (X)                                cycles=1
0x8dc5:  fa             or A, (X)                                cycles=1
0x8dc6:  fa             or A, (X)                                cycles=1
0x8dc7:  fa             or A, (X)                                cycles=1
0x8dc8:  fa             or A, (X)                                cycles=1
0x8dc9:  fa             or A, (X)                                cycles=1
0x8dca:  fa             or A, (X)                                cycles=1
0x8dcb:  fa             or A, (X)                                cycles=1
0x8dcc:  fa             or A, (X)                                cycles=1
0x8dcd:  fa             or A, (X)                                cycles=1
0x8dce:  fa             or A, (X)                                cycles=1
0x8dcf:  fa             or A, (X)                                cycles=1
0x8dd0:  fa             or A, (X)                                cycles=1
0x8dd1:  fa             or A, (X)                                cycles=1
0x8dd2:  fa             or A, (X)                                cycles=1
0x8dd3:  fa             or A, (X)                                cycles=1
0x8dd4:  fa             or A, (X)                                cycles=1
0x8dd5:  fa             or A, (X)                                cycles=1
0x8dd6:  fa             or A, (X)                                cycles=1
0x8dd7:  fa             or A, (X)                                cycles=1
0x8dd8:  fa             or A, (X)                                cycles=1
0x8dd9:  fa             or A, (X)                                cycles=1
0x8dda:  fa             or A, (X)                                cycles=1
0x8ddb:  fa             or A, (X)                                cycles=1
0x8ddc:  fa             or A, (X)                                cycles=1
0x8ddd:  fa             or A, (X)                                cycles=1
0x8dde:  fa             or A, (X)                                cycles=1
0x8ddf:  fa             or A, (X)                                cycles=1
0x8de0:  fa             or A, (X)                                cycles=1
0x8de1:  fa             or A, (X)                                cycles=1
0x8de2:  fa             or A, (X)                                cycles=1
0x8de3:  fa             or A, (X)                                cycles=1
0x8de4:  fa             or A, (X)                                cycles=1
0x8de5:  fa             or A, (X)                                cycles=1
0x8de6:  fa             or A, (X)                                cycles=1
0x8de7:  fa             or A, (X)                                cycles=1
0x8de8:  fa             or A, (X)                                cycles=1
0x8de9:  fa             or A, (X)                                cycles=1
0x8dea:  fa             or A, (X)                                cycles=1
0x8deb:  fa             or A, (X)                                cycles=1
0x8dec:  fa             or A, (X)                                cycles=1
0x8ded:  fa             or A, (X)                                cycles=1
0x8dee:  fa             or A, (X)                                cycles=1
0x8def:  fa             or A, (X)                                cycles=1
0x8df0:  fa             or A, (X)                                cycles=1
0x8df1:  fa             or A, (X)                                cycles=1
0x8df2:  fa             or A, (X)                                cycles=1
0x8df3:  fa             or A, (X)                                cycles=1
0x8df4:  fa             or A, (X)                                cycles=1
0x8df5:  fa             or A, (X)                                cycles=1
0x8df6:  fa             or A, (X)                                cycles=1
0x8df7:  fa             or A, (X)                                cycles=1
0x8df8:  fa             or A, (X)                                cycles=1
0x8df9:  fa             or A, (X)                                cycles=1
0x8dfa:  fa             or A, (X)                                cycles=1
0x8dfb:  fa             or A, (X)                                cycles=1
0x8dfc:  fa             or A, (X)                                cycles=1
0x8dfd:  fa             or A, (X)                                cycles=1
0x8dfe:  fa             or A, (X)                                cycles=1
0x8dff:  fa             or A, (X)                                cycles=1
0x8e00:  fa             or A, (X)                                cycles=1
0x8e01:  fa             or A, (X)                                cycles=1
0x8e02:  fa             or A, (X)                                cycles=1
0x8e03:  f9             adc A, (X)                               cycles=1
0x8e04:  f7             ld (X),A                                 cycles=1
0x8e05:  f6             ld A, (X)                                cycles=1
0x8e06:  f5             bcp A, (X)                               cycles=1
0x8e07:  f4             and A, (X)                               cycles=1
0x8e08:  f3             cpw Y, (X)                               cycles=2
0x8e09:  f1             cp A, (X)                                cycles=1
0x8e0a:  f0             sub A, (X)                               cycles=1
0x8e0b:  ef ee          ldw ($ee,X),Y                            cycles=2
0x8e0d:  ed eb          call ($eb,X)                             cycles=4
0x8e0f:  ea e9          or A, ($e9,X)                            cycles=1
0x8e11:  e8 e7          xor A, ($e7,X)                           cycles=1
0x8e13:  e5 e4          bcp A, ($e4,X)                           cycles=1
0x8e15:  e3 e2          cpw Y, ($e2,X)                           cycles=2
0x8e17:  e1 e0          cp A, ($e0,X)                            cycles=1
0x8e19:  df dd dc       ldw ($dddc,X),Y                          cycles=2
0x8e1c:  db da d9       add A, ($dad9,X)                         cycles=1
0x8e1f:  d8 d7 d6       xor A, ($d7d6,X)                         cycles=1
0x8e22:  d4 d3 d2       and A, ($d3d2,X)                         cycles=1
0x8e25:  d1 d0 cf       cp A, ($d0cf,X)                          cycles=1
0x8e28:  ce cd cc       ldw X, $cdcc                             cycles=2
0x8e2b:  cb ca c9       add A, $cac9                             cycles=1
0x8e2e:  c8 c6 c5       xor A, $c6c5                             cycles=1
0x8e31:  c4 c3 c2       and A, $c3c2                             cycles=1
0x8e34:  c1 c0 bf       cp A, $c0bf                              cycles=1
0x8e37:  be bd          ldw X, $bd                               cycles=2
0x8e39:  bc bb ba b9    ldf A, $bbbab9                           cycles=1
0x8e3d:  b8 b7          xor A, $b7                               cycles=1
0x8e3f:  b6 b5          ld A, $b5                                cycles=1
0x8e41:  b4 b3          and A, $b3                               cycles=1
0x8e43:  b2 b1          sbc A, $b1                               cycles=1
0x8e45:  b0 af          sub A, $af                               cycles=1
0x8e47:  ae ad ac       ldw X, #$adac                            cycles=2
0x8e4a:  ab aa          add A, #$aa                              cycles=1
0x8e4c:  a9 a8          adc A, #$a8                              cycles=1
0x8e4e:  a7 a6 a5 a4    ldf ($a6a5a4,X),A                        cycles=1
0x8e52:  a3 a2 a1       cpw X, #$a2a1                            cycles=2
0x8e55:  a0 9f          sub A, #$9f                              cycles=1
0x8e57:  9e             ld A, XH                                 cycles=1
0x8e58:  9d             nop                                      cycles=1
0x8e59:  9c             rvf                                      cycles=5
0x8e5a:  9b             sim                                      cycles=1
0x8e5b:  9a             rim                                      cycles=1
0x8e5c:  9a             rim                                      cycles=1
0x8e5d:  99             scf                                      cycles=1
0x8e5e:  98             rcf                                      cycles=1
0x8e5f:  97             ld XL, A                                 cycles=1
0x8e60:  96             ldw X, SP                                cycles=1
0x8e61:  95             ld XH, A                                 cycles=1
0x8e62:  94             ldw SP, X                                cycles=1
0x8e63:  93             ldw X, Y                                 cycles=1
0x8e64:  92 91          ???                                      cycles=?
0x8e66:  90 8f          ???                                      cycles=?
0x8e68:  8e             halt                                     cycles=1
0x8e69:  8d 8c 8b 8a    callf $8c8b8a                            cycles=5
0x8e6d:  89             pushw X                                  cycles=2
0x8e6e:  88             push A                                   cycles=1
0x8e6f:  87             retf                                     cycles=5
0x8e70:  86             pop CC                                   cycles=1
0x8e71:  86             pop CC                                   cycles=1
0x8e72:  85             popw X                                   cycles=2
0x8e73:  84             pop A                                    cycles=1
0x8e74:  83             trap                                     cycles=9
0x8e75:  82 81 80 7f    int $81807f                              cycles=2
0x8e79:  7e             swap (X)                                 cycles=1
0x8e7a:  7d             tnz (X)                                  cycles=1
0x8e7b:  7c             inc (X)                                  cycles=1
0x8e7c:  7b 7a          ld A, ($7a,SP)                           cycles=1
0x8e7e:  79             rlc (X)                                  cycles=1
0x8e7f:  78             sll (X)                                  cycles=1
0x8e80:  77             sra (X)                                  cycles=1
0x8e81:  76             rrc (X)                                  cycles=1
0x8e82:  75             ???                                      cycles=?
0x8e83:  74             srl (X)                                  cycles=1
0x8e84:  74             srl (X)                                  cycles=1
0x8e85:  73             cpl (X)                                  cycles=1
0x8e86:  72 71          ???                                      cycles=?
0x8e88:  70             neg (X)                                  cycles=1
0x8e89:  6f 6e          clr ($6e,X)                              cycles=1
0x8e8b:  6d 6c          tnz ($6c,X)                              cycles=1
0x8e8d:  6b 6a          ld ($6a,SP),A                            cycles=1
0x8e8f:  69 68          rlc ($68,X)                              cycles=1
0x8e91:  67 66          sra ($66,X)                              cycles=1
0x8e93:  65             divw X, Y                                cycles=2-17
0x8e94:  64 63          srl ($63,X)                              cycles=1
0x8e96:  62             div X, A                                 cycles=2-17
0x8e97:  61             exg A, YL                                cycles=1
0x8e98:  60 60          neg ($60,X)                              cycles=1
0x8e9a:  5f             clrw X                                   cycles=1
0x8e9b:  5e             swapw X                                  cycles=1
0x8e9c:  5d             tnzw X                                   cycles=2
0x8e9d:  5c             incw X                                   cycles=1
0x8e9e:  5b 5a          addw SP, #$5a                            cycles=2
0x8ea0:  59             rlcw X                                   cycles=2
0x8ea1:  58             sllw X                                   cycles=2
0x8ea2:  57             sraw X                                   cycles=2
0x8ea3:  56             rrcw X                                   cycles=2
0x8ea4:  55 54 53 52 51 mov $5251, $5453                         cycles=1
0x8ea9:  50             negw X                                   cycles=1
0x8eaa:  4f             clr A                                    cycles=1
0x8eab:  4e             swap A                                   cycles=1
0x8eac:  4d             tnz A                                    cycles=1
0x8ead:  4c             inc A                                    cycles=1
0x8eae:  4b 4a          push #$4a                                cycles=1
0x8eb0:  49             rlc A                                    cycles=1
0x8eb1:  48             sll A                                    cycles=1
0x8eb2:  47             sra A                                    cycles=1
0x8eb3:  46             rrc A                                    cycles=1
0x8eb4:  45 44 43       mov $43, $44                             cycles=1
0x8eb7:  42             mul X, A                                 cycles=4
0x8eb8:  41             exg A, XL                                cycles=1
0x8eb9:  40             neg A                                    cycles=1
0x8eba:  3f 3e          clr $3e                                  cycles=1
0x8ebc:  3d 3c          tnz $3c                                  cycles=1
0x8ebe:  3b 3a 39       push $3a39                               cycles=1
0x8ec1:  38 37          sll $37                                  cycles=1
0x8ec3:  36 35          rrc $35                                  cycles=1
0x8ec5:  34 32          srl $32                                  cycles=1
0x8ec7:  31 30 2f       exg A, $302f                             cycles=3
0x8eca:  2e 2d          jrsge $8ef9  (offset=45)                 cycles=1-2
0x8ecc:  2c 2b          jrsgt $8ef9  (offset=43)                 cycles=1-2
0x8ece:  2a 29          jrpl $8ef9  (offset=41)                  cycles=1-2
0x8ed0:  28 27          jrnv $8ef9  (offset=39)                  cycles=1-2
0x8ed2:  26 24          jrne $8ef8  (offset=36)                  cycles=1-2
0x8ed4:  23 22          jrule $8ef8  (offset=34)                 cycles=1-2
0x8ed6:  21 20          jrf $8ef8  (offset=32)                   cycles=1-2
0x8ed8:  1f 1e          ldw ($1e,SP),X                           cycles=2
0x8eda:  1d 1b 1a       subw X, #$1b1a                           cycles=2
0x8edd:  19 18          adc A, ($18,SP)                          cycles=1
0x8edf:  17 16          ldw ($16,SP),Y                           cycles=2
0x8ee1:  15 13          bcp A, ($13,SP)                          cycles=1
0x8ee3:  12 11          sbc A, ($11,SP)                          cycles=1
0x8ee5:  10 0f          sub A, ($0f,SP)                          cycles=1
0x8ee7:  0d 0c          tnz ($0c,SP)                             cycles=1
0x8ee9:  0b             ???                                      cycles=?
0x8eea:  0a 09          dec ($09,SP)                             cycles=1
0x8eec:  07 06          sra ($06,SP)                             cycles=1
0x8eee:  05             ???                                      cycles=?
0x8eef:  04 03          srl ($03,SP)                             cycles=1
0x8ef1:  01             rrwa X, A                                cycles=1
0x8ef2:  00 00          neg ($00,SP)                             cycles=1
0x8ef4:  00 00          neg ($00,SP)                             cycles=1
0x8ef6:  00 00          neg ($00,SP)                             cycles=1
0x8ef8:  00 00          neg ($00,SP)                             cycles=1
0x8efa:  0b             ???                                      cycles=?
0x8efb:  0c 0d          inc ($0d,SP)                             cycles=1
0x8efd:  0e 0f          swap ($0f,SP)                            cycles=1
0x8eff:  10 11          sub A, ($11,SP)                          cycles=1
0x8f01:  12 12          sbc A, ($12,SP)                          cycles=1
0x8f03:  13 14          cpw X, ($14,SP)                          cycles=2
0x8f05:  15 16          bcp A, ($16,SP)                          cycles=1
0x8f07:  17 18          ldw ($18,SP),Y                           cycles=2
0x8f09:  19 19          adc A, ($19,SP)                          cycles=1
0x8f0b:  1a 1b          or A, ($1b,SP)                           cycles=1
0x8f0d:  1c 1d 1e       addw X, #$1d1e                           cycles=2
0x8f10:  1f 20          ldw ($20,SP),X                           cycles=2
0x8f12:  21 22          jrf $8f36  (offset=34)                   cycles=1-2
0x8f14:  22 23          jrugt $8f39  (offset=35)                 cycles=1-2
0x8f16:  24 25          jrnc $8f3d  (offset=37)                  cycles=1-2
0x8f18:  26 27          jrne $8f41  (offset=39)                  cycles=1-2
0x8f1a:  28 29          jrnv $8f45  (offset=41)                  cycles=1-2
0x8f1c:  2a 2b          jrpl $8f49  (offset=43)                  cycles=1-2
0x8f1e:  2c 2d          jrsgt $8f4d  (offset=45)                 cycles=1-2
0x8f20:  2e 2e          jrsge $8f50  (offset=46)                 cycles=1-2
0x8f22:  2f 30          jrslt $8f54  (offset=48)                 cycles=1-2
0x8f24:  31 32 33       exg A, $3233                             cycles=3
0x8f27:  34 35          srl $35                                  cycles=1
0x8f29:  36 37          rrc $37                                  cycles=1
0x8f2b:  38 39          sll $39                                  cycles=1
0x8f2d:  3a 3b          dec $3b                                  cycles=1
0x8f2f:  3c 3d          inc $3d                                  cycles=1
0x8f31:  3e 3f          swap $3f                                 cycles=1
0x8f33:  40             neg A                                    cycles=1
0x8f34:  41             exg A, XL                                cycles=1
0x8f35:  42             mul X, A                                 cycles=4
0x8f36:  43             cpl A                                    cycles=1
0x8f37:  44             srl A                                    cycles=1
0x8f38:  45 46 47       mov $47, $46                             cycles=1
0x8f3b:  48             sll A                                    cycles=1
0x8f3c:  49             rlc A                                    cycles=1
0x8f3d:  4a             dec A                                    cycles=1
0x8f3e:  4b 4c          push #$4c                                cycles=1
0x8f40:  4d             tnz A                                    cycles=1
0x8f41:  4e             swap A                                   cycles=1
0x8f42:  4f             clr A                                    cycles=1
0x8f43:  50             negw X                                   cycles=1
0x8f44:  51             exgw X, Y                                cycles=1
0x8f45:  52 53          sub SP, #$53                             cycles=1
0x8f47:  54             srlw X                                   cycles=2
0x8f48:  55 56 57 58 59 mov $5859, $5657                         cycles=1
0x8f4d:  5a             decw X                                   cycles=1
0x8f4e:  5b 5c          addw SP, #$5c                            cycles=2
0x8f50:  5d             tnzw X                                   cycles=2
0x8f51:  5e             swapw X                                  cycles=1
0x8f52:  5f             clrw X                                   cycles=1
0x8f53:  60 60          neg ($60,X)                              cycles=1
0x8f55:  61             exg A, YL                                cycles=1
0x8f56:  62             div X, A                                 cycles=2-17
0x8f57:  63 64          cpl ($64,X)                              cycles=1
0x8f59:  65             divw X, Y                                cycles=2-17
0x8f5a:  66 67          rrc ($67,X)                              cycles=1
0x8f5c:  68 69          sll ($69,X)                              cycles=1
0x8f5e:  6a 6b          dec ($6b,X)                              cycles=1
0x8f60:  6c 6d          inc ($6d,X)                              cycles=1
0x8f62:  6e 6f          swap ($6f,X)                             cycles=1
0x8f64:  70             neg (X)                                  cycles=1
0x8f65:  71             ???                                      cycles=?
0x8f66:  72 73          ???                                      cycles=?
0x8f68:  74             srl (X)                                  cycles=1
0x8f69:  74             srl (X)                                  cycles=1
0x8f6a:  75             ???                                      cycles=?
0x8f6b:  76             rrc (X)                                  cycles=1
0x8f6c:  77             sra (X)                                  cycles=1
0x8f6d:  78             sll (X)                                  cycles=1
0x8f6e:  79             rlc (X)                                  cycles=1
0x8f6f:  7a             dec (X)                                  cycles=1
0x8f70:  7b 7c          ld A, ($7c,SP)                           cycles=1
0x8f72:  7d             tnz (X)                                  cycles=1
0x8f73:  7e             swap (X)                                 cycles=1
0x8f74:  7f             clr (X)                                  cycles=1
0x8f75:  80             iret                                     cycles=11
0x8f76:  81             ret                                      cycles=4
0x8f77:  82 83 84 85    int $838485                              cycles=2
0x8f7b:  86             pop CC                                   cycles=1
0x8f7c:  86             pop CC                                   cycles=1
0x8f7d:  87             retf                                     cycles=5
0x8f7e:  88             push A                                   cycles=1
0x8f7f:  89             pushw X                                  cycles=2
0x8f80:  8a             push CC                                  cycles=1
0x8f81:  8b             break                                    cycles=1
0x8f82:  8c             ccf                                      cycles=1
0x8f83:  8d 8e 8f 90    callf $8e8f90                            cycles=5
0x8f87:  91 92          ???                                      cycles=?
0x8f89:  93             ldw X, Y                                 cycles=1
0x8f8a:  94             ldw SP, X                                cycles=1
0x8f8b:  95             ld XH, A                                 cycles=1
0x8f8c:  96             ldw X, SP                                cycles=1
0x8f8d:  97             ld XL, A                                 cycles=1
0x8f8e:  98             rcf                                      cycles=1
0x8f8f:  99             scf                                      cycles=1
0x8f90:  9a             rim                                      cycles=1
0x8f91:  9a             rim                                      cycles=1
0x8f92:  9b             sim                                      cycles=1
0x8f93:  9c             rvf                                      cycles=5
0x8f94:  9d             nop                                      cycles=1
0x8f95:  9e             ld A, XH                                 cycles=1
0x8f96:  9f             ld A, XL                                 cycles=1
0x8f97:  a0 a1          sub A, #$a1                              cycles=1
0x8f99:  a2 a3          sbc A, #$a3                              cycles=1
0x8f9b:  a4 a5          and A, #$a5                              cycles=1
0x8f9d:  a6 a7          ld A, #$a7                               cycles=1
0x8f9f:  a8 a9          xor A, #$a9                              cycles=1
0x8fa1:  aa ab          or A, #$ab                               cycles=1
0x8fa3:  ac ad ae af    jpf $adaeaf                              cycles=2
0x8fa7:  b0 b1          sub A, $b1                               cycles=1
0x8fa9:  b2 b3          sbc A, $b3                               cycles=1
0x8fab:  b4 b5          and A, $b5                               cycles=1
0x8fad:  b6 b7          ld A, $b7                                cycles=1
0x8faf:  b8 b9          xor A, $b9                               cycles=1
0x8fb1:  ba bb          or A, $bb                                cycles=1
0x8fb3:  bc bd be bf    ldf A, $bdbebf                           cycles=1
0x8fb7:  c0 c1 c2       sub A, $c1c2                             cycles=1
0x8fba:  c3 c4 c5       cpw X, $c4c5                             cycles=2
0x8fbd:  c6 c7 c8       ld A, $c7c8                              cycles=1
0x8fc0:  c9 ca cb       adc A, $cacb                             cycles=1
0x8fc3:  cc cc cd       jp $cccd                                 cycles=1
0x8fc6:  ce cf d0       ldw X, $cfd0                             cycles=2
0x8fc9:  d1 d2 d3       cp A, ($d2d3,X)                          cycles=1
0x8fcc:  d4 d5 d6       and A, ($d5d6,X)                         cycles=1
0x8fcf:  d7 d8 d8       ld ($d8d8,X),A                           cycles=1
0x8fd2:  d9 da db       adc A, ($dadb,X)                         cycles=1
0x8fd5:  dc dd de       jp ($ddde,X)                             cycles=1
0x8fd8:  df e0 e1       ldw ($e0e1,X),Y                          cycles=2
0x8fdb:  e1 e2          cp A, ($e2,X)                            cycles=1
0x8fdd:  e3 e4          cpw Y, ($e4,X)                           cycles=2
0x8fdf:  e5 e6          bcp A, ($e6,X)                           cycles=1
0x8fe1:  e7 e8          ld ($e8,X),A                             cycles=1
0x8fe3:  e8 e9          xor A, ($e9,X)                           cycles=1
0x8fe5:  ea eb          or A, ($eb,X)                            cycles=1
0x8fe7:  ec ed          jp ($ed,X)                               cycles=1
0x8fe9:  ee ef          ldw X, ($ef,X)                           cycles=2
0x8feb:  ef ef          ldw ($ef,X),Y                            cycles=2
0x8fed:  ef f0          ldw ($f0,X),Y                            cycles=2
0x8fef:  f0             sub A, (X)                               cycles=1
0x8ff0:  f0             sub A, (X)                               cycles=1
0x8ff1:  f0             sub A, (X)                               cycles=1
0x8ff2:  f1             cp A, (X)                                cycles=1
0x8ff3:  f1             cp A, (X)                                cycles=1
0x8ff4:  f1             cp A, (X)                                cycles=1
0x8ff5:  f2             sbc A, (X)                               cycles=1
0x8ff6:  f2             sbc A, (X)                               cycles=1
0x8ff7:  f2             sbc A, (X)                               cycles=1
0x8ff8:  f2             sbc A, (X)                               cycles=1
0x8ff9:  f3             cpw Y, (X)                               cycles=2
0x8ffa:  f3             cpw Y, (X)                               cycles=2
0x8ffb:  f3             cpw Y, (X)                               cycles=2
0x8ffc:  f3             cpw Y, (X)                               cycles=2
0x8ffd:  f4             and A, (X)                               cycles=1
0x8ffe:  f4             and A, (X)                               cycles=1
0x8fff:  f4             and A, (X)                               cycles=1
0x9000:  f4             and A, (X)                               cycles=1
0x9001:  f4             and A, (X)                               cycles=1
0x9002:  f5             bcp A, (X)                               cycles=1
0x9003:  f5             bcp A, (X)                               cycles=1
0x9004:  f5             bcp A, (X)                               cycles=1
0x9005:  f5             bcp A, (X)                               cycles=1
0x9006:  f6             ld A, (X)                                cycles=1
0x9007:  f6             ld A, (X)                                cycles=1
0x9008:  f6             ld A, (X)                                cycles=1
0x9009:  f6             ld A, (X)                                cycles=1
0x900a:  f7             ld (X),A                                 cycles=1
0x900b:  f7             ld (X),A                                 cycles=1
0x900c:  f7             ld (X),A                                 cycles=1
0x900d:  f7             ld (X),A                                 cycles=1
0x900e:  f7             ld (X),A                                 cycles=1
0x900f:  f8             xor A, (X)                               cycles=1
0x9010:  f8             xor A, (X)                               cycles=1
0x9011:  f8             xor A, (X)                               cycles=1
0x9012:  f8             xor A, (X)                               cycles=1
0x9013:  f8             xor A, (X)                               cycles=1
0x9014:  f9             adc A, (X)                               cycles=1
0x9015:  f9             adc A, (X)                               cycles=1
0x9016:  f9             adc A, (X)                               cycles=1
0x9017:  f9             adc A, (X)                               cycles=1
0x9018:  f9             adc A, (X)                               cycles=1
0x9019:  f9             adc A, (X)                               cycles=1
0x901a:  fa             or A, (X)                                cycles=1
0x901b:  fa             or A, (X)                                cycles=1
0x901c:  fa             or A, (X)                                cycles=1
0x901d:  fa             or A, (X)                                cycles=1
0x901e:  fa             or A, (X)                                cycles=1
0x901f:  fa             or A, (X)                                cycles=1
0x9020:  fa             or A, (X)                                cycles=1
0x9021:  fa             or A, (X)                                cycles=1
0x9022:  fa             or A, (X)                                cycles=1
0x9023:  fa             or A, (X)                                cycles=1
0x9024:  fa             or A, (X)                                cycles=1
0x9025:  fa             or A, (X)                                cycles=1
0x9026:  fa             or A, (X)                                cycles=1
0x9027:  fa             or A, (X)                                cycles=1
0x9028:  fa             or A, (X)                                cycles=1
0x9029:  fa             or A, (X)                                cycles=1
0x902a:  fa             or A, (X)                                cycles=1
0x902b:  fa             or A, (X)                                cycles=1
0x902c:  fa             or A, (X)                                cycles=1
0x902d:  fa             or A, (X)                                cycles=1
0x902e:  fa             or A, (X)                                cycles=1
0x902f:  fa             or A, (X)                                cycles=1
0x9030:  fa             or A, (X)                                cycles=1
0x9031:  fa             or A, (X)                                cycles=1
0x9032:  fa             or A, (X)                                cycles=1
0x9033:  fa             or A, (X)                                cycles=1
0x9034:  fa             or A, (X)                                cycles=1
0x9035:  fa             or A, (X)                                cycles=1
0x9036:  fa             or A, (X)                                cycles=1
0x9037:  fa             or A, (X)                                cycles=1
0x9038:  fa             or A, (X)                                cycles=1
0x9039:  fa             or A, (X)                                cycles=1
0x903a:  fa             or A, (X)                                cycles=1
0x903b:  fa             or A, (X)                                cycles=1
0x903c:  fa             or A, (X)                                cycles=1
0x903d:  fa             or A, (X)                                cycles=1
0x903e:  fa             or A, (X)                                cycles=1
0x903f:  fa             or A, (X)                                cycles=1
0x9040:  fa             or A, (X)                                cycles=1
0x9041:  fa             or A, (X)                                cycles=1
0x9042:  fa             or A, (X)                                cycles=1
0x9043:  fa             or A, (X)                                cycles=1
0x9044:  fa             or A, (X)                                cycles=1
0x9045:  fa             or A, (X)                                cycles=1
0x9046:  fa             or A, (X)                                cycles=1
0x9047:  fa             or A, (X)                                cycles=1
0x9048:  fa             or A, (X)                                cycles=1
0x9049:  fa             or A, (X)                                cycles=1
0x904a:  fa             or A, (X)                                cycles=1
0x904b:  fa             or A, (X)                                cycles=1
0x904c:  fa             or A, (X)                                cycles=1
0x904d:  fa             or A, (X)                                cycles=1
0x904e:  fa             or A, (X)                                cycles=1
0x904f:  fa             or A, (X)                                cycles=1
0x9050:  fa             or A, (X)                                cycles=1
0x9051:  fa             or A, (X)                                cycles=1
0x9052:  fa             or A, (X)                                cycles=1
0x9053:  fa             or A, (X)                                cycles=1
0x9054:  fa             or A, (X)                                cycles=1
0x9055:  fa             or A, (X)                                cycles=1
0x9056:  fa             or A, (X)                                cycles=1
0x9057:  fa             or A, (X)                                cycles=1
0x9058:  fa             or A, (X)                                cycles=1
0x9059:  fa             or A, (X)                                cycles=1
0x905a:  fa             or A, (X)                                cycles=1
0x905b:  fa             or A, (X)                                cycles=1
0x905c:  fa             or A, (X)                                cycles=1
0x905d:  fa             or A, (X)                                cycles=1
0x905e:  fa             or A, (X)                                cycles=1
0x905f:  fa             or A, (X)                                cycles=1
0x9060:  fa             or A, (X)                                cycles=1
0x9061:  fa             or A, (X)                                cycles=1
0x9062:  fa             or A, (X)                                cycles=1
0x9063:  fa             or A, (X)                                cycles=1
0x9064:  fa             or A, (X)                                cycles=1
0x9065:  fa             or A, (X)                                cycles=1
0x9066:  fa             or A, (X)                                cycles=1
0x9067:  fa             or A, (X)                                cycles=1
0x9068:  fa             or A, (X)                                cycles=1
0x9069:  fa             or A, (X)                                cycles=1
0x906a:  fa             or A, (X)                                cycles=1
0x906b:  fa             or A, (X)                                cycles=1
0x906c:  fa             or A, (X)                                cycles=1
0x906d:  fa             or A, (X)                                cycles=1
0x906e:  fa             or A, (X)                                cycles=1
0x906f:  fa             or A, (X)                                cycles=1
0x9070:  fa             or A, (X)                                cycles=1
0x9071:  fa             or A, (X)                                cycles=1
0x9072:  fa             or A, (X)                                cycles=1
0x9073:  fa             or A, (X)                                cycles=1
0x9074:  fa             or A, (X)                                cycles=1
0x9075:  fa             or A, (X)                                cycles=1
0x9076:  fa             or A, (X)                                cycles=1
0x9077:  fa             or A, (X)                                cycles=1
0x9078:  fa             or A, (X)                                cycles=1
0x9079:  fa             or A, (X)                                cycles=1
0x907a:  fa             or A, (X)                                cycles=1
0x907b:  fa             or A, (X)                                cycles=1
0x907c:  fa             or A, (X)                                cycles=1
0x907d:  fa             or A, (X)                                cycles=1
0x907e:  fa             or A, (X)                                cycles=1
0x907f:  fa             or A, (X)                                cycles=1
0x9080:  fa             or A, (X)                                cycles=1
0x9081:  fa             or A, (X)                                cycles=1
0x9082:  fa             or A, (X)                                cycles=1
0x9083:  fa             or A, (X)                                cycles=1
0x9084:  fa             or A, (X)                                cycles=1
0x9085:  fa             or A, (X)                                cycles=1
0x9086:  fa             or A, (X)                                cycles=1
0x9087:  fa             or A, (X)                                cycles=1
0x9088:  fa             or A, (X)                                cycles=1
0x9089:  fa             or A, (X)                                cycles=1
0x908a:  fa             or A, (X)                                cycles=1
0x908b:  fa             or A, (X)                                cycles=1
0x908c:  fa             or A, (X)                                cycles=1
0x908d:  fa             or A, (X)                                cycles=1
0x908e:  fa             or A, (X)                                cycles=1
0x908f:  fa             or A, (X)                                cycles=1
0x9090:  fa             or A, (X)                                cycles=1
0x9091:  fa             or A, (X)                                cycles=1
0x9092:  fa             or A, (X)                                cycles=1
0x9093:  fa             or A, (X)                                cycles=1
0x9094:  fa             or A, (X)                                cycles=1
0x9095:  fa             or A, (X)                                cycles=1
0x9096:  fa             or A, (X)                                cycles=1
0x9097:  fa             or A, (X)                                cycles=1
0x9098:  fa             or A, (X)                                cycles=1
0x9099:  fa             or A, (X)                                cycles=1
0x909a:  fa             or A, (X)                                cycles=1
0x909b:  fa             or A, (X)                                cycles=1
0x909c:  fa             or A, (X)                                cycles=1
0x909d:  fa             or A, (X)                                cycles=1
0x909e:  fa             or A, (X)                                cycles=1
0x909f:  fa             or A, (X)                                cycles=1
0x90a0:  fa             or A, (X)                                cycles=1
0x90a1:  fa             or A, (X)                                cycles=1
0x90a2:  fa             or A, (X)                                cycles=1
0x90a3:  fa             or A, (X)                                cycles=1
0x90a4:  fa             or A, (X)                                cycles=1
0x90a5:  fa             or A, (X)                                cycles=1
0x90a6:  fa             or A, (X)                                cycles=1
0x90a7:  fa             or A, (X)                                cycles=1
0x90a8:  fa             or A, (X)                                cycles=1
0x90a9:  fa             or A, (X)                                cycles=1
0x90aa:  fa             or A, (X)                                cycles=1
0x90ab:  f9             adc A, (X)                               cycles=1
0x90ac:  f9             adc A, (X)                               cycles=1
0x90ad:  f9             adc A, (X)                               cycles=1
0x90ae:  f9             adc A, (X)                               cycles=1
0x90af:  f9             adc A, (X)                               cycles=1
0x90b0:  f9             adc A, (X)                               cycles=1
0x90b1:  f8             xor A, (X)                               cycles=1
0x90b2:  f8             xor A, (X)                               cycles=1
0x90b3:  f8             xor A, (X)                               cycles=1
0x90b4:  f8             xor A, (X)                               cycles=1
0x90b5:  f8             xor A, (X)                               cycles=1
0x90b6:  f7             ld (X),A                                 cycles=1
0x90b7:  f7             ld (X),A                                 cycles=1
0x90b8:  f7             ld (X),A                                 cycles=1
0x90b9:  f7             ld (X),A                                 cycles=1
0x90ba:  f7             ld (X),A                                 cycles=1
0x90bb:  f6             ld A, (X)                                cycles=1
0x90bc:  f6             ld A, (X)                                cycles=1
0x90bd:  f6             ld A, (X)                                cycles=1
0x90be:  f6             ld A, (X)                                cycles=1
0x90bf:  f5             bcp A, (X)                               cycles=1
0x90c0:  f5             bcp A, (X)                               cycles=1
0x90c1:  f5             bcp A, (X)                               cycles=1
0x90c2:  f5             bcp A, (X)                               cycles=1
0x90c3:  f4             and A, (X)                               cycles=1
0x90c4:  f4             and A, (X)                               cycles=1
0x90c5:  f4             and A, (X)                               cycles=1
0x90c6:  f4             and A, (X)                               cycles=1
0x90c7:  f4             and A, (X)                               cycles=1
0x90c8:  f3             cpw Y, (X)                               cycles=2
0x90c9:  f3             cpw Y, (X)                               cycles=2
0x90ca:  f3             cpw Y, (X)                               cycles=2
0x90cb:  f3             cpw Y, (X)                               cycles=2
0x90cc:  f2             sbc A, (X)                               cycles=1
0x90cd:  f2             sbc A, (X)                               cycles=1
0x90ce:  f2             sbc A, (X)                               cycles=1
0x90cf:  f2             sbc A, (X)                               cycles=1
0x90d0:  f1             cp A, (X)                                cycles=1
0x90d1:  f1             cp A, (X)                                cycles=1
0x90d2:  f1             cp A, (X)                                cycles=1
0x90d3:  f0             sub A, (X)                               cycles=1
0x90d4:  f0             sub A, (X)                               cycles=1
0x90d5:  f0             sub A, (X)                               cycles=1
0x90d6:  f0             sub A, (X)                               cycles=1
0x90d7:  ef ef          ldw ($ef,X),Y                            cycles=2
0x90d9:  ef ef          ldw ($ef,X),Y                            cycles=2
0x90db:  ef ef          ldw ($ef,X),Y                            cycles=2
0x90dd:  ef f0          ldw ($f0,X),Y                            cycles=2
0x90df:  f0             sub A, (X)                               cycles=1
0x90e0:  f0             sub A, (X)                               cycles=1
0x90e1:  f0             sub A, (X)                               cycles=1
0x90e2:  f1             cp A, (X)                                cycles=1
0x90e3:  f1             cp A, (X)                                cycles=1
0x90e4:  f1             cp A, (X)                                cycles=1
0x90e5:  f2             sbc A, (X)                               cycles=1
0x90e6:  f2             sbc A, (X)                               cycles=1
0x90e7:  f2             sbc A, (X)                               cycles=1
0x90e8:  f2             sbc A, (X)                               cycles=1
0x90e9:  f3             cpw Y, (X)                               cycles=2
0x90ea:  f3             cpw Y, (X)                               cycles=2
0x90eb:  f3             cpw Y, (X)                               cycles=2
0x90ec:  f3             cpw Y, (X)                               cycles=2
0x90ed:  f4             and A, (X)                               cycles=1
0x90ee:  f4             and A, (X)                               cycles=1
0x90ef:  f4             and A, (X)                               cycles=1
0x90f0:  f4             and A, (X)                               cycles=1
0x90f1:  f4             and A, (X)                               cycles=1
0x90f2:  f5             bcp A, (X)                               cycles=1
0x90f3:  f5             bcp A, (X)                               cycles=1
0x90f4:  f5             bcp A, (X)                               cycles=1
0x90f5:  f5             bcp A, (X)                               cycles=1
0x90f6:  f6             ld A, (X)                                cycles=1
0x90f7:  f6             ld A, (X)                                cycles=1
0x90f8:  f6             ld A, (X)                                cycles=1
0x90f9:  f6             ld A, (X)                                cycles=1
0x90fa:  f7             ld (X),A                                 cycles=1
0x90fb:  f7             ld (X),A                                 cycles=1
0x90fc:  f7             ld (X),A                                 cycles=1
0x90fd:  f7             ld (X),A                                 cycles=1
0x90fe:  f7             ld (X),A                                 cycles=1
0x90ff:  f8             xor A, (X)                               cycles=1
0x9100:  f8             xor A, (X)                               cycles=1
0x9101:  f8             xor A, (X)                               cycles=1
0x9102:  f8             xor A, (X)                               cycles=1
0x9103:  f8             xor A, (X)                               cycles=1
0x9104:  f9             adc A, (X)                               cycles=1
0x9105:  f9             adc A, (X)                               cycles=1
0x9106:  f9             adc A, (X)                               cycles=1
0x9107:  f9             adc A, (X)                               cycles=1
0x9108:  f9             adc A, (X)                               cycles=1
0x9109:  f9             adc A, (X)                               cycles=1
0x910a:  fa             or A, (X)                                cycles=1
0x910b:  fa             or A, (X)                                cycles=1
0x910c:  fa             or A, (X)                                cycles=1
0x910d:  fa             or A, (X)                                cycles=1
0x910e:  fa             or A, (X)                                cycles=1
0x910f:  fa             or A, (X)                                cycles=1
0x9110:  fa             or A, (X)                                cycles=1
0x9111:  fa             or A, (X)                                cycles=1
0x9112:  fa             or A, (X)                                cycles=1
0x9113:  fa             or A, (X)                                cycles=1
0x9114:  fa             or A, (X)                                cycles=1
0x9115:  fa             or A, (X)                                cycles=1
0x9116:  fa             or A, (X)                                cycles=1
0x9117:  fa             or A, (X)                                cycles=1
0x9118:  fa             or A, (X)                                cycles=1
0x9119:  fa             or A, (X)                                cycles=1
0x911a:  fa             or A, (X)                                cycles=1
0x911b:  fa             or A, (X)                                cycles=1
0x911c:  fa             or A, (X)                                cycles=1
0x911d:  fa             or A, (X)                                cycles=1
0x911e:  fa             or A, (X)                                cycles=1
0x911f:  fa             or A, (X)                                cycles=1
0x9120:  fa             or A, (X)                                cycles=1
0x9121:  fa             or A, (X)                                cycles=1
0x9122:  fa             or A, (X)                                cycles=1
0x9123:  fa             or A, (X)                                cycles=1
0x9124:  fa             or A, (X)                                cycles=1
0x9125:  fa             or A, (X)                                cycles=1
0x9126:  fa             or A, (X)                                cycles=1
0x9127:  fa             or A, (X)                                cycles=1
0x9128:  fa             or A, (X)                                cycles=1
0x9129:  fa             or A, (X)                                cycles=1
0x912a:  fa             or A, (X)                                cycles=1
0x912b:  fa             or A, (X)                                cycles=1
0x912c:  fa             or A, (X)                                cycles=1
0x912d:  fa             or A, (X)                                cycles=1
0x912e:  fa             or A, (X)                                cycles=1
0x912f:  fa             or A, (X)                                cycles=1
0x9130:  fa             or A, (X)                                cycles=1
0x9131:  fa             or A, (X)                                cycles=1
0x9132:  fa             or A, (X)                                cycles=1
0x9133:  fa             or A, (X)                                cycles=1
0x9134:  fa             or A, (X)                                cycles=1
0x9135:  fa             or A, (X)                                cycles=1
0x9136:  fa             or A, (X)                                cycles=1
0x9137:  fa             or A, (X)                                cycles=1
0x9138:  fa             or A, (X)                                cycles=1
0x9139:  fa             or A, (X)                                cycles=1
0x913a:  fa             or A, (X)                                cycles=1
0x913b:  fa             or A, (X)                                cycles=1
0x913c:  fa             or A, (X)                                cycles=1
0x913d:  fa             or A, (X)                                cycles=1
0x913e:  fa             or A, (X)                                cycles=1
0x913f:  fa             or A, (X)                                cycles=1
0x9140:  fa             or A, (X)                                cycles=1
0x9141:  fa             or A, (X)                                cycles=1
0x9142:  fa             or A, (X)                                cycles=1
0x9143:  fa             or A, (X)                                cycles=1
0x9144:  fa             or A, (X)                                cycles=1
0x9145:  fa             or A, (X)                                cycles=1
0x9146:  fa             or A, (X)                                cycles=1
0x9147:  fa             or A, (X)                                cycles=1
0x9148:  fa             or A, (X)                                cycles=1
0x9149:  fa             or A, (X)                                cycles=1
0x914a:  fa             or A, (X)                                cycles=1
0x914b:  fa             or A, (X)                                cycles=1
0x914c:  fa             or A, (X)                                cycles=1
0x914d:  fa             or A, (X)                                cycles=1
0x914e:  fa             or A, (X)                                cycles=1
0x914f:  fa             or A, (X)                                cycles=1
0x9150:  fa             or A, (X)                                cycles=1
0x9151:  fa             or A, (X)                                cycles=1
0x9152:  fa             or A, (X)                                cycles=1
0x9153:  fa             or A, (X)                                cycles=1
0x9154:  fa             or A, (X)                                cycles=1
0x9155:  fa             or A, (X)                                cycles=1
0x9156:  fa             or A, (X)                                cycles=1
0x9157:  fa             or A, (X)                                cycles=1
0x9158:  fa             or A, (X)                                cycles=1
0x9159:  fa             or A, (X)                                cycles=1
0x915a:  fa             or A, (X)                                cycles=1
0x915b:  fa             or A, (X)                                cycles=1
0x915c:  fa             or A, (X)                                cycles=1
0x915d:  fa             or A, (X)                                cycles=1
0x915e:  fa             or A, (X)                                cycles=1
0x915f:  fa             or A, (X)                                cycles=1
0x9160:  fa             or A, (X)                                cycles=1
0x9161:  fa             or A, (X)                                cycles=1
0x9162:  fa             or A, (X)                                cycles=1
0x9163:  fa             or A, (X)                                cycles=1
0x9164:  fa             or A, (X)                                cycles=1
0x9165:  fa             or A, (X)                                cycles=1
0x9166:  fa             or A, (X)                                cycles=1
0x9167:  fa             or A, (X)                                cycles=1
0x9168:  fa             or A, (X)                                cycles=1
0x9169:  fa             or A, (X)                                cycles=1
0x916a:  fa             or A, (X)                                cycles=1
0x916b:  fa             or A, (X)                                cycles=1
0x916c:  fa             or A, (X)                                cycles=1
0x916d:  fa             or A, (X)                                cycles=1
0x916e:  fa             or A, (X)                                cycles=1
0x916f:  fa             or A, (X)                                cycles=1
0x9170:  fa             or A, (X)                                cycles=1
0x9171:  fa             or A, (X)                                cycles=1
0x9172:  fa             or A, (X)                                cycles=1
0x9173:  fa             or A, (X)                                cycles=1
0x9174:  fa             or A, (X)                                cycles=1
0x9175:  fa             or A, (X)                                cycles=1
0x9176:  fa             or A, (X)                                cycles=1
0x9177:  fa             or A, (X)                                cycles=1
0x9178:  fa             or A, (X)                                cycles=1
0x9179:  fa             or A, (X)                                cycles=1
0x917a:  fa             or A, (X)                                cycles=1
0x917b:  fa             or A, (X)                                cycles=1
0x917c:  fa             or A, (X)                                cycles=1
0x917d:  fa             or A, (X)                                cycles=1
0x917e:  fa             or A, (X)                                cycles=1
0x917f:  fa             or A, (X)                                cycles=1
0x9180:  fa             or A, (X)                                cycles=1
0x9181:  fa             or A, (X)                                cycles=1
0x9182:  fa             or A, (X)                                cycles=1
0x9183:  fa             or A, (X)                                cycles=1
0x9184:  fa             or A, (X)                                cycles=1
0x9185:  fa             or A, (X)                                cycles=1
0x9186:  fa             or A, (X)                                cycles=1
0x9187:  fa             or A, (X)                                cycles=1
0x9188:  fa             or A, (X)                                cycles=1
0x9189:  fa             or A, (X)                                cycles=1
0x918a:  fa             or A, (X)                                cycles=1
0x918b:  fa             or A, (X)                                cycles=1
0x918c:  fa             or A, (X)                                cycles=1
0x918d:  fa             or A, (X)                                cycles=1
0x918e:  fa             or A, (X)                                cycles=1
0x918f:  fa             or A, (X)                                cycles=1
0x9190:  fa             or A, (X)                                cycles=1
0x9191:  fa             or A, (X)                                cycles=1
0x9192:  fa             or A, (X)                                cycles=1
0x9193:  fa             or A, (X)                                cycles=1
0x9194:  fa             or A, (X)                                cycles=1
0x9195:  fa             or A, (X)                                cycles=1
0x9196:  fa             or A, (X)                                cycles=1
0x9197:  fa             or A, (X)                                cycles=1
0x9198:  fa             or A, (X)                                cycles=1
0x9199:  fa             or A, (X)                                cycles=1
0x919a:  fa             or A, (X)                                cycles=1
0x919b:  f9             adc A, (X)                               cycles=1
0x919c:  f9             adc A, (X)                               cycles=1
0x919d:  f9             adc A, (X)                               cycles=1
0x919e:  f9             adc A, (X)                               cycles=1
0x919f:  f9             adc A, (X)                               cycles=1
0x91a0:  f9             adc A, (X)                               cycles=1
0x91a1:  f8             xor A, (X)                               cycles=1
0x91a2:  f8             xor A, (X)                               cycles=1
0x91a3:  f8             xor A, (X)                               cycles=1
0x91a4:  f8             xor A, (X)                               cycles=1
0x91a5:  f8             xor A, (X)                               cycles=1
0x91a6:  f7             ld (X),A                                 cycles=1
0x91a7:  f7             ld (X),A                                 cycles=1
0x91a8:  f7             ld (X),A                                 cycles=1
0x91a9:  f7             ld (X),A                                 cycles=1
0x91aa:  f7             ld (X),A                                 cycles=1
0x91ab:  f6             ld A, (X)                                cycles=1
0x91ac:  f6             ld A, (X)                                cycles=1
0x91ad:  f6             ld A, (X)                                cycles=1
0x91ae:  f6             ld A, (X)                                cycles=1
0x91af:  f5             bcp A, (X)                               cycles=1
0x91b0:  f5             bcp A, (X)                               cycles=1
0x91b1:  f5             bcp A, (X)                               cycles=1
0x91b2:  f5             bcp A, (X)                               cycles=1
0x91b3:  f4             and A, (X)                               cycles=1
0x91b4:  f4             and A, (X)                               cycles=1
0x91b5:  f4             and A, (X)                               cycles=1
0x91b6:  f4             and A, (X)                               cycles=1
0x91b7:  f4             and A, (X)                               cycles=1
0x91b8:  f3             cpw Y, (X)                               cycles=2
0x91b9:  f3             cpw Y, (X)                               cycles=2
0x91ba:  f3             cpw Y, (X)                               cycles=2
0x91bb:  f3             cpw Y, (X)                               cycles=2
0x91bc:  f2             sbc A, (X)                               cycles=1
0x91bd:  f2             sbc A, (X)                               cycles=1
0x91be:  f2             sbc A, (X)                               cycles=1
0x91bf:  f2             sbc A, (X)                               cycles=1
0x91c0:  f1             cp A, (X)                                cycles=1
0x91c1:  f1             cp A, (X)                                cycles=1
0x91c2:  f1             cp A, (X)                                cycles=1
0x91c3:  f0             sub A, (X)                               cycles=1
0x91c4:  f0             sub A, (X)                               cycles=1
0x91c5:  f0             sub A, (X)                               cycles=1
0x91c6:  f0             sub A, (X)                               cycles=1
0x91c7:  ef ef          ldw ($ef,X),Y                            cycles=2
0x91c9:  ef ef          ldw ($ef,X),Y                            cycles=2
0x91cb:  ee ed          ldw X, ($ed,X)                           cycles=2
0x91cd:  ec eb          jp ($eb,X)                               cycles=1
0x91cf:  ea e9          or A, ($e9,X)                            cycles=1
0x91d1:  e8 e8          xor A, ($e8,X)                           cycles=1
0x91d3:  e7 e6          ld ($e6,X),A                             cycles=1
0x91d5:  e5 e4          bcp A, ($e4,X)                           cycles=1
0x91d7:  e3 e2          cpw Y, ($e2,X)                           cycles=2
0x91d9:  e1 e1          cp A, ($e1,X)                            cycles=1
0x91db:  e0 df          sub A, ($df,X)                           cycles=1
0x91dd:  de dd dc       ldw X, ($dddc,X)                         cycles=2
0x91e0:  db da d9       add A, ($dad9,X)                         cycles=1
0x91e3:  d8 d8 d7       xor A, ($d8d7,X)                         cycles=1
0x91e6:  d6 d5 d4       ld A, ($d5d4,X)                          cycles=1
0x91e9:  d3 d2 d1       cpw Y, ($d2d1,X)                         cycles=2
0x91ec:  d0 cf ce       sub A, ($cfce,X)                         cycles=1
0x91ef:  cd cc cc       call $cccc                               cycles=4
0x91f2:  cb ca c9       add A, $cac9                             cycles=1
0x91f5:  c8 c7 c6       xor A, $c7c6                             cycles=1
0x91f8:  c5 c4 c3       bcp A, $c4c3                             cycles=1
0x91fb:  c2 c1 c0       sbc A, $c1c0                             cycles=1
0x91fe:  bf be          ldw $be,X                                cycles=2
0x9200:  bd bc bb ba    ldf $bcbbba,A                            cycles=1
0x9204:  b9 b8          adc A, $b8                               cycles=1
0x9206:  b7 b6          ld $b6,A                                 cycles=1
0x9208:  b5 b4          bcp A, $b4                               cycles=1
0x920a:  b3 b2          cpw X, $b2                               cycles=2
0x920c:  b1 b0          cp A, $b0                                cycles=1
0x920e:  af ae ad ac    ldf A, ($aeadac,X)                       cycles=1
0x9212:  ab aa          add A, #$aa                              cycles=1
0x9214:  a9 a8          adc A, #$a8                              cycles=1
0x9216:  a7 a6 a5 a4    ldf ($a6a5a4,X),A                        cycles=1
0x921a:  a3 a2 a1       cpw X, #$a2a1                            cycles=2
0x921d:  a0 9f          sub A, #$9f                              cycles=1
0x921f:  9e             ld A, XH                                 cycles=1
0x9220:  9d             nop                                      cycles=1
0x9221:  9c             rvf                                      cycles=5
0x9222:  9b             sim                                      cycles=1
0x9223:  9a             rim                                      cycles=1
0x9224:  9a             rim                                      cycles=1
0x9225:  99             scf                                      cycles=1
0x9226:  98             rcf                                      cycles=1
0x9227:  97             ld XL, A                                 cycles=1
0x9228:  96             ldw X, SP                                cycles=1
0x9229:  95             ld XH, A                                 cycles=1
0x922a:  94             ldw SP, X                                cycles=1
0x922b:  93             ldw X, Y                                 cycles=1
0x922c:  92 91          ???                                      cycles=?
0x922e:  90 8f          ???                                      cycles=?
0x9230:  8e             halt                                     cycles=1
0x9231:  8d 8c 8b 8a    callf $8c8b8a                            cycles=5
0x9235:  89             pushw X                                  cycles=2
0x9236:  88             push A                                   cycles=1
0x9237:  87             retf                                     cycles=5
0x9238:  86             pop CC                                   cycles=1
0x9239:  86             pop CC                                   cycles=1
0x923a:  85             popw X                                   cycles=2
0x923b:  84             pop A                                    cycles=1
0x923c:  83             trap                                     cycles=9
0x923d:  82 81 80 7f    int $81807f                              cycles=2
0x9241:  7e             swap (X)                                 cycles=1
0x9242:  7d             tnz (X)                                  cycles=1
0x9243:  7c             inc (X)                                  cycles=1
0x9244:  7b 7a          ld A, ($7a,SP)                           cycles=1
0x9246:  79             rlc (X)                                  cycles=1
0x9247:  78             sll (X)                                  cycles=1
0x9248:  77             sra (X)                                  cycles=1
0x9249:  76             rrc (X)                                  cycles=1
0x924a:  75             ???                                      cycles=?
0x924b:  74             srl (X)                                  cycles=1
0x924c:  74             srl (X)                                  cycles=1
0x924d:  73             cpl (X)                                  cycles=1
0x924e:  72 71          ???                                      cycles=?
0x9250:  70             neg (X)                                  cycles=1
0x9251:  6f 6e          clr ($6e,X)                              cycles=1
0x9253:  6d 6c          tnz ($6c,X)                              cycles=1
0x9255:  6b 6a          ld ($6a,SP),A                            cycles=1
0x9257:  69 68          rlc ($68,X)                              cycles=1
0x9259:  67 66          sra ($66,X)                              cycles=1
0x925b:  65             divw X, Y                                cycles=2-17
0x925c:  64 63          srl ($63,X)                              cycles=1
0x925e:  62             div X, A                                 cycles=2-17
0x925f:  61             exg A, YL                                cycles=1
0x9260:  60 60          neg ($60,X)                              cycles=1
0x9262:  5f             clrw X                                   cycles=1
0x9263:  5e             swapw X                                  cycles=1
0x9264:  5d             tnzw X                                   cycles=2
0x9265:  5c             incw X                                   cycles=1
0x9266:  5b 5a          addw SP, #$5a                            cycles=2
0x9268:  59             rlcw X                                   cycles=2
0x9269:  58             sllw X                                   cycles=2
0x926a:  57             sraw X                                   cycles=2
0x926b:  56             rrcw X                                   cycles=2
0x926c:  55 54 53 52 51 mov $5251, $5453                         cycles=1
0x9271:  50             negw X                                   cycles=1
0x9272:  4f             clr A                                    cycles=1
0x9273:  4e             swap A                                   cycles=1
0x9274:  4d             tnz A                                    cycles=1
0x9275:  4c             inc A                                    cycles=1
0x9276:  4b 4a          push #$4a                                cycles=1
0x9278:  49             rlc A                                    cycles=1
0x9279:  48             sll A                                    cycles=1
0x927a:  47             sra A                                    cycles=1
0x927b:  46             rrc A                                    cycles=1
0x927c:  45 44 43       mov $43, $44                             cycles=1
0x927f:  42             mul X, A                                 cycles=4
0x9280:  41             exg A, XL                                cycles=1
0x9281:  40             neg A                                    cycles=1
0x9282:  3f 3e          clr $3e                                  cycles=1
0x9284:  3d 3c          tnz $3c                                  cycles=1
0x9286:  3b 3a 39       push $3a39                               cycles=1
0x9289:  38 37          sll $37                                  cycles=1
0x928b:  36 35          rrc $35                                  cycles=1
0x928d:  34 33          srl $33                                  cycles=1
0x928f:  32 31 30       pop $3130                                cycles=1
0x9292:  2f 2e          jrslt $92c2  (offset=46)                 cycles=1-2
0x9294:  2e 2d          jrsge $92c3  (offset=45)                 cycles=1-2
0x9296:  2c 2b          jrsgt $92c3  (offset=43)                 cycles=1-2
0x9298:  2a 29          jrpl $92c3  (offset=41)                  cycles=1-2
0x929a:  28 27          jrnv $92c3  (offset=39)                  cycles=1-2
0x929c:  26 25          jrne $92c3  (offset=37)                  cycles=1-2
0x929e:  24 23          jrnc $92c3  (offset=35)                  cycles=1-2
0x92a0:  22 22          jrugt $92c4  (offset=34)                 cycles=1-2
0x92a2:  21 20          jrf $92c4  (offset=32)                   cycles=1-2
0x92a4:  1f 1e          ldw ($1e,SP),X                           cycles=2
0x92a6:  1d 1c 1b       subw X, #$1c1b                           cycles=2
0x92a9:  1a 19          or A, ($19,SP)                           cycles=1
0x92ab:  19 18          adc A, ($18,SP)                          cycles=1
0x92ad:  17 16          ldw ($16,SP),Y                           cycles=2
0x92af:  15 14          bcp A, ($14,SP)                          cycles=1
0x92b1:  13 12          cpw X, ($12,SP)                          cycles=2
0x92b3:  12 11          sbc A, ($11,SP)                          cycles=1
0x92b5:  10 0f          sub A, ($0f,SP)                          cycles=1
0x92b7:  0e 0d          swap ($0d,SP)                            cycles=1
0x92b9:  0c 0b          inc ($0b,SP)                             cycles=1
0x92bb:  0b             ???                                      cycles=?
0x92bc:  0b             ???                                      cycles=?
0x92bd:  0b             ???                                      cycles=?
0x92be:  0a 0a          dec ($0a,SP)                             cycles=1
0x92c0:  0a 0a          dec ($0a,SP)                             cycles=1
0x92c2:  09 09          rlc ($09,SP)                             cycles=1
0x92c4:  09 08          rlc ($08,SP)                             cycles=1
0x92c6:  08 08          sll ($08,SP)                             cycles=1
0x92c8:  08 07          sll ($07,SP)                             cycles=1
0x92ca:  07 07          sra ($07,SP)                             cycles=1
0x92cc:  07 06          sra ($06,SP)                             cycles=1
0x92ce:  06 06          rrc ($06,SP)                             cycles=1
0x92d0:  06 06          rrc ($06,SP)                             cycles=1
0x92d2:  05             ???                                      cycles=?
0x92d3:  05             ???                                      cycles=?
0x92d4:  05             ???                                      cycles=?
0x92d5:  05             ???                                      cycles=?
0x92d6:  04 04          srl ($04,SP)                             cycles=1
0x92d8:  04 04          srl ($04,SP)                             cycles=1
0x92da:  03 03          cpl ($03,SP)                             cycles=1
0x92dc:  03 03          cpl ($03,SP)                             cycles=1
0x92de:  03 02          cpl ($02,SP)                             cycles=1
0x92e0:  02             rlwa X, A                                cycles=1
0x92e1:  02             rlwa X, A                                cycles=1
0x92e2:  02             rlwa X, A                                cycles=1
0x92e3:  02             rlwa X, A                                cycles=1
0x92e4:  01             rrwa X, A                                cycles=1
0x92e5:  01             rrwa X, A                                cycles=1
0x92e6:  01             rrwa X, A                                cycles=1
0x92e7:  01             rrwa X, A                                cycles=1
0x92e8:  01             rrwa X, A                                cycles=1
0x92e9:  01             rrwa X, A                                cycles=1
0x92ea:  00 00          neg ($00,SP)                             cycles=1
0x92ec:  00 00          neg ($00,SP)                             cycles=1
0x92ee:  00 00          neg ($00,SP)                             cycles=1
0x92f0:  00 00          neg ($00,SP)                             cycles=1
0x92f2:  00 00          neg ($00,SP)                             cycles=1
0x92f4:  00 00          neg ($00,SP)                             cycles=1
0x92f6:  00 00          neg ($00,SP)                             cycles=1
0x92f8:  00 00          neg ($00,SP)                             cycles=1
0x92fa:  00 00          neg ($00,SP)                             cycles=1
0x92fc:  00 00          neg ($00,SP)                             cycles=1
0x92fe:  00 00          neg ($00,SP)                             cycles=1
0x9300:  00 00          neg ($00,SP)                             cycles=1
0x9302:  00 00          neg ($00,SP)                             cycles=1
0x9304:  00 00          neg ($00,SP)                             cycles=1
0x9306:  00 00          neg ($00,SP)                             cycles=1
0x9308:  00 00          neg ($00,SP)                             cycles=1
0x930a:  00 00          neg ($00,SP)                             cycles=1
0x930c:  00 00          neg ($00,SP)                             cycles=1
0x930e:  00 00          neg ($00,SP)                             cycles=1
0x9310:  00 00          neg ($00,SP)                             cycles=1
0x9312:  00 00          neg ($00,SP)                             cycles=1
0x9314:  00 00          neg ($00,SP)                             cycles=1
0x9316:  00 00          neg ($00,SP)                             cycles=1
0x9318:  00 00          neg ($00,SP)                             cycles=1
0x931a:  00 00          neg ($00,SP)                             cycles=1
0x931c:  00 00          neg ($00,SP)                             cycles=1
0x931e:  00 00          neg ($00,SP)                             cycles=1
0x9320:  00 00          neg ($00,SP)                             cycles=1
0x9322:  00 00          neg ($00,SP)                             cycles=1
0x9324:  00 00          neg ($00,SP)                             cycles=1
0x9326:  00 00          neg ($00,SP)                             cycles=1
0x9328:  00 00          neg ($00,SP)                             cycles=1
0x932a:  00 00          neg ($00,SP)                             cycles=1
0x932c:  00 00          neg ($00,SP)                             cycles=1
0x932e:  00 00          neg ($00,SP)                             cycles=1
0x9330:  00 00          neg ($00,SP)                             cycles=1
0x9332:  00 00          neg ($00,SP)                             cycles=1
0x9334:  00 00          neg ($00,SP)                             cycles=1
0x9336:  00 00          neg ($00,SP)                             cycles=1
0x9338:  00 00          neg ($00,SP)                             cycles=1
0x933a:  00 00          neg ($00,SP)                             cycles=1
0x933c:  00 00          neg ($00,SP)                             cycles=1
0x933e:  00 00          neg ($00,SP)                             cycles=1
0x9340:  00 00          neg ($00,SP)                             cycles=1
0x9342:  00 00          neg ($00,SP)                             cycles=1
0x9344:  00 00          neg ($00,SP)                             cycles=1
0x9346:  00 00          neg ($00,SP)                             cycles=1
0x9348:  00 00          neg ($00,SP)                             cycles=1
0x934a:  00 00          neg ($00,SP)                             cycles=1
0x934c:  00 00          neg ($00,SP)                             cycles=1
0x934e:  00 00          neg ($00,SP)                             cycles=1
0x9350:  00 00          neg ($00,SP)                             cycles=1
0x9352:  00 00          neg ($00,SP)                             cycles=1
0x9354:  00 00          neg ($00,SP)                             cycles=1
0x9356:  00 00          neg ($00,SP)                             cycles=1
0x9358:  00 00          neg ($00,SP)                             cycles=1
0x935a:  00 00          neg ($00,SP)                             cycles=1
0x935c:  00 00          neg ($00,SP)                             cycles=1
0x935e:  00 00          neg ($00,SP)                             cycles=1
0x9360:  00 00          neg ($00,SP)                             cycles=1
0x9362:  00 00          neg ($00,SP)                             cycles=1
0x9364:  00 00          neg ($00,SP)                             cycles=1
0x9366:  00 00          neg ($00,SP)                             cycles=1
0x9368:  00 00          neg ($00,SP)                             cycles=1
0x936a:  00 00          neg ($00,SP)                             cycles=1
0x936c:  00 00          neg ($00,SP)                             cycles=1
0x936e:  00 00          neg ($00,SP)                             cycles=1
0x9370:  00 00          neg ($00,SP)                             cycles=1
0x9372:  00 00          neg ($00,SP)                             cycles=1
0x9374:  00 00          neg ($00,SP)                             cycles=1
0x9376:  00 00          neg ($00,SP)                             cycles=1
0x9378:  00 00          neg ($00,SP)                             cycles=1
0x937a:  00 01          neg ($01,SP)                             cycles=1
0x937c:  01             rrwa X, A                                cycles=1
0x937d:  01             rrwa X, A                                cycles=1
0x937e:  01             rrwa X, A                                cycles=1
0x937f:  01             rrwa X, A                                cycles=1
0x9380:  01             rrwa X, A                                cycles=1
0x9381:  02             rlwa X, A                                cycles=1
0x9382:  02             rlwa X, A                                cycles=1
0x9383:  02             rlwa X, A                                cycles=1
0x9384:  02             rlwa X, A                                cycles=1
0x9385:  02             rlwa X, A                                cycles=1
0x9386:  03 03          cpl ($03,SP)                             cycles=1
0x9388:  03 03          cpl ($03,SP)                             cycles=1
0x938a:  03 04          cpl ($04,SP)                             cycles=1
0x938c:  04 04          srl ($04,SP)                             cycles=1
0x938e:  04 05          srl ($05,SP)                             cycles=1
0x9390:  05             ???                                      cycles=?
0x9391:  05             ???                                      cycles=?
0x9392:  05             ???                                      cycles=?
0x9393:  06 06          rrc ($06,SP)                             cycles=1
0x9395:  06 06          rrc ($06,SP)                             cycles=1
0x9397:  06 07          rrc ($07,SP)                             cycles=1
0x9399:  07 07          sra ($07,SP)                             cycles=1
0x939b:  07 08          sra ($08,SP)                             cycles=1
0x939d:  08 08          sll ($08,SP)                             cycles=1
0x939f:  08 09          sll ($09,SP)                             cycles=1
0x93a1:  09 09          rlc ($09,SP)                             cycles=1
0x93a3:  0a 0a          dec ($0a,SP)                             cycles=1
0x93a5:  0a 0a          dec ($0a,SP)                             cycles=1
0x93a7:  0b             ???                                      cycles=?
0x93a8:  0b             ???                                      cycles=?
0x93a9:  0b             ???                                      cycles=?
0x93aa:  0b             ???                                      cycles=?
0x93ab:  0b             ???                                      cycles=?
0x93ac:  0b             ???                                      cycles=?
0x93ad:  0b             ???                                      cycles=?
0x93ae:  0a 0a          dec ($0a,SP)                             cycles=1
0x93b0:  0a 0a          dec ($0a,SP)                             cycles=1
0x93b2:  09 09          rlc ($09,SP)                             cycles=1
0x93b4:  09 08          rlc ($08,SP)                             cycles=1
0x93b6:  08 08          sll ($08,SP)                             cycles=1
0x93b8:  08 07          sll ($07,SP)                             cycles=1
0x93ba:  07 07          sra ($07,SP)                             cycles=1
0x93bc:  07 06          sra ($06,SP)                             cycles=1
0x93be:  06 06          rrc ($06,SP)                             cycles=1
0x93c0:  06 06          rrc ($06,SP)                             cycles=1
0x93c2:  05             ???                                      cycles=?
0x93c3:  05             ???                                      cycles=?
0x93c4:  05             ???                                      cycles=?
0x93c5:  05             ???                                      cycles=?
0x93c6:  04 04          srl ($04,SP)                             cycles=1
0x93c8:  04 04          srl ($04,SP)                             cycles=1
0x93ca:  03 03          cpl ($03,SP)                             cycles=1
0x93cc:  03 03          cpl ($03,SP)                             cycles=1
0x93ce:  03 02          cpl ($02,SP)                             cycles=1
0x93d0:  02             rlwa X, A                                cycles=1
0x93d1:  02             rlwa X, A                                cycles=1
0x93d2:  02             rlwa X, A                                cycles=1
0x93d3:  02             rlwa X, A                                cycles=1
0x93d4:  01             rrwa X, A                                cycles=1
0x93d5:  01             rrwa X, A                                cycles=1
0x93d6:  01             rrwa X, A                                cycles=1
0x93d7:  01             rrwa X, A                                cycles=1
0x93d8:  01             rrwa X, A                                cycles=1
0x93d9:  01             rrwa X, A                                cycles=1
0x93da:  00 00          neg ($00,SP)                             cycles=1
0x93dc:  00 00          neg ($00,SP)                             cycles=1
0x93de:  00 00          neg ($00,SP)                             cycles=1
0x93e0:  00 00          neg ($00,SP)                             cycles=1
0x93e2:  00 00          neg ($00,SP)                             cycles=1
0x93e4:  00 00          neg ($00,SP)                             cycles=1
0x93e6:  00 00          neg ($00,SP)                             cycles=1
0x93e8:  00 00          neg ($00,SP)                             cycles=1
0x93ea:  00 00          neg ($00,SP)                             cycles=1
0x93ec:  00 00          neg ($00,SP)                             cycles=1
0x93ee:  00 00          neg ($00,SP)                             cycles=1
0x93f0:  00 00          neg ($00,SP)                             cycles=1
0x93f2:  00 00          neg ($00,SP)                             cycles=1
0x93f4:  00 00          neg ($00,SP)                             cycles=1
0x93f6:  00 00          neg ($00,SP)                             cycles=1
0x93f8:  00 00          neg ($00,SP)                             cycles=1
0x93fa:  00 00          neg ($00,SP)                             cycles=1
0x93fc:  00 00          neg ($00,SP)                             cycles=1
0x93fe:  00 00          neg ($00,SP)                             cycles=1
0x9400:  00 00          neg ($00,SP)                             cycles=1
0x9402:  00 00          neg ($00,SP)                             cycles=1
0x9404:  00 00          neg ($00,SP)                             cycles=1
0x9406:  00 00          neg ($00,SP)                             cycles=1
0x9408:  00 00          neg ($00,SP)                             cycles=1
0x940a:  00 00          neg ($00,SP)                             cycles=1
0x940c:  00 00          neg ($00,SP)                             cycles=1
0x940e:  00 00          neg ($00,SP)                             cycles=1
0x9410:  00 00          neg ($00,SP)                             cycles=1
0x9412:  00 00          neg ($00,SP)                             cycles=1
0x9414:  00 00          neg ($00,SP)                             cycles=1
0x9416:  00 00          neg ($00,SP)                             cycles=1
0x9418:  00 00          neg ($00,SP)                             cycles=1
0x941a:  00 00          neg ($00,SP)                             cycles=1
0x941c:  00 00          neg ($00,SP)                             cycles=1
0x941e:  00 00          neg ($00,SP)                             cycles=1
0x9420:  00 00          neg ($00,SP)                             cycles=1
0x9422:  00 00          neg ($00,SP)                             cycles=1
0x9424:  00 00          neg ($00,SP)                             cycles=1
0x9426:  00 00          neg ($00,SP)                             cycles=1
0x9428:  00 00          neg ($00,SP)                             cycles=1
0x942a:  00 00          neg ($00,SP)                             cycles=1
0x942c:  00 00          neg ($00,SP)                             cycles=1
0x942e:  00 00          neg ($00,SP)                             cycles=1
0x9430:  00 00          neg ($00,SP)                             cycles=1
0x9432:  00 00          neg ($00,SP)                             cycles=1
0x9434:  00 00          neg ($00,SP)                             cycles=1
0x9436:  00 00          neg ($00,SP)                             cycles=1
0x9438:  00 00          neg ($00,SP)                             cycles=1
0x943a:  00 00          neg ($00,SP)                             cycles=1
0x943c:  00 00          neg ($00,SP)                             cycles=1
0x943e:  00 00          neg ($00,SP)                             cycles=1
0x9440:  00 00          neg ($00,SP)                             cycles=1
0x9442:  00 00          neg ($00,SP)                             cycles=1
0x9444:  00 00          neg ($00,SP)                             cycles=1
0x9446:  00 00          neg ($00,SP)                             cycles=1
0x9448:  00 00          neg ($00,SP)                             cycles=1
0x944a:  00 00          neg ($00,SP)                             cycles=1
0x944c:  00 00          neg ($00,SP)                             cycles=1
0x944e:  00 00          neg ($00,SP)                             cycles=1
0x9450:  00 00          neg ($00,SP)                             cycles=1
0x9452:  00 00          neg ($00,SP)                             cycles=1
0x9454:  00 00          neg ($00,SP)                             cycles=1
0x9456:  00 00          neg ($00,SP)                             cycles=1
0x9458:  00 00          neg ($00,SP)                             cycles=1
0x945a:  00 00          neg ($00,SP)                             cycles=1
0x945c:  00 00          neg ($00,SP)                             cycles=1
0x945e:  00 00          neg ($00,SP)                             cycles=1
0x9460:  00 00          neg ($00,SP)                             cycles=1
0x9462:  00 00          neg ($00,SP)                             cycles=1
0x9464:  00 00          neg ($00,SP)                             cycles=1
0x9466:  00 00          neg ($00,SP)                             cycles=1
0x9468:  00 00          neg ($00,SP)                             cycles=1
0x946a:  00 01          neg ($01,SP)                             cycles=1
0x946c:  01             rrwa X, A                                cycles=1
0x946d:  01             rrwa X, A                                cycles=1
0x946e:  01             rrwa X, A                                cycles=1
0x946f:  01             rrwa X, A                                cycles=1
0x9470:  01             rrwa X, A                                cycles=1
0x9471:  02             rlwa X, A                                cycles=1
0x9472:  02             rlwa X, A                                cycles=1
0x9473:  02             rlwa X, A                                cycles=1
0x9474:  02             rlwa X, A                                cycles=1
0x9475:  02             rlwa X, A                                cycles=1
0x9476:  03 03          cpl ($03,SP)                             cycles=1
0x9478:  03 03          cpl ($03,SP)                             cycles=1
0x947a:  03 04          cpl ($04,SP)                             cycles=1
0x947c:  04 04          srl ($04,SP)                             cycles=1
0x947e:  04 05          srl ($05,SP)                             cycles=1
0x9480:  05             ???                                      cycles=?
0x9481:  05             ???                                      cycles=?
0x9482:  05             ???                                      cycles=?
0x9483:  06 06          rrc ($06,SP)                             cycles=1
0x9485:  06 06          rrc ($06,SP)                             cycles=1
0x9487:  06 07          rrc ($07,SP)                             cycles=1
0x9489:  07 07          sra ($07,SP)                             cycles=1
0x948b:  07 08          sra ($08,SP)                             cycles=1
0x948d:  08 08          sll ($08,SP)                             cycles=1
0x948f:  08 09          sll ($09,SP)                             cycles=1
0x9491:  09 09          rlc ($09,SP)                             cycles=1
0x9493:  0a 0a          dec ($0a,SP)                             cycles=1
0x9495:  0a 0a          dec ($0a,SP)                             cycles=1
0x9497:  0b             ???                                      cycles=?
0x9498:  0b             ???                                      cycles=?
0x9499:  0b             ???                                      cycles=?
0x949a:  11 12          cp A, ($12,SP)                           cycles=1
0x949c:  12 13          sbc A, ($13,SP)                          cycles=1
0x949e:  14 15          and A, ($15,SP)                          cycles=1
0x94a0:  16 17          ldw Y, ($17,SP)                          cycles=2
0x94a2:  17 18          ldw ($18,SP),Y                           cycles=2
0x94a4:  19 1a          adc A, ($1a,SP)                          cycles=1
0x94a6:  1b 1c          add A, ($1c,SP)                          cycles=1
0x94a8:  1c 1d 1e       addw X, #$1d1e                           cycles=2
0x94ab:  1f 20          ldw ($20,SP),X                           cycles=2
0x94ad:  21 22          jrf $94d1  (offset=34)                   cycles=1-2
0x94af:  22 23          jrugt $94d4  (offset=35)                 cycles=1-2
0x94b1:  24 25          jrnc $94d8  (offset=37)                  cycles=1-2
0x94b3:  26 27          jrne $94dc  (offset=39)                  cycles=1-2
0x94b5:  28 28          jrnv $94df  (offset=40)                  cycles=1-2
0x94b7:  29 2a          jrv $94e3  (offset=42)                   cycles=1-2
0x94b9:  2b 2c          jrmi $94e7  (offset=44)                  cycles=1-2
0x94bb:  2d 2e          jrsle $94eb  (offset=46)                 cycles=1-2
0x94bd:  2f 2f          jrslt $94ee  (offset=47)                 cycles=1-2
0x94bf:  30 31          neg $31                                  cycles=1
0x94c1:  32 33 34       pop $3334                                cycles=1
0x94c4:  35 36 37 37    mov $3737, #$36                          cycles=1
0x94c8:  38 39          sll $39                                  cycles=1
0x94ca:  3a 3b          dec $3b                                  cycles=1
0x94cc:  3c 3d          inc $3d                                  cycles=1
0x94ce:  3e 3f          swap $3f                                 cycles=1
0x94d0:  40             neg A                                    cycles=1
0x94d1:  40             neg A                                    cycles=1
0x94d2:  41             exg A, XL                                cycles=1
0x94d3:  42             mul X, A                                 cycles=4
0x94d4:  43             cpl A                                    cycles=1
0x94d5:  44             srl A                                    cycles=1
0x94d6:  45 46 47       mov $47, $46                             cycles=1
0x94d9:  48             sll A                                    cycles=1
0x94da:  49             rlc A                                    cycles=1
0x94db:  4a             dec A                                    cycles=1
0x94dc:  4a             dec A                                    cycles=1
0x94dd:  4b 4c          push #$4c                                cycles=1
0x94df:  4d             tnz A                                    cycles=1
0x94e0:  4e             swap A                                   cycles=1
0x94e1:  4f             clr A                                    cycles=1
0x94e2:  50             negw X                                   cycles=1
0x94e3:  51             exgw X, Y                                cycles=1
0x94e4:  52 53          sub SP, #$53                             cycles=1
0x94e6:  54             srlw X                                   cycles=2
0x94e7:  55 56 56 57 58 mov $5758, $5656                         cycles=1
0x94ec:  59             rlcw X                                   cycles=2
0x94ed:  5a             decw X                                   cycles=1
0x94ee:  5b 5c          addw SP, #$5c                            cycles=2
0x94f0:  5d             tnzw X                                   cycles=2
0x94f1:  5e             swapw X                                  cycles=1
0x94f2:  5f             clrw X                                   cycles=1
0x94f3:  60 61          neg ($61,X)                              cycles=1
0x94f5:  62             div X, A                                 cycles=2-17
0x94f6:  63 64          cpl ($64,X)                              cycles=1
0x94f8:  64 65          srl ($65,X)                              cycles=1
0x94fa:  66 67          rrc ($67,X)                              cycles=1
0x94fc:  68 69          sll ($69,X)                              cycles=1
0x94fe:  6a 6b          dec ($6b,X)                              cycles=1
0x9500:  6c 6d          inc ($6d,X)                              cycles=1
0x9502:  6e 6f          swap ($6f,X)                             cycles=1
0x9504:  70             neg (X)                                  cycles=1
0x9505:  71             ???                                      cycles=?
0x9506:  72 73          ???                                      cycles=?
0x9508:  74             srl (X)                                  cycles=1
0x9509:  75             ???                                      cycles=?
0x950a:  75             ???                                      cycles=?
0x950b:  76             rrc (X)                                  cycles=1
0x950c:  77             sra (X)                                  cycles=1
0x950d:  78             sll (X)                                  cycles=1
0x950e:  79             rlc (X)                                  cycles=1
0x950f:  7a             dec (X)                                  cycles=1
0x9510:  7b 7c          ld A, ($7c,SP)                           cycles=1
0x9512:  7d             tnz (X)                                  cycles=1
0x9513:  7e             swap (X)                                 cycles=1
0x9514:  7f             clr (X)                                  cycles=1
0x9515:  80             iret                                     cycles=11
0x9516:  81             ret                                      cycles=4
0x9517:  82 83 84 85    int $838485                              cycles=2
0x951b:  85             popw X                                   cycles=2
0x951c:  86             pop CC                                   cycles=1
0x951d:  87             retf                                     cycles=5
0x951e:  88             push A                                   cycles=1
0x951f:  89             pushw X                                  cycles=2
0x9520:  8a             push CC                                  cycles=1
0x9521:  8b             break                                    cycles=1
0x9522:  8c             ccf                                      cycles=1
0x9523:  8d 8e 8f 90    callf $8e8f90                            cycles=5
0x9527:  91 92          ???                                      cycles=?
0x9529:  93             ldw X, Y                                 cycles=1
0x952a:  94             ldw SP, X                                cycles=1
0x952b:  95             ld XH, A                                 cycles=1
0x952c:  96             ldw X, SP                                cycles=1
0x952d:  96             ldw X, SP                                cycles=1
0x952e:  97             ld XL, A                                 cycles=1
0x952f:  98             rcf                                      cycles=1
0x9530:  99             scf                                      cycles=1
0x9531:  9a             rim                                      cycles=1
0x9532:  9b             sim                                      cycles=1
0x9533:  9c             rvf                                      cycles=5
0x9534:  9d             nop                                      cycles=1
0x9535:  9e             ld A, XH                                 cycles=1
0x9536:  9f             ld A, XL                                 cycles=1
0x9537:  a0 a1          sub A, #$a1                              cycles=1
0x9539:  a2 a3          sbc A, #$a3                              cycles=1
0x953b:  a4 a4          and A, #$a4                              cycles=1
0x953d:  a5 a6          bcp A, #$a6                              cycles=1
0x953f:  a7 a8 a9 aa    ldf ($a8a9aa,X),A                        cycles=1
0x9543:  ab ac          add A, #$ac                              cycles=1
0x9545:  ad ae          callr $94f5  (offset=-82)                cycles=4
0x9547:  af b0 b0 b1    ldf A, ($b0b0b1,X)                       cycles=1
0x954b:  b2 b3          sbc A, $b3                               cycles=1
0x954d:  b4 b5          and A, $b5                               cycles=1
0x954f:  b6 b7          ld A, $b7                                cycles=1
0x9551:  b8 b9          xor A, $b9                               cycles=1
0x9553:  ba ba          or A, $ba                                cycles=1
0x9555:  bb bc          add A, $bc                               cycles=1
0x9557:  bd be bf c0    ldf $bebfc0,A                            cycles=1
0x955b:  c1 c2 c3       cp A, $c2c3                              cycles=1
0x955e:  c3 c4 c5       cpw X, $c4c5                             cycles=2
0x9561:  c6 c7 c8       ld A, $c7c8                              cycles=1
0x9564:  c9 ca cb       adc A, $cacb                             cycles=1
0x9567:  cb cc cd       add A, $cccd                             cycles=1
0x956a:  ce cf d0       ldw X, $cfd0                             cycles=2
0x956d:  d1 d2 d2       cp A, ($d2d2,X)                          cycles=1
0x9570:  d3 d4 d5       cpw Y, ($d4d5,X)                         cycles=2
0x9573:  d6 d7 d8       ld A, ($d7d8,X)                          cycles=1
0x9576:  d8 d9 da       xor A, ($d9da,X)                         cycles=1
0x9579:  db dc dd       add A, ($dcdd,X)                         cycles=1
0x957c:  de de df       ldw X, ($dedf,X)                         cycles=2
0x957f:  e0 e1          sub A, ($e1,X)                           cycles=1
0x9581:  e2 e3          sbc A, ($e3,X)                           cycles=1
0x9583:  e3 e4          cpw Y, ($e4,X)                           cycles=2
0x9585:  e5 e6          bcp A, ($e6,X)                           cycles=1
0x9587:  e7 e8          ld ($e8,X),A                             cycles=1
0x9589:  e8 e9          xor A, ($e9,X)                           cycles=1
0x958b:  ea ea          or A, ($ea,X)                            cycles=1
0x958d:  ea ea          or A, ($ea,X)                            cycles=1
0x958f:  eb eb          add A, ($eb,X)                           cycles=1
0x9591:  eb eb          add A, ($eb,X)                           cycles=1
0x9593:  ec ec          jp ($ec,X)                               cycles=1
0x9595:  ec ec          jp ($ec,X)                               cycles=1
0x9597:  ed ed          call ($ed,X)                             cycles=4
0x9599:  ed ed          call ($ed,X)                             cycles=4
0x959b:  ee ee          ldw X, ($ee,X)                           cycles=2
0x959d:  ee ee          ldw X, ($ee,X)                           cycles=2
0x959f:  ef ef          ldw ($ef,X),Y                            cycles=2
0x95a1:  ef ef          ldw ($ef,X),Y                            cycles=2
0x95a3:  ef f0          ldw ($f0,X),Y                            cycles=2
0x95a5:  f0             sub A, (X)                               cycles=1
0x95a6:  f0             sub A, (X)                               cycles=1
0x95a7:  f0             sub A, (X)                               cycles=1
0x95a8:  f0             sub A, (X)                               cycles=1
0x95a9:  f1             cp A, (X)                                cycles=1
0x95aa:  f1             cp A, (X)                                cycles=1
0x95ab:  f1             cp A, (X)                                cycles=1
0x95ac:  f1             cp A, (X)                                cycles=1
0x95ad:  f2             sbc A, (X)                               cycles=1
0x95ae:  f2             sbc A, (X)                               cycles=1
0x95af:  f2             sbc A, (X)                               cycles=1
0x95b0:  f2             sbc A, (X)                               cycles=1
0x95b1:  f2             sbc A, (X)                               cycles=1
0x95b2:  f2             sbc A, (X)                               cycles=1
0x95b3:  f3             cpw Y, (X)                               cycles=2
0x95b4:  f3             cpw Y, (X)                               cycles=2
0x95b5:  f3             cpw Y, (X)                               cycles=2
0x95b6:  f3             cpw Y, (X)                               cycles=2
0x95b7:  f3             cpw Y, (X)                               cycles=2
0x95b8:  f4             and A, (X)                               cycles=1
0x95b9:  f4             and A, (X)                               cycles=1
0x95ba:  f4             and A, (X)                               cycles=1
0x95bb:  f4             and A, (X)                               cycles=1
0x95bc:  f4             and A, (X)                               cycles=1
0x95bd:  f4             and A, (X)                               cycles=1
0x95be:  f5             bcp A, (X)                               cycles=1
0x95bf:  f5             bcp A, (X)                               cycles=1
0x95c0:  f5             bcp A, (X)                               cycles=1
0x95c1:  f5             bcp A, (X)                               cycles=1
0x95c2:  f5             bcp A, (X)                               cycles=1
0x95c3:  f5             bcp A, (X)                               cycles=1
0x95c4:  f5             bcp A, (X)                               cycles=1
0x95c5:  f6             ld A, (X)                                cycles=1
0x95c6:  f6             ld A, (X)                                cycles=1
0x95c7:  f6             ld A, (X)                                cycles=1
0x95c8:  f6             ld A, (X)                                cycles=1
0x95c9:  f6             ld A, (X)                                cycles=1
0x95ca:  f6             ld A, (X)                                cycles=1
0x95cb:  f6             ld A, (X)                                cycles=1
0x95cc:  f7             ld (X),A                                 cycles=1
0x95cd:  f7             ld (X),A                                 cycles=1
0x95ce:  f7             ld (X),A                                 cycles=1
0x95cf:  f7             ld (X),A                                 cycles=1
0x95d0:  f7             ld (X),A                                 cycles=1
0x95d1:  f7             ld (X),A                                 cycles=1
0x95d2:  f7             ld (X),A                                 cycles=1
0x95d3:  f7             ld (X),A                                 cycles=1
0x95d4:  f7             ld (X),A                                 cycles=1
0x95d5:  f8             xor A, (X)                               cycles=1
0x95d6:  f8             xor A, (X)                               cycles=1
0x95d7:  f8             xor A, (X)                               cycles=1
0x95d8:  f8             xor A, (X)                               cycles=1
0x95d9:  f8             xor A, (X)                               cycles=1
0x95da:  f8             xor A, (X)                               cycles=1
0x95db:  f8             xor A, (X)                               cycles=1
0x95dc:  f8             xor A, (X)                               cycles=1
0x95dd:  f8             xor A, (X)                               cycles=1
0x95de:  f8             xor A, (X)                               cycles=1
0x95df:  f9             adc A, (X)                               cycles=1
0x95e0:  f9             adc A, (X)                               cycles=1
0x95e1:  f9             adc A, (X)                               cycles=1
0x95e2:  f9             adc A, (X)                               cycles=1
0x95e3:  f9             adc A, (X)                               cycles=1
0x95e4:  f9             adc A, (X)                               cycles=1
0x95e5:  f9             adc A, (X)                               cycles=1
0x95e6:  f9             adc A, (X)                               cycles=1
0x95e7:  f9             adc A, (X)                               cycles=1
0x95e8:  f9             adc A, (X)                               cycles=1
0x95e9:  f9             adc A, (X)                               cycles=1
0x95ea:  f9             adc A, (X)                               cycles=1
0x95eb:  f9             adc A, (X)                               cycles=1
0x95ec:  f9             adc A, (X)                               cycles=1
0x95ed:  f9             adc A, (X)                               cycles=1
0x95ee:  fa             or A, (X)                                cycles=1
0x95ef:  fa             or A, (X)                                cycles=1
0x95f0:  fa             or A, (X)                                cycles=1
0x95f1:  fa             or A, (X)                                cycles=1
0x95f2:  fa             or A, (X)                                cycles=1
0x95f3:  fa             or A, (X)                                cycles=1
0x95f4:  fa             or A, (X)                                cycles=1
0x95f5:  fa             or A, (X)                                cycles=1
0x95f6:  fa             or A, (X)                                cycles=1
0x95f7:  fa             or A, (X)                                cycles=1
0x95f8:  fa             or A, (X)                                cycles=1
0x95f9:  fa             or A, (X)                                cycles=1
0x95fa:  fa             or A, (X)                                cycles=1
0x95fb:  fa             or A, (X)                                cycles=1
0x95fc:  fa             or A, (X)                                cycles=1
0x95fd:  fa             or A, (X)                                cycles=1
0x95fe:  fa             or A, (X)                                cycles=1
0x95ff:  fa             or A, (X)                                cycles=1
0x9600:  fa             or A, (X)                                cycles=1
0x9601:  fa             or A, (X)                                cycles=1
0x9602:  fa             or A, (X)                                cycles=1
0x9603:  fa             or A, (X)                                cycles=1
0x9604:  fa             or A, (X)                                cycles=1
0x9605:  fa             or A, (X)                                cycles=1
0x9606:  fa             or A, (X)                                cycles=1
0x9607:  fa             or A, (X)                                cycles=1
0x9608:  fa             or A, (X)                                cycles=1
0x9609:  fa             or A, (X)                                cycles=1
0x960a:  fa             or A, (X)                                cycles=1
0x960b:  fa             or A, (X)                                cycles=1
0x960c:  fa             or A, (X)                                cycles=1
0x960d:  fa             or A, (X)                                cycles=1
0x960e:  fa             or A, (X)                                cycles=1
0x960f:  fa             or A, (X)                                cycles=1
0x9610:  fa             or A, (X)                                cycles=1
0x9611:  fa             or A, (X)                                cycles=1
0x9612:  fa             or A, (X)                                cycles=1
0x9613:  fa             or A, (X)                                cycles=1
0x9614:  fa             or A, (X)                                cycles=1
0x9615:  fa             or A, (X)                                cycles=1
0x9616:  fa             or A, (X)                                cycles=1
0x9617:  f9             adc A, (X)                               cycles=1
0x9618:  f9             adc A, (X)                               cycles=1
0x9619:  f9             adc A, (X)                               cycles=1
0x961a:  f9             adc A, (X)                               cycles=1
0x961b:  f9             adc A, (X)                               cycles=1
0x961c:  f9             adc A, (X)                               cycles=1
0x961d:  f9             adc A, (X)                               cycles=1
0x961e:  f9             adc A, (X)                               cycles=1
0x961f:  f9             adc A, (X)                               cycles=1
0x9620:  f9             adc A, (X)                               cycles=1
0x9621:  f9             adc A, (X)                               cycles=1
0x9622:  f9             adc A, (X)                               cycles=1
0x9623:  f9             adc A, (X)                               cycles=1
0x9624:  f9             adc A, (X)                               cycles=1
0x9625:  f9             adc A, (X)                               cycles=1
0x9626:  f8             xor A, (X)                               cycles=1
0x9627:  f8             xor A, (X)                               cycles=1
0x9628:  f8             xor A, (X)                               cycles=1
0x9629:  f8             xor A, (X)                               cycles=1
0x962a:  f8             xor A, (X)                               cycles=1
0x962b:  f8             xor A, (X)                               cycles=1
0x962c:  f8             xor A, (X)                               cycles=1
0x962d:  f8             xor A, (X)                               cycles=1
0x962e:  f8             xor A, (X)                               cycles=1
0x962f:  f8             xor A, (X)                               cycles=1
0x9630:  f7             ld (X),A                                 cycles=1
0x9631:  f7             ld (X),A                                 cycles=1
0x9632:  f7             ld (X),A                                 cycles=1
0x9633:  f7             ld (X),A                                 cycles=1
0x9634:  f7             ld (X),A                                 cycles=1
0x9635:  f7             ld (X),A                                 cycles=1
0x9636:  f7             ld (X),A                                 cycles=1
0x9637:  f7             ld (X),A                                 cycles=1
0x9638:  f7             ld (X),A                                 cycles=1
0x9639:  f6             ld A, (X)                                cycles=1
0x963a:  f6             ld A, (X)                                cycles=1
0x963b:  f6             ld A, (X)                                cycles=1
0x963c:  f6             ld A, (X)                                cycles=1
0x963d:  f6             ld A, (X)                                cycles=1
0x963e:  f6             ld A, (X)                                cycles=1
0x963f:  f6             ld A, (X)                                cycles=1
0x9640:  f5             bcp A, (X)                               cycles=1
0x9641:  f5             bcp A, (X)                               cycles=1
0x9642:  f5             bcp A, (X)                               cycles=1
0x9643:  f5             bcp A, (X)                               cycles=1
0x9644:  f5             bcp A, (X)                               cycles=1
0x9645:  f5             bcp A, (X)                               cycles=1
0x9646:  f5             bcp A, (X)                               cycles=1
0x9647:  f4             and A, (X)                               cycles=1
0x9648:  f4             and A, (X)                               cycles=1
0x9649:  f4             and A, (X)                               cycles=1
0x964a:  f4             and A, (X)                               cycles=1
0x964b:  f4             and A, (X)                               cycles=1
0x964c:  f4             and A, (X)                               cycles=1
0x964d:  f3             cpw Y, (X)                               cycles=2
0x964e:  f3             cpw Y, (X)                               cycles=2
0x964f:  f3             cpw Y, (X)                               cycles=2
0x9650:  f3             cpw Y, (X)                               cycles=2
0x9651:  f3             cpw Y, (X)                               cycles=2
0x9652:  f2             sbc A, (X)                               cycles=1
0x9653:  f2             sbc A, (X)                               cycles=1
0x9654:  f2             sbc A, (X)                               cycles=1
0x9655:  f2             sbc A, (X)                               cycles=1
0x9656:  f2             sbc A, (X)                               cycles=1
0x9657:  f2             sbc A, (X)                               cycles=1
0x9658:  f1             cp A, (X)                                cycles=1
0x9659:  f1             cp A, (X)                                cycles=1
0x965a:  f1             cp A, (X)                                cycles=1
0x965b:  f1             cp A, (X)                                cycles=1
0x965c:  f0             sub A, (X)                               cycles=1
0x965d:  f0             sub A, (X)                               cycles=1
0x965e:  f0             sub A, (X)                               cycles=1
0x965f:  f0             sub A, (X)                               cycles=1
0x9660:  f0             sub A, (X)                               cycles=1
0x9661:  ef ef          ldw ($ef,X),Y                            cycles=2
0x9663:  ef ef          ldw ($ef,X),Y                            cycles=2
0x9665:  ef ee          ldw ($ee,X),Y                            cycles=2
0x9667:  ee ee          ldw X, ($ee,X)                           cycles=2
0x9669:  ee ed          ldw X, ($ed,X)                           cycles=2
0x966b:  ed ed          call ($ed,X)                             cycles=4
0x966d:  ed ec          call ($ec,X)                             cycles=4
0x966f:  ec ec          jp ($ec,X)                               cycles=1
0x9671:  ec eb          jp ($eb,X)                               cycles=1
0x9673:  eb eb          add A, ($eb,X)                           cycles=1
0x9675:  eb ea          add A, ($ea,X)                           cycles=1
0x9677:  ea ea          or A, ($ea,X)                            cycles=1
0x9679:  ea e9          or A, ($e9,X)                            cycles=1
0x967b:  ea ea          or A, ($ea,X)                            cycles=1
0x967d:  ea ea          or A, ($ea,X)                            cycles=1
0x967f:  eb eb          add A, ($eb,X)                           cycles=1
0x9681:  eb eb          add A, ($eb,X)                           cycles=1
0x9683:  ec ec          jp ($ec,X)                               cycles=1
0x9685:  ec ec          jp ($ec,X)                               cycles=1
0x9687:  ed ed          call ($ed,X)                             cycles=4
0x9689:  ed ed          call ($ed,X)                             cycles=4
0x968b:  ee ee          ldw X, ($ee,X)                           cycles=2
0x968d:  ee ee          ldw X, ($ee,X)                           cycles=2
0x968f:  ef ef          ldw ($ef,X),Y                            cycles=2
0x9691:  ef ef          ldw ($ef,X),Y                            cycles=2
0x9693:  ef f0          ldw ($f0,X),Y                            cycles=2
0x9695:  f0             sub A, (X)                               cycles=1
0x9696:  f0             sub A, (X)                               cycles=1
0x9697:  f0             sub A, (X)                               cycles=1
0x9698:  f0             sub A, (X)                               cycles=1
0x9699:  f1             cp A, (X)                                cycles=1
0x969a:  f1             cp A, (X)                                cycles=1
0x969b:  f1             cp A, (X)                                cycles=1
0x969c:  f1             cp A, (X)                                cycles=1
0x969d:  f2             sbc A, (X)                               cycles=1
0x969e:  f2             sbc A, (X)                               cycles=1
0x969f:  f2             sbc A, (X)                               cycles=1
0x96a0:  f2             sbc A, (X)                               cycles=1
0x96a1:  f2             sbc A, (X)                               cycles=1
0x96a2:  f2             sbc A, (X)                               cycles=1
0x96a3:  f3             cpw Y, (X)                               cycles=2
0x96a4:  f3             cpw Y, (X)                               cycles=2
0x96a5:  f3             cpw Y, (X)                               cycles=2
0x96a6:  f3             cpw Y, (X)                               cycles=2
0x96a7:  f3             cpw Y, (X)                               cycles=2
0x96a8:  f4             and A, (X)                               cycles=1
0x96a9:  f4             and A, (X)                               cycles=1
0x96aa:  f4             and A, (X)                               cycles=1
0x96ab:  f4             and A, (X)                               cycles=1
0x96ac:  f4             and A, (X)                               cycles=1
0x96ad:  f4             and A, (X)                               cycles=1
0x96ae:  f5             bcp A, (X)                               cycles=1
0x96af:  f5             bcp A, (X)                               cycles=1
0x96b0:  f5             bcp A, (X)                               cycles=1
0x96b1:  f5             bcp A, (X)                               cycles=1
0x96b2:  f5             bcp A, (X)                               cycles=1
0x96b3:  f5             bcp A, (X)                               cycles=1
0x96b4:  f5             bcp A, (X)                               cycles=1
0x96b5:  f6             ld A, (X)                                cycles=1
0x96b6:  f6             ld A, (X)                                cycles=1
0x96b7:  f6             ld A, (X)                                cycles=1
0x96b8:  f6             ld A, (X)                                cycles=1
0x96b9:  f6             ld A, (X)                                cycles=1
0x96ba:  f6             ld A, (X)                                cycles=1
0x96bb:  f6             ld A, (X)                                cycles=1
0x96bc:  f7             ld (X),A                                 cycles=1
0x96bd:  f7             ld (X),A                                 cycles=1
0x96be:  f7             ld (X),A                                 cycles=1
0x96bf:  f7             ld (X),A                                 cycles=1
0x96c0:  f7             ld (X),A                                 cycles=1
0x96c1:  f7             ld (X),A                                 cycles=1
0x96c2:  f7             ld (X),A                                 cycles=1
0x96c3:  f7             ld (X),A                                 cycles=1
0x96c4:  f7             ld (X),A                                 cycles=1
0x96c5:  f8             xor A, (X)                               cycles=1
0x96c6:  f8             xor A, (X)                               cycles=1
0x96c7:  f8             xor A, (X)                               cycles=1
0x96c8:  f8             xor A, (X)                               cycles=1
0x96c9:  f8             xor A, (X)                               cycles=1
0x96ca:  f8             xor A, (X)                               cycles=1
0x96cb:  f8             xor A, (X)                               cycles=1
0x96cc:  f8             xor A, (X)                               cycles=1
0x96cd:  f8             xor A, (X)                               cycles=1
0x96ce:  f8             xor A, (X)                               cycles=1
0x96cf:  f9             adc A, (X)                               cycles=1
0x96d0:  f9             adc A, (X)                               cycles=1
0x96d1:  f9             adc A, (X)                               cycles=1
0x96d2:  f9             adc A, (X)                               cycles=1
0x96d3:  f9             adc A, (X)                               cycles=1
0x96d4:  f9             adc A, (X)                               cycles=1
0x96d5:  f9             adc A, (X)                               cycles=1
0x96d6:  f9             adc A, (X)                               cycles=1
0x96d7:  f9             adc A, (X)                               cycles=1
0x96d8:  f9             adc A, (X)                               cycles=1
0x96d9:  f9             adc A, (X)                               cycles=1
0x96da:  f9             adc A, (X)                               cycles=1
0x96db:  f9             adc A, (X)                               cycles=1
0x96dc:  f9             adc A, (X)                               cycles=1
0x96dd:  f9             adc A, (X)                               cycles=1
0x96de:  fa             or A, (X)                                cycles=1
0x96df:  fa             or A, (X)                                cycles=1
0x96e0:  fa             or A, (X)                                cycles=1
0x96e1:  fa             or A, (X)                                cycles=1
0x96e2:  fa             or A, (X)                                cycles=1
0x96e3:  fa             or A, (X)                                cycles=1
0x96e4:  fa             or A, (X)                                cycles=1
0x96e5:  fa             or A, (X)                                cycles=1
0x96e6:  fa             or A, (X)                                cycles=1
0x96e7:  fa             or A, (X)                                cycles=1
0x96e8:  fa             or A, (X)                                cycles=1
0x96e9:  fa             or A, (X)                                cycles=1
0x96ea:  fa             or A, (X)                                cycles=1
0x96eb:  fa             or A, (X)                                cycles=1
0x96ec:  fa             or A, (X)                                cycles=1
0x96ed:  fa             or A, (X)                                cycles=1
0x96ee:  fa             or A, (X)                                cycles=1
0x96ef:  fa             or A, (X)                                cycles=1
0x96f0:  fa             or A, (X)                                cycles=1
0x96f1:  fa             or A, (X)                                cycles=1
0x96f2:  fa             or A, (X)                                cycles=1
0x96f3:  fa             or A, (X)                                cycles=1
0x96f4:  fa             or A, (X)                                cycles=1
0x96f5:  fa             or A, (X)                                cycles=1
0x96f6:  fa             or A, (X)                                cycles=1
0x96f7:  fa             or A, (X)                                cycles=1
0x96f8:  fa             or A, (X)                                cycles=1
0x96f9:  fa             or A, (X)                                cycles=1
0x96fa:  fa             or A, (X)                                cycles=1
0x96fb:  fa             or A, (X)                                cycles=1
0x96fc:  fa             or A, (X)                                cycles=1
0x96fd:  fa             or A, (X)                                cycles=1
0x96fe:  fa             or A, (X)                                cycles=1
0x96ff:  fa             or A, (X)                                cycles=1
0x9700:  fa             or A, (X)                                cycles=1
0x9701:  fa             or A, (X)                                cycles=1
0x9702:  fa             or A, (X)                                cycles=1
0x9703:  fa             or A, (X)                                cycles=1
0x9704:  fa             or A, (X)                                cycles=1
0x9705:  fa             or A, (X)                                cycles=1
0x9706:  fa             or A, (X)                                cycles=1
0x9707:  f9             adc A, (X)                               cycles=1
0x9708:  f9             adc A, (X)                               cycles=1
0x9709:  f9             adc A, (X)                               cycles=1
0x970a:  f9             adc A, (X)                               cycles=1
0x970b:  f9             adc A, (X)                               cycles=1
0x970c:  f9             adc A, (X)                               cycles=1
0x970d:  f9             adc A, (X)                               cycles=1
0x970e:  f9             adc A, (X)                               cycles=1
0x970f:  f9             adc A, (X)                               cycles=1
0x9710:  f9             adc A, (X)                               cycles=1
0x9711:  f9             adc A, (X)                               cycles=1
0x9712:  f9             adc A, (X)                               cycles=1
0x9713:  f9             adc A, (X)                               cycles=1
0x9714:  f9             adc A, (X)                               cycles=1
0x9715:  f9             adc A, (X)                               cycles=1
0x9716:  f8             xor A, (X)                               cycles=1
0x9717:  f8             xor A, (X)                               cycles=1
0x9718:  f8             xor A, (X)                               cycles=1
0x9719:  f8             xor A, (X)                               cycles=1
0x971a:  f8             xor A, (X)                               cycles=1
0x971b:  f8             xor A, (X)                               cycles=1
0x971c:  f8             xor A, (X)                               cycles=1
0x971d:  f8             xor A, (X)                               cycles=1
0x971e:  f8             xor A, (X)                               cycles=1
0x971f:  f8             xor A, (X)                               cycles=1
0x9720:  f7             ld (X),A                                 cycles=1
0x9721:  f7             ld (X),A                                 cycles=1
0x9722:  f7             ld (X),A                                 cycles=1
0x9723:  f7             ld (X),A                                 cycles=1
0x9724:  f7             ld (X),A                                 cycles=1
0x9725:  f7             ld (X),A                                 cycles=1
0x9726:  f7             ld (X),A                                 cycles=1
0x9727:  f7             ld (X),A                                 cycles=1
0x9728:  f7             ld (X),A                                 cycles=1
0x9729:  f6             ld A, (X)                                cycles=1
0x972a:  f6             ld A, (X)                                cycles=1
0x972b:  f6             ld A, (X)                                cycles=1
0x972c:  f6             ld A, (X)                                cycles=1
0x972d:  f6             ld A, (X)                                cycles=1
0x972e:  f6             ld A, (X)                                cycles=1
0x972f:  f6             ld A, (X)                                cycles=1
0x9730:  f5             bcp A, (X)                               cycles=1
0x9731:  f5             bcp A, (X)                               cycles=1
0x9732:  f5             bcp A, (X)                               cycles=1
0x9733:  f5             bcp A, (X)                               cycles=1
0x9734:  f5             bcp A, (X)                               cycles=1
0x9735:  f5             bcp A, (X)                               cycles=1
0x9736:  f5             bcp A, (X)                               cycles=1
0x9737:  f4             and A, (X)                               cycles=1
0x9738:  f4             and A, (X)                               cycles=1
0x9739:  f4             and A, (X)                               cycles=1
0x973a:  f4             and A, (X)                               cycles=1
0x973b:  f4             and A, (X)                               cycles=1
0x973c:  f4             and A, (X)                               cycles=1
0x973d:  f3             cpw Y, (X)                               cycles=2
0x973e:  f3             cpw Y, (X)                               cycles=2
0x973f:  f3             cpw Y, (X)                               cycles=2
0x9740:  f3             cpw Y, (X)                               cycles=2
0x9741:  f3             cpw Y, (X)                               cycles=2
0x9742:  f2             sbc A, (X)                               cycles=1
0x9743:  f2             sbc A, (X)                               cycles=1
0x9744:  f2             sbc A, (X)                               cycles=1
0x9745:  f2             sbc A, (X)                               cycles=1
0x9746:  f2             sbc A, (X)                               cycles=1
0x9747:  f2             sbc A, (X)                               cycles=1
0x9748:  f1             cp A, (X)                                cycles=1
0x9749:  f1             cp A, (X)                                cycles=1
0x974a:  f1             cp A, (X)                                cycles=1
0x974b:  f1             cp A, (X)                                cycles=1
0x974c:  f0             sub A, (X)                               cycles=1
0x974d:  f0             sub A, (X)                               cycles=1
0x974e:  f0             sub A, (X)                               cycles=1
0x974f:  f0             sub A, (X)                               cycles=1
0x9750:  f0             sub A, (X)                               cycles=1
0x9751:  ef ef          ldw ($ef,X),Y                            cycles=2
0x9753:  ef ef          ldw ($ef,X),Y                            cycles=2
0x9755:  ef ee          ldw ($ee,X),Y                            cycles=2
0x9757:  ee ee          ldw X, ($ee,X)                           cycles=2
0x9759:  ee ed          ldw X, ($ed,X)                           cycles=2
0x975b:  ed ed          call ($ed,X)                             cycles=4
0x975d:  ed ec          call ($ec,X)                             cycles=4
0x975f:  ec ec          jp ($ec,X)                               cycles=1
0x9761:  ec eb          jp ($eb,X)                               cycles=1
0x9763:  eb eb          add A, ($eb,X)                           cycles=1
0x9765:  eb ea          add A, ($ea,X)                           cycles=1
0x9767:  ea ea          or A, ($ea,X)                            cycles=1
0x9769:  ea e9          or A, ($e9,X)                            cycles=1
0x976b:  e8 e8          xor A, ($e8,X)                           cycles=1
0x976d:  e7 e6          ld ($e6,X),A                             cycles=1
0x976f:  e5 e4          bcp A, ($e4,X)                           cycles=1
0x9771:  e3 e3          cpw Y, ($e3,X)                           cycles=2
0x9773:  e2 e1          sbc A, ($e1,X)                           cycles=1
0x9775:  e0 df          sub A, ($df,X)                           cycles=1
0x9777:  de de dd       ldw X, ($dedd,X)                         cycles=2
0x977a:  dc db da       jp ($dbda,X)                             cycles=1
0x977d:  d9 d8 d8       adc A, ($d8d8,X)                         cycles=1
0x9780:  d7 d6 d5       ld ($d6d5,X),A                           cycles=1
0x9783:  d4 d3 d2       and A, ($d3d2,X)                         cycles=1
0x9786:  d2 d1 d0       sbc A, ($d1d0,X)                         cycles=1
0x9789:  cf ce cd       ldw $cecd,X                              cycles=2
0x978c:  cc cb cb       jp $cbcb                                 cycles=1
0x978f:  ca c9 c8       or A, $c9c8                              cycles=1
0x9792:  c7 c6 c5       ld $c6c5,A                               cycles=1
0x9795:  c4 c3 c3       and A, $c3c3                             cycles=1
0x9798:  c2 c1 c0       sbc A, $c1c0                             cycles=1
0x979b:  bf be          ldw $be,X                                cycles=2
0x979d:  bd bc bb ba    ldf $bcbbba,A                            cycles=1
0x97a1:  ba b9          or A, $b9                                cycles=1
0x97a3:  b8 b7          xor A, $b7                               cycles=1
0x97a5:  b6 b5          ld A, $b5                                cycles=1
0x97a7:  b4 b3          and A, $b3                               cycles=1
0x97a9:  b2 b1          sbc A, $b1                               cycles=1
0x97ab:  b0 b0          sub A, $b0                               cycles=1
0x97ad:  af ae ad ac    ldf A, ($aeadac,X)                       cycles=1
0x97b1:  ab aa          add A, #$aa                              cycles=1
0x97b3:  a9 a8          adc A, #$a8                              cycles=1
0x97b5:  a7 a6 a5 a4    ldf ($a6a5a4,X),A                        cycles=1
0x97b9:  a4 a3          and A, #$a3                              cycles=1
0x97bb:  a2 a1          sbc A, #$a1                              cycles=1
0x97bd:  a0 9f          sub A, #$9f                              cycles=1
0x97bf:  9e             ld A, XH                                 cycles=1
0x97c0:  9d             nop                                      cycles=1
0x97c1:  9c             rvf                                      cycles=5
0x97c2:  9b             sim                                      cycles=1
0x97c3:  9a             rim                                      cycles=1
0x97c4:  99             scf                                      cycles=1
0x97c5:  98             rcf                                      cycles=1
0x97c6:  97             ld XL, A                                 cycles=1
0x97c7:  96             ldw X, SP                                cycles=1
0x97c8:  96             ldw X, SP                                cycles=1
0x97c9:  95             ld XH, A                                 cycles=1
0x97ca:  94             ldw SP, X                                cycles=1
0x97cb:  93             ldw X, Y                                 cycles=1
0x97cc:  92 91          ???                                      cycles=?
0x97ce:  90 8f          ???                                      cycles=?
0x97d0:  8e             halt                                     cycles=1
0x97d1:  8d 8c 8b 8a    callf $8c8b8a                            cycles=5
0x97d5:  89             pushw X                                  cycles=2
0x97d6:  88             push A                                   cycles=1
0x97d7:  87             retf                                     cycles=5
0x97d8:  86             pop CC                                   cycles=1
0x97d9:  85             popw X                                   cycles=2
0x97da:  85             popw X                                   cycles=2
0x97db:  84             pop A                                    cycles=1
0x97dc:  83             trap                                     cycles=9
0x97dd:  82 81 80 7f    int $81807f                              cycles=2
0x97e1:  7e             swap (X)                                 cycles=1
0x97e2:  7d             tnz (X)                                  cycles=1
0x97e3:  7c             inc (X)                                  cycles=1
0x97e4:  7b 7a          ld A, ($7a,SP)                           cycles=1
0x97e6:  79             rlc (X)                                  cycles=1
0x97e7:  78             sll (X)                                  cycles=1
0x97e8:  77             sra (X)                                  cycles=1
0x97e9:  76             rrc (X)                                  cycles=1
0x97ea:  75             ???                                      cycles=?
0x97eb:  75             ???                                      cycles=?
0x97ec:  74             srl (X)                                  cycles=1
0x97ed:  73             cpl (X)                                  cycles=1
0x97ee:  72 71          ???                                      cycles=?
0x97f0:  70             neg (X)                                  cycles=1
0x97f1:  6f 6e          clr ($6e,X)                              cycles=1
0x97f3:  6d 6c          tnz ($6c,X)                              cycles=1
0x97f5:  6b 6a          ld ($6a,SP),A                            cycles=1
0x97f7:  69 68          rlc ($68,X)                              cycles=1
0x97f9:  67 66          sra ($66,X)                              cycles=1
0x97fb:  65             divw X, Y                                cycles=2-17
0x97fc:  64 64          srl ($64,X)                              cycles=1
0x97fe:  63 62          cpl ($62,X)                              cycles=1
0x9800:  61             exg A, YL                                cycles=1
0x9801:  60 5f          neg ($5f,X)                              cycles=1
0x9803:  5e             swapw X                                  cycles=1
0x9804:  5d             tnzw X                                   cycles=2
0x9805:  5c             incw X                                   cycles=1
0x9806:  5b 5a          addw SP, #$5a                            cycles=2
0x9808:  59             rlcw X                                   cycles=2
0x9809:  58             sllw X                                   cycles=2
0x980a:  57             sraw X                                   cycles=2
0x980b:  56             rrcw X                                   cycles=2
0x980c:  56             rrcw X                                   cycles=2
0x980d:  55 54 53 52 51 mov $5251, $5453                         cycles=1
0x9812:  50             negw X                                   cycles=1
0x9813:  4f             clr A                                    cycles=1
0x9814:  4e             swap A                                   cycles=1
0x9815:  4d             tnz A                                    cycles=1
0x9816:  4c             inc A                                    cycles=1
0x9817:  4b 4a          push #$4a                                cycles=1
0x9819:  4a             dec A                                    cycles=1
0x981a:  49             rlc A                                    cycles=1
0x981b:  48             sll A                                    cycles=1
0x981c:  47             sra A                                    cycles=1
0x981d:  46             rrc A                                    cycles=1
0x981e:  45 44 43       mov $43, $44                             cycles=1
0x9821:  42             mul X, A                                 cycles=4
0x9822:  41             exg A, XL                                cycles=1
0x9823:  40             neg A                                    cycles=1
0x9824:  40             neg A                                    cycles=1
0x9825:  3f 3e          clr $3e                                  cycles=1
0x9827:  3d 3c          tnz $3c                                  cycles=1
0x9829:  3b 3a 39       push $3a39                               cycles=1
0x982c:  38 37          sll $37                                  cycles=1
0x982e:  37 36          sra $36                                  cycles=1
0x9830:  35 34 33 32    mov $3332, #$34                          cycles=1
0x9834:  31 30 2f       exg A, $302f                             cycles=3
0x9837:  2f 2e          jrslt $9867  (offset=46)                 cycles=1-2
0x9839:  2d 2c          jrsle $9867  (offset=44)                 cycles=1-2
0x983b:  2b 2a          jrmi $9867  (offset=42)                  cycles=1-2
0x983d:  29 28          jrv $9867  (offset=40)                   cycles=1-2
0x983f:  28 27          jrnv $9868  (offset=39)                  cycles=1-2
0x9841:  26 25          jrne $9868  (offset=37)                  cycles=1-2
0x9843:  24 23          jrnc $9868  (offset=35)                  cycles=1-2
0x9845:  22 22          jrugt $9869  (offset=34)                 cycles=1-2
0x9847:  21 20          jrf $9869  (offset=32)                   cycles=1-2
0x9849:  1f 1e          ldw ($1e,SP),X                           cycles=2
0x984b:  1d 1c 1c       subw X, #$1c1c                           cycles=2
0x984e:  1b 1a          add A, ($1a,SP)                          cycles=1
0x9850:  19 18          adc A, ($18,SP)                          cycles=1
0x9852:  17 17          ldw ($17,SP),Y                           cycles=2
0x9854:  16 15          ldw Y, ($15,SP)                          cycles=2
0x9856:  14 13          and A, ($13,SP)                          cycles=1
0x9858:  12 12          sbc A, ($12,SP)                          cycles=1
0x985a:  11 10          cp A, ($10,SP)                           cycles=1
0x985c:  10 10          sub A, ($10,SP)                          cycles=1
0x985e:  10 0f          sub A, ($0f,SP)                          cycles=1
0x9860:  0f 0f          clr ($0f,SP)                             cycles=1
0x9862:  0f 0e          clr ($0e,SP)                             cycles=1
0x9864:  0e 0e          swap ($0e,SP)                            cycles=1
0x9866:  0e 0d          swap ($0d,SP)                            cycles=1
0x9868:  0d 0d          tnz ($0d,SP)                             cycles=1
0x986a:  0d 0c          tnz ($0c,SP)                             cycles=1
0x986c:  0c 0c          inc ($0c,SP)                             cycles=1
0x986e:  0c 0b          inc ($0b,SP)                             cycles=1
0x9870:  0b             ???                                      cycles=?
0x9871:  0b             ???                                      cycles=?
0x9872:  0b             ???                                      cycles=?
0x9873:  0b             ???                                      cycles=?
0x9874:  0a 0a          dec ($0a,SP)                             cycles=1
0x9876:  0a 0a          dec ($0a,SP)                             cycles=1
0x9878:  0a 09          dec ($09,SP)                             cycles=1
0x987a:  09 09          rlc ($09,SP)                             cycles=1
0x987c:  09 08          rlc ($08,SP)                             cycles=1
0x987e:  08 08          sll ($08,SP)                             cycles=1
0x9880:  08 08          sll ($08,SP)                             cycles=1
0x9882:  08 07          sll ($07,SP)                             cycles=1
0x9884:  07 07          sra ($07,SP)                             cycles=1
0x9886:  07 07          sra ($07,SP)                             cycles=1
0x9888:  06 06          rrc ($06,SP)                             cycles=1
0x988a:  06 06          rrc ($06,SP)                             cycles=1
0x988c:  06 06          rrc ($06,SP)                             cycles=1
0x988e:  05             ???                                      cycles=?
0x988f:  05             ???                                      cycles=?
0x9890:  05             ???                                      cycles=?
0x9891:  05             ???                                      cycles=?
0x9892:  05             ???                                      cycles=?
0x9893:  05             ???                                      cycles=?
0x9894:  05             ???                                      cycles=?
0x9895:  04 04          srl ($04,SP)                             cycles=1
0x9897:  04 04          srl ($04,SP)                             cycles=1
0x9899:  04 04          srl ($04,SP)                             cycles=1
0x989b:  04 03          srl ($03,SP)                             cycles=1
0x989d:  03 03          cpl ($03,SP)                             cycles=1
0x989f:  03 03          cpl ($03,SP)                             cycles=1
0x98a1:  03 03          cpl ($03,SP)                             cycles=1
0x98a3:  03 03          cpl ($03,SP)                             cycles=1
0x98a5:  02             rlwa X, A                                cycles=1
0x98a6:  02             rlwa X, A                                cycles=1
0x98a7:  02             rlwa X, A                                cycles=1
0x98a8:  02             rlwa X, A                                cycles=1
0x98a9:  02             rlwa X, A                                cycles=1
0x98aa:  02             rlwa X, A                                cycles=1
0x98ab:  02             rlwa X, A                                cycles=1
0x98ac:  02             rlwa X, A                                cycles=1
0x98ad:  02             rlwa X, A                                cycles=1
0x98ae:  02             rlwa X, A                                cycles=1
0x98af:  01             rrwa X, A                                cycles=1
0x98b0:  01             rrwa X, A                                cycles=1
0x98b1:  01             rrwa X, A                                cycles=1
0x98b2:  01             rrwa X, A                                cycles=1
0x98b3:  01             rrwa X, A                                cycles=1
0x98b4:  01             rrwa X, A                                cycles=1
0x98b5:  01             rrwa X, A                                cycles=1
0x98b6:  01             rrwa X, A                                cycles=1
0x98b7:  01             rrwa X, A                                cycles=1
0x98b8:  01             rrwa X, A                                cycles=1
0x98b9:  01             rrwa X, A                                cycles=1
0x98ba:  01             rrwa X, A                                cycles=1
0x98bb:  01             rrwa X, A                                cycles=1
0x98bc:  01             rrwa X, A                                cycles=1
0x98bd:  01             rrwa X, A                                cycles=1
0x98be:  00 00          neg ($00,SP)                             cycles=1
0x98c0:  00 00          neg ($00,SP)                             cycles=1
0x98c2:  00 00          neg ($00,SP)                             cycles=1
0x98c4:  00 00          neg ($00,SP)                             cycles=1
0x98c6:  00 00          neg ($00,SP)                             cycles=1
0x98c8:  00 00          neg ($00,SP)                             cycles=1
0x98ca:  00 00          neg ($00,SP)                             cycles=1
0x98cc:  00 00          neg ($00,SP)                             cycles=1
0x98ce:  00 00          neg ($00,SP)                             cycles=1
0x98d0:  00 00          neg ($00,SP)                             cycles=1
0x98d2:  00 00          neg ($00,SP)                             cycles=1
0x98d4:  00 00          neg ($00,SP)                             cycles=1
0x98d6:  00 00          neg ($00,SP)                             cycles=1
0x98d8:  00 00          neg ($00,SP)                             cycles=1
0x98da:  00 00          neg ($00,SP)                             cycles=1
0x98dc:  00 00          neg ($00,SP)                             cycles=1
0x98de:  00 00          neg ($00,SP)                             cycles=1
0x98e0:  00 00          neg ($00,SP)                             cycles=1
0x98e2:  00 00          neg ($00,SP)                             cycles=1
0x98e4:  00 00          neg ($00,SP)                             cycles=1
0x98e6:  00 01          neg ($01,SP)                             cycles=1
0x98e8:  01             rrwa X, A                                cycles=1
0x98e9:  01             rrwa X, A                                cycles=1
0x98ea:  01             rrwa X, A                                cycles=1
0x98eb:  01             rrwa X, A                                cycles=1
0x98ec:  01             rrwa X, A                                cycles=1
0x98ed:  01             rrwa X, A                                cycles=1
0x98ee:  01             rrwa X, A                                cycles=1
0x98ef:  01             rrwa X, A                                cycles=1
0x98f0:  01             rrwa X, A                                cycles=1
0x98f1:  01             rrwa X, A                                cycles=1
0x98f2:  01             rrwa X, A                                cycles=1
0x98f3:  01             rrwa X, A                                cycles=1
0x98f4:  01             rrwa X, A                                cycles=1
0x98f5:  01             rrwa X, A                                cycles=1
0x98f6:  02             rlwa X, A                                cycles=1
0x98f7:  02             rlwa X, A                                cycles=1
0x98f8:  02             rlwa X, A                                cycles=1
0x98f9:  02             rlwa X, A                                cycles=1
0x98fa:  02             rlwa X, A                                cycles=1
0x98fb:  02             rlwa X, A                                cycles=1
0x98fc:  02             rlwa X, A                                cycles=1
0x98fd:  02             rlwa X, A                                cycles=1
0x98fe:  02             rlwa X, A                                cycles=1
0x98ff:  02             rlwa X, A                                cycles=1
0x9900:  03 03          cpl ($03,SP)                             cycles=1
0x9902:  03 03          cpl ($03,SP)                             cycles=1
0x9904:  03 03          cpl ($03,SP)                             cycles=1
0x9906:  03 03          cpl ($03,SP)                             cycles=1
0x9908:  03 04          cpl ($04,SP)                             cycles=1
0x990a:  04 04          srl ($04,SP)                             cycles=1
0x990c:  04 04          srl ($04,SP)                             cycles=1
0x990e:  04 04          srl ($04,SP)                             cycles=1
0x9910:  05             ???                                      cycles=?
0x9911:  05             ???                                      cycles=?
0x9912:  05             ???                                      cycles=?
0x9913:  05             ???                                      cycles=?
0x9914:  05             ???                                      cycles=?
0x9915:  05             ???                                      cycles=?
0x9916:  05             ???                                      cycles=?
0x9917:  06 06          rrc ($06,SP)                             cycles=1
0x9919:  06 06          rrc ($06,SP)                             cycles=1
0x991b:  06 06          rrc ($06,SP)                             cycles=1
0x991d:  07 07          sra ($07,SP)                             cycles=1
0x991f:  07 07          sra ($07,SP)                             cycles=1
0x9921:  07 08          sra ($08,SP)                             cycles=1
0x9923:  08 08          sll ($08,SP)                             cycles=1
0x9925:  08 08          sll ($08,SP)                             cycles=1
0x9927:  08 09          sll ($09,SP)                             cycles=1
0x9929:  09 09          rlc ($09,SP)                             cycles=1
0x992b:  09 0a          rlc ($0a,SP)                             cycles=1
0x992d:  0a 0a          dec ($0a,SP)                             cycles=1
0x992f:  0a 0a          dec ($0a,SP)                             cycles=1
0x9931:  0b             ???                                      cycles=?
0x9932:  0b             ???                                      cycles=?
0x9933:  0b             ???                                      cycles=?
0x9934:  0b             ???                                      cycles=?
0x9935:  0b             ???                                      cycles=?
0x9936:  0c 0c          inc ($0c,SP)                             cycles=1
0x9938:  0c 0c          inc ($0c,SP)                             cycles=1
0x993a:  0d 0d          tnz ($0d,SP)                             cycles=1
0x993c:  0d 0d          tnz ($0d,SP)                             cycles=1
0x993e:  0e 0e          swap ($0e,SP)                            cycles=1
0x9940:  0e 0e          swap ($0e,SP)                            cycles=1
0x9942:  0f 0f          clr ($0f,SP)                             cycles=1
0x9944:  0f 0f          clr ($0f,SP)                             cycles=1
0x9946:  10 10          sub A, ($10,SP)                          cycles=1
0x9948:  10 10          sub A, ($10,SP)                          cycles=1
0x994a:  11 10          cp A, ($10,SP)                           cycles=1
0x994c:  10 10          sub A, ($10,SP)                          cycles=1
0x994e:  10 0f          sub A, ($0f,SP)                          cycles=1
0x9950:  0f 0f          clr ($0f,SP)                             cycles=1
0x9952:  0f 0e          clr ($0e,SP)                             cycles=1
0x9954:  0e 0e          swap ($0e,SP)                            cycles=1
0x9956:  0e 0d          swap ($0d,SP)                            cycles=1
0x9958:  0d 0d          tnz ($0d,SP)                             cycles=1
0x995a:  0d 0c          tnz ($0c,SP)                             cycles=1
0x995c:  0c 0c          inc ($0c,SP)                             cycles=1
0x995e:  0c 0b          inc ($0b,SP)                             cycles=1
0x9960:  0b             ???                                      cycles=?
0x9961:  0b             ???                                      cycles=?
0x9962:  0b             ???                                      cycles=?
0x9963:  0b             ???                                      cycles=?
0x9964:  0a 0a          dec ($0a,SP)                             cycles=1
0x9966:  0a 0a          dec ($0a,SP)                             cycles=1
0x9968:  0a 09          dec ($09,SP)                             cycles=1
0x996a:  09 09          rlc ($09,SP)                             cycles=1
0x996c:  09 08          rlc ($08,SP)                             cycles=1
0x996e:  08 08          sll ($08,SP)                             cycles=1
0x9970:  08 08          sll ($08,SP)                             cycles=1
0x9972:  08 07          sll ($07,SP)                             cycles=1
0x9974:  07 07          sra ($07,SP)                             cycles=1
0x9976:  07 07          sra ($07,SP)                             cycles=1
0x9978:  06 06          rrc ($06,SP)                             cycles=1
0x997a:  06 06          rrc ($06,SP)                             cycles=1
0x997c:  06 06          rrc ($06,SP)                             cycles=1
0x997e:  05             ???                                      cycles=?
0x997f:  05             ???                                      cycles=?
0x9980:  05             ???                                      cycles=?
0x9981:  05             ???                                      cycles=?
0x9982:  05             ???                                      cycles=?
0x9983:  05             ???                                      cycles=?
0x9984:  05             ???                                      cycles=?
0x9985:  04 04          srl ($04,SP)                             cycles=1
0x9987:  04 04          srl ($04,SP)                             cycles=1
0x9989:  04 04          srl ($04,SP)                             cycles=1
0x998b:  04 03          srl ($03,SP)                             cycles=1
0x998d:  03 03          cpl ($03,SP)                             cycles=1
0x998f:  03 03          cpl ($03,SP)                             cycles=1
0x9991:  03 03          cpl ($03,SP)                             cycles=1
0x9993:  03 03          cpl ($03,SP)                             cycles=1
0x9995:  02             rlwa X, A                                cycles=1
0x9996:  02             rlwa X, A                                cycles=1
0x9997:  02             rlwa X, A                                cycles=1
0x9998:  02             rlwa X, A                                cycles=1
0x9999:  02             rlwa X, A                                cycles=1
0x999a:  02             rlwa X, A                                cycles=1
0x999b:  02             rlwa X, A                                cycles=1
0x999c:  02             rlwa X, A                                cycles=1
0x999d:  02             rlwa X, A                                cycles=1
0x999e:  02             rlwa X, A                                cycles=1
0x999f:  01             rrwa X, A                                cycles=1
0x99a0:  01             rrwa X, A                                cycles=1
0x99a1:  01             rrwa X, A                                cycles=1
0x99a2:  01             rrwa X, A                                cycles=1
0x99a3:  01             rrwa X, A                                cycles=1
0x99a4:  01             rrwa X, A                                cycles=1
0x99a5:  01             rrwa X, A                                cycles=1
0x99a6:  01             rrwa X, A                                cycles=1
0x99a7:  01             rrwa X, A                                cycles=1
0x99a8:  01             rrwa X, A                                cycles=1
0x99a9:  01             rrwa X, A                                cycles=1
0x99aa:  01             rrwa X, A                                cycles=1
0x99ab:  01             rrwa X, A                                cycles=1
0x99ac:  01             rrwa X, A                                cycles=1
0x99ad:  01             rrwa X, A                                cycles=1
0x99ae:  00 00          neg ($00,SP)                             cycles=1
0x99b0:  00 00          neg ($00,SP)                             cycles=1
0x99b2:  00 00          neg ($00,SP)                             cycles=1
0x99b4:  00 00          neg ($00,SP)                             cycles=1
0x99b6:  00 00          neg ($00,SP)                             cycles=1
0x99b8:  00 00          neg ($00,SP)                             cycles=1
0x99ba:  00 00          neg ($00,SP)                             cycles=1
0x99bc:  00 00          neg ($00,SP)                             cycles=1
0x99be:  00 00          neg ($00,SP)                             cycles=1
0x99c0:  00 00          neg ($00,SP)                             cycles=1
0x99c2:  00 00          neg ($00,SP)                             cycles=1
0x99c4:  00 00          neg ($00,SP)                             cycles=1
0x99c6:  00 00          neg ($00,SP)                             cycles=1
0x99c8:  00 00          neg ($00,SP)                             cycles=1
0x99ca:  00 00          neg ($00,SP)                             cycles=1
0x99cc:  00 00          neg ($00,SP)                             cycles=1
0x99ce:  00 00          neg ($00,SP)                             cycles=1
0x99d0:  00 00          neg ($00,SP)                             cycles=1
0x99d2:  00 00          neg ($00,SP)                             cycles=1
0x99d4:  00 00          neg ($00,SP)                             cycles=1
0x99d6:  00 01          neg ($01,SP)                             cycles=1
0x99d8:  01             rrwa X, A                                cycles=1
0x99d9:  01             rrwa X, A                                cycles=1
0x99da:  01             rrwa X, A                                cycles=1
0x99db:  01             rrwa X, A                                cycles=1
0x99dc:  01             rrwa X, A                                cycles=1
0x99dd:  01             rrwa X, A                                cycles=1
0x99de:  01             rrwa X, A                                cycles=1
0x99df:  01             rrwa X, A                                cycles=1
0x99e0:  01             rrwa X, A                                cycles=1
0x99e1:  01             rrwa X, A                                cycles=1
0x99e2:  01             rrwa X, A                                cycles=1
0x99e3:  01             rrwa X, A                                cycles=1
0x99e4:  01             rrwa X, A                                cycles=1
0x99e5:  01             rrwa X, A                                cycles=1
0x99e6:  02             rlwa X, A                                cycles=1
0x99e7:  02             rlwa X, A                                cycles=1
0x99e8:  02             rlwa X, A                                cycles=1
0x99e9:  02             rlwa X, A                                cycles=1
0x99ea:  02             rlwa X, A                                cycles=1
0x99eb:  02             rlwa X, A                                cycles=1
0x99ec:  02             rlwa X, A                                cycles=1
0x99ed:  02             rlwa X, A                                cycles=1
0x99ee:  02             rlwa X, A                                cycles=1
0x99ef:  02             rlwa X, A                                cycles=1
0x99f0:  03 03          cpl ($03,SP)                             cycles=1
0x99f2:  03 03          cpl ($03,SP)                             cycles=1
0x99f4:  03 03          cpl ($03,SP)                             cycles=1
0x99f6:  03 03          cpl ($03,SP)                             cycles=1
0x99f8:  03 04          cpl ($04,SP)                             cycles=1
0x99fa:  04 04          srl ($04,SP)                             cycles=1
0x99fc:  04 04          srl ($04,SP)                             cycles=1
0x99fe:  04 04          srl ($04,SP)                             cycles=1
0x9a00:  05             ???                                      cycles=?
0x9a01:  05             ???                                      cycles=?
0x9a02:  05             ???                                      cycles=?
0x9a03:  05             ???                                      cycles=?
0x9a04:  05             ???                                      cycles=?
0x9a05:  05             ???                                      cycles=?
0x9a06:  05             ???                                      cycles=?
0x9a07:  06 06          rrc ($06,SP)                             cycles=1
0x9a09:  06 06          rrc ($06,SP)                             cycles=1
0x9a0b:  06 06          rrc ($06,SP)                             cycles=1
0x9a0d:  07 07          sra ($07,SP)                             cycles=1
0x9a0f:  07 07          sra ($07,SP)                             cycles=1
0x9a11:  07 08          sra ($08,SP)                             cycles=1
0x9a13:  08 08          sll ($08,SP)                             cycles=1
0x9a15:  08 08          sll ($08,SP)                             cycles=1
0x9a17:  08 09          sll ($09,SP)                             cycles=1
0x9a19:  09 09          rlc ($09,SP)                             cycles=1
0x9a1b:  09 0a          rlc ($0a,SP)                             cycles=1
0x9a1d:  0a 0a          dec ($0a,SP)                             cycles=1
0x9a1f:  0a 0a          dec ($0a,SP)                             cycles=1
0x9a21:  0b             ???                                      cycles=?
0x9a22:  0b             ???                                      cycles=?
0x9a23:  0b             ???                                      cycles=?
0x9a24:  0b             ???                                      cycles=?
0x9a25:  0b             ???                                      cycles=?
0x9a26:  0c 0c          inc ($0c,SP)                             cycles=1
0x9a28:  0c 0c          inc ($0c,SP)                             cycles=1
0x9a2a:  0d 0d          tnz ($0d,SP)                             cycles=1
0x9a2c:  0d 0d          tnz ($0d,SP)                             cycles=1
0x9a2e:  0e 0e          swap ($0e,SP)                            cycles=1
0x9a30:  0e 0e          swap ($0e,SP)                            cycles=1
0x9a32:  0f 0f          clr ($0f,SP)                             cycles=1
0x9a34:  0f 0f          clr ($0f,SP)                             cycles=1
0x9a36:  10 10          sub A, ($10,SP)                          cycles=1
0x9a38:  10 10          sub A, ($10,SP)                          cycles=1
0x9a3a:  00 00          neg ($00,SP)                             cycles=1
0x9a3c:  00 00          neg ($00,SP)                             cycles=1
0x9a3e:  00 00          neg ($00,SP)                             cycles=1
0x9a40:  00 00          neg ($00,SP)                             cycles=1
0x9a42:  00 00          neg ($00,SP)                             cycles=1
0x9a44:  00 00          neg ($00,SP)                             cycles=1
0x9a46:  00 72          neg ($72,SP)                             cycles=1
0x9a48:  10 52          sub A, ($52,SP)                          cycles=1
0x9a4a:  5c             incw X                                   cycles=1
0x9a4b:  72 18 52 5c    bset $525c, #4                           cycles=1
0x9a4f:  72 14 52 5c    bset $525c, #2                           cycles=1
0x9a53:  72 1c 52 5c    bset $525c, #6                           cycles=1
0x9a57:  72 10 52 5d    bset $525d, #0                           cycles=1
0x9a5b:  72 14 52 5d    bset $525d, #2                           cycles=1
0x9a5f:  72 1a 52 57    bset $5257, #5                           cycles=1
0x9a63:  81             ret                                      cycles=4
0x9a64:  81             ret                                      cycles=4
0x9a65:  81             ret                                      cycles=4
0x9a66:  81             ret                                      cycles=4
0x9a67:  72 0d 00 96 23 btjf $96, #6, $9a8f  (offset=35)         cycles=2-3
0x9a6c:  3f c4          clr $c4                                  cycles=1
0x9a6e:  72 1d 00 96    bres $96, #6                             cycles=1
0x9a72:  72 1b 52 50    bres $5250, #5                           cycles=1
0x9a76:  72 17 52 53    bres $5253, #3                           cycles=1
0x9a7a:  72 15 52 53    bres $5253, #2                           cycles=1
0x9a7e:  72 13 52 53    bres $5253, #1                           cycles=1
0x9a82:  72 1f 00 98    bres $98, #7                             cycles=1
0x9a86:  cd 88 ab       call $88ab                               cycles=4
0x9a89:  cd ab 28       call $ab28                               cycles=4
0x9a8c:  cd a9 9b       call $a99b                               cycles=4
0x9a8f:  81             ret                                      cycles=4
0x9a90:  72 0a 00 85 21 btjt $85, #5, $9ab6  (offset=33)         cycles=2-3
0x9a95:  72 04 00 8a 1c btjt $8a, #2, $9ab6  (offset=28)         cycles=2-3
0x9a9a:  a6 10          ld A, #$10                               cycles=1
0x9a9c:  b7 31          ld $31,A                                 cycles=1
0x9a9e:  cd b9 90       call $b990                               cycles=4
0x9aa1:  a6 07          ld A, #$07                               cycles=1
0x9aa3:  cd b9 83       call $b983                               cycles=4
0x9aa6:  c6 54 04       ld A, $5404                              cycles=1
0x9aa9:  97             ld XL, A                                 cycles=1
0x9aaa:  b6 31          ld A, $31                                cycles=1
0x9aac:  a1 10          cp A, #$10                               cycles=1
0x9aae:  26 e0          jrne $9a90  (offset=-32)                 cycles=1-2
0x9ab0:  9f             ld A, XL                                 cycles=1
0x9ab1:  a1 55          cp A, #$55                               cycles=1
0x9ab3:  24 01          jrnc $9ab6  (offset=1)                   cycles=1-2
0x9ab5:  83             trap                                     cycles=9
0x9ab6:  81             ret                                      cycles=4
0x9ab7:  cd 9a ce       call $9ace                               cycles=4
0x9aba:  3c ab          inc $ab                                  cycles=1
0x9abc:  b6 ab          ld A, $ab                                cycles=1
0x9abe:  a1 02          cp A, #$02                               cycles=1
0x9ac0:  25 0b          jrc $9acd  (offset=11)                   cycles=1-2
0x9ac2:  3f ab          clr $ab                                  cycles=1
0x9ac4:  3c ac          inc $ac                                  cycles=1
0x9ac6:  b6 ac          ld A, $ac                                cycles=1
0x9ac8:  a1 06          cp A, #$06                               cycles=1
0x9aca:  25 01          jrc $9acd  (offset=1)                    cycles=1-2
0x9acc:  83             trap                                     cycles=9
0x9acd:  81             ret                                      cycles=4
0x9ace:  72 0c 50 0b 10 btjt $500b, #6, $9ae3  (offset=16)       cycles=2-3
0x9ad3:  b6 6b          ld A, $6b                                cycles=1
0x9ad5:  a1 32          cp A, #$32                               cycles=1
0x9ad7:  27 04          jreq $9add  (offset=4)                   cycles=1-2
0x9ad9:  3c 6b          inc $6b                                  cycles=1
0x9adb:  20 12          jra $9aef  (offset=18)                   cycles=2
0x9add:  72 18 00 92    bset $92, #4                             cycles=1
0x9ae1:  20 0c          jra $9aef  (offset=12)                   cycles=2
0x9ae3:  3d 6b          tnz $6b                                  cycles=1
0x9ae5:  27 04          jreq $9aeb  (offset=4)                   cycles=1-2
0x9ae7:  3a 6b          dec $6b                                  cycles=1
0x9ae9:  20 04          jra $9aef  (offset=4)                    cycles=2
0x9aeb:  72 19 00 92    bres $92, #4                             cycles=1
0x9aef:  cd a0 80       call $a080                               cycles=4
0x9af2:  81             ret                                      cycles=4
0x9af3:  3c e0          inc $e0                                  cycles=1
0x9af5:  b6 e0          ld A, $e0                                cycles=1
0x9af7:  a1 96          cp A, #$96                               cycles=1
0x9af9:  25 30          jrc $9b2b  (offset=48)                   cycles=1-2
0x9afb:  3f e0          clr $e0                                  cycles=1
0x9afd:  b6 22          ld A, $22                                cycles=1
0x9aff:  b1 dc          cp A, $dc                                cycles=1
0x9b01:  25 22          jrc $9b25  (offset=34)                   cycles=1-2
0x9b03:  b0 dc          sub A, $dc                               cycles=1
0x9b05:  b7 da          ld $da,A                                 cycles=1
0x9b07:  b6 da          ld A, $da                                cycles=1
0x9b09:  a1 0a          cp A, #$0a                               cycles=1
0x9b0b:  25 18          jrc $9b25  (offset=24)                   cycles=1-2
0x9b0d:  b6 22          ld A, $22                                cycles=1
0x9b0f:  b1 e1          cp A, $e1                                cycles=1
0x9b11:  25 12          jrc $9b25  (offset=18)                   cycles=1-2
0x9b13:  b0 e1          sub A, $e1                               cycles=1
0x9b15:  a1 1e          cp A, #$1e                               cycles=1
0x9b17:  25 0c          jrc $9b25  (offset=12)                   cycles=1-2
0x9b19:  72 14 00 8a    bset $8a, #2                             cycles=1
0x9b1d:  72 16 00 8a    bset $8a, #3                             cycles=1
0x9b21:  3f 12          clr $12                                  cycles=1
0x9b23:  3f a8          clr $a8                                  cycles=1
0x9b25:  b6 22          ld A, $22                                cycles=1
0x9b27:  b7 dc          ld $dc,A                                 cycles=1
0x9b29:  3f e0          clr $e0                                  cycles=1
0x9b2b:  81             ret                                      cycles=4
0x9b2c:  b6 20          ld A, $20                                cycles=1
0x9b2e:  b7 dc          ld $dc,A                                 cycles=1
0x9b30:  3f e0          clr $e0                                  cycles=1
0x9b32:  b1 d9          cp A, $d9                                cycles=1
0x9b34:  24 15          jrnc $9b4b  (offset=21)                  cycles=1-2
0x9b36:  3d d6          tnz $d6                                  cycles=1
0x9b38:  26 05          jrne $9b3f  (offset=5)                   cycles=1-2
0x9b3a:  a6 00          ld A, #$00                               cycles=1
0x9b3c:  b7 d6          ld $d6,A                                 cycles=1
0x9b3e:  81             ret                                      cycles=4
0x9b3f:  3a d6          dec $d6                                  cycles=1
0x9b41:  3d d7          tnz $d7                                  cycles=1
0x9b43:  27 02          jreq $9b47  (offset=2)                   cycles=1-2
0x9b45:  3a d7          dec $d7                                  cycles=1
0x9b47:  81             ret                                      cycles=4
0x9b48:  cc 9b d9       jp $9bd9                                 cycles=1
0x9b4b:  b0 d9          sub A, $d9                               cycles=1
0x9b4d:  72 0e 00 8a 04 btjt $8a, #7, $9b56  (offset=4)          cycles=2-3
0x9b52:  a1 00          cp A, #$00                               cycles=1
0x9b54:  25 e0          jrc $9b36  (offset=-32)                  cycles=1-2
0x9b56:  b1 db          cp A, $db                                cycles=1
0x9b58:  27 08          jreq $9b62  (offset=8)                   cycles=1-2
0x9b5a:  24 04          jrnc $9b60  (offset=4)                   cycles=1-2
0x9b5c:  b7 db          ld $db,A                                 cycles=1
0x9b5e:  20 00          jra $9b60  (offset=0)                    cycles=2
0x9b60:  3c db          inc $db                                  cycles=1
0x9b62:  5f             clrw X                                   cycles=1
0x9b63:  72 08 00 96 2d btjt $96, #4, $9b95  (offset=45)         cycles=2-3
0x9b68:  72 00 00 8c 32 btjt $8c, #0, $9b9f  (offset=50)         cycles=2-3
0x9b6d:  72 0a 00 96 19 btjt $96, #5, $9b8b  (offset=25)         cycles=2-3
0x9b72:  72 02 00 8c 0a btjt $8c, #1, $9b81  (offset=10)         cycles=2-3
0x9b77:  a6 64          ld A, #$64                               cycles=1
0x9b79:  b7 dd          ld $dd,A                                 cycles=1
0x9b7b:  a6 5a          ld A, #$5a                               cycles=1
0x9b7d:  b7 ec          ld $ec,A                                 cycles=1
0x9b7f:  20 26          jra $9ba7  (offset=38)                   cycles=2
0x9b81:  a6 5a          ld A, #$5a                               cycles=1
0x9b83:  b7 dd          ld $dd,A                                 cycles=1
0x9b85:  a6 32          ld A, #$32                               cycles=1
0x9b87:  b7 ec          ld $ec,A                                 cycles=1
0x9b89:  20 1c          jra $9ba7  (offset=28)                   cycles=2
0x9b8b:  a6 5a          ld A, #$5a                               cycles=1
0x9b8d:  b7 dd          ld $dd,A                                 cycles=1
0x9b8f:  a6 19          ld A, #$19                               cycles=1
0x9b91:  b7 ec          ld $ec,A                                 cycles=1
0x9b93:  20 12          jra $9ba7  (offset=18)                   cycles=2
0x9b95:  a6 5a          ld A, #$5a                               cycles=1
0x9b97:  b7 dd          ld $dd,A                                 cycles=1
0x9b99:  a6 0a          ld A, #$0a                               cycles=1
0x9b9b:  b7 ec          ld $ec,A                                 cycles=1
0x9b9d:  20 08          jra $9ba7  (offset=8)                    cycles=2
0x9b9f:  a6 5a          ld A, #$5a                               cycles=1
0x9ba1:  b7 dd          ld $dd,A                                 cycles=1
0x9ba3:  a6 0c          ld A, #$0c                               cycles=1
0x9ba5:  b7 ec          ld $ec,A                                 cycles=1
0x9ba7:  b6 db          ld A, $db                                cycles=1
0x9ba9:  a1 02          cp A, #$02                               cycles=1
0x9bab:  24 00          jrnc $9bad  (offset=0)                   cycles=1-2
0x9bad:  b6 eb          ld A, $eb                                cycles=1
0x9baf:  97             ld XL, A                                 cycles=1
0x9bb0:  b6 db          ld A, $db                                cycles=1
0x9bb2:  42             mul X, A                                 cycles=4
0x9bb3:  a6 42          ld A, #$42                               cycles=1
0x9bb5:  62             div X, A                                 cycles=2-17
0x9bb6:  9f             ld A, XL                                 cycles=1
0x9bb7:  b1 d8          cp A, $d8                                cycles=1
0x9bb9:  24 15          jrnc $9bd0  (offset=21)                  cycles=1-2
0x9bbb:  5f             clrw X                                   cycles=1
0x9bbc:  97             ld XL, A                                 cycles=1
0x9bbd:  b6 9d          ld A, $9d                                cycles=1
0x9bbf:  42             mul X, A                                 cycles=4
0x9bc0:  b6 d8          ld A, $d8                                cycles=1
0x9bc2:  62             div X, A                                 cycles=2-17
0x9bc3:  9f             ld A, XL                                 cycles=1
0x9bc4:  ab 00          add A, #$00                              cycles=1
0x9bc6:  25 08          jrc $9bd0  (offset=8)                    cycles=1-2
0x9bc8:  b1 f5          cp A, $f5                                cycles=1
0x9bca:  24 04          jrnc $9bd0  (offset=4)                   cycles=1-2
0x9bcc:  b7 d6          ld $d6,A                                 cycles=1
0x9bce:  20 04          jra $9bd4  (offset=4)                    cycles=2
0x9bd0:  b6 f5          ld A, $f5                                cycles=1
0x9bd2:  b7 d6          ld $d6,A                                 cycles=1
0x9bd4:  a1 01          cp A, #$01                               cycles=1
0x9bd6:  25 01          jrc $9bd9  (offset=1)                    cycles=1-2
0x9bd8:  81             ret                                      cycles=4
0x9bd9:  a6 01          ld A, #$01                               cycles=1
0x9bdb:  b7 d6          ld $d6,A                                 cycles=1
0x9bdd:  81             ret                                      cycles=4
0x9bde:  81             ret                                      cycles=4
0x9bdf:  5f             clrw X                                   cycles=1
0x9be0:  b6 3f          ld A, $3f                                cycles=1
0x9be2:  97             ld XL, A                                 cycles=1
0x9be3:  b6 3e          ld A, $3e                                cycles=1
0x9be5:  95             ld XH, A                                 cycles=1
0x9be6:  a6 64          ld A, #$64                               cycles=1
0x9be8:  62             div X, A                                 cycles=2-17
0x9be9:  a6 0a          ld A, #$0a                               cycles=1
0x9beb:  62             div X, A                                 cycles=2-17
0x9bec:  a3 00 ff       cpw X, #$ff                              cycles=2
0x9bef:  25 03          jrc $9bf4  (offset=3)                    cycles=1-2
0x9bf1:  a6 ff          ld A, #$ff                               cycles=1
0x9bf3:  81             ret                                      cycles=4
0x9bf4:  9f             ld A, XL                                 cycles=1
0x9bf5:  81             ret                                      cycles=4
0x9bf6:  a6 40          ld A, #$40                               cycles=1
0x9bf8:  c7 50 0e       ld $500e,A                               cycles=1
0x9bfb:  a6 02          ld A, #$02                               cycles=1
0x9bfd:  c7 50 13       ld $5013,A                               cycles=1
0x9c00:  a6 02          ld A, #$02                               cycles=1
0x9c02:  c7 50 18       ld $5018,A                               cycles=1
0x9c05:  81             ret                                      cycles=4
0x9c06:  a6 20          ld A, #$20                               cycles=1
0x9c08:  c7 50 0e       ld $500e,A                               cycles=1
0x9c0b:  a6 04          ld A, #$04                               cycles=1
0x9c0d:  c7 50 13       ld $5013,A                               cycles=1
0x9c10:  a6 20          ld A, #$20                               cycles=1
0x9c12:  c7 50 18       ld $5018,A                               cycles=1
0x9c15:  81             ret                                      cycles=4
0x9c16:  a6 00          ld A, #$00                               cycles=1
0x9c18:  c7 50 0e       ld $500e,A                               cycles=1
0x9c1b:  a6 00          ld A, #$00                               cycles=1
0x9c1d:  c7 50 13       ld $5013,A                               cycles=1
0x9c20:  a6 00          ld A, #$00                               cycles=1
0x9c22:  c7 50 18       ld $5018,A                               cycles=1
0x9c25:  81             ret                                      cycles=4
0x9c26:  a6 50          ld A, #$50                               cycles=1
0x9c28:  c7 50 02       ld $5002,A                               cycles=1
0x9c2b:  a6 76          ld A, #$76                               cycles=1
0x9c2d:  c7 50 03       ld $5003,A                               cycles=1
0x9c30:  a6 00          ld A, #$00                               cycles=1
0x9c32:  c7 50 04       ld $5004,A                               cycles=1
0x9c35:  a6 40          ld A, #$40                               cycles=1
0x9c37:  c7 50 00       ld $5000,A                               cycles=1
0x9c3a:  a6 07          ld A, #$07                               cycles=1
0x9c3c:  c7 50 07       ld $5007,A                               cycles=1
0x9c3f:  a6 07          ld A, #$07                               cycles=1
0x9c41:  c7 50 08       ld $5008,A                               cycles=1
0x9c44:  a6 00          ld A, #$00                               cycles=1
0x9c46:  c7 50 09       ld $5009,A                               cycles=1
0x9c49:  a6 00          ld A, #$00                               cycles=1
0x9c4b:  c7 50 05       ld $5005,A                               cycles=1
0x9c4e:  a6 8e          ld A, #$8e                               cycles=1
0x9c50:  c7 50 0c       ld $500c,A                               cycles=1
0x9c53:  a6 4e          ld A, #$4e                               cycles=1
0x9c55:  c7 50 0d       ld $500d,A                               cycles=1
0x9c58:  a6 20          ld A, #$20                               cycles=1
0x9c5a:  c7 50 0e       ld $500e,A                               cycles=1
0x9c5d:  a6 80          ld A, #$80                               cycles=1
0x9c5f:  c7 50 0a       ld $500a,A                               cycles=1
0x9c62:  a6 38          ld A, #$38                               cycles=1
0x9c64:  c7 50 11       ld $5011,A                               cycles=1
0x9c67:  a6 df          ld A, #$df                               cycles=1
0x9c69:  c7 50 12       ld $5012,A                               cycles=1
0x9c6c:  a6 04          ld A, #$04                               cycles=1
0x9c6e:  c7 50 13       ld $5013,A                               cycles=1
0x9c71:  a6 20          ld A, #$20                               cycles=1
0x9c73:  c7 50 0f       ld $500f,A                               cycles=1
0x9c76:  a6 00          ld A, #$00                               cycles=1
0x9c78:  c7 50 16       ld $5016,A                               cycles=1
0x9c7b:  a6 02          ld A, #$02                               cycles=1
0x9c7d:  c7 50 17       ld $5017,A                               cycles=1
0x9c80:  a6 20          ld A, #$20                               cycles=1
0x9c82:  c7 50 18       ld $5018,A                               cycles=1
0x9c85:  a6 00          ld A, #$00                               cycles=1
0x9c87:  c7 50 14       ld $5014,A                               cycles=1
0x9c8a:  a6 00          ld A, #$00                               cycles=1
0x9c8c:  c7 50 20       ld $5020,A                               cycles=1
0x9c8f:  a6 03          ld A, #$03                               cycles=1
0x9c91:  c7 50 21       ld $5021,A                               cycles=1
0x9c94:  a6 00          ld A, #$00                               cycles=1
0x9c96:  c7 50 22       ld $5022,A                               cycles=1
0x9c99:  a6 00          ld A, #$00                               cycles=1
0x9c9b:  c7 50 1e       ld $501e,A                               cycles=1
0x9c9e:  a6 00          ld A, #$00                               cycles=1
0x9ca0:  c7 50 c6       ld $50c6,A                               cycles=1
0x9ca3:  a6 cc          ld A, #$cc                               cycles=1
0x9ca5:  c7 50 e0       ld $50e0,A                               cycles=1
0x9ca8:  a6 aa          ld A, #$aa                               cycles=1
0x9caa:  c7 50 e0       ld $50e0,A                               cycles=1
0x9cad:  a6 55          ld A, #$55                               cycles=1
0x9caf:  c7 50 e0       ld $50e0,A                               cycles=1
0x9cb2:  a6 01          ld A, #$01                               cycles=1
0x9cb4:  c7 50 e1       ld $50e1,A                               cycles=1
0x9cb7:  a6 ff          ld A, #$ff                               cycles=1
0x9cb9:  c7 50 e2       ld $50e2,A                               cycles=1
0x9cbc:  a6 05          ld A, #$05                               cycles=1
0x9cbe:  c7 53 20       ld $5320,A                               cycles=1
0x9cc1:  a6 01          ld A, #$01                               cycles=1
0x9cc3:  c7 53 21       ld $5321,A                               cycles=1
0x9cc6:  a6 04          ld A, #$04                               cycles=1
0x9cc8:  c7 53 2a       ld $532a,A                               cycles=1
0x9ccb:  a6 ff          ld A, #$ff                               cycles=1
0x9ccd:  c7 53 2b       ld $532b,A                               cycles=1
0x9cd0:  a6 ff          ld A, #$ff                               cycles=1
0x9cd2:  c7 53 2c       ld $532c,A                               cycles=1
0x9cd5:  a6 00          ld A, #$00                               cycles=1
0x9cd7:  c7 53 2d       ld $532d,A                               cycles=1
0x9cda:  a6 00          ld A, #$00                               cycles=1
0x9cdc:  c7 53 2e       ld $532e,A                               cycles=1
0x9cdf:  a6 00          ld A, #$00                               cycles=1
0x9ce1:  c7 54 02       ld $5402,A                               cycles=1
0x9ce4:  a6 00          ld A, #$00                               cycles=1
0x9ce6:  c7 54 03       ld $5403,A                               cycles=1
0x9ce9:  a6 03          ld A, #$03                               cycles=1
0x9ceb:  c7 54 00       ld $5400,A                               cycles=1
0x9cee:  a6 01          ld A, #$01                               cycles=1
0x9cf0:  c7 54 01       ld $5401,A                               cycles=1
0x9cf3:  a6 c5          ld A, #$c5                               cycles=1
0x9cf5:  c7 52 50       ld $5250,A                               cycles=1
0x9cf8:  a6 90          ld A, #$90                               cycles=1
0x9cfa:  c7 52 54       ld $5254,A                               cycles=1
0x9cfd:  a6 00          ld A, #$00                               cycles=1
0x9cff:  c7 52 60       ld $5260,A                               cycles=1
0x9d02:  a6 00          ld A, #$00                               cycles=1
0x9d04:  c7 52 61       ld $5261,A                               cycles=1
0x9d07:  a6 90          ld A, #$90                               cycles=1
0x9d09:  c7 52 6d       ld $526d,A                               cycles=1
0x9d0c:  a6 31          ld A, #$31                               cycles=1
0x9d0e:  c7 52 6e       ld $526e,A                               cycles=1
0x9d11:  a6 2a          ld A, #$2a                               cycles=1
0x9d13:  c7 52 6f       ld $526f,A                               cycles=1
0x9d16:  a6 68          ld A, #$68                               cycles=1
0x9d18:  c7 52 58       ld $5258,A                               cycles=1
0x9d1b:  c7 52 59       ld $5259,A                               cycles=1
0x9d1e:  c7 52 5a       ld $525a,A                               cycles=1
0x9d21:  a6 00          ld A, #$00                               cycles=1
0x9d23:  c7 52 5c       ld $525c,A                               cycles=1
0x9d26:  a6 00          ld A, #$00                               cycles=1
0x9d28:  c7 52 5d       ld $525d,A                               cycles=1
0x9d2b:  a6 01          ld A, #$01                               cycles=1
0x9d2d:  c7 52 62       ld $5262,A                               cycles=1
0x9d30:  a6 f4          ld A, #$f4                               cycles=1
0x9d32:  c7 52 63       ld $5263,A                               cycles=1
0x9d35:  a6 85          ld A, #$85                               cycles=1
0x9d37:  c7 53 00       ld $5300,A                               cycles=1
0x9d3a:  a6 00          ld A, #$00                               cycles=1
0x9d3c:  c7 53 01       ld $5301,A                               cycles=1
0x9d3f:  a6 68          ld A, #$68                               cycles=1
0x9d41:  c7 53 05       ld $5305,A                               cycles=1
0x9d44:  a6 01          ld A, #$01                               cycles=1
0x9d46:  c7 53 08       ld $5308,A                               cycles=1
0x9d49:  a6 00          ld A, #$00                               cycles=1
0x9d4b:  c7 53 0c       ld $530c,A                               cycles=1
0x9d4e:  a6 01          ld A, #$01                               cycles=1
0x9d50:  c7 53 0d       ld $530d,A                               cycles=1
0x9d53:  a6 f4          ld A, #$f4                               cycles=1
0x9d55:  c7 53 0e       ld $530e,A                               cycles=1
0x9d58:  72 5f 53 0f    clr $530f                                cycles=1
0x9d5c:  72 5f 53 10    clr $5310                                cycles=1
0x9d60:  a6 68          ld A, #$68                               cycles=1
0x9d62:  c7 53 06       ld $5306,A                               cycles=1
0x9d65:  a6 11          ld A, #$11                               cycles=1
0x9d67:  c7 53 08       ld $5308,A                               cycles=1
0x9d6a:  5f             clrw X                                   cycles=1
0x9d6b:  a6 01          ld A, #$01                               cycles=1
0x9d6d:  95             ld XH, A                                 cycles=1
0x9d6e:  a6 40          ld A, #$40                               cycles=1
0x9d70:  97             ld XL, A                                 cycles=1
0x9d71:  9e             ld A, XH                                 cycles=1
0x9d72:  c7 53 0d       ld $530d,A                               cycles=1
0x9d75:  9f             ld A, XL                                 cycles=1
0x9d76:  c7 53 0e       ld $530e,A                               cycles=1
0x9d79:  ae 00 24       ldw X, #$24                              cycles=2
0x9d7c:  9e             ld A, XH                                 cycles=1
0x9d7d:  c7 53 11       ld $5311,A                               cycles=1
0x9d80:  9f             ld A, XL                                 cycles=1
0x9d81:  c7 53 12       ld $5312,A                               cycles=1
0x9d84:  72 5f 52 65    clr $5265                                cycles=1
0x9d88:  72 5f 52 67    clr $5267                                cycles=1
0x9d8c:  72 5f 52 69    clr $5269                                cycles=1
0x9d90:  72 5f 52 6b    clr $526b                                cycles=1
0x9d94:  72 5f 52 66    clr $5266                                cycles=1
0x9d98:  72 5f 52 68    clr $5268                                cycles=1
0x9d9c:  72 5f 52 6a    clr $526a                                cycles=1
0x9da0:  72 5f 52 6c    clr $526c                                cycles=1
0x9da4:  a6 01          ld A, #$01                               cycles=1
0x9da6:  c7 52 57       ld $5257,A                               cycles=1
0x9da9:  a6 01          ld A, #$01                               cycles=1
0x9dab:  c7 52 51       ld $5251,A                               cycles=1
0x9dae:  a6 01          ld A, #$01                               cycles=1
0x9db0:  c7 53 40       ld $5340,A                               cycles=1
0x9db3:  a6 01          ld A, #$01                               cycles=1
0x9db5:  c7 53 41       ld $5341,A                               cycles=1
0x9db8:  a6 00          ld A, #$00                               cycles=1
0x9dba:  c7 53 43       ld $5343,A                               cycles=1
0x9dbd:  a6 07          ld A, #$07                               cycles=1
0x9dbf:  c7 53 45       ld $5345,A                               cycles=1
0x9dc2:  a6 3f          ld A, #$3f                               cycles=1
0x9dc4:  c7 53 46       ld $5346,A                               cycles=1
0x9dc7:  a6 00          ld A, #$00                               cycles=1
0x9dc9:  c7 52 44       ld $5244,A                               cycles=1
0x9dcc:  a6 20          ld A, #$20                               cycles=1
0x9dce:  c7 52 45       ld $5245,A                               cycles=1
0x9dd1:  a6 00          ld A, #$00                               cycles=1
0x9dd3:  c7 52 46       ld $5246,A                               cycles=1
0x9dd6:  a6 68          ld A, #$68                               cycles=1
0x9dd8:  c7 52 42       ld $5242,A                               cycles=1
0x9ddb:  a6 02          ld A, #$02                               cycles=1
0x9ddd:  c7 52 43       ld $5243,A                               cycles=1
0x9de0:  a6 f0          ld A, #$f0                               cycles=1
0x9de2:  c7 50 a0       ld $50a0,A                               cycles=1
0x9de5:  a6 03          ld A, #$03                               cycles=1
0x9de7:  c7 50 a1       ld $50a1,A                               cycles=1
0x9dea:  a6 55          ld A, #$55                               cycles=1
0x9dec:  c7 7f 70       ld $7f70,A                               cycles=1
0x9def:  c7 7f 72       ld $7f72,A                               cycles=1
0x9df2:  c7 7f 74       ld $7f74,A                               cycles=1
0x9df5:  c7 7f 76       ld $7f76,A                               cycles=1
0x9df8:  a6 01          ld A, #$01                               cycles=1
0x9dfa:  c7 7f 71       ld $7f71,A                               cycles=1
0x9dfd:  a6 d5          ld A, #$d5                               cycles=1
0x9dff:  c7 7f 72       ld $7f72,A                               cycles=1
0x9e02:  a6 54          ld A, #$54                               cycles=1
0x9e04:  c7 7f 73       ld $7f73,A                               cycles=1
0x9e07:  a6 54          ld A, #$54                               cycles=1
0x9e09:  c7 7f 74       ld $7f74,A                               cycles=1
0x9e0c:  a6 55          ld A, #$55                               cycles=1
0x9e0e:  c7 7f 75       ld $7f75,A                               cycles=1
0x9e11:  ae 00 00       ldw X, #$0                               cycles=2
0x9e14:  a6 00          ld A, #$00                               cycles=1
0x9e16:  f7             ld (X),A                                 cycles=1
0x9e17:  5c             incw X                                   cycles=1
0x9e18:  a3 00 ff       cpw X, #$ff                              cycles=2
0x9e1b:  26 f9          jrne $9e16  (offset=-7)                  cycles=1-2
0x9e1d:  a6 0c          ld A, #$0c                               cycles=1
0x9e1f:  b7 76          ld $76,A                                 cycles=1
0x9e21:  a6 07          ld A, #$07                               cycles=1
0x9e23:  b7 aa          ld $aa,A                                 cycles=1
0x9e25:  b7 7c          ld $7c,A                                 cycles=1
0x9e27:  b7 39          ld $39,A                                 cycles=1
0x9e29:  b7 7b          ld $7b,A                                 cycles=1
0x9e2b:  a6 00          ld A, #$00                               cycles=1
0x9e2d:  b7 ca          ld $ca,A                                 cycles=1
0x9e2f:  a6 64          ld A, #$64                               cycles=1
0x9e31:  b7 a4          ld $a4,A                                 cycles=1
0x9e33:  b7 95          ld $95,A                                 cycles=1
0x9e35:  72 19 00 94    bres $94, #4                             cycles=1
0x9e39:  81             ret                                      cycles=4
0x9e3a:  a6 56          ld A, #$56                               cycles=1
0x9e3c:  c7 50 62       ld $5062,A                               cycles=1
0x9e3f:  a6 ae          ld A, #$ae                               cycles=1
0x9e41:  c7 50 62       ld $5062,A                               cycles=1
0x9e44:  cd 9e c3       call $9ec3                               cycles=4
0x9e47:  72 03 50 5f 0f btjf $505f, #1, $9e5b  (offset=15)       cycles=2-3
0x9e4c:  5f             clrw X                                   cycles=1
0x9e4d:  b6 37          ld A, $37                                cycles=1
0x9e4f:  97             ld XL, A                                 cycles=1
0x9e50:  b6 36          ld A, $36                                cycles=1
0x9e52:  d7 80 80       ld ($8080,X),A                           cycles=1
0x9e55:  9d             nop                                      cycles=1
0x9e56:  72 05 50 5f fa btjf $505f, #2, $9e55  (offset=-6)       cycles=2-3
0x9e5b:  72 13 50 5f    bres $505f, #1                           cycles=1
0x9e5f:  cd 9e 8a       call $9e8a                               cycles=4
0x9e62:  81             ret                                      cycles=4
0x9e63:  a6 ae          ld A, #$ae                               cycles=1
0x9e65:  c7 50 64       ld $5064,A                               cycles=1
0x9e68:  a6 56          ld A, #$56                               cycles=1
0x9e6a:  c7 50 64       ld $5064,A                               cycles=1
0x9e6d:  cd 9e c3       call $9ec3                               cycles=4
0x9e70:  72 07 50 5f 0f btjf $505f, #3, $9e84  (offset=15)       cycles=2-3
0x9e75:  5f             clrw X                                   cycles=1
0x9e76:  b6 37          ld A, $37                                cycles=1
0x9e78:  97             ld XL, A                                 cycles=1
0x9e79:  b6 36          ld A, $36                                cycles=1
0x9e7b:  d7 40 00       ld ($4000,X),A                           cycles=1
0x9e7e:  9d             nop                                      cycles=1
0x9e7f:  72 05 50 5f fa btjf $505f, #2, $9e7e  (offset=-6)       cycles=2-3
0x9e84:  72 17 50 5f    bres $505f, #3                           cycles=1
0x9e88:  81             ret                                      cycles=4
0x9e89:  81             ret                                      cycles=4
0x9e8a:  a6 aa          ld A, #$aa                               cycles=1
0x9e8c:  c7 50 e0       ld $50e0,A                               cycles=1
0x9e8f:  81             ret                                      cycles=4
0x9e90:  a6 64          ld A, #$64                               cycles=1
0x9e92:  20 0a          jra $9e9e  (offset=10)                   cycles=2
0x9e94:  a6 fa          ld A, #$fa                               cycles=1
0x9e96:  20 06          jra $9e9e  (offset=6)                    cycles=2
0x9e98:  a6 32          ld A, #$32                               cycles=1
0x9e9a:  20 02          jra $9e9e  (offset=2)                    cycles=2
0x9e9c:  a6 01          ld A, #$01                               cycles=1
0x9e9e:  b7 00          ld $00,A                                 cycles=1
0x9ea0:  48             sll A                                    cycles=1
0x9ea1:  72 1d 00 89    bres $89, #6                             cycles=1
0x9ea5:  cd 9e 8a       call $9e8a                               cycles=4
0x9ea8:  72 0d 00 89 f8 btjf $89, #6, $9ea5  (offset=-8)         cycles=2-3
0x9ead:  3a 00          dec $00                                  cycles=1
0x9eaf:  3d 00          tnz $00                                  cycles=1
0x9eb1:  26 ee          jrne $9ea1  (offset=-18)                 cycles=1-2
0x9eb3:  81             ret                                      cycles=4
0x9eb4:  a6 64          ld A, #$64                               cycles=1
0x9eb6:  b7 00          ld $00,A                                 cycles=1
0x9eb8:  cd 9e 8a       call $9e8a                               cycles=4
0x9ebb:  cd 9e c3       call $9ec3                               cycles=4
0x9ebe:  3a 00          dec $00                                  cycles=1
0x9ec0:  26 f6          jrne $9eb8  (offset=-10)                 cycles=1-2
0x9ec2:  81             ret                                      cycles=4
0x9ec3:  ae 14 96       ldw X, #$1496                            cycles=2
0x9ec6:  5a             decw X                                   cycles=1
0x9ec7:  26 fd          jrne $9ec6  (offset=-3)                  cycles=1-2
0x9ec9:  81             ret                                      cycles=4
0x9eca:  3f b1          clr $b1                                  cycles=1
0x9ecc:  81             ret                                      cycles=4
0x9ecd:  72 0f 00 8a 08 btjf $8a, #7, $9eda  (offset=8)          cycles=2-3
0x9ed2:  b6 b0          ld A, $b0                                cycles=1
0x9ed4:  48             sll A                                    cycles=1
0x9ed5:  48             sll A                                    cycles=1
0x9ed6:  b1 1d          cp A, $1d                                cycles=1
0x9ed8:  25 f0          jrc $9eca  (offset=-16)                  cycles=1-2
0x9eda:  b6 b1          ld A, $b1                                cycles=1
0x9edc:  a1 64          cp A, #$64                               cycles=1
0x9ede:  25 ec          jrc $9ecc  (offset=-20)                  cycles=1-2
0x9ee0:  3f b1          clr $b1                                  cycles=1
0x9ee2:  b6 1f          ld A, $1f                                cycles=1
0x9ee4:  b1 a2          cp A, $a2                                cycles=1
0x9ee6:  25 04          jrc $9eec  (offset=4)                    cycles=1-2
0x9ee8:  b1 a3          cp A, $a3                                cycles=1
0x9eea:  24 07          jrnc $9ef3  (offset=7)                   cycles=1-2
0x9eec:  3f 76          clr $76                                  cycles=1
0x9eee:  72 10 00 77    bset $77, #0                             cycles=1
0x9ef2:  81             ret                                      cycles=4
0x9ef3:  72 11 00 77    bres $77, #0                             cycles=1
0x9ef7:  b0 a3          sub A, $a3                               cycles=1
0x9ef9:  b7 76          ld $76,A                                 cycles=1
0x9efb:  a1 1e          cp A, #$1e                               cycles=1
0x9efd:  25 02          jrc $9f01  (offset=2)                    cycles=1-2
0x9eff:  a6 1e          ld A, #$1e                               cycles=1
0x9f01:  5f             clrw X                                   cycles=1
0x9f02:  97             ld XL, A                                 cycles=1
0x9f03:  b6 ad          ld A, $ad                                cycles=1
0x9f05:  42             mul X, A                                 cycles=4
0x9f06:  b6 ae          ld A, $ae                                cycles=1
0x9f08:  62             div X, A                                 cycles=2-17
0x9f09:  9f             ld A, XL                                 cycles=1
0x9f0a:  bb af          add A, $af                               cycles=1
0x9f0c:  b7 76          ld $76,A                                 cycles=1
0x9f0e:  81             ret                                      cycles=4
0x9f0f:  72 08 00 8d 03 btjt $8d, #4, $9f17  (offset=3)          cycles=2-3
0x9f14:  3f 7a          clr $7a                                  cycles=1
0x9f16:  81             ret                                      cycles=4
0x9f17:  72 0f 00 8a 01 btjf $8a, #7, $9f1d  (offset=1)          cycles=2-3
0x9f1c:  83             trap                                     cycles=9
0x9f1d:  72 06 00 8d 70 btjt $8d, #3, $9f92  (offset=112)        cycles=2-3
0x9f22:  72 0f 52 6d 6d btjf $526d, #7, $9f94  (offset=109)      cycles=2-3
0x9f27:  72 0a 00 8f 55 btjt $8f, #5, $9f81  (offset=85)         cycles=2-3
0x9f2c:  72 00 00 89 1d btjt $89, #0, $9f4e  (offset=29)         cycles=2-3
0x9f31:  72 0c 00 8f 36 btjt $8f, #6, $9f6c  (offset=54)         cycles=2-3
0x9f36:  72 00 00 93 2f btjt $93, #0, $9f6a  (offset=47)         cycles=2-3
0x9f3b:  72 0a 00 8d 2a btjt $8d, #5, $9f6a  (offset=42)         cycles=2-3
0x9f40:  20 61          jra $9fa3  (offset=97)                   cycles=2
0x9f42:  b6 fb          ld A, $fb                                cycles=1
0x9f44:  a1 85          cp A, #$85                               cycles=1
0x9f46:  25 66          jrc $9fae  (offset=102)                  cycles=1-2
0x9f48:  72 19 00 94    bres $94, #4                             cycles=1
0x9f4c:  20 55          jra $9fa3  (offset=85)                   cycles=2
0x9f4e:  b6 1f          ld A, $1f                                cycles=1
0x9f50:  b1 a2          cp A, $a2                                cycles=1
0x9f52:  25 5a          jrc $9fae  (offset=90)                   cycles=1-2
0x9f54:  b6 a4          ld A, $a4                                cycles=1
0x9f56:  a1 04          cp A, #$04                               cycles=1
0x9f58:  24 04          jrnc $9f5e  (offset=4)                   cycles=1-2
0x9f5a:  a6 04          ld A, #$04                               cycles=1
0x9f5c:  b7 a4          ld $a4,A                                 cycles=1
0x9f5e:  3c a4          inc $a4                                  cycles=1
0x9f60:  72 11 00 89    bres $89, #0                             cycles=1
0x9f64:  72 19 00 8f    bres $8f, #4                             cycles=1
0x9f68:  20 39          jra $9fa3  (offset=57)                   cycles=2
0x9f6a:  20 42          jra $9fae  (offset=66)                   cycles=2
0x9f6c:  b6 5a          ld A, $5a                                cycles=1
0x9f6e:  a1 55          cp A, #$55                               cycles=1
0x9f70:  24 3c          jrnc $9fae  (offset=60)                  cycles=1-2
0x9f72:  72 04 00 8a 37 btjt $8a, #2, $9fae  (offset=55)         cycles=2-3
0x9f77:  72 19 00 8a    bres $8a, #4                             cycles=1
0x9f7b:  72 1d 00 8f    bres $8f, #6                             cycles=1
0x9f7f:  20 22          jra $9fa3  (offset=34)                   cycles=2
0x9f81:  b6 1c          ld A, $1c                                cycles=1
0x9f83:  a1 55          cp A, #$55                               cycles=1
0x9f85:  24 27          jrnc $9fae  (offset=39)                  cycles=1-2
0x9f87:  72 04 00 8a 22 btjt $8a, #2, $9fae  (offset=34)         cycles=2-3
0x9f8c:  72 1b 00 8f    bres $8f, #5                             cycles=1
0x9f90:  20 11          jra $9fa3  (offset=17)                   cycles=2
0x9f92:  20 1a          jra $9fae  (offset=26)                   cycles=2
0x9f94:  b6 1c          ld A, $1c                                cycles=1
0x9f96:  a1 55          cp A, #$55                               cycles=1
0x9f98:  24 14          jrnc $9fae  (offset=20)                  cycles=1-2
0x9f9a:  72 04 00 8a 0f btjt $8a, #2, $9fae  (offset=15)         cycles=2-3
0x9f9f:  72 1e 52 6d    bset $526d, #7                           cycles=1
0x9fa3:  72 1c 50 00    bset $5000, #6                           cycles=1
0x9fa7:  72 19 00 8d    bres $8d, #4                             cycles=1
0x9fab:  3f 34          clr $34                                  cycles=1
0x9fad:  81             ret                                      cycles=4
0x9fae:  72 01 00 8f 12 btjf $8f, #0, $9fc5  (offset=18)         cycles=2-3
0x9fb3:  72 11 00 8f    bres $8f, #0                             cycles=1
0x9fb7:  3d 34          tnz $34                                  cycles=1
0x9fb9:  27 12          jreq $9fcd  (offset=18)                  cycles=1-2
0x9fbb:  3a 34          dec $34                                  cycles=1
0x9fbd:  b6 34          ld A, $34                                cycles=1
0x9fbf:  a1 0a          cp A, #$0a                               cycles=1
0x9fc1:  25 09          jrc $9fcc  (offset=9)                    cycles=1-2
0x9fc3:  20 01          jra $9fc6  (offset=1)                    cycles=2
0x9fc5:  81             ret                                      cycles=4
0x9fc6:  72 0c 50 00 01 btjt $5000, #6, $9fcc  (offset=1)        cycles=2-3
0x9fcb:  81             ret                                      cycles=4
0x9fcc:  81             ret                                      cycles=4
0x9fcd:  72 06 00 8d 31 btjt $8d, #3, $a003  (offset=49)         cycles=2-3
0x9fd2:  72 0f 52 6d 3e btjf $526d, #7, $a015  (offset=62)       cycles=2-3
0x9fd7:  72 00 00 89 42 btjt $89, #0, $a01e  (offset=66)         cycles=2-3
0x9fdc:  72 0a 00 8f 10 btjt $8f, #5, $9ff1  (offset=16)         cycles=2-3
0x9fe1:  72 0c 00 8f 14 btjt $8f, #6, $9ffa  (offset=20)         cycles=2-3
0x9fe6:  72 00 00 93 3c btjt $93, #0, $a027  (offset=60)         cycles=2-3
0x9feb:  72 0a 00 8d 1c btjt $8d, #5, $a00c  (offset=28)         cycles=2-3
0x9ff0:  81             ret                                      cycles=4
0x9ff1:  a6 04          ld A, #$04                               cycles=1
0x9ff3:  b7 7a          ld $7a,A                                 cycles=1
0x9ff5:  a6 12          ld A, #$12                               cycles=1
0x9ff7:  b7 34          ld $34,A                                 cycles=1
0x9ff9:  81             ret                                      cycles=4
0x9ffa:  a6 02          ld A, #$02                               cycles=1
0x9ffc:  b7 7a          ld $7a,A                                 cycles=1
0x9ffe:  a6 0e          ld A, #$0e                               cycles=1
0xa000:  b7 34          ld $34,A                                 cycles=1
0xa002:  81             ret                                      cycles=4
0xa003:  a6 03          ld A, #$03                               cycles=1
0xa005:  b7 7a          ld $7a,A                                 cycles=1
0xa007:  a6 10          ld A, #$10                               cycles=1
0xa009:  b7 34          ld $34,A                                 cycles=1
0xa00b:  81             ret                                      cycles=4
0xa00c:  a6 05          ld A, #$05                               cycles=1
0xa00e:  b7 7a          ld $7a,A                                 cycles=1
0xa010:  a6 14          ld A, #$14                               cycles=1
0xa012:  b7 34          ld $34,A                                 cycles=1
0xa014:  81             ret                                      cycles=4
0xa015:  a6 02          ld A, #$02                               cycles=1
0xa017:  b7 7a          ld $7a,A                                 cycles=1
0xa019:  a6 0e          ld A, #$0e                               cycles=1
0xa01b:  b7 34          ld $34,A                                 cycles=1
0xa01d:  81             ret                                      cycles=4
0xa01e:  a6 08          ld A, #$08                               cycles=1
0xa020:  b7 7a          ld $7a,A                                 cycles=1
0xa022:  a6 1a          ld A, #$1a                               cycles=1
0xa024:  b7 34          ld $34,A                                 cycles=1
0xa026:  81             ret                                      cycles=4
0xa027:  a6 08          ld A, #$08                               cycles=1
0xa029:  b7 7a          ld $7a,A                                 cycles=1
0xa02b:  a6 1a          ld A, #$1a                               cycles=1
0xa02d:  b7 34          ld $34,A                                 cycles=1
0xa02f:  81             ret                                      cycles=4
0xa030:  72 00 00 96 4a btjt $96, #0, $a07f  (offset=74)         cycles=2-3
0xa035:  72 0f 00 89 45 btjf $89, #7, $a07f  (offset=69)         cycles=2-3
0xa03a:  72 1f 00 89    bres $89, #7                             cycles=1
0xa03e:  b6 1f          ld A, $1f                                cycles=1
0xa040:  b1 a2          cp A, $a2                                cycles=1
0xa042:  25 1e          jrc $a062  (offset=30)                   cycles=1-2
0xa044:  72 10 00 98    bset $98, #0                             cycles=1
0xa048:  72 11 00 89    bres $89, #0                             cycles=1
0xa04c:  3c a4          inc $a4                                  cycles=1
0xa04e:  b6 a4          ld A, $a4                                cycles=1
0xa050:  a1 66          cp A, #$66                               cycles=1
0xa052:  25 2b          jrc $a07f  (offset=43)                   cycles=1-2
0xa054:  a6 66          ld A, #$66                               cycles=1
0xa056:  b7 a4          ld $a4,A                                 cycles=1
0xa058:  20 25          jra $a07f  (offset=37)                   cycles=2
0xa05a:  72 11 00 89    bres $89, #0                             cycles=1
0xa05e:  3c a4          inc $a4                                  cycles=1
0xa060:  20 1d          jra $a07f  (offset=29)                   cycles=2
0xa062:  b6 a4          ld A, $a4                                cycles=1
0xa064:  a1 0a          cp A, #$0a                               cycles=1
0xa066:  24 15          jrnc $a07d  (offset=21)                  cycles=1-2
0xa068:  72 10 00 89    bset $89, #0                             cycles=1
0xa06c:  72 10 00 77    bset $77, #0                             cycles=1
0xa070:  3f 76          clr $76                                  cycles=1
0xa072:  72 18 00 8d    bset $8d, #4                             cycles=1
0xa076:  72 18 00 8f    bset $8f, #4                             cycles=1
0xa07a:  83             trap                                     cycles=9
0xa07b:  20 02          jra $a07f  (offset=2)                    cycles=2
0xa07d:  3a a4          dec $a4                                  cycles=1
0xa07f:  81             ret                                      cycles=4
0xa080:  72 08 00 92 0f btjt $92, #4, $a094  (offset=15)         cycles=2-3
0xa085:  72 0b 00 89 4c btjf $89, #5, $a0d6  (offset=76)         cycles=2-3
0xa08a:  72 1b 00 89    bres $89, #5                             cycles=1
0xa08e:  83             trap                                     cycles=9
0xa08f:  72 15 00 8f    bres $8f, #2                             cycles=1
0xa093:  81             ret                                      cycles=4
0xa094:  72 14 00 8f    bset $8f, #2                             cycles=1
0xa098:  72 0a 00 89 39 btjt $89, #5, $a0d6  (offset=57)         cycles=2-3
0xa09d:  72 0d 00 96 10 btjf $96, #6, $a0b2  (offset=16)         cycles=2-3
0xa0a2:  72 1a 00 8a    bset $8a, #5                             cycles=1
0xa0a6:  72 1e 00 8c    bset $8c, #7                             cycles=1
0xa0aa:  cd 9e 8a       call $9e8a                               cycles=4
0xa0ad:  72 0a 00 8a f8 btjt $8a, #5, $a0aa  (offset=-8)         cycles=2-3
0xa0b2:  72 1a 00 89    bset $89, #5                             cycles=1
0xa0b6:  b6 1f          ld A, $1f                                cycles=1
0xa0b8:  b7 28          ld $28,A                                 cycles=1
0xa0ba:  72 01 00 91 17 btjf $91, #0, $a0d6  (offset=23)         cycles=2-3
0xa0bf:  72 1d 00 8c    bres $8c, #6                             cycles=1
0xa0c3:  9b             sim                                      cycles=1
0xa0c4:  83             trap                                     cycles=9
0xa0c5:  72 14 52 5c    bset $525c, #2                           cycles=1
0xa0c9:  72 1c 52 5c    bset $525c, #6                           cycles=1
0xa0cd:  72 14 52 5d    bset $525d, #2                           cycles=1
0xa0d1:  72 1a 52 57    bset $5257, #5                           cycles=1
0xa0d5:  9a             rim                                      cycles=1
0xa0d6:  81             ret                                      cycles=4
0xa0d7:  81             ret                                      cycles=4
0xa0d8:  72 1f 00 92    bres $92, #7                             cycles=1
0xa0dc:  81             ret                                      cycles=4
0xa0dd:  81             ret                                      cycles=4
0xa0de:  81             ret                                      cycles=4
0xa0df:  81             ret                                      cycles=4
0xa0e0:  72 0c 00 96 fa btjt $96, #6, $a0df  (offset=-6)         cycles=2-3
0xa0e5:  72 00 00 8d f5 btjt $8d, #0, $a0df  (offset=-11)        cycles=2-3
0xa0ea:  72 0a 00 90 f0 btjt $90, #5, $a0df  (offset=-16)        cycles=2-3
0xa0ef:  72 0a 00 89 eb btjt $89, #5, $a0df  (offset=-21)        cycles=2-3
0xa0f4:  72 0f 00 8a 5c btjf $8a, #7, $a155  (offset=92)         cycles=2-3
0xa0f9:  b6 1e          ld A, $1e                                cycles=1
0xa0fb:  72 00 00 8a 04 btjt $8a, #0, $a104  (offset=4)          cycles=2-3
0xa100:  25 53          jrc $a155  (offset=83)                   cycles=1-2
0xa102:  20 02          jra $a106  (offset=2)                    cycles=2
0xa104:  25 4f          jrc $a155  (offset=79)                   cycles=1-2
0xa106:  c6 52 65       ld A, $5265                              cycles=1
0xa109:  95             ld XH, A                                 cycles=1
0xa10a:  c6 52 66       ld A, $5266                              cycles=1
0xa10d:  97             ld XL, A                                 cycles=1
0xa10e:  72 00 00 8a 0c btjt $8a, #0, $a11f  (offset=12)         cycles=2-3
0xa113:  a3 00 64       cpw X, #$64                              cycles=2
0xa116:  25 3d          jrc $a155  (offset=61)                   cycles=1-2
0xa118:  a3 01 cc       cpw X, #$1cc                             cycles=2
0xa11b:  24 38          jrnc $a155  (offset=56)                  cycles=1-2
0xa11d:  20 0a          jra $a129  (offset=10)                   cycles=2
0xa11f:  a3 00 64       cpw X, #$64                              cycles=2
0xa122:  25 31          jrc $a155  (offset=49)                   cycles=1-2
0xa124:  a3 03 84       cpw X, #$384                             cycles=2
0xa127:  24 2c          jrnc $a155  (offset=44)                  cycles=1-2
0xa129:  72 10 00 8a    bset $8a, #0                             cycles=1
0xa12d:  72 01 52 5c 05 btjf $525c, #0, $a137  (offset=5)        cycles=2-3
0xa132:  cd a1 84       call $a184                               cycles=4
0xa135:  20 03          jra $a13a  (offset=3)                    cycles=2
0xa137:  cd a1 63       call $a163                               cycles=4
0xa13a:  72 09 52 5c 05 btjf $525c, #4, $a144  (offset=5)        cycles=2-3
0xa13f:  cd a1 8f       call $a18f                               cycles=4
0xa142:  20 03          jra $a147  (offset=3)                    cycles=2
0xa144:  cd a1 6e       call $a16e                               cycles=4
0xa147:  72 01 52 5d 05 btjf $525d, #0, $a151  (offset=5)        cycles=2-3
0xa14c:  cd a1 9a       call $a19a                               cycles=4
0xa14f:  20 03          jra $a154  (offset=3)                    cycles=2
0xa151:  cd a1 79       call $a179                               cycles=4
0xa154:  81             ret                                      cycles=4
0xa155:  72 11 00 8a    bres $8a, #0                             cycles=1
0xa159:  cd a1 63       call $a163                               cycles=4
0xa15c:  cd a1 6e       call $a16e                               cycles=4
0xa15f:  cd a1 79       call $a179                               cycles=4
0xa162:  81             ret                                      cycles=4
0xa163:  72 05 52 5c 41 btjf $525c, #2, $a1a9  (offset=65)       cycles=2-3
0xa168:  72 15 52 5c    bres $525c, #2                           cycles=1
0xa16c:  20 37          jra $a1a5  (offset=55)                   cycles=2
0xa16e:  72 0d 52 5c 36 btjf $525c, #6, $a1a9  (offset=54)       cycles=2-3
0xa173:  72 1d 52 5c    bres $525c, #6                           cycles=1
0xa177:  20 2c          jra $a1a5  (offset=44)                   cycles=2
0xa179:  72 05 52 5d 2b btjf $525d, #2, $a1a9  (offset=43)       cycles=2-3
0xa17e:  72 15 52 5d    bres $525d, #2                           cycles=1
0xa182:  20 21          jra $a1a5  (offset=33)                   cycles=2
0xa184:  72 04 52 5c 20 btjt $525c, #2, $a1a9  (offset=32)       cycles=2-3
0xa189:  72 14 52 5c    bset $525c, #2                           cycles=1
0xa18d:  20 16          jra $a1a5  (offset=22)                   cycles=2
0xa18f:  72 0c 52 5c 15 btjt $525c, #6, $a1a9  (offset=21)       cycles=2-3
0xa194:  72 1c 52 5c    bset $525c, #6                           cycles=1
0xa198:  20 0b          jra $a1a5  (offset=11)                   cycles=2
0xa19a:  72 04 52 5d 0a btjt $525d, #2, $a1a9  (offset=10)       cycles=2-3
0xa19f:  72 14 52 5d    bset $525d, #2                           cycles=1
0xa1a3:  20 00          jra $a1a5  (offset=0)                    cycles=2
0xa1a5:  72 1a 52 57    bset $5257, #5                           cycles=1
0xa1a9:  81             ret                                      cycles=4
0xa1aa:  72 10 00 8d    bset $8d, #0                             cycles=1
0xa1ae:  72 10 52 5c    bset $525c, #0                           cycles=1
0xa1b2:  72 1a 52 57    bset $5257, #5                           cycles=1
0xa1b6:  a6 01          ld A, #$01                               cycles=1
0xa1b8:  c7 52 65       ld $5265,A                               cycles=1
0xa1bb:  a6 f4          ld A, #$f4                               cycles=1
0xa1bd:  c7 52 66       ld $5266,A                               cycles=1
0xa1c0:  72 10 52 57    bset $5257, #0                           cycles=1
0xa1c4:  cd a2 c6       call $a2c6                               cycles=4
0xa1c7:  72 0f 52 6d 62 btjf $526d, #7, $a22e  (offset=98)       cycles=2-3
0xa1cc:  72 11 52 5c    bres $525c, #0                           cycles=1
0xa1d0:  72 1a 52 57    bset $5257, #5                           cycles=1
0xa1d4:  cd a2 c6       call $a2c6                               cycles=4
0xa1d7:  cd a2 c6       call $a2c6                               cycles=4
0xa1da:  72 18 52 5c    bset $525c, #4                           cycles=1
0xa1de:  72 1a 52 57    bset $5257, #5                           cycles=1
0xa1e2:  a6 01          ld A, #$01                               cycles=1
0xa1e4:  c7 52 67       ld $5267,A                               cycles=1
0xa1e7:  a6 f4          ld A, #$f4                               cycles=1
0xa1e9:  c7 52 68       ld $5268,A                               cycles=1
0xa1ec:  72 10 52 57    bset $5257, #0                           cycles=1
0xa1f0:  cd a2 c6       call $a2c6                               cycles=4
0xa1f3:  72 0f 52 6d 36 btjf $526d, #7, $a22e  (offset=54)       cycles=2-3
0xa1f8:  72 19 52 5c    bres $525c, #4                           cycles=1
0xa1fc:  72 1a 52 57    bset $5257, #5                           cycles=1
0xa200:  cd a2 c6       call $a2c6                               cycles=4
0xa203:  cd a2 c6       call $a2c6                               cycles=4
0xa206:  72 10 52 5d    bset $525d, #0                           cycles=1
0xa20a:  72 1a 52 57    bset $5257, #5                           cycles=1
0xa20e:  a6 01          ld A, #$01                               cycles=1
0xa210:  c7 52 69       ld $5269,A                               cycles=1
0xa213:  a6 f4          ld A, #$f4                               cycles=1
0xa215:  c7 52 6a       ld $526a,A                               cycles=1
0xa218:  72 10 52 57    bset $5257, #0                           cycles=1
0xa21c:  cd a2 c6       call $a2c6                               cycles=4
0xa21f:  72 0f 52 6d 0a btjf $526d, #7, $a22e  (offset=10)       cycles=2-3
0xa224:  72 11 52 5d    bres $525d, #0                           cycles=1
0xa228:  72 1a 52 57    bset $5257, #5                           cycles=1
0xa22c:  20 03          jra $a231  (offset=3)                    cycles=2
0xa22e:  cc a2 b8       jp $a2b8                                 cycles=1
0xa231:  83             trap                                     cycles=9
0xa232:  cd a2 c6       call $a2c6                               cycles=4
0xa235:  cd a2 c6       call $a2c6                               cycles=4
0xa238:  72 14 52 5c    bset $525c, #2                           cycles=1
0xa23c:  72 1a 52 57    bset $5257, #5                           cycles=1
0xa240:  a6 01          ld A, #$01                               cycles=1
0xa242:  c7 52 65       ld $5265,A                               cycles=1
0xa245:  a6 f4          ld A, #$f4                               cycles=1
0xa247:  c7 52 66       ld $5266,A                               cycles=1
0xa24a:  72 10 52 57    bset $5257, #0                           cycles=1
0xa24e:  cd a2 c6       call $a2c6                               cycles=4
0xa251:  72 0f 52 6d d8 btjf $526d, #7, $a22e  (offset=-40)      cycles=2-3
0xa256:  72 15 52 5c    bres $525c, #2                           cycles=1
0xa25a:  72 1a 52 57    bset $5257, #5                           cycles=1
0xa25e:  cd a2 c6       call $a2c6                               cycles=4
0xa261:  cd a2 c6       call $a2c6                               cycles=4
0xa264:  72 1c 52 5c    bset $525c, #6                           cycles=1
0xa268:  72 1a 52 57    bset $5257, #5                           cycles=1
0xa26c:  a6 01          ld A, #$01                               cycles=1
0xa26e:  c7 52 67       ld $5267,A                               cycles=1
0xa271:  a6 f4          ld A, #$f4                               cycles=1
0xa273:  c7 52 68       ld $5268,A                               cycles=1
0xa276:  72 10 52 57    bset $5257, #0                           cycles=1
0xa27a:  cd a2 c6       call $a2c6                               cycles=4
0xa27d:  72 0f 52 6d ac btjf $526d, #7, $a22e  (offset=-84)      cycles=2-3
0xa282:  72 1d 52 5c    bres $525c, #6                           cycles=1
0xa286:  72 1a 52 57    bset $5257, #5                           cycles=1
0xa28a:  cd a2 c6       call $a2c6                               cycles=4
0xa28d:  cd a2 c6       call $a2c6                               cycles=4
0xa290:  72 14 52 5d    bset $525d, #2                           cycles=1
0xa294:  72 1a 52 57    bset $5257, #5                           cycles=1
0xa298:  a6 01          ld A, #$01                               cycles=1
0xa29a:  c7 52 69       ld $5269,A                               cycles=1
0xa29d:  a6 f4          ld A, #$f4                               cycles=1
0xa29f:  c7 52 6a       ld $526a,A                               cycles=1
0xa2a2:  72 10 52 57    bset $5257, #0                           cycles=1
0xa2a6:  cd a2 c6       call $a2c6                               cycles=4
0xa2a9:  72 0f 52 6d 80 btjf $526d, #7, $a22e  (offset=-128)     cycles=2-3
0xa2ae:  72 15 52 5d    bres $525d, #2                           cycles=1
0xa2b2:  72 1a 52 57    bset $5257, #5                           cycles=1
0xa2b6:  20 08          jra $a2c0  (offset=8)                    cycles=2
0xa2b8:  72 16 00 8d    bset $8d, #3                             cycles=1
0xa2bc:  72 18 00 8d    bset $8d, #4                             cycles=1
0xa2c0:  83             trap                                     cycles=9
0xa2c1:  72 11 00 8d    bres $8d, #0                             cycles=1
0xa2c5:  81             ret                                      cycles=4
0xa2c6:  a6 c8          ld A, #$c8                               cycles=1
0xa2c8:  7d             tnz (X)                                  cycles=1
0xa2c9:  4a             dec A                                    cycles=1
0xa2ca:  26 fc          jrne $a2c8  (offset=-4)                  cycles=1-2
0xa2cc:  81             ret                                      cycles=4
0xa2cd:  b6 1c          ld A, $1c                                cycles=1
0xa2cf:  a1 55          cp A, #$55                               cycles=1
0xa2d1:  25 08          jrc $a2db  (offset=8)                    cycles=1-2
0xa2d3:  72 18 00 8d    bset $8d, #4                             cycles=1
0xa2d7:  72 1a 00 8f    bset $8f, #5                             cycles=1
0xa2db:  b6 20          ld A, $20                                cycles=1
0xa2dd:  a1 f0          cp A, #$f0                               cycles=1
0xa2df:  24 28          jrnc $a309  (offset=40)                  cycles=1-2
0xa2e1:  a1 0a          cp A, #$0a                               cycles=1
0xa2e3:  25 23          jrc $a308  (offset=35)                   cycles=1-2
0xa2e5:  b7 e1          ld $e1,A                                 cycles=1
0xa2e7:  b7 e2          ld $e2,A                                 cycles=1
0xa2e9:  b7 e3          ld $e3,A                                 cycles=1
0xa2eb:  ab 0a          add A, #$0a                              cycles=1
0xa2ed:  b7 d9          ld $d9,A                                 cycles=1
0xa2ef:  b6 e1          ld A, $e1                                cycles=1
0xa2f1:  ab 06          add A, #$06                              cycles=1
0xa2f3:  b7 e4          ld $e4,A                                 cycles=1
0xa2f5:  a0 06          sub A, #$06                              cycles=1
0xa2f7:  a0 06          sub A, #$06                              cycles=1
0xa2f9:  b7 e5          ld $e5,A                                 cycles=1
0xa2fb:  b7 36          ld $36,A                                 cycles=1
0xa2fd:  a6 06          ld A, #$06                               cycles=1
0xa2ff:  b7 37          ld $37,A                                 cycles=1
0xa301:  cd 9e 63       call $9e63                               cycles=4
0xa304:  72 16 00 97    bset $97, #3                             cycles=1
0xa308:  81             ret                                      cycles=4
0xa309:  a6 f0          ld A, #$f0                               cycles=1
0xa30b:  b7 d9          ld $d9,A                                 cycles=1
0xa30d:  b7 e1          ld $e1,A                                 cycles=1
0xa30f:  b7 e2          ld $e2,A                                 cycles=1
0xa311:  b6 e1          ld A, $e1                                cycles=1
0xa313:  ab 06          add A, #$06                              cycles=1
0xa315:  b7 e4          ld $e4,A                                 cycles=1
0xa317:  a0 06          sub A, #$06                              cycles=1
0xa319:  a0 06          sub A, #$06                              cycles=1
0xa31b:  b7 e5          ld $e5,A                                 cycles=1
0xa31d:  b7 36          ld $36,A                                 cycles=1
0xa31f:  a6 06          ld A, #$06                               cycles=1
0xa321:  b7 37          ld $37,A                                 cycles=1
0xa323:  cd 9e 63       call $9e63                               cycles=4
0xa326:  72 16 00 97    bset $97, #3                             cycles=1
0xa32a:  81             ret                                      cycles=4
0xa32b:  72 06 00 90 05 btjt $90, #3, $a335  (offset=5)          cycles=2-3
0xa330:  72 11 00 90    bres $90, #0                             cycles=1
0xa334:  81             ret                                      cycles=4
0xa335:  72 14 00 8f    bset $8f, #2                             cycles=1
0xa339:  83             trap                                     cycles=9
0xa33a:  72 1a 00 90    bset $90, #5                             cycles=1
0xa33e:  cd 9e 90       call $9e90                               cycles=4
0xa341:  b6 1f          ld A, $1f                                cycles=1
0xa343:  b7 28          ld $28,A                                 cycles=1
0xa345:  9b             sim                                      cycles=1
0xa346:  83             trap                                     cycles=9
0xa347:  72 14 52 5c    bset $525c, #2                           cycles=1
0xa34b:  72 1c 52 5c    bset $525c, #6                           cycles=1
0xa34f:  72 14 52 5d    bset $525d, #2                           cycles=1
0xa353:  72 1a 52 57    bset $5257, #5                           cycles=1
0xa357:  9a             rim                                      cycles=1
0xa358:  72 07 00 90 20 btjf $90, #3, $a37d  (offset=32)         cycles=2-3
0xa35d:  72 03 00 8a 05 btjf $8a, #1, $a367  (offset=5)          cycles=2-3
0xa362:  cd 9e 9c       call $9e9c                               cycles=4
0xa365:  20 f1          jra $a358  (offset=-15)                  cycles=2
0xa367:  72 03 00 8f 09 btjf $8f, #1, $a375  (offset=9)          cycles=2-3
0xa36c:  72 1b 00 90    bres $90, #5                             cycles=1
0xa370:  83             trap                                     cycles=9
0xa371:  72 10 00 90    bset $90, #0                             cycles=1
0xa375:  cd 9e 94       call $9e94                               cycles=4
0xa378:  72 06 00 90 f8 btjt $90, #3, $a375  (offset=-8)         cycles=2-3
0xa37d:  72 1b 00 90    bres $90, #5                             cycles=1
0xa381:  83             trap                                     cycles=9
0xa382:  72 11 00 90    bres $90, #0                             cycles=1
0xa386:  72 15 00 8f    bres $8f, #2                             cycles=1
0xa38a:  81             ret                                      cycles=4
0xa38b:  81             ret                                      cycles=4
0xa38c:  72 05 00 8a 5e btjf $8a, #2, $a3ef  (offset=94)         cycles=2-3
0xa391:  72 09 00 89 58 btjf $89, #4, $a3ee  (offset=88)         cycles=2-3
0xa396:  72 19 00 89    bres $89, #4                             cycles=1
0xa39a:  b6 a8          ld A, $a8                                cycles=1
0xa39c:  a1 04          cp A, #$04                               cycles=1
0xa39e:  25 0f          jrc $a3af  (offset=15)                   cycles=1-2
0xa3a0:  b6 17          ld A, $17                                cycles=1
0xa3a2:  5f             clrw X                                   cycles=1
0xa3a3:  97             ld XL, A                                 cycles=1
0xa3a4:  a6 32          ld A, #$32                               cycles=1
0xa3a6:  42             mul X, A                                 cycles=4
0xa3a7:  a6 0a          ld A, #$0a                               cycles=1
0xa3a9:  62             div X, A                                 cycles=2-17
0xa3aa:  a3 00 96       cpw X, #$96                              cycles=2
0xa3ad:  25 06          jrc $a3b5  (offset=6)                    cycles=1-2
0xa3af:  a6 96          ld A, #$96                               cycles=1
0xa3b1:  b7 16          ld $16,A                                 cycles=1
0xa3b3:  20 09          jra $a3be  (offset=9)                    cycles=2
0xa3b5:  9f             ld A, XL                                 cycles=1
0xa3b6:  a1 06          cp A, #$06                               cycles=1
0xa3b8:  24 02          jrnc $a3bc  (offset=2)                   cycles=1-2
0xa3ba:  a6 06          ld A, #$06                               cycles=1
0xa3bc:  b7 16          ld $16,A                                 cycles=1
0xa3be:  b6 17          ld A, $17                                cycles=1
0xa3c0:  a1 03          cp A, #$03                               cycles=1
0xa3c2:  24 02          jrnc $a3c6  (offset=2)                   cycles=1-2
0xa3c4:  a6 03          ld A, #$03                               cycles=1
0xa3c6:  5f             clrw X                                   cycles=1
0xa3c7:  ae 01 2c       ldw X, #$12c                             cycles=2
0xa3ca:  62             div X, A                                 cycles=2-17
0xa3cb:  9f             ld A, XL                                 cycles=1
0xa3cc:  a1 64          cp A, #$64                               cycles=1
0xa3ce:  25 04          jrc $a3d4  (offset=4)                    cycles=1-2
0xa3d0:  a6 64          ld A, #$64                               cycles=1
0xa3d2:  20 06          jra $a3da  (offset=6)                    cycles=2
0xa3d4:  a1 14          cp A, #$14                               cycles=1
0xa3d6:  24 02          jrnc $a3da  (offset=2)                   cycles=1-2
0xa3d8:  a6 14          ld A, #$14                               cycles=1
0xa3da:  b1 18          cp A, $18                                cycles=1
0xa3dc:  25 04          jrc $a3e2  (offset=4)                    cycles=1-2
0xa3de:  3c 18          inc $18                                  cycles=1
0xa3e0:  20 02          jra $a3e4  (offset=2)                    cycles=2
0xa3e2:  3a 18          dec $18                                  cycles=1
0xa3e4:  b6 18          ld A, $18                                cycles=1
0xa3e6:  a1 0a          cp A, #$0a                               cycles=1
0xa3e8:  24 02          jrnc $a3ec  (offset=2)                   cycles=1-2
0xa3ea:  a6 0a          ld A, #$0a                               cycles=1
0xa3ec:  b7 04          ld $04,A                                 cycles=1
0xa3ee:  81             ret                                      cycles=4
0xa3ef:  3f 04          clr $04                                  cycles=1
0xa3f1:  81             ret                                      cycles=4
0xa3f2:  72 0f 00 8a 4a btjf $8a, #7, $a441  (offset=74)         cycles=2-3
0xa3f7:  72 0c 00 8a 25 btjt $8a, #6, $a421  (offset=37)         cycles=2-3
0xa3fc:  72 04 00 8a 20 btjt $8a, #2, $a421  (offset=32)         cycles=2-3
0xa401:  72 04 50 01 33 btjt $5001, #2, $a439  (offset=51)       cycles=2-3
0xa406:  72 0e 00 8b 36 btjt $8b, #7, $a441  (offset=54)         cycles=2-3
0xa40b:  72 1c 00 8b    bset $8b, #6                             cycles=1
0xa40f:  72 0b 00 8b 2d btjf $8b, #5, $a441  (offset=45)         cycles=2-3
0xa414:  72 1d 00 8b    bres $8b, #6                             cycles=1
0xa418:  72 1b 00 8b    bres $8b, #5                             cycles=1
0xa41c:  72 05 00 8b 06 btjf $8b, #2, $a427  (offset=6)          cycles=2-3
0xa421:  72 15 00 8b    bres $8b, #2                             cycles=1
0xa425:  20 0c          jra $a433  (offset=12)                   cycles=2
0xa427:  72 14 00 8b    bset $8b, #2                             cycles=1
0xa42b:  72 13 00 8b    bres $8b, #1                             cycles=1
0xa42f:  72 11 00 8b    bres $8b, #0                             cycles=1
0xa433:  72 1e 00 8b    bset $8b, #7                             cycles=1
0xa437:  20 08          jra $a441  (offset=8)                    cycles=2
0xa439:  72 1f 00 8b    bres $8b, #7                             cycles=1
0xa43d:  72 1d 00 8b    bres $8b, #6                             cycles=1
0xa441:  81             ret                                      cycles=4
0xa442:  81             ret                                      cycles=4
0xa443:  5f             clrw X                                   cycles=1
0xa444:  a6 08          ld A, #$08                               cycles=1
0xa446:  97             ld XL, A                                 cycles=1
0xa447:  d6 80 80       ld A, ($8080,X)                          cycles=1
0xa44a:  b7 5d          ld $5d,A                                 cycles=1
0xa44c:  5c             incw X                                   cycles=1
0xa44d:  d6 80 80       ld A, ($8080,X)                          cycles=1
0xa450:  b7 5e          ld $5e,A                                 cycles=1
0xa452:  5c             incw X                                   cycles=1
0xa453:  d6 80 80       ld A, ($8080,X)                          cycles=1
0xa456:  b7 5f          ld $5f,A                                 cycles=1
0xa458:  5c             incw X                                   cycles=1
0xa459:  d6 80 80       ld A, ($8080,X)                          cycles=1
0xa45c:  b7 60          ld $60,A                                 cycles=1
0xa45e:  5c             incw X                                   cycles=1
0xa45f:  d6 80 80       ld A, ($8080,X)                          cycles=1
0xa462:  b7 61          ld $61,A                                 cycles=1
0xa464:  5c             incw X                                   cycles=1
0xa465:  d6 80 80       ld A, ($8080,X)                          cycles=1
0xa468:  b7 62          ld $62,A                                 cycles=1
0xa46a:  81             ret                                      cycles=4
0xa46b:  72 12 00 8f    bset $8f, #1                             cycles=1
0xa46f:  72 1c 00 92    bset $92, #6                             cycles=1
0xa473:  20 00          jra $a475  (offset=0)                    cycles=2
0xa475:  81             ret                                      cycles=4
0xa476:  a6 0e          ld A, #$0e                               cycles=1
0xa478:  b7 37          ld $37,A                                 cycles=1
0xa47a:  72 02 00 8f 04 btjt $8f, #1, $a483  (offset=4)          cycles=2-3
0xa47f:  a6 01          ld A, #$01                               cycles=1
0xa481:  20 01          jra $a484  (offset=1)                    cycles=2
0xa483:  4f             clr A                                    cycles=1
0xa484:  b7 36          ld $36,A                                 cycles=1
0xa486:  cd 9e 3a       call $9e3a                               cycles=4
0xa489:  81             ret                                      cycles=4
0xa48a:  72 18 00 90    bset $90, #4                             cycles=1
0xa48e:  81             ret                                      cycles=4
0xa48f:  ae 00 06       ldw X, #$6                               cycles=2
0xa492:  d6 80 80       ld A, ($8080,X)                          cycles=1
0xa495:  4d             tnz A                                    cycles=1
0xa496:  27 09          jreq $a4a1  (offset=9)                   cycles=1-2
0xa498:  72 08 00 90 14 btjt $90, #4, $a4b1  (offset=20)         cycles=2-3
0xa49d:  a6 00          ld A, #$00                               cycles=1
0xa49f:  20 07          jra $a4a8  (offset=7)                    cycles=2
0xa4a1:  72 09 00 90 0b btjf $90, #4, $a4b1  (offset=11)         cycles=2-3
0xa4a6:  a6 01          ld A, #$01                               cycles=1
0xa4a8:  b7 36          ld $36,A                                 cycles=1
0xa4aa:  a6 06          ld A, #$06                               cycles=1
0xa4ac:  b7 37          ld $37,A                                 cycles=1
0xa4ae:  cd 9e 3a       call $9e3a                               cycles=4
0xa4b1:  81             ret                                      cycles=4
0xa4b2:  81             ret                                      cycles=4
0xa4b3:  72 0f 00 8d 03 btjf $8d, #7, $a4bb  (offset=3)          cycles=2-3
0xa4b8:  4f             clr A                                    cycles=1
0xa4b9:  20 02          jra $a4bd  (offset=2)                    cycles=2
0xa4bb:  a6 01          ld A, #$01                               cycles=1
0xa4bd:  b7 36          ld $36,A                                 cycles=1
0xa4bf:  a6 07          ld A, #$07                               cycles=1
0xa4c1:  b7 37          ld $37,A                                 cycles=1
0xa4c3:  cd 9e 3a       call $9e3a                               cycles=4
0xa4c6:  81             ret                                      cycles=4
0xa4c7:  72 0f 00 8a 31 btjf $8a, #7, $a4fd  (offset=49)         cycles=2-3
0xa4cc:  72 00 00 8c 2c btjt $8c, #0, $a4fd  (offset=44)         cycles=2-3
0xa4d1:  72 02 00 8c 27 btjt $8c, #1, $a4fd  (offset=39)         cycles=2-3
0xa4d6:  72 06 00 89 22 btjt $89, #3, $a4fd  (offset=34)         cycles=2-3
0xa4db:  72 01 00 92 1d btjf $92, #0, $a4fd  (offset=29)         cycles=2-3
0xa4e0:  72 02 53 21 21 btjt $5321, #1, $a506  (offset=33)       cycles=2-3
0xa4e5:  72 03 00 92 13 btjf $92, #1, $a4fd  (offset=19)         cycles=2-3
0xa4ea:  b6 3e          ld A, $3e                                cycles=1
0xa4ec:  c7 53 2d       ld $532d,A                               cycles=1
0xa4ef:  b6 3f          ld A, $3f                                cycles=1
0xa4f1:  c7 53 2e       ld $532e,A                               cycles=1
0xa4f4:  72 13 53 22    bres $5322, #1                           cycles=1
0xa4f8:  72 12 53 21    bset $5321, #1                           cycles=1
0xa4fc:  81             ret                                      cycles=4
0xa4fd:  72 13 53 22    bres $5322, #1                           cycles=1
0xa501:  72 13 53 21    bres $5321, #1                           cycles=1
0xa505:  81             ret                                      cycles=4
0xa506:  81             ret                                      cycles=4
0xa507:  72 0f 00 8a 2b btjf $8a, #7, $a537  (offset=43)         cycles=2-3
0xa50c:  72 02 00 8f 0a btjt $8f, #1, $a51b  (offset=10)         cycles=2-3
0xa511:  72 02 00 89 05 btjt $89, #1, $a51b  (offset=5)          cycles=2-3
0xa516:  72 0c 00 8a 21 btjt $8a, #6, $a53c  (offset=33)         cycles=2-3
0xa51b:  81             ret                                      cycles=4
0xa51c:  b6 6c          ld A, $6c                                cycles=1
0xa51e:  a1 05          cp A, #$05                               cycles=1
0xa520:  25 0c          jrc $a52e  (offset=12)                   cycles=1-2
0xa522:  a0 05          sub A, #$05                              cycles=1
0xa524:  b7 6c          ld $6c,A                                 cycles=1
0xa526:  81             ret                                      cycles=4
0xa527:  3d 6c          tnz $6c                                  cycles=1
0xa529:  27 03          jreq $a52e  (offset=3)                   cycles=1-2
0xa52b:  3a 6c          dec $6c                                  cycles=1
0xa52d:  81             ret                                      cycles=4
0xa52e:  83             trap                                     cycles=9
0xa52f:  72 18 00 8d    bset $8d, #4                             cycles=1
0xa533:  72 1c 00 8f    bset $8f, #6                             cycles=1
0xa537:  a6 1e          ld A, #$1e                               cycles=1
0xa539:  b7 6c          ld $6c,A                                 cycles=1
0xa53b:  81             ret                                      cycles=4
0xa53c:  cd a5 1c       call $a51c                               cycles=4
0xa53f:  cd 9c 16       call $9c16                               cycles=4
0xa542:  5f             clrw X                                   cycles=1
0xa543:  b6 08          ld A, $08                                cycles=1
0xa545:  97             ld XL, A                                 cycles=1
0xa546:  72 0e 00 8d 05 btjt $8d, #7, $a550  (offset=5)          cycles=2-3
0xa54b:  d6 ab d3       ld A, ($abd3,X)                          cycles=1
0xa54e:  20 03          jra $a553  (offset=3)                    cycles=2
0xa550:  d6 ab db       ld A, ($abdb,X)                          cycles=1
0xa553:  b7 08          ld $08,A                                 cycles=1
0xa555:  72 5f 52 65    clr $5265                                cycles=1
0xa559:  72 5f 52 67    clr $5267                                cycles=1
0xa55d:  72 5f 52 69    clr $5269                                cycles=1
0xa561:  72 5f 52 6b    clr $526b                                cycles=1
0xa565:  3f 24          clr $24                                  cycles=1
0xa567:  72 5f 52 66    clr $5266                                cycles=1
0xa56b:  72 5f 52 68    clr $5268                                cycles=1
0xa56f:  72 5f 52 6a    clr $526a                                cycles=1
0xa573:  72 5f 52 6c    clr $526c                                cycles=1
0xa577:  3f 25          clr $25                                  cycles=1
0xa579:  72 10 52 57    bset $5257, #0                           cycles=1
0xa57d:  72 11 52 5c    bres $525c, #0                           cycles=1
0xa581:  72 19 52 5c    bres $525c, #4                           cycles=1
0xa585:  72 11 52 5d    bres $525d, #0                           cycles=1
0xa589:  72 15 52 5c    bres $525c, #2                           cycles=1
0xa58d:  72 1d 52 5c    bres $525c, #6                           cycles=1
0xa591:  72 15 52 5d    bres $525d, #2                           cycles=1
0xa595:  72 1a 52 57    bset $5257, #5                           cycles=1
0xa599:  72 11 50 05    bres $5005, #0                           cycles=1
0xa59d:  72 13 50 05    bres $5005, #1                           cycles=1
0xa5a1:  72 15 50 05    bres $5005, #2                           cycles=1
0xa5a5:  cd 9e 9c       call $9e9c                               cycles=4
0xa5a8:  72 12 00 89    bset $89, #1                             cycles=1
0xa5ac:  cd a8 8b       call $a88b                               cycles=4
0xa5af:  cd ab 98       call $ab98                               cycles=4
0xa5b2:  a6 64          ld A, #$64                               cycles=1
0xa5b4:  b7 4e          ld $4e,A                                 cycles=1
0xa5b6:  a6 05          ld A, #$05                               cycles=1
0xa5b8:  b7 4f          ld $4f,A                                 cycles=1
0xa5ba:  72 16 00 92    bset $92, #3                             cycles=1
0xa5be:  72 06 00 92 03 btjt $92, #3, $a5c6  (offset=3)          cycles=2-3
0xa5c3:  cc a5 3c       jp $a53c                                 cycles=1
0xa5c6:  cd 9e 8a       call $9e8a                               cycles=4
0xa5c9:  72 0f 00 8a 25 btjf $8a, #7, $a5f3  (offset=37)         cycles=2-3
0xa5ce:  72 08 00 8d 20 btjt $8d, #4, $a5f3  (offset=32)         cycles=2-3
0xa5d3:  cd a6 c7       call $a6c7                               cycles=4
0xa5d6:  9f             ld A, XL                                 cycles=1
0xa5d7:  b1 08          cp A, $08                                cycles=1
0xa5d9:  26 e3          jrne $a5be  (offset=-29)                 cycles=1-2
0xa5db:  9b             sim                                      cycles=1
0xa5dc:  cd a9 9b       call $a99b                               cycles=4
0xa5df:  cd a0 dd       call $a0dd                               cycles=4
0xa5e2:  cd b9 f9       call $b9f9                               cycles=4
0xa5e5:  9a             rim                                      cycles=1
0xa5e6:  a6 64          ld A, #$64                               cycles=1
0xa5e8:  b7 4e          ld $4e,A                                 cycles=1
0xa5ea:  a6 05          ld A, #$05                               cycles=1
0xa5ec:  b7 4f          ld $4f,A                                 cycles=1
0xa5ee:  72 16 00 92    bset $92, #3                             cycles=1
0xa5f2:  81             ret                                      cycles=4
0xa5f3:  81             ret                                      cycles=4
0xa5f4:  72 02 00 8f fa btjt $8f, #1, $a5f3  (offset=-6)         cycles=2-3
0xa5f9:  72 0c 50 0e f5 btjt $500e, #6, $a5f3  (offset=-11)      cycles=2-3
0xa5fe:  72 0f 00 8a 12 btjf $8a, #7, $a615  (offset=18)         cycles=2-3
0xa603:  72 0d 00 8a 0d btjf $8a, #6, $a615  (offset=13)         cycles=2-3
0xa608:  72 03 00 89 e6 btjf $89, #1, $a5f3  (offset=-26)        cycles=2-3
0xa60d:  72 06 00 92 03 btjt $92, #3, $a615  (offset=3)          cycles=2-3
0xa612:  cc a5 3c       jp $a53c                                 cycles=1
0xa615:  72 0b 00 8c d9 btjf $8c, #5, $a5f3  (offset=-39)        cycles=2-3
0xa61a:  72 1b 00 8c    bres $8c, #5                             cycles=1
0xa61e:  cd a6 c7       call $a6c7                               cycles=4
0xa621:  9f             ld A, XL                                 cycles=1
0xa622:  b1 07          cp A, $07                                cycles=1
0xa624:  27 cd          jreq $a5f3  (offset=-51)                 cycles=1-2
0xa626:  b1 06          cp A, $06                                cycles=1
0xa628:  27 06          jreq $a630  (offset=6)                   cycles=1-2
0xa62a:  a6 1e          ld A, #$1e                               cycles=1
0xa62c:  b7 50          ld $50,A                                 cycles=1
0xa62e:  20 c3          jra $a5f3  (offset=-61)                  cycles=2
0xa630:  b7 08          ld $08,A                                 cycles=1
0xa632:  72 0f 00 8a 3e btjf $8a, #7, $a675  (offset=62)         cycles=2-3
0xa637:  b6 50          ld A, $50                                cycles=1
0xa639:  a1 32          cp A, #$32                               cycles=1
0xa63b:  27 04          jreq $a641  (offset=4)                   cycles=1-2
0xa63d:  b6 59          ld A, $59                                cycles=1
0xa63f:  20 02          jra $a643  (offset=2)                    cycles=2
0xa641:  a6 1e          ld A, #$1e                               cycles=1
0xa643:  5f             clrw X                                   cycles=1
0xa644:  97             ld XL, A                                 cycles=1
0xa645:  a3 00 1e       cpw X, #$1e                              cycles=2
0xa648:  24 06          jrnc $a650  (offset=6)                   cycles=1-2
0xa64a:  d6 a6 9f       ld A, ($a69f,X)                          cycles=1
0xa64d:  97             ld XL, A                                 cycles=1
0xa64e:  20 0b          jra $a65b  (offset=11)                   cycles=2
0xa650:  a3 00 32       cpw X, #$32                              cycles=2
0xa653:  25 03          jrc $a658  (offset=3)                    cycles=1-2
0xa655:  cd a5 27       call $a527                               cycles=4
0xa658:  ae 00 50       ldw X, #$50                              cycles=2
0xa65b:  5d             tnzw X                                   cycles=2
0xa65c:  27 06          jreq $a664  (offset=6)                   cycles=1-2
0xa65e:  cd a6 be       call $a6be                               cycles=4
0xa661:  5a             decw X                                   cycles=1
0xa662:  26 fa          jrne $a65e  (offset=-6)                  cycles=1-2
0xa664:  9b             sim                                      cycles=1
0xa665:  cd ba ca       call $baca                               cycles=4
0xa668:  b6 0e          ld A, $0e                                cycles=1
0xa66a:  b7 59          ld $59,A                                 cycles=1
0xa66c:  cd a9 9b       call $a99b                               cycles=4
0xa66f:  cd a0 dd       call $a0dd                               cycles=4
0xa672:  9a             rim                                      cycles=1
0xa673:  20 07          jra $a67c  (offset=7)                    cycles=2
0xa675:  cd ba ca       call $baca                               cycles=4
0xa678:  b6 0e          ld A, $0e                                cycles=1
0xa67a:  b7 59          ld $59,A                                 cycles=1
0xa67c:  cd ab 98       call $ab98                               cycles=4
0xa67f:  cd b9 f9       call $b9f9                               cycles=4
0xa682:  a6 64          ld A, #$64                               cycles=1
0xa684:  b7 4e          ld $4e,A                                 cycles=1
0xa686:  a6 05          ld A, #$05                               cycles=1
0xa688:  b7 4f          ld $4f,A                                 cycles=1
0xa68a:  72 16 00 92    bset $92, #3                             cycles=1
0xa68e:  3d 50          tnz $50                                  cycles=1
0xa690:  27 04          jreq $a696  (offset=4)                   cycles=1-2
0xa692:  3a 50          dec $50                                  cycles=1
0xa694:  20 08          jra $a69e  (offset=8)                    cycles=2
0xa696:  b6 59          ld A, $59                                cycles=1
0xa698:  a1 07          cp A, #$07                               cycles=1
0xa69a:  24 02          jrnc $a69e  (offset=2)                   cycles=1-2
0xa69c:  9b             sim                                      cycles=1
0xa69d:  9a             rim                                      cycles=1
0xa69e:  81             ret                                      cycles=4
0xa69f:  00 00          neg ($00,SP)                             cycles=1
0xa6a1:  00 00          neg ($00,SP)                             cycles=1
0xa6a3:  00 00          neg ($00,SP)                             cycles=1
0xa6a5:  00 00          neg ($00,SP)                             cycles=1
0xa6a7:  00 00          neg ($00,SP)                             cycles=1
0xa6a9:  00 01          neg ($01,SP)                             cycles=1
0xa6ab:  03 06          cpl ($06,SP)                             cycles=1
0xa6ad:  09 0c          rlc ($0c,SP)                             cycles=1
0xa6af:  0f 12          clr ($12,SP)                             cycles=1
0xa6b1:  15 18          bcp A, ($18,SP)                          cycles=1
0xa6b3:  1b 1e          add A, ($1e,SP)                          cycles=1
0xa6b5:  23 28          jrule $a6df  (offset=40)                 cycles=1-2
0xa6b7:  2d 32          jrsle $a6eb  (offset=50)                 cycles=1-2
0xa6b9:  37 3c          sra $3c                                  cycles=1
0xa6bb:  41             exg A, XL                                cycles=1
0xa6bc:  46             rrc A                                    cycles=1
0xa6bd:  4b 90          push #$90                                cycles=1
0xa6bf:  ae 01 f4       ldw X, #$1f4                             cycles=2
0xa6c2:  90 5a          decw Y                                   cycles=1
0xa6c4:  26 fc          jrne $a6c2  (offset=-4)                  cycles=1-2
0xa6c6:  81             ret                                      cycles=4
0xa6c7:  5f             clrw X                                   cycles=1
0xa6c8:  3f 4d          clr $4d                                  cycles=1
0xa6ca:  cd 9e 8a       call $9e8a                               cycles=4
0xa6cd:  4f             clr A                                    cycles=1
0xa6ce:  72 03 50 15 02 btjf $5015, #1, $a6d5  (offset=2)        cycles=2-3
0xa6d3:  aa 04          or A, #$04                               cycles=1
0xa6d5:  72 03 50 10 02 btjf $5010, #1, $a6dc  (offset=2)        cycles=2-3
0xa6da:  aa 02          or A, #$02                               cycles=1
0xa6dc:  aa 01          or A, #$01                               cycles=1
0xa6de:  b7 00          ld $00,A                                 cycles=1
0xa6e0:  9f             ld A, XL                                 cycles=1
0xa6e1:  b1 00          cp A, $00                                cycles=1
0xa6e3:  27 07          jreq $a6ec  (offset=7)                   cycles=1-2
0xa6e5:  b6 00          ld A, $00                                cycles=1
0xa6e7:  97             ld XL, A                                 cycles=1
0xa6e8:  3f 4d          clr $4d                                  cycles=1
0xa6ea:  20 de          jra $a6ca  (offset=-34)                  cycles=2
0xa6ec:  3c 4d          inc $4d                                  cycles=1
0xa6ee:  b6 4d          ld A, $4d                                cycles=1
0xa6f0:  72 02 00 8d 06 btjt $8d, #1, $a6fb  (offset=6)          cycles=2-3
0xa6f5:  a1 64          cp A, #$64                               cycles=1
0xa6f7:  25 d1          jrc $a6ca  (offset=-47)                  cycles=1-2
0xa6f9:  20 04          jra $a6ff  (offset=4)                    cycles=2
0xa6fb:  a1 c8          cp A, #$c8                               cycles=1
0xa6fd:  25 cb          jrc $a6ca  (offset=-53)                  cycles=1-2
0xa6ff:  81             ret                                      cycles=4
0xa700:  72 03 00 8f 01 btjf $8f, #1, $a706  (offset=1)          cycles=2-3
0xa705:  81             ret                                      cycles=4
0xa706:  3f 00          clr $00                                  cycles=1
0xa708:  72 0b 50 0b 04 btjf $500b, #5, $a711  (offset=4)        cycles=2-3
0xa70d:  72 14 00 00    bset $0, #2                              cycles=1
0xa711:  72 05 50 10 04 btjf $5010, #2, $a71a  (offset=4)        cycles=2-3
0xa716:  72 12 00 00    bset $0, #1                              cycles=1
0xa71a:  72 0b 50 15 04 btjf $5015, #5, $a723  (offset=4)        cycles=2-3
0xa71f:  72 10 00 00    bset $0, #0                              cycles=1
0xa723:  b1 00          cp A, $00                                cycles=1
0xa725:  27 04          jreq $a72b  (offset=4)                   cycles=1-2
0xa727:  b6 00          ld A, $00                                cycles=1
0xa729:  20 db          jra $a706  (offset=-37)                  cycles=2
0xa72b:  b1 57          cp A, $57                                cycles=1
0xa72d:  26 01          jrne $a730  (offset=1)                   cycles=1-2
0xa72f:  81             ret                                      cycles=4
0xa730:  b7 57          ld $57,A                                 cycles=1
0xa732:  72 03 00 8d 0d btjf $8d, #1, $a744  (offset=13)         cycles=2-3
0xa737:  72 0d 50 0e 07 btjf $500e, #6, $a743  (offset=7)        cycles=2-3
0xa73c:  cd a6 c7       call $a6c7                               cycles=4
0xa73f:  b6 57          ld A, $57                                cycles=1
0xa741:  e7 5c          ld ($5c,X),A                             cycles=1
0xa743:  81             ret                                      cycles=4
0xa744:  cd a6 c7       call $a6c7                               cycles=4
0xa747:  e6 5c          ld A, ($5c,X)                            cycles=1
0xa749:  b1 57          cp A, $57                                cycles=1
0xa74b:  27 07          jreq $a754  (offset=7)                   cycles=1-2
0xa74d:  3f 52          clr $52                                  cycles=1
0xa74f:  3f 88          clr $88                                  cycles=1
0xa751:  3f 51          clr $51                                  cycles=1
0xa753:  81             ret                                      cycles=4
0xa754:  a6 06          ld A, #$06                               cycles=1
0xa756:  42             mul X, A                                 cycles=4
0xa757:  dc a7 5a       jp ($a75a,X)                             cycles=1
0xa75a:  72 10 00 88    bset $88, #0                             cycles=1
0xa75e:  20 28          jra $a788  (offset=40)                   cycles=2
0xa760:  72 12 00 88    bset $88, #1                             cycles=1
0xa764:  20 22          jra $a788  (offset=34)                   cycles=2
0xa766:  72 14 00 88    bset $88, #2                             cycles=1
0xa76a:  20 1c          jra $a788  (offset=28)                   cycles=2
0xa76c:  72 16 00 88    bset $88, #3                             cycles=1
0xa770:  20 16          jra $a788  (offset=22)                   cycles=2
0xa772:  72 18 00 88    bset $88, #4                             cycles=1
0xa776:  20 10          jra $a788  (offset=16)                   cycles=2
0xa778:  72 1a 00 88    bset $88, #5                             cycles=1
0xa77c:  20 0a          jra $a788  (offset=10)                   cycles=2
0xa77e:  72 1c 00 88    bset $88, #6                             cycles=1
0xa782:  20 04          jra $a788  (offset=4)                    cycles=2
0xa784:  72 1e 00 88    bset $88, #7                             cycles=1
0xa788:  3c 52          inc $52                                  cycles=1
0xa78a:  b6 52          ld A, $52                                cycles=1
0xa78c:  a1 06          cp A, #$06                               cycles=1
0xa78e:  25 30          jrc $a7c0  (offset=48)                   cycles=1-2
0xa790:  3f 52          clr $52                                  cycles=1
0xa792:  b6 88          ld A, $88                                cycles=1
0xa794:  3f 88          clr $88                                  cycles=1
0xa796:  a1 7e          cp A, #$7e                               cycles=1
0xa798:  27 03          jreq $a79d  (offset=3)                   cycles=1-2
0xa79a:  3f 51          clr $51                                  cycles=1
0xa79c:  81             ret                                      cycles=4
0xa79d:  b6 51          ld A, $51                                cycles=1
0xa79f:  a1 0a          cp A, #$0a                               cycles=1
0xa7a1:  24 04          jrnc $a7a7  (offset=4)                   cycles=1-2
0xa7a3:  3c 51          inc $51                                  cycles=1
0xa7a5:  20 19          jra $a7c0  (offset=25)                   cycles=2
0xa7a7:  72 0e 00 8a 14 btjt $8a, #7, $a7c0  (offset=20)         cycles=2-3
0xa7ac:  9b             sim                                      cycles=1
0xa7ad:  83             trap                                     cycles=9
0xa7ae:  72 12 00 8f    bset $8f, #1                             cycles=1
0xa7b2:  cd 9c 06       call $9c06                               cycles=4
0xa7b5:  cd ab 28       call $ab28                               cycles=4
0xa7b8:  cd ab 98       call $ab98                               cycles=4
0xa7bb:  9a             rim                                      cycles=1
0xa7bc:  20 02          jra $a7c0  (offset=2)                    cycles=2
0xa7be:  3f 51          clr $51                                  cycles=1
0xa7c0:  81             ret                                      cycles=4
0xa7c1:  3d 6f          tnz $6f                                  cycles=1
0xa7c3:  26 16          jrne $a7db  (offset=22)                  cycles=1-2
0xa7c5:  a6 43          ld A, #$43                               cycles=1
0xa7c7:  b7 75          ld $75,A                                 cycles=1
0xa7c9:  b6 75          ld A, $75                                cycles=1
0xa7cb:  bb 76          add A, $76                               cycles=1
0xa7cd:  bb 77          add A, $77                               cycles=1
0xa7cf:  bb 78          add A, $78                               cycles=1
0xa7d1:  bb 79          add A, $79                               cycles=1
0xa7d3:  bb 7a          add A, $7a                               cycles=1
0xa7d5:  bb 7b          add A, $7b                               cycles=1
0xa7d7:  bb 7c          add A, $7c                               cycles=1
0xa7d9:  b7 7d          ld $7d,A                                 cycles=1
0xa7db:  72 07 52 45 16 btjf $5245, #3, $a7f6  (offset=22)       cycles=2-3
0xa7e0:  72 0d 52 40 17 btjf $5240, #6, $a7fc  (offset=23)       cycles=2-3
0xa7e5:  5f             clrw X                                   cycles=1
0xa7e6:  b6 6f          ld A, $6f                                cycles=1
0xa7e8:  a1 09          cp A, #$09                               cycles=1
0xa7ea:  24 0a          jrnc $a7f6  (offset=10)                  cycles=1-2
0xa7ec:  97             ld XL, A                                 cycles=1
0xa7ed:  e6 75          ld A, ($75,X)                            cycles=1
0xa7ef:  c7 52 41       ld $5241,A                               cycles=1
0xa7f2:  3c 6f          inc $6f                                  cycles=1
0xa7f4:  20 06          jra $a7fc  (offset=6)                    cycles=2
0xa7f6:  3f 6f          clr $6f                                  cycles=1
0xa7f8:  72 17 52 45    bres $5245, #3                           cycles=1
0xa7fc:  81             ret                                      cycles=4
0xa7fd:  b6 1c          ld A, $1c                                cycles=1
0xa7ff:  a1 49          cp A, #$49                               cycles=1
0xa801:  24 5d          jrnc $a860  (offset=93)                  cycles=1-2
0xa803:  72 02 00 85 2c btjt $85, #1, $a834  (offset=44)         cycles=2-3
0xa808:  72 04 00 85 3d btjt $85, #2, $a84a  (offset=61)         cycles=2-3
0xa80d:  72 06 00 85 4e btjt $85, #3, $a860  (offset=78)         cycles=2-3
0xa812:  72 0c 00 85 5f btjt $85, #6, $a876  (offset=95)         cycles=2-3
0xa817:  72 0e 00 85 02 btjt $85, #7, $a81e  (offset=2)          cycles=2-3
0xa81c:  20 42          jra $a860  (offset=66)                   cycles=2
0xa81e:  72 18 00 96    bset $96, #4                             cycles=1
0xa822:  72 1b 00 96    bres $96, #5                             cycles=1
0xa826:  72 11 00 8c    bres $8c, #0                             cycles=1
0xa82a:  72 13 00 8c    bres $8c, #1                             cycles=1
0xa82e:  a6 0c          ld A, #$0c                               cycles=1
0xa830:  b7 a6          ld $a6,A                                 cycles=1
0xa832:  20 56          jra $a88a  (offset=86)                   cycles=2
0xa834:  72 11 00 8c    bres $8c, #0                             cycles=1
0xa838:  72 13 00 8c    bres $8c, #1                             cycles=1
0xa83c:  72 19 00 96    bres $96, #4                             cycles=1
0xa840:  72 1a 00 96    bset $96, #5                             cycles=1
0xa844:  a6 26          ld A, #$26                               cycles=1
0xa846:  b7 a6          ld $a6,A                                 cycles=1
0xa848:  20 40          jra $a88a  (offset=64)                   cycles=2
0xa84a:  72 11 00 8c    bres $8c, #0                             cycles=1
0xa84e:  72 12 00 8c    bset $8c, #1                             cycles=1
0xa852:  72 19 00 96    bres $96, #4                             cycles=1
0xa856:  72 1b 00 96    bres $96, #5                             cycles=1
0xa85a:  a6 46          ld A, #$46                               cycles=1
0xa85c:  b7 a6          ld $a6,A                                 cycles=1
0xa85e:  20 2a          jra $a88a  (offset=42)                   cycles=2
0xa860:  72 11 00 8c    bres $8c, #0                             cycles=1
0xa864:  72 13 00 8c    bres $8c, #1                             cycles=1
0xa868:  72 19 00 96    bres $96, #4                             cycles=1
0xa86c:  72 1b 00 96    bres $96, #5                             cycles=1
0xa870:  a6 64          ld A, #$64                               cycles=1
0xa872:  b7 a6          ld $a6,A                                 cycles=1
0xa874:  20 14          jra $a88a  (offset=20)                   cycles=2
0xa876:  72 10 00 8c    bset $8c, #0                             cycles=1
0xa87a:  72 13 00 8c    bres $8c, #1                             cycles=1
0xa87e:  72 19 00 96    bres $96, #4                             cycles=1
0xa882:  72 1b 00 96    bres $96, #5                             cycles=1
0xa886:  a6 17          ld A, #$17                               cycles=1
0xa888:  b7 a6          ld $a6,A                                 cycles=1
0xa88a:  81             ret                                      cycles=4
0xa88b:  72 15 52 5c    bres $525c, #2                           cycles=1
0xa88f:  72 1d 52 5c    bres $525c, #6                           cycles=1
0xa893:  72 15 52 5d    bres $525d, #2                           cycles=1
0xa897:  72 1a 52 57    bset $5257, #5                           cycles=1
0xa89b:  5f             clrw X                                   cycles=1
0xa89c:  b6 08          ld A, $08                                cycles=1
0xa89e:  97             ld XL, A                                 cycles=1
0xa89f:  a6 03          ld A, #$03                               cycles=1
0xa8a1:  42             mul X, A                                 cycles=4
0xa8a2:  dc a8 a5       jp ($a8a5,X)                             cycles=1
0xa8a5:  cc a9 31       jp $a931                                 cycles=1
0xa8a8:  cc a8 bd       jp $a8bd                                 cycles=1
0xa8ab:  cc a8 da       jp $a8da                                 cycles=1
0xa8ae:  cc a8 f7       jp $a8f7                                 cycles=1
0xa8b1:  cc a9 14       jp $a914                                 cycles=1
0xa8b4:  cc a9 31       jp $a931                                 cycles=1
0xa8b7:  cc a9 4e       jp $a94e                                 cycles=1
0xa8ba:  cc a8 da       jp $a8da                                 cycles=1
0xa8bd:  72 15 50 05    bres $5005, #2                           cycles=1
0xa8c1:  72 12 50 05    bset $5005, #1                           cycles=1
0xa8c5:  72 10 50 05    bset $5005, #0                           cycles=1
0xa8c9:  72 11 52 5c    bres $525c, #0                           cycles=1
0xa8cd:  72 19 52 5c    bres $525c, #4                           cycles=1
0xa8d1:  72 10 52 5d    bset $525d, #0                           cycles=1
0xa8d5:  72 1a 52 57    bset $5257, #5                           cycles=1
0xa8d9:  81             ret                                      cycles=4
0xa8da:  72 14 50 05    bset $5005, #2                           cycles=1
0xa8de:  72 13 50 05    bres $5005, #1                           cycles=1
0xa8e2:  72 10 50 05    bset $5005, #0                           cycles=1
0xa8e6:  72 11 52 5c    bres $525c, #0                           cycles=1
0xa8ea:  72 18 52 5c    bset $525c, #4                           cycles=1
0xa8ee:  72 11 52 5d    bres $525d, #0                           cycles=1
0xa8f2:  72 1a 52 57    bset $5257, #5                           cycles=1
0xa8f6:  81             ret                                      cycles=4
0xa8f7:  72 15 50 05    bres $5005, #2                           cycles=1
0xa8fb:  72 13 50 05    bres $5005, #1                           cycles=1
0xa8ff:  72 10 50 05    bset $5005, #0                           cycles=1
0xa903:  72 11 52 5c    bres $525c, #0                           cycles=1
0xa907:  72 18 52 5c    bset $525c, #4                           cycles=1
0xa90b:  72 10 52 5d    bset $525d, #0                           cycles=1
0xa90f:  72 1a 52 57    bset $5257, #5                           cycles=1
0xa913:  81             ret                                      cycles=4
0xa914:  72 14 50 05    bset $5005, #2                           cycles=1
0xa918:  72 12 50 05    bset $5005, #1                           cycles=1
0xa91c:  72 11 50 05    bres $5005, #0                           cycles=1
0xa920:  72 10 52 5c    bset $525c, #0                           cycles=1
0xa924:  72 19 52 5c    bres $525c, #4                           cycles=1
0xa928:  72 11 52 5d    bres $525d, #0                           cycles=1
0xa92c:  72 1a 52 57    bset $5257, #5                           cycles=1
0xa930:  81             ret                                      cycles=4
0xa931:  72 15 50 05    bres $5005, #2                           cycles=1
0xa935:  72 12 50 05    bset $5005, #1                           cycles=1
0xa939:  72 11 50 05    bres $5005, #0                           cycles=1
0xa93d:  72 10 52 5c    bset $525c, #0                           cycles=1
0xa941:  72 19 52 5c    bres $525c, #4                           cycles=1
0xa945:  72 10 52 5d    bset $525d, #0                           cycles=1
0xa949:  72 1a 52 57    bset $5257, #5                           cycles=1
0xa94d:  81             ret                                      cycles=4
0xa94e:  72 14 50 05    bset $5005, #2                           cycles=1
0xa952:  72 13 50 05    bres $5005, #1                           cycles=1
0xa956:  72 11 50 05    bres $5005, #0                           cycles=1
0xa95a:  72 10 52 5c    bset $525c, #0                           cycles=1
0xa95e:  72 18 52 5c    bset $525c, #4                           cycles=1
0xa962:  72 11 52 5d    bres $525d, #0                           cycles=1
0xa966:  72 1a 52 57    bset $5257, #5                           cycles=1
0xa96a:  81             ret                                      cycles=4
0xa96b:  72 15 52 5c    bres $525c, #2                           cycles=1
0xa96f:  72 1d 52 5c    bres $525c, #6                           cycles=1
0xa973:  72 15 52 5d    bres $525d, #2                           cycles=1
0xa977:  72 1a 52 57    bset $5257, #5                           cycles=1
0xa97b:  72 12 00 89    bset $89, #1                             cycles=1
0xa97f:  81             ret                                      cycles=4
0xa980:  cd aa 3c       call $aa3c                               cycles=4
0xa983:  72 0e 00 8d 69 btjt $8d, #7, $a9f1  (offset=105)        cycles=2-3
0xa988:  72 09 00 90 07 btjf $90, #4, $a994  (offset=7)          cycles=2-3
0xa98d:  72 02 00 90 72 btjt $90, #1, $aa04  (offset=114)        cycles=2-3
0xa992:  20 0f          jra $a9a3  (offset=15)                   cycles=2
0xa994:  72 03 00 90 6b btjf $90, #1, $aa04  (offset=107)        cycles=2-3
0xa999:  20 08          jra $a9a3  (offset=8)                    cycles=2
0xa99b:  cd aa 3c       call $aa3c                               cycles=4
0xa99e:  72 0e 00 8d 27 btjt $8d, #7, $a9ca  (offset=39)         cycles=2-3
0xa9a3:  72 07 00 89 07 btjf $89, #3, $a9af  (offset=7)          cycles=2-3
0xa9a8:  72 02 00 8f 57 btjt $8f, #1, $aa04  (offset=87)         cycles=2-3
0xa9ad:  20 27          jra $a9d6  (offset=39)                   cycles=2
0xa9af:  dc a9 b2       jp ($a9b2,X)                             cycles=1
0xa9b2:  cc aa 3a       jp $aa3a                                 cycles=1
0xa9b5:  cc aa 81       jp $aa81                                 cycles=1
0xa9b8:  cc aa bb       jp $aabb                                 cycles=1
0xa9bb:  cc aa 47       jp $aa47                                 cycles=1
0xa9be:  cc aa d8       jp $aad8                                 cycles=1
0xa9c1:  cc aa 64       jp $aa64                                 cycles=1
0xa9c4:  cc aa 9e       jp $aa9e                                 cycles=1
0xa9c7:  cc aa 3a       jp $aa3a                                 cycles=1
0xa9ca:  72 07 00 89 07 btjf $89, #3, $a9d6  (offset=7)          cycles=2-3
0xa9cf:  72 02 00 8f 4b btjt $8f, #1, $aa1f  (offset=75)         cycles=2-3
0xa9d4:  20 d9          jra $a9af  (offset=-39)                  cycles=2
0xa9d6:  dc a9 d9       jp ($a9d9,X)                             cycles=1
0xa9d9:  cc aa 3a       jp $aa3a                                 cycles=1
0xa9dc:  cc aa 47       jp $aa47                                 cycles=1
0xa9df:  cc aa 9e       jp $aa9e                                 cycles=1
0xa9e2:  cc aa bb       jp $aabb                                 cycles=1
0xa9e5:  cc aa 64       jp $aa64                                 cycles=1
0xa9e8:  cc aa 81       jp $aa81                                 cycles=1
0xa9eb:  cc aa d8       jp $aad8                                 cycles=1
0xa9ee:  cc aa 3a       jp $aa3a                                 cycles=1
0xa9f1:  72 08 00 90 07 btjt $90, #4, $a9fd  (offset=7)          cycles=2-3
0xa9f6:  72 02 00 90 24 btjt $90, #1, $aa1f  (offset=36)         cycles=2-3
0xa9fb:  20 cd          jra $a9ca  (offset=-51)                  cycles=2
0xa9fd:  72 03 00 90 1d btjf $90, #1, $aa1f  (offset=29)         cycles=2-3
0xaa02:  20 c6          jra $a9ca  (offset=-58)                  cycles=2
0xaa04:  dc aa 07       jp ($aa07,X)                             cycles=1
0xaa07:  cc aa 3a       jp $aa3a                                 cycles=1
0xaa0a:  cc aa 9e       jp $aa9e                                 cycles=1
0xaa0d:  cc aa 64       jp $aa64                                 cycles=1
0xaa10:  cc aa d8       jp $aad8                                 cycles=1
0xaa13:  cc aa 47       jp $aa47                                 cycles=1
0xaa16:  cc aa bb       jp $aabb                                 cycles=1
0xaa19:  cc aa 81       jp $aa81                                 cycles=1
0xaa1c:  cc aa 3a       jp $aa3a                                 cycles=1
0xaa1f:  dc aa 22       jp ($aa22,X)                             cycles=1
0xaa22:  cc aa 3a       jp $aa3a                                 cycles=1
0xaa25:  cc aa d8       jp $aad8                                 cycles=1
0xaa28:  cc aa 81       jp $aa81                                 cycles=1
0xaa2b:  cc aa 64       jp $aa64                                 cycles=1
0xaa2e:  cc aa bb       jp $aabb                                 cycles=1
0xaa31:  cc aa 9e       jp $aa9e                                 cycles=1
0xaa34:  cc aa 47       jp $aa47                                 cycles=1
0xaa37:  cc aa 3a       jp $aa3a                                 cycles=1
0xaa3a:  83             trap                                     cycles=9
0xaa3b:  81             ret                                      cycles=4
0xaa3c:  cd a9 6b       call $a96b                               cycles=4
0xaa3f:  5f             clrw X                                   cycles=1
0xaa40:  b6 08          ld A, $08                                cycles=1
0xaa42:  97             ld XL, A                                 cycles=1
0xaa43:  a6 03          ld A, #$03                               cycles=1
0xaa45:  42             mul X, A                                 cycles=4
0xaa46:  81             ret                                      cycles=4
0xaa47:  72 11 52 5c    bres $525c, #0                           cycles=1
0xaa4b:  72 10 52 5d    bset $525d, #0                           cycles=1
0xaa4f:  72 19 52 5c    bres $525c, #4                           cycles=1
0xaa53:  72 1a 52 57    bset $5257, #5                           cycles=1
0xaa57:  72 15 50 05    bres $5005, #2                           cycles=1
0xaa5b:  72 10 50 05    bset $5005, #0                           cycles=1
0xaa5f:  72 13 50 05    bres $5005, #1                           cycles=1
0xaa63:  81             ret                                      cycles=4
0xaa64:  72 19 52 5c    bres $525c, #4                           cycles=1
0xaa68:  72 10 52 5c    bset $525c, #0                           cycles=1
0xaa6c:  72 11 52 5d    bres $525d, #0                           cycles=1
0xaa70:  72 1a 52 57    bset $5257, #5                           cycles=1
0xaa74:  72 11 50 05    bres $5005, #0                           cycles=1
0xaa78:  72 12 50 05    bset $5005, #1                           cycles=1
0xaa7c:  72 15 50 05    bres $5005, #2                           cycles=1
0xaa80:  81             ret                                      cycles=4
0xaa81:  72 15 50 05    bres $5005, #2                           cycles=1
0xaa85:  72 12 50 05    bset $5005, #1                           cycles=1
0xaa89:  72 11 50 05    bres $5005, #0                           cycles=1
0xaa8d:  72 19 52 5c    bres $525c, #4                           cycles=1
0xaa91:  72 10 52 5d    bset $525d, #0                           cycles=1
0xaa95:  72 11 52 5c    bres $525c, #0                           cycles=1
0xaa99:  72 1a 52 57    bset $5257, #5                           cycles=1
0xaa9d:  81             ret                                      cycles=4
0xaa9e:  72 11 52 5d    bres $525d, #0                           cycles=1
0xaaa2:  72 18 52 5c    bset $525c, #4                           cycles=1
0xaaa6:  72 11 52 5c    bres $525c, #0                           cycles=1
0xaaaa:  72 1a 52 57    bset $5257, #5                           cycles=1
0xaaae:  72 13 50 05    bres $5005, #1                           cycles=1
0xaab2:  72 14 50 05    bset $5005, #2                           cycles=1
0xaab6:  72 11 50 05    bres $5005, #0                           cycles=1
0xaaba:  81             ret                                      cycles=4
0xaabb:  72 13 50 05    bres $5005, #1                           cycles=1
0xaabf:  72 10 50 05    bset $5005, #0                           cycles=1
0xaac3:  72 15 50 05    bres $5005, #2                           cycles=1
0xaac7:  72 11 52 5c    bres $525c, #0                           cycles=1
0xaacb:  72 18 52 5c    bset $525c, #4                           cycles=1
0xaacf:  72 11 52 5d    bres $525d, #0                           cycles=1
0xaad3:  72 1a 52 57    bset $5257, #5                           cycles=1
0xaad7:  81             ret                                      cycles=4
0xaad8:  72 11 50 05    bres $5005, #0                           cycles=1
0xaadc:  72 14 50 05    bset $5005, #2                           cycles=1
0xaae0:  72 13 50 05    bres $5005, #1                           cycles=1
0xaae4:  72 11 52 5d    bres $525d, #0                           cycles=1
0xaae8:  72 10 52 5c    bset $525c, #0                           cycles=1
0xaaec:  72 19 52 5c    bres $525c, #4                           cycles=1
0xaaf0:  72 1a 52 57    bset $5257, #5                           cycles=1
0xaaf4:  81             ret                                      cycles=4
0xaaf5:  5f             clrw X                                   cycles=1
0xaaf6:  3f 09          clr $09                                  cycles=1
0xaaf8:  3f 0c          clr $0c                                  cycles=1
0xaafa:  72 03 50 15 04 btjf $5015, #1, $ab03  (offset=4)        cycles=2-3
0xaaff:  72 14 00 0c    bset $c, #2                              cycles=1
0xab03:  72 03 50 10 04 btjf $5010, #1, $ab0c  (offset=4)        cycles=2-3
0xab08:  72 12 00 0c    bset $c, #1                              cycles=1
0xab0c:  72 10 00 0c    bset $c, #0                              cycles=1
0xab10:  9f             ld A, XL                                 cycles=1
0xab11:  b1 0c          cp A, $0c                                cycles=1
0xab13:  27 07          jreq $ab1c  (offset=7)                   cycles=1-2
0xab15:  b6 0c          ld A, $0c                                cycles=1
0xab17:  97             ld XL, A                                 cycles=1
0xab18:  3f 09          clr $09                                  cycles=1
0xab1a:  20 dc          jra $aaf8  (offset=-36)                  cycles=2
0xab1c:  3c 09          inc $09                                  cycles=1
0xab1e:  b6 09          ld A, $09                                cycles=1
0xab20:  a1 03          cp A, #$03                               cycles=1
0xab22:  25 d4          jrc $aaf8  (offset=-44)                  cycles=1-2
0xab24:  9f             ld A, XL                                 cycles=1
0xab25:  b7 08          ld $08,A                                 cycles=1
0xab27:  81             ret                                      cycles=4
0xab28:  72 03 00 8f c8 btjf $8f, #1, $aaf5  (offset=-56)        cycles=2-3
0xab2d:  3f 09          clr $09                                  cycles=1
0xab2f:  5f             clrw X                                   cycles=1
0xab30:  3f 0c          clr $0c                                  cycles=1
0xab32:  72 0b 50 0b 04 btjf $500b, #5, $ab3b  (offset=4)        cycles=2-3
0xab37:  72 14 00 0c    bset $c, #2                              cycles=1
0xab3b:  72 05 50 10 04 btjf $5010, #2, $ab44  (offset=4)        cycles=2-3
0xab40:  72 12 00 0c    bset $c, #1                              cycles=1
0xab44:  72 0b 50 15 04 btjf $5015, #5, $ab4d  (offset=4)        cycles=2-3
0xab49:  72 10 00 0c    bset $c, #0                              cycles=1
0xab4d:  9f             ld A, XL                                 cycles=1
0xab4e:  b1 0c          cp A, $0c                                cycles=1
0xab50:  27 07          jreq $ab59  (offset=7)                   cycles=1-2
0xab52:  b6 0c          ld A, $0c                                cycles=1
0xab54:  97             ld XL, A                                 cycles=1
0xab55:  3f 09          clr $09                                  cycles=1
0xab57:  20 d7          jra $ab30  (offset=-41)                  cycles=2
0xab59:  3c 09          inc $09                                  cycles=1
0xab5b:  b6 09          ld A, $09                                cycles=1
0xab5d:  a1 03          cp A, #$03                               cycles=1
0xab5f:  25 cf          jrc $ab30  (offset=-49)                  cycles=1-2
0xab61:  72 02 00 8d 2e btjt $8d, #1, $ab94  (offset=46)         cycles=2-3
0xab66:  b6 0c          ld A, $0c                                cycles=1
0xab68:  ae 00 01       ldw X, #$1                               cycles=2
0xab6b:  b1 5d          cp A, $5d                                cycles=1
0xab6d:  27 25          jreq $ab94  (offset=37)                  cycles=1-2
0xab6f:  5c             incw X                                   cycles=1
0xab70:  b1 5e          cp A, $5e                                cycles=1
0xab72:  27 20          jreq $ab94  (offset=32)                  cycles=1-2
0xab74:  5c             incw X                                   cycles=1
0xab75:  b1 5f          cp A, $5f                                cycles=1
0xab77:  27 1b          jreq $ab94  (offset=27)                  cycles=1-2
0xab79:  5c             incw X                                   cycles=1
0xab7a:  b1 60          cp A, $60                                cycles=1
0xab7c:  27 16          jreq $ab94  (offset=22)                  cycles=1-2
0xab7e:  5c             incw X                                   cycles=1
0xab7f:  b1 61          cp A, $61                                cycles=1
0xab81:  27 11          jreq $ab94  (offset=17)                  cycles=1-2
0xab83:  5c             incw X                                   cycles=1
0xab84:  b1 62          cp A, $62                                cycles=1
0xab86:  27 0c          jreq $ab94  (offset=12)                  cycles=1-2
0xab88:  72 11 50 05    bres $5005, #0                           cycles=1
0xab8c:  72 13 50 05    bres $5005, #1                           cycles=1
0xab90:  72 15 50 05    bres $5005, #2                           cycles=1
0xab94:  9f             ld A, XL                                 cycles=1
0xab95:  b7 08          ld $08,A                                 cycles=1
0xab97:  81             ret                                      cycles=4
0xab98:  5f             clrw X                                   cycles=1
0xab99:  b6 08          ld A, $08                                cycles=1
0xab9b:  b7 07          ld $07,A                                 cycles=1
0xab9d:  97             ld XL, A                                 cycles=1
0xab9e:  72 0e 00 8d 10 btjt $8d, #7, $abb3  (offset=16)         cycles=2-3
0xaba3:  72 06 00 89 10 btjt $89, #3, $abb8  (offset=16)         cycles=2-3
0xaba8:  d6 ab d3       ld A, ($abd3,X)                          cycles=1
0xabab:  b7 06          ld $06,A                                 cycles=1
0xabad:  d6 ab db       ld A, ($abdb,X)                          cycles=1
0xabb0:  b7 05          ld $05,A                                 cycles=1
0xabb2:  81             ret                                      cycles=4
0xabb3:  72 06 00 89 f0 btjt $89, #3, $aba8  (offset=-16)        cycles=2-3
0xabb8:  d6 ab db       ld A, ($abdb,X)                          cycles=1
0xabbb:  b7 06          ld $06,A                                 cycles=1
0xabbd:  d6 ab d3       ld A, ($abd3,X)                          cycles=1
0xabc0:  b7 05          ld $05,A                                 cycles=1
0xabc2:  81             ret                                      cycles=4
0xabc3:  02             rlwa X, A                                cycles=1
0xabc4:  03 05          cpl ($05,SP)                             cycles=1
0xabc6:  01             rrwa X, A                                cycles=1
0xabc7:  06 02          rrc ($02,SP)                             cycles=1
0xabc9:  04 05          srl ($05,SP)                             cycles=1
0xabcb:  02             rlwa X, A                                cycles=1
0xabcc:  04 02          srl ($02,SP)                             cycles=1
0xabce:  06 01          rrc ($01,SP)                             cycles=1
0xabd0:  05             ???                                      cycles=?
0xabd1:  03 05          cpl ($05,SP)                             cycles=1
0xabd3:  01             rrwa X, A                                cycles=1
0xabd4:  05             ???                                      cycles=?
0xabd5:  03 01          cpl ($01,SP)                             cycles=1
0xabd7:  06 04          rrc ($04,SP)                             cycles=1
0xabd9:  02             rlwa X, A                                cycles=1
0xabda:  06 04          rrc ($04,SP)                             cycles=1
0xabdc:  03 06          cpl ($06,SP)                             cycles=1
0xabde:  02             rlwa X, A                                cycles=1
0xabdf:  05             ???                                      cycles=?
0xabe0:  01             rrwa X, A                                cycles=1
0xabe1:  04 03          srl ($03,SP)                             cycles=1
0xabe3:  00 01          neg ($01,SP)                             cycles=1
0xabe5:  02             rlwa X, A                                cycles=1
0xabe6:  03 04          cpl ($04,SP)                             cycles=1
0xabe8:  05             ???                                      cycles=?
0xabe9:  06 07          rrc ($07,SP)                             cycles=1
0xabeb:  08 09          sll ($09,SP)                             cycles=1
0xabed:  0a 0b          dec ($0b,SP)                             cycles=1
0xabef:  0c 0d          inc ($0d,SP)                             cycles=1
0xabf1:  0e 0f          swap ($0f,SP)                            cycles=1
0xabf3:  10 11          sub A, ($11,SP)                          cycles=1
0xabf5:  12 13          sbc A, ($13,SP)                          cycles=1
0xabf7:  14 15          and A, ($15,SP)                          cycles=1
0xabf9:  16 17          ldw Y, ($17,SP)                          cycles=2
0xabfb:  18 19          xor A, ($19,SP)                          cycles=1
0xabfd:  1a 1b          or A, ($1b,SP)                           cycles=1
0xabff:  1c 1d 1e       addw X, #$1d1e                           cycles=2
0xac02:  1f 20          ldw ($20,SP),X                           cycles=2
0xac04:  21 22          jrf $ac28  (offset=34)                   cycles=1-2
0xac06:  23 24          jrule $ac2c  (offset=36)                 cycles=1-2
0xac08:  25 26          jrc $ac30  (offset=38)                   cycles=1-2
0xac0a:  27 28          jreq $ac34  (offset=40)                  cycles=1-2
0xac0c:  29 2a          jrv $ac38  (offset=42)                   cycles=1-2
0xac0e:  2b 2c          jrmi $ac3c  (offset=44)                  cycles=1-2
0xac10:  2d 2e          jrsle $ac40  (offset=46)                 cycles=1-2
0xac12:  2f 30          jrslt $ac44  (offset=48)                 cycles=1-2
0xac14:  31 32 34       exg A, $3234                             cycles=3
0xac17:  36 38          rrc $38                                  cycles=1
0xac19:  3a 3c          dec $3c                                  cycles=1
0xac1b:  3c 3c          inc $3c                                  cycles=1
0xac1d:  3c 3c          inc $3c                                  cycles=1
0xac1f:  3c 3c          inc $3c                                  cycles=1
0xac21:  3c 3c          inc $3c                                  cycles=1
0xac23:  3c 3c          inc $3c                                  cycles=1
0xac25:  3c 3c          inc $3c                                  cycles=1
0xac27:  3c 3c          inc $3c                                  cycles=1
0xac29:  3c 3c          inc $3c                                  cycles=1
0xac2b:  3c 3c          inc $3c                                  cycles=1
0xac2d:  3c 3c          inc $3c                                  cycles=1
0xac2f:  3c 3c          inc $3c                                  cycles=1
0xac31:  3c 3c          inc $3c                                  cycles=1
0xac33:  b6 b6          ld A, $b6                                cycles=1
0xac35:  b7 bc          ld $bc,A                                 cycles=1
0xac37:  b6 b9          ld A, $b9                                cycles=1
0xac39:  b7 bf          ld $bf,A                                 cycles=1
0xac3b:  b6 b4          ld A, $b4                                cycles=1
0xac3d:  b7 ba          ld $ba,A                                 cycles=1
0xac3f:  b6 b7          ld A, $b7                                cycles=1
0xac41:  b7 bd          ld $bd,A                                 cycles=1
0xac43:  b6 b5          ld A, $b5                                cycles=1
0xac45:  b7 bb          ld $bb,A                                 cycles=1
0xac47:  b6 b8          ld A, $b8                                cycles=1
0xac49:  b7 be          ld $be,A                                 cycles=1
0xac4b:  81             ret                                      cycles=4
0xac4c:  cd ab 28       call $ab28                               cycles=4
0xac4f:  cd 89 91       call $8991                               cycles=4
0xac52:  cd ac 33       call $ac33                               cycles=4
0xac55:  cd ba ca       call $baca                               cycles=4
0xac58:  cd 89 20       call $8920                               cycles=4
0xac5b:  81             ret                                      cycles=4
0xac5c:  72 0d 00 96 0f btjf $96, #6, $ac70  (offset=15)         cycles=2-3
0xac61:  cd ac 4c       call $ac4c                               cycles=4
0xac64:  cd ab 98       call $ab98                               cycles=4
0xac67:  cd b9 f9       call $b9f9                               cycles=4
0xac6a:  72 0d 00 96 01 btjf $96, #6, $ac70  (offset=1)          cycles=2-3
0xac6f:  80             iret                                     cycles=11
0xac70:  72 0e 00 8a 03 btjt $8a, #7, $ac78  (offset=3)          cycles=2-3
0xac75:  cd b9 c5       call $b9c5                               cycles=4
0xac78:  cd ab 28       call $ab28                               cycles=4
0xac7b:  72 00 00 90 29 btjt $90, #0, $aca9  (offset=41)         cycles=2-3
0xac80:  72 0f 00 8a 22 btjf $8a, #7, $aca7  (offset=34)         cycles=2-3
0xac85:  72 03 00 8f 21 btjf $8f, #1, $acab  (offset=33)         cycles=2-3
0xac8a:  b6 08          ld A, $08                                cycles=1
0xac8c:  b1 06          cp A, $06                                cycles=1
0xac8e:  27 41          jreq $acd1  (offset=65)                  cycles=1-2
0xac90:  b1 07          cp A, $07                                cycles=1
0xac92:  27 12          jreq $aca6  (offset=18)                  cycles=1-2
0xac94:  b6 08          ld A, $08                                cycles=1
0xac96:  b1 05          cp A, $05                                cycles=1
0xac98:  27 31          jreq $accb  (offset=49)                  cycles=1-2
0xac9a:  72 11 50 05    bres $5005, #0                           cycles=1
0xac9e:  72 13 50 05    bres $5005, #1                           cycles=1
0xaca2:  72 15 50 05    bres $5005, #2                           cycles=1
0xaca6:  80             iret                                     cycles=11
0xaca7:  20 4e          jra $acf7  (offset=78)                   cycles=2
0xaca9:  20 46          jra $acf1  (offset=70)                   cycles=2
0xacab:  72 0f 00 8a 47 btjf $8a, #7, $acf7  (offset=71)         cycles=2-3
0xacb0:  b6 08          ld A, $08                                cycles=1
0xacb2:  b1 05          cp A, $05                                cycles=1
0xacb4:  27 1b          jreq $acd1  (offset=27)                  cycles=1-2
0xacb6:  b1 06          cp A, $06                                cycles=1
0xacb8:  27 17          jreq $acd1  (offset=23)                  cycles=1-2
0xacba:  b1 07          cp A, $07                                cycles=1
0xacbc:  27 e8          jreq $aca6  (offset=-24)                 cycles=1-2
0xacbe:  72 11 50 05    bres $5005, #0                           cycles=1
0xacc2:  72 13 50 05    bres $5005, #1                           cycles=1
0xacc6:  72 15 50 05    bres $5005, #2                           cycles=1
0xacca:  80             iret                                     cycles=11
0xaccb:  72 19 00 90    bres $90, #4                             cycles=1
0xaccf:  20 04          jra $acd5  (offset=4)                    cycles=2
0xacd1:  72 18 00 90    bset $90, #4                             cycles=1
0xacd5:  cd 89 91       call $8991                               cycles=4
0xacd8:  cd ac 33       call $ac33                               cycles=4
0xacdb:  cd b9 c5       call $b9c5                               cycles=4
0xacde:  cd 89 20       call $8920                               cycles=4
0xace1:  cd 86 1f       call $861f                               cycles=4
0xace4:  72 0d 00 96 03 btjf $96, #6, $acec  (offset=3)          cycles=2-3
0xace9:  cc ac f7       jp $acf7                                 cycles=1
0xacec:  cd a9 9b       call $a99b                               cycles=4
0xacef:  20 03          jra $acf4  (offset=3)                    cycles=2
0xacf1:  cd a3 8b       call $a38b                               cycles=4
0xacf4:  cd a0 dd       call $a0dd                               cycles=4
0xacf7:  cd ab 98       call $ab98                               cycles=4
0xacfa:  cd b9 f9       call $b9f9                               cycles=4
0xacfd:  80             iret                                     cycles=11
0xacfe:  72 15 52 5c    bres $525c, #2                           cycles=1
0xad02:  72 1d 52 5c    bres $525c, #6                           cycles=1
0xad06:  72 15 52 5d    bres $525d, #2                           cycles=1
0xad0a:  72 1a 52 57    bset $5257, #5                           cycles=1
0xad0e:  72 11 50 05    bres $5005, #0                           cycles=1
0xad12:  72 13 50 05    bres $5005, #1                           cycles=1
0xad16:  72 15 50 05    bres $5005, #2                           cycles=1
0xad1a:  72 1f 52 55    bres $5255, #7                           cycles=1
0xad1e:  83             trap                                     cycles=9
0xad1f:  72 18 00 8d    bset $8d, #4                             cycles=1
0xad23:  72 16 00 8f    bset $8f, #3                             cycles=1
0xad27:  80             iret                                     cycles=11
0xad28:  80             iret                                     cycles=11
0xad29:  72 11 52 55    bres $5255, #0                           cycles=1
0xad2d:  80             iret                                     cycles=11
0xad2e:  80             iret                                     cycles=11
0xad2f:  72 0c 00 96 46 btjt $96, #6, $ad7a  (offset=70)         cycles=2-3
0xad34:  cd b9 90       call $b990                               cycles=4
0xad37:  a6 05          ld A, #$05                               cycles=1
0xad39:  cd b9 83       call $b983                               cycles=4
0xad3c:  a6 14          ld A, #$14                               cycles=1
0xad3e:  b7 31          ld $31,A                                 cycles=1
0xad40:  c6 54 04       ld A, $5404                              cycles=1
0xad43:  a1 3f          cp A, #$3f                               cycles=1
0xad45:  24 07          jrnc $ad4e  (offset=7)                   cycles=1-2
0xad47:  48             sll A                                    cycles=1
0xad48:  48             sll A                                    cycles=1
0xad49:  ca 54 05       or A, $5405                              cycles=1
0xad4c:  20 02          jra $ad50  (offset=2)                    cycles=2
0xad4e:  a6 ff          ld A, #$ff                               cycles=1
0xad50:  b7 68          ld $68,A                                 cycles=1
0xad52:  c6 54 04       ld A, $5404                              cycles=1
0xad55:  b1 1e          cp A, $1e                                cycles=1
0xad57:  27 08          jreq $ad61  (offset=8)                   cycles=1-2
0xad59:  25 04          jrc $ad5f  (offset=4)                    cycles=1-2
0xad5b:  3c 1e          inc $1e                                  cycles=1
0xad5d:  20 02          jra $ad61  (offset=2)                    cycles=2
0xad5f:  3a 1e          dec $1e                                  cycles=1
0xad61:  72 0c 00 96 14 btjt $96, #6, $ad7a  (offset=20)         cycles=2-3
0xad66:  c6 52 65       ld A, $5265                              cycles=1
0xad69:  b7 32          ld $32,A                                 cycles=1
0xad6b:  95             ld XH, A                                 cycles=1
0xad6c:  c6 52 66       ld A, $5266                              cycles=1
0xad6f:  b7 33          ld $33,A                                 cycles=1
0xad71:  97             ld XL, A                                 cycles=1
0xad72:  72 0e 00 8a 03 btjt $8a, #7, $ad7a  (offset=3)          cycles=2-3
0xad77:  cc ae 27       jp $ae27                                 cycles=1
0xad7a:  72 0d 00 96 09 btjf $96, #6, $ad88  (offset=9)          cycles=2-3
0xad7f:  72 0e 00 8a 40 btjt $8a, #7, $adc4  (offset=64)         cycles=2-3
0xad84:  5f             clrw X                                   cycles=1
0xad85:  cc ae 27       jp $ae27                                 cycles=1
0xad88:  b6 68          ld A, $68                                cycles=1
0xad8a:  b1 1a          cp A, $1a                                cycles=1
0xad8c:  27 29          jreq $adb7  (offset=41)                  cycles=1-2
0xad8e:  24 29          jrnc $adb9  (offset=41)                  cycles=1-2
0xad90:  72 03 00 89 56 btjf $89, #1, $adeb  (offset=86)         cycles=2-3
0xad95:  b6 32          ld A, $32                                cycles=1
0xad97:  b1 24          cp A, $24                                cycles=1
0xad99:  27 08          jreq $ada3  (offset=8)                   cycles=1-2
0xad9b:  25 14          jrc $adb1  (offset=20)                   cycles=1-2
0xad9d:  b6 24          ld A, $24                                cycles=1
0xad9f:  b7 32          ld $32,A                                 cycles=1
0xada1:  20 08          jra $adab  (offset=8)                    cycles=2
0xada3:  b6 33          ld A, $33                                cycles=1
0xada5:  b1 25          cp A, $25                                cycles=1
0xada7:  27 66          jreq $ae0f  (offset=102)                 cycles=1-2
0xada9:  25 06          jrc $adb1  (offset=6)                    cycles=1-2
0xadab:  b6 25          ld A, $25                                cycles=1
0xadad:  b7 33          ld $33,A                                 cycles=1
0xadaf:  20 5e          jra $ae0f  (offset=94)                   cycles=2
0xadb1:  3c 33          inc $33                                  cycles=1
0xadb3:  26 5a          jrne $ae0f  (offset=90)                  cycles=1-2
0xadb5:  3c 32          inc $32                                  cycles=1
0xadb7:  20 56          jra $ae0f  (offset=86)                   cycles=2
0xadb9:  72 0d 00 96 45 btjf $96, #6, $ae03  (offset=69)         cycles=2-3
0xadbe:  3d c0          tnz $c0                                  cycles=1
0xadc0:  27 02          jreq $adc4  (offset=2)                   cycles=1-2
0xadc2:  3a c0          dec $c0                                  cycles=1
0xadc4:  cd 87 85       call $8785                               cycles=4
0xadc7:  cd af 29       call $af29                               cycles=4
0xadca:  cd 87 91       call $8791                               cycles=4
0xadcd:  9e             ld A, XH                                 cycles=1
0xadce:  c7 52 65       ld $5265,A                               cycles=1
0xadd1:  9f             ld A, XL                                 cycles=1
0xadd2:  c7 52 66       ld $5266,A                               cycles=1
0xadd5:  cd 87 ef       call $87ef                               cycles=4
0xadd8:  9e             ld A, XH                                 cycles=1
0xadd9:  c7 52 67       ld $5267,A                               cycles=1
0xaddc:  9f             ld A, XL                                 cycles=1
0xaddd:  c7 52 68       ld $5268,A                               cycles=1
0xade0:  cd 88 4d       call $884d                               cycles=4
0xade3:  9e             ld A, XH                                 cycles=1
0xade4:  c7 52 69       ld $5269,A                               cycles=1
0xade7:  9f             ld A, XL                                 cycles=1
0xade8:  c7 52 6a       ld $526a,A                               cycles=1
0xadeb:  20 3a          jra $ae27  (offset=58)                   cycles=2
0xaded:  a6 00          ld A, #$00                               cycles=1
0xadef:  c7 52 65       ld $5265,A                               cycles=1
0xadf2:  c7 52 67       ld $5267,A                               cycles=1
0xadf5:  c7 52 69       ld $5269,A                               cycles=1
0xadf8:  c7 52 66       ld $5266,A                               cycles=1
0xadfb:  c7 52 68       ld $5268,A                               cycles=1
0xadfe:  c7 52 6a       ld $526a,A                               cycles=1
0xae01:  20 24          jra $ae27  (offset=36)                   cycles=2
0xae03:  3d 33          tnz $33                                  cycles=1
0xae05:  26 06          jrne $ae0d  (offset=6)                   cycles=1-2
0xae07:  3d 32          tnz $32                                  cycles=1
0xae09:  27 04          jreq $ae0f  (offset=4)                   cycles=1-2
0xae0b:  3a 32          dec $32                                  cycles=1
0xae0d:  3a 33          dec $33                                  cycles=1
0xae0f:  b6 32          ld A, $32                                cycles=1
0xae11:  c7 52 65       ld $5265,A                               cycles=1
0xae14:  c7 52 67       ld $5267,A                               cycles=1
0xae17:  c7 52 69       ld $5269,A                               cycles=1
0xae1a:  95             ld XH, A                                 cycles=1
0xae1b:  b6 33          ld A, $33                                cycles=1
0xae1d:  c7 52 66       ld $5266,A                               cycles=1
0xae20:  c7 52 68       ld $5268,A                               cycles=1
0xae23:  c7 52 6a       ld $526a,A                               cycles=1
0xae26:  97             ld XL, A                                 cycles=1
0xae27:  a3 00 32       cpw X, #$32                              cycles=2
0xae2a:  24 05          jrnc $ae31  (offset=5)                   cycles=1-2
0xae2c:  ae 01 2c       ldw X, #$12c                             cycles=2
0xae2f:  20 03          jra $ae34  (offset=3)                    cycles=2
0xae31:  1d 00 32       subw X, #$32                             cycles=2
0xae34:  9e             ld A, XH                                 cycles=1
0xae35:  c7 52 6b       ld $526b,A                               cycles=1
0xae38:  9f             ld A, XL                                 cycles=1
0xae39:  c7 52 6c       ld $526c,A                               cycles=1
0xae3c:  cc ae cd       jp $aecd                                 cycles=1
0xae3f:  c6 54 04       ld A, $5404                              cycles=1
0xae42:  5f             clrw X                                   cycles=1
0xae43:  97             ld XL, A                                 cycles=1
0xae44:  a6 fa          ld A, #$fa                               cycles=1
0xae46:  42             mul X, A                                 cycles=4
0xae47:  90 be 32       ldw Y, $32                               cycles=2
0xae4a:  90 54          srlw Y                                   cycles=2
0xae4c:  90 5d          tnzw Y                                   cycles=2
0xae4e:  27 68          jreq $aeb8  (offset=104)                 cycles=1-2
0xae50:  65             divw X, Y                                cycles=2-17
0xae51:  9f             ld A, XL                                 cycles=1
0xae52:  b7 4b          ld $4b,A                                 cycles=1
0xae54:  72 0e 00 8a 03 btjt $8a, #7, $ae5c  (offset=3)          cycles=2-3
0xae59:  cc ae b8       jp $aeb8                                 cycles=1
0xae5c:  b6 4b          ld A, $4b                                cycles=1
0xae5e:  b1 9b          cp A, $9b                                cycles=1
0xae60:  25 2e          jrc $ae90  (offset=46)                   cycles=1-2
0xae62:  3c 3c          inc $3c                                  cycles=1
0xae64:  b6 3c          ld A, $3c                                cycles=1
0xae66:  a1 0f          cp A, #$0f                               cycles=1
0xae68:  25 28          jrc $ae92  (offset=40)                   cycles=1-2
0xae6a:  3f 3c          clr $3c                                  cycles=1
0xae6c:  a6 00          ld A, #$00                               cycles=1
0xae6e:  c7 52 65       ld $5265,A                               cycles=1
0xae71:  c7 52 67       ld $5267,A                               cycles=1
0xae74:  c7 52 69       ld $5269,A                               cycles=1
0xae77:  b7 32          ld $32,A                                 cycles=1
0xae79:  3f 24          clr $24                                  cycles=1
0xae7b:  a6 00          ld A, #$00                               cycles=1
0xae7d:  c7 52 66       ld $5266,A                               cycles=1
0xae80:  c7 52 68       ld $5268,A                               cycles=1
0xae83:  c7 52 6a       ld $526a,A                               cycles=1
0xae86:  b7 33          ld $33,A                                 cycles=1
0xae88:  3f 25          clr $25                                  cycles=1
0xae8a:  72 10 52 57    bset $5257, #0                           cycles=1
0xae8e:  20 28          jra $aeb8  (offset=40)                   cycles=2
0xae90:  3f 3c          clr $3c                                  cycles=1
0xae92:  b6 4b          ld A, $4b                                cycles=1
0xae94:  b1 38          cp A, $38                                cycles=1
0xae96:  25 20          jrc $aeb8  (offset=32)                   cycles=1-2
0xae98:  c6 52 65       ld A, $5265                              cycles=1
0xae9b:  95             ld XH, A                                 cycles=1
0xae9c:  c6 52 66       ld A, $5266                              cycles=1
0xae9f:  97             ld XL, A                                 cycles=1
0xaea0:  5d             tnzw X                                   cycles=2
0xaea1:  27 15          jreq $aeb8  (offset=21)                  cycles=1-2
0xaea3:  5a             decw X                                   cycles=1
0xaea4:  9e             ld A, XH                                 cycles=1
0xaea5:  c7 52 65       ld $5265,A                               cycles=1
0xaea8:  c7 52 67       ld $5267,A                               cycles=1
0xaeab:  c7 52 69       ld $5269,A                               cycles=1
0xaeae:  9f             ld A, XL                                 cycles=1
0xaeaf:  c7 52 66       ld $5266,A                               cycles=1
0xaeb2:  c7 52 68       ld $5268,A                               cycles=1
0xaeb5:  c7 52 6a       ld $526a,A                               cycles=1
0xaeb8:  c6 52 65       ld A, $5265                              cycles=1
0xaebb:  95             ld XH, A                                 cycles=1
0xaebc:  c6 52 66       ld A, $5266                              cycles=1
0xaebf:  97             ld XL, A                                 cycles=1
0xaec0:  54             srlw X                                   cycles=2
0xaec1:  54             srlw X                                   cycles=2
0xaec2:  1c 00 0a       addw X, #$a                              cycles=2
0xaec5:  9e             ld A, XH                                 cycles=1
0xaec6:  c7 52 6b       ld $526b,A                               cycles=1
0xaec9:  9f             ld A, XL                                 cycles=1
0xaeca:  c7 52 6c       ld $526c,A                               cycles=1
0xaecd:  72 19 52 55    bres $5255, #4                           cycles=1
0xaed1:  72 17 52 55    bres $5255, #3                           cycles=1
0xaed5:  72 15 52 55    bres $5255, #2                           cycles=1
0xaed9:  72 13 52 55    bres $5255, #1                           cycles=1
0xaedd:  80             iret                                     cycles=11
0xaede:  81             ret                                      cycles=4
0xaedf:  5f             clrw X                                   cycles=1
0xaee0:  90 5f          clrw Y                                   cycles=1
0xaee2:  c6 53 28       ld A, $5328                              cycles=1
0xaee5:  95             ld XH, A                                 cycles=1
0xaee6:  c6 53 29       ld A, $5329                              cycles=1
0xaee9:  97             ld XL, A                                 cycles=1
0xaeea:  90 be cd       ldw Y, $cd                               cycles=2
0xaeed:  65             divw X, Y                                cycles=2-17
0xaeee:  72 03 00 98 04 btjf $98, #1, $aef7  (offset=4)          cycles=2-3
0xaef3:  a6 0a          ld A, #$0a                               cycles=1
0xaef5:  20 02          jra $aef9  (offset=2)                    cycles=2
0xaef7:  a6 08          ld A, #$08                               cycles=1
0xaef9:  42             mul X, A                                 cycles=4
0xaefa:  a3 00 ff       cpw X, #$ff                              cycles=2
0xaefd:  24 24          jrnc $af23  (offset=36)                  cycles=1-2
0xaeff:  9f             ld A, XL                                 cycles=1
0xaf00:  b7 c1          ld $c1,A                                 cycles=1
0xaf02:  90 a3 00 ff    cpw Y, #$ff                              cycles=2
0xaf06:  25 00          jrc $af08  (offset=0)                    cycles=1-2
0xaf08:  72 02 00 98 06 btjt $98, #1, $af13  (offset=6)          cycles=2-3
0xaf0d:  93             ldw X, Y                                 cycles=1
0xaf0e:  58             sllw X                                   cycles=2
0xaf0f:  58             sllw X                                   cycles=2
0xaf10:  58             sllw X                                   cycles=2
0xaf11:  20 04          jra $af17  (offset=4)                    cycles=2
0xaf13:  93             ldw X, Y                                 cycles=1
0xaf14:  a6 0a          ld A, #$0a                               cycles=1
0xaf16:  42             mul X, A                                 cycles=4
0xaf17:  90 be cd       ldw Y, $cd                               cycles=2
0xaf1a:  65             divw X, Y                                cycles=2-17
0xaf1b:  9f             ld A, XL                                 cycles=1
0xaf1c:  bb c1          add A, $c1                               cycles=1
0xaf1e:  25 03          jrc $af23  (offset=3)                    cycles=1-2
0xaf20:  b7 c1          ld $c1,A                                 cycles=1
0xaf22:  81             ret                                      cycles=4
0xaf23:  a6 ff          ld A, #$ff                               cycles=1
0xaf25:  b7 c1          ld $c1,A                                 cycles=1
0xaf27:  81             ret                                      cycles=4
0xaf28:  81             ret                                      cycles=4
0xaf29:  72 0d 00 96 b0 btjf $96, #6, $aede  (offset=-80)        cycles=2-3
0xaf2e:  cd ae df       call $aedf                               cycles=4
0xaf31:  b6 b6          ld A, $b6                                cycles=1
0xaf33:  b7 bc          ld $bc,A                                 cycles=1
0xaf35:  b6 b9          ld A, $b9                                cycles=1
0xaf37:  b7 bf          ld $bf,A                                 cycles=1
0xaf39:  b6 b4          ld A, $b4                                cycles=1
0xaf3b:  b7 ba          ld $ba,A                                 cycles=1
0xaf3d:  b6 b7          ld A, $b7                                cycles=1
0xaf3f:  b7 bd          ld $bd,A                                 cycles=1
0xaf41:  b6 b5          ld A, $b5                                cycles=1
0xaf43:  b7 bb          ld $bb,A                                 cycles=1
0xaf45:  b6 b8          ld A, $b8                                cycles=1
0xaf47:  b7 be          ld $be,A                                 cycles=1
0xaf49:  b6 b6          ld A, $b6                                cycles=1
0xaf4b:  bb c1          add A, $c1                               cycles=1
0xaf4d:  25 04          jrc $af53  (offset=4)                    cycles=1-2
0xaf4f:  b7 bc          ld $bc,A                                 cycles=1
0xaf51:  20 04          jra $af57  (offset=4)                    cycles=2
0xaf53:  b7 bc          ld $bc,A                                 cycles=1
0xaf55:  3c bf          inc $bf                                  cycles=1
0xaf57:  b6 bf          ld A, $bf                                cycles=1
0xaf59:  95             ld XH, A                                 cycles=1
0xaf5a:  b6 bc          ld A, $bc                                cycles=1
0xaf5c:  97             ld XL, A                                 cycles=1
0xaf5d:  a3 05 a0       cpw X, #$5a0                             cycles=2
0xaf60:  25 08          jrc $af6a  (offset=8)                    cycles=1-2
0xaf62:  1d 05 a0       subw X, #$5a0                            cycles=2
0xaf65:  9f             ld A, XL                                 cycles=1
0xaf66:  b7 bc          ld $bc,A                                 cycles=1
0xaf68:  3f bf          clr $bf                                  cycles=1
0xaf6a:  b6 b4          ld A, $b4                                cycles=1
0xaf6c:  bb c1          add A, $c1                               cycles=1
0xaf6e:  25 04          jrc $af74  (offset=4)                    cycles=1-2
0xaf70:  b7 ba          ld $ba,A                                 cycles=1
0xaf72:  20 04          jra $af78  (offset=4)                    cycles=2
0xaf74:  b7 ba          ld $ba,A                                 cycles=1
0xaf76:  3c bd          inc $bd                                  cycles=1
0xaf78:  b6 bd          ld A, $bd                                cycles=1
0xaf7a:  95             ld XH, A                                 cycles=1
0xaf7b:  b6 ba          ld A, $ba                                cycles=1
0xaf7d:  97             ld XL, A                                 cycles=1
0xaf7e:  a3 05 a0       cpw X, #$5a0                             cycles=2
0xaf81:  25 08          jrc $af8b  (offset=8)                    cycles=1-2
0xaf83:  1d 05 a0       subw X, #$5a0                            cycles=2
0xaf86:  9f             ld A, XL                                 cycles=1
0xaf87:  b7 ba          ld $ba,A                                 cycles=1
0xaf89:  3f bd          clr $bd                                  cycles=1
0xaf8b:  b6 b5          ld A, $b5                                cycles=1
0xaf8d:  bb c1          add A, $c1                               cycles=1
0xaf8f:  25 04          jrc $af95  (offset=4)                    cycles=1-2
0xaf91:  b7 bb          ld $bb,A                                 cycles=1
0xaf93:  20 04          jra $af99  (offset=4)                    cycles=2
0xaf95:  b7 bb          ld $bb,A                                 cycles=1
0xaf97:  3c be          inc $be                                  cycles=1
0xaf99:  b6 be          ld A, $be                                cycles=1
0xaf9b:  95             ld XH, A                                 cycles=1
0xaf9c:  b6 bb          ld A, $bb                                cycles=1
0xaf9e:  97             ld XL, A                                 cycles=1
0xaf9f:  a3 05 a0       cpw X, #$5a0                             cycles=2
0xafa2:  25 08          jrc $afac  (offset=8)                    cycles=1-2
0xafa4:  1d 05 a0       subw X, #$5a0                            cycles=2
0xafa7:  9f             ld A, XL                                 cycles=1
0xafa8:  b7 bb          ld $bb,A                                 cycles=1
0xafaa:  3f be          clr $be                                  cycles=1
0xafac:  81             ret                                      cycles=4
0xafad:  81             ret                                      cycles=4
0xafae:  72 02 53 22 05 btjt $5322, #1, $afb8  (offset=5)        cycles=2-3
0xafb3:  72 04 53 22 0e btjt $5322, #2, $afc6  (offset=14)       cycles=2-3
0xafb8:  72 13 53 22    bres $5322, #1                           cycles=1
0xafbc:  80             iret                                     cycles=11
0xafbd:  72 13 53 22    bres $5322, #1                           cycles=1
0xafc1:  72 13 53 21    bres $5321, #1                           cycles=1
0xafc5:  80             iret                                     cycles=11
0xafc6:  72 15 53 22    bres $5322, #2                           cycles=1
0xafca:  80             iret                                     cycles=11
0xafcb:  72 11 53 22    bres $5322, #0                           cycles=1
0xafcf:  ae fe b0       ldw X, #$feb0                            cycles=2
0xafd2:  9e             ld A, XH                                 cycles=1
0xafd3:  c7 53 28       ld $5328,A                               cycles=1
0xafd6:  b7 3e          ld $3e,A                                 cycles=1
0xafd8:  9f             ld A, XL                                 cycles=1
0xafd9:  c7 53 29       ld $5329,A                               cycles=1
0xafdc:  b7 3f          ld $3f,A                                 cycles=1
0xafde:  72 0f 00 8a 00 btjf $8a, #7, $afe3  (offset=0)          cycles=2-3
0xafe3:  cd 9a 67       call $9a67                               cycles=4
0xafe6:  80             iret                                     cycles=11
0xafe7:  72 11 53 42    bres $5342, #0                           cycles=1
0xafeb:  cd 9e 8a       call $9e8a                               cycles=4
0xafee:  72 1c 00 89    bset $89, #6                             cycles=1
0xaff2:  3c b1          inc $b1                                  cycles=1
0xaff4:  72 1a 00 8c    bset $8c, #5                             cycles=1
0xaff8:  72 0e 00 8a 06 btjt $8a, #7, $b003  (offset=6)          cycles=2-3
0xaffd:  72 15 00 77    bres $77, #2                             cycles=1
0xb001:  20 04          jra $b007  (offset=4)                    cycles=2
0xb003:  72 14 00 77    bset $77, #2                             cycles=1
0xb007:  3f ab          clr $ab                                  cycles=1
0xb009:  3f ac          clr $ac                                  cycles=1
0xb00b:  cd 9e 8a       call $9e8a                               cycles=4
0xb00e:  3c 2d          inc $2d                                  cycles=1
0xb010:  b6 2d          ld A, $2d                                cycles=1
0xb012:  72 0c 00 96 16 btjt $96, #6, $b02d  (offset=22)         cycles=2-3
0xb017:  72 02 00 8f 0b btjt $8f, #1, $b027  (offset=11)         cycles=2-3
0xb01c:  72 0c 50 0e 06 btjt $500e, #6, $b027  (offset=6)        cycles=2-3
0xb021:  a1 0a          cp A, #$0a                               cycles=1
0xb023:  25 12          jrc $b037  (offset=18)                   cycles=1-2
0xb025:  20 0a          jra $b031  (offset=10)                   cycles=2
0xb027:  a1 0a          cp A, #$0a                               cycles=1
0xb029:  25 0c          jrc $b037  (offset=12)                   cycles=1-2
0xb02b:  20 04          jra $b031  (offset=4)                    cycles=2
0xb02d:  a1 01          cp A, #$01                               cycles=1
0xb02f:  25 06          jrc $b037  (offset=6)                    cycles=1-2
0xb031:  3f 2d          clr $2d                                  cycles=1
0xb033:  72 14 00 89    bset $89, #2                             cycles=1
0xb037:  cd 84 a2       call $84a2                               cycles=4
0xb03a:  cd 84 dd       call $84dd                               cycles=4
0xb03d:  3c 30          inc $30                                  cycles=1
0xb03f:  b6 30          ld A, $30                                cycles=1
0xb041:  a1 0f          cp A, #$0f                               cycles=1
0xb043:  25 49          jrc $b08e  (offset=73)                   cycles=1-2
0xb045:  cd 83 87       call $8387                               cycles=4
0xb048:  cd 83 d0       call $83d0                               cycles=4
0xb04b:  cd 84 19       call $8419                               cycles=4
0xb04e:  3f 30          clr $30                                  cycles=1
0xb050:  72 18 00 89    bset $89, #4                             cycles=1
0xb054:  b6 20          ld A, $20                                cycles=1
0xb056:  b1 e4          cp A, $e4                                cycles=1
0xb058:  24 30          jrnc $b08a  (offset=48)                  cycles=1-2
0xb05a:  b1 e2          cp A, $e2                                cycles=1
0xb05c:  25 2c          jrc $b08a  (offset=44)                   cycles=1-2
0xb05e:  b1 e5          cp A, $e5                                cycles=1
0xb060:  3c e7          inc $e7                                  cycles=1
0xb062:  b6 e7          ld A, $e7                                cycles=1
0xb064:  a1 ff          cp A, #$ff                               cycles=1
0xb066:  25 26          jrc $b08e  (offset=38)                   cycles=1-2
0xb068:  3f e7          clr $e7                                  cycles=1
0xb06a:  3c e6          inc $e6                                  cycles=1
0xb06c:  b6 e6          ld A, $e6                                cycles=1
0xb06e:  a1 02          cp A, #$02                               cycles=1
0xb070:  25 1c          jrc $b08e  (offset=28)                   cycles=1-2
0xb072:  3f e6          clr $e6                                  cycles=1
0xb074:  b6 20          ld A, $20                                cycles=1
0xb076:  b7 e1          ld $e1,A                                 cycles=1
0xb078:  ab 0a          add A, #$0a                              cycles=1
0xb07a:  b7 d9          ld $d9,A                                 cycles=1
0xb07c:  b6 e1          ld A, $e1                                cycles=1
0xb07e:  ab 06          add A, #$06                              cycles=1
0xb080:  b7 e4          ld $e4,A                                 cycles=1
0xb082:  a0 06          sub A, #$06                              cycles=1
0xb084:  a0 06          sub A, #$06                              cycles=1
0xb086:  b7 e5          ld $e5,A                                 cycles=1
0xb088:  20 04          jra $b08e  (offset=4)                    cycles=2
0xb08a:  3f e7          clr $e7                                  cycles=1
0xb08c:  3f e6          clr $e6                                  cycles=1
0xb08e:  72 0c 00 8a 15 btjt $8a, #6, $b0a8  (offset=21)         cycles=2-3
0xb093:  3c 0e          inc $0e                                  cycles=1
0xb095:  b6 0e          ld A, $0e                                cycles=1
0xb097:  a1 62          cp A, #$62                               cycles=1
0xb099:  25 0d          jrc $b0a8  (offset=13)                   cycles=1-2
0xb09b:  3f 0e          clr $0e                                  cycles=1
0xb09d:  72 1c 00 8a    bset $8a, #6                             cycles=1
0xb0a1:  cd 9a 67       call $9a67                               cycles=4
0xb0a4:  a6 32          ld A, #$32                               cycles=1
0xb0a6:  b7 50          ld $50,A                                 cycles=1
0xb0a8:  72 00 00 90 3c btjt $90, #0, $b0e9  (offset=60)         cycles=2-3
0xb0ad:  72 0f 00 8a 37 btjf $8a, #7, $b0e9  (offset=55)         cycles=2-3
0xb0b2:  72 02 00 8d 32 btjt $8d, #1, $b0e9  (offset=50)         cycles=2-3
0xb0b7:  b6 1d          ld A, $1d                                cycles=1
0xb0b9:  a1 15          cp A, #$15                               cycles=1
0xb0bb:  25 2c          jrc $b0e9  (offset=44)                   cycles=1-2
0xb0bd:  72 08 00 8a 2b btjt $8a, #4, $b0ed  (offset=43)         cycles=2-3
0xb0c2:  3c 0f          inc $0f                                  cycles=1
0xb0c4:  b6 0f          ld A, $0f                                cycles=1
0xb0c6:  a1 fa          cp A, #$fa                               cycles=1
0xb0c8:  25 23          jrc $b0ed  (offset=35)                   cycles=1-2
0xb0ca:  3f 0f          clr $0f                                  cycles=1
0xb0cc:  3c 10          inc $10                                  cycles=1
0xb0ce:  b6 10          ld A, $10                                cycles=1
0xb0d0:  a1 1e          cp A, #$1e                               cycles=1
0xb0d2:  25 19          jrc $b0ed  (offset=25)                   cycles=1-2
0xb0d4:  3f 10          clr $10                                  cycles=1
0xb0d6:  72 18 00 8a    bset $8a, #4                             cycles=1
0xb0da:  72 02 00 8d 0e btjt $8d, #1, $b0ed  (offset=14)         cycles=2-3
0xb0df:  72 18 00 8d    bset $8d, #4                             cycles=1
0xb0e3:  72 1c 00 8f    bset $8f, #6                             cycles=1
0xb0e7:  20 04          jra $b0ed  (offset=4)                    cycles=2
0xb0e9:  3f 0f          clr $0f                                  cycles=1
0xb0eb:  3f 10          clr $10                                  cycles=1
0xb0ed:  3c 35          inc $35                                  cycles=1
0xb0ef:  b6 35          ld A, $35                                cycles=1
0xb0f1:  a1 c8          cp A, #$c8                               cycles=1
0xb0f3:  25 09          jrc $b0fe  (offset=9)                    cycles=1-2
0xb0f5:  3f 35          clr $35                                  cycles=1
0xb0f7:  72 10 00 8f    bset $8f, #0                             cycles=1
0xb0fb:  cd 84 53       call $8453                               cycles=4
0xb0fe:  3c 1b          inc $1b                                  cycles=1
0xb100:  b6 1b          ld A, $1b                                cycles=1
0xb102:  a1 0a          cp A, #$0a                               cycles=1
0xb104:  25 0a          jrc $b110  (offset=10)                   cycles=1-2
0xb106:  3f 1b          clr $1b                                  cycles=1
0xb108:  72 1e 00 89    bset $89, #7                             cycles=1
0xb10c:  72 1c 50 00    bset $5000, #6                           cycles=1
0xb110:  c6 52 65       ld A, $5265                              cycles=1
0xb113:  95             ld XH, A                                 cycles=1
0xb114:  c6 52 66       ld A, $5266                              cycles=1
0xb117:  97             ld XL, A                                 cycles=1
0xb118:  72 0a 00 90 0f btjt $90, #5, $b12c  (offset=15)         cycles=2-3
0xb11d:  72 0b 00 89 60 btjf $89, #5, $b182  (offset=96)         cycles=2-3
0xb122:  72 01 00 91 5b btjf $91, #0, $b182  (offset=91)         cycles=2-3
0xb127:  72 0c 00 8c 1d btjt $8c, #6, $b149  (offset=29)         cycles=2-3
0xb12c:  72 05 52 5c 51 btjf $525c, #2, $b182  (offset=81)       cycles=2-3
0xb131:  72 0d 52 5c 4c btjf $525c, #6, $b182  (offset=76)       cycles=2-3
0xb136:  72 05 52 5d 47 btjf $525d, #2, $b182  (offset=71)       cycles=2-3
0xb13b:  b6 28          ld A, $28                                cycles=1
0xb13d:  ab 0a          add A, #$0a                              cycles=1
0xb13f:  b1 1f          cp A, $1f                                cycles=1
0xb141:  24 13          jrnc $b156  (offset=19)                  cycles=1-2
0xb143:  5d             tnzw X                                   cycles=2
0xb144:  27 3c          jreq $b182  (offset=60)                  cycles=1-2
0xb146:  5a             decw X                                   cycles=1
0xb147:  20 25          jra $b16e  (offset=37)                   cycles=2
0xb149:  a3 00 14       cpw X, #$14                              cycles=2
0xb14c:  24 03          jrnc $b151  (offset=3)                   cycles=1-2
0xb14e:  5f             clrw X                                   cycles=1
0xb14f:  20 1d          jra $b16e  (offset=29)                   cycles=2
0xb151:  1d 00 14       subw X, #$14                             cycles=2
0xb154:  20 18          jra $b16e  (offset=24)                   cycles=2
0xb156:  72 0b 00 90 07 btjf $90, #5, $b162  (offset=7)          cycles=2-3
0xb15b:  a3 03 e9       cpw X, #$3e9                             cycles=2
0xb15e:  24 0c          jrnc $b16c  (offset=12)                  cycles=1-2
0xb160:  20 05          jra $b167  (offset=5)                    cycles=2
0xb162:  a3 03 20       cpw X, #$320                             cycles=2
0xb165:  24 05          jrnc $b16c  (offset=5)                   cycles=1-2
0xb167:  5c             incw X                                   cycles=1
0xb168:  5c             incw X                                   cycles=1
0xb169:  5c             incw X                                   cycles=1
0xb16a:  20 02          jra $b16e  (offset=2)                    cycles=2
0xb16c:  20 14          jra $b182  (offset=20)                   cycles=2
0xb16e:  9e             ld A, XH                                 cycles=1
0xb16f:  c7 52 65       ld $5265,A                               cycles=1
0xb172:  c7 52 67       ld $5267,A                               cycles=1
0xb175:  c7 52 69       ld $5269,A                               cycles=1
0xb178:  9f             ld A, XL                                 cycles=1
0xb179:  c7 52 66       ld $5266,A                               cycles=1
0xb17c:  c7 52 68       ld $5268,A                               cycles=1
0xb17f:  c7 52 6a       ld $526a,A                               cycles=1
0xb182:  cd 85 56       call $8556                               cycles=4
0xb185:  72 0c 00 96 21 btjt $96, #6, $b1ab  (offset=33)         cycles=2-3
0xb18a:  72 0b 00 8a 1c btjf $8a, #5, $b1ab  (offset=28)         cycles=2-3
0xb18f:  3c 6a          inc $6a                                  cycles=1
0xb191:  b6 6a          ld A, $6a                                cycles=1
0xb193:  a1 02          cp A, #$02                               cycles=1
0xb195:  25 11          jrc $b1a8  (offset=17)                   cycles=1-2
0xb197:  3f 6a          clr $6a                                  cycles=1
0xb199:  b6 1a          ld A, $1a                                cycles=1
0xb19b:  a1 19          cp A, #$19                               cycles=1
0xb19d:  25 04          jrc $b1a3  (offset=4)                    cycles=1-2
0xb19f:  3a 1a          dec $1a                                  cycles=1
0xb1a1:  20 05          jra $b1a8  (offset=5)                    cycles=2
0xb1a3:  83             trap                                     cycles=9
0xb1a4:  72 1b 00 8a    bres $8a, #5                             cycles=1
0xb1a8:  cc b2 01       jp $b201                                 cycles=1
0xb1ab:  b6 9f          ld A, $9f                                cycles=1
0xb1ad:  b7 d4          ld $d4,A                                 cycles=1
0xb1af:  b6 a7          ld A, $a7                                cycles=1
0xb1b1:  b7 d5          ld $d5,A                                 cycles=1
0xb1b3:  20 00          jra $b1b5  (offset=0)                    cycles=2
0xb1b5:  72 07 00 97 22 btjf $97, #3, $b1dc  (offset=34)         cycles=2-3
0xb1ba:  b6 1c          ld A, $1c                                cycles=1
0xb1bc:  a1 49          cp A, #$49                               cycles=1
0xb1be:  24 1c          jrnc $b1dc  (offset=28)                  cycles=1-2
0xb1c0:  b6 d7          ld A, $d7                                cycles=1
0xb1c2:  b1 d4          cp A, $d4                                cycles=1
0xb1c4:  25 02          jrc $b1c8  (offset=2)                    cycles=1-2
0xb1c6:  b6 d4          ld A, $d4                                cycles=1
0xb1c8:  a1 02          cp A, #$02                               cycles=1
0xb1ca:  24 02          jrnc $b1ce  (offset=2)                   cycles=1-2
0xb1cc:  a6 02          ld A, #$02                               cycles=1
0xb1ce:  b7 1a          ld $1a,A                                 cycles=1
0xb1d0:  b6 d7          ld A, $d7                                cycles=1
0xb1d2:  b1 d5          cp A, $d5                                cycles=1
0xb1d4:  25 02          jrc $b1d8  (offset=2)                    cycles=1-2
0xb1d6:  b6 d5          ld A, $d5                                cycles=1
0xb1d8:  b7 9c          ld $9c,A                                 cycles=1
0xb1da:  20 25          jra $b201  (offset=37)                   cycles=2
0xb1dc:  b6 d4          ld A, $d4                                cycles=1
0xb1de:  44             srl A                                    cycles=1
0xb1df:  5f             clrw X                                   cycles=1
0xb1e0:  97             ld XL, A                                 cycles=1
0xb1e1:  b6 a6          ld A, $a6                                cycles=1
0xb1e3:  42             mul X, A                                 cycles=4
0xb1e4:  a6 5a          ld A, #$5a                               cycles=1
0xb1e6:  62             div X, A                                 cycles=2-17
0xb1e7:  9f             ld A, XL                                 cycles=1
0xb1e8:  b7 1a          ld $1a,A                                 cycles=1
0xb1ea:  b6 d5          ld A, $d5                                cycles=1
0xb1ec:  5f             clrw X                                   cycles=1
0xb1ed:  97             ld XL, A                                 cycles=1
0xb1ee:  b6 a6          ld A, $a6                                cycles=1
0xb1f0:  42             mul X, A                                 cycles=4
0xb1f1:  a6 78          ld A, #$78                               cycles=1
0xb1f3:  62             div X, A                                 cycles=2-17
0xb1f4:  b6 f9          ld A, $f9                                cycles=1
0xb1f6:  a1 0d          cp A, #$0d                               cycles=1
0xb1f8:  24 04          jrnc $b1fe  (offset=4)                   cycles=1-2
0xb1fa:  42             mul X, A                                 cycles=4
0xb1fb:  a6 0d          ld A, #$0d                               cycles=1
0xb1fd:  62             div X, A                                 cycles=2-17
0xb1fe:  9f             ld A, XL                                 cycles=1
0xb1ff:  b7 9c          ld $9c,A                                 cycles=1
0xb201:  cd 86 99       call $8699                               cycles=4
0xb204:  b6 1c          ld A, $1c                                cycles=1
0xb206:  a1 eb          cp A, #$eb                               cycles=1
0xb208:  25 1a          jrc $b224  (offset=26)                   cycles=1-2
0xb20a:  3c 26          inc $26                                  cycles=1
0xb20c:  b6 26          ld A, $26                                cycles=1
0xb20e:  a1 fa          cp A, #$fa                               cycles=1
0xb210:  25 16          jrc $b228  (offset=22)                   cycles=1-2
0xb212:  3f 26          clr $26                                  cycles=1
0xb214:  3c 27          inc $27                                  cycles=1
0xb216:  b6 27          ld A, $27                                cycles=1
0xb218:  a1 02          cp A, #$02                               cycles=1
0xb21a:  25 0c          jrc $b228  (offset=12)                   cycles=1-2
0xb21c:  72 18 00 8d    bset $8d, #4                             cycles=1
0xb220:  72 1a 00 8f    bset $8f, #5                             cycles=1
0xb224:  3f 26          clr $26                                  cycles=1
0xb226:  3f 27          clr $27                                  cycles=1
0xb228:  3c 4c          inc $4c                                  cycles=1
0xb22a:  b6 4c          ld A, $4c                                cycles=1
0xb22c:  a1 14          cp A, #$14                               cycles=1
0xb22e:  25 5d          jrc $b28d  (offset=93)                   cycles=1-2
0xb230:  3f 4c          clr $4c                                  cycles=1
0xb232:  cd 84 2e       call $842e                               cycles=4
0xb235:  b6 0a          ld A, $0a                                cycles=1
0xb237:  3f 0a          clr $0a                                  cycles=1
0xb239:  b1 0b          cp A, $0b                                cycles=1
0xb23b:  27 0a          jreq $b247  (offset=10)                  cycles=1-2
0xb23d:  25 04          jrc $b243  (offset=4)                    cycles=1-2
0xb23f:  3c 0b          inc $0b                                  cycles=1
0xb241:  20 06          jra $b249  (offset=6)                    cycles=2
0xb243:  3a 0b          dec $0b                                  cycles=1
0xb245:  20 02          jra $b249  (offset=2)                    cycles=2
0xb247:  b7 0b          ld $0b,A                                 cycles=1
0xb249:  3d 7c          tnz $7c                                  cycles=1
0xb24b:  26 06          jrne $b253  (offset=6)                   cycles=1-2
0xb24d:  b6 7b          ld A, $7b                                cycles=1
0xb24f:  b1 f3          cp A, $f3                                cycles=1
0xb251:  25 06          jrc $b259  (offset=6)                    cycles=1-2
0xb253:  b6 0b          ld A, $0b                                cycles=1
0xb255:  a1 bc          cp A, #$bc                               cycles=1
0xb257:  25 08          jrc $b261  (offset=8)                    cycles=1-2
0xb259:  3d a5          tnz $a5                                  cycles=1
0xb25b:  27 10          jreq $b26d  (offset=16)                  cycles=1-2
0xb25d:  3a a5          dec $a5                                  cycles=1
0xb25f:  20 0c          jra $b26d  (offset=12)                   cycles=2
0xb261:  3c a5          inc $a5                                  cycles=1
0xb263:  b6 a5          ld A, $a5                                cycles=1
0xb265:  a1 5f          cp A, #$5f                               cycles=1
0xb267:  25 04          jrc $b26d  (offset=4)                    cycles=1-2
0xb269:  a6 5f          ld A, #$5f                               cycles=1
0xb26b:  b7 a5          ld $a5,A                                 cycles=1
0xb26d:  b6 0b          ld A, $0b                                cycles=1
0xb26f:  72 0c 00 8a 0f btjt $8a, #6, $b283  (offset=15)         cycles=2-3
0xb274:  72 03 00 8a 06 btjf $8a, #1, $b27f  (offset=6)          cycles=2-3
0xb279:  a1 02          cp A, #$02                               cycles=1
0xb27b:  24 0c          jrnc $b289  (offset=12)                  cycles=1-2
0xb27d:  20 04          jra $b283  (offset=4)                    cycles=2
0xb27f:  a1 06          cp A, #$06                               cycles=1
0xb281:  24 06          jrnc $b289  (offset=6)                   cycles=1-2
0xb283:  72 13 00 8a    bres $8a, #1                             cycles=1
0xb287:  20 04          jra $b28d  (offset=4)                    cycles=2
0xb289:  72 12 00 8a    bset $8a, #1                             cycles=1
0xb28d:  3c 11          inc $11                                  cycles=1
0xb28f:  b6 11          ld A, $11                                cycles=1
0xb291:  72 05 00 8a 3e btjf $8a, #2, $b2d4  (offset=62)         cycles=2-3
0xb296:  a1 05          cp A, #$05                               cycles=1
0xb298:  25 2c          jrc $b2c6  (offset=44)                   cycles=1-2
0xb29a:  3f 11          clr $11                                  cycles=1
0xb29c:  3c f8          inc $f8                                  cycles=1
0xb29e:  b6 f8          ld A, $f8                                cycles=1
0xb2a0:  a1 02          cp A, #$02                               cycles=1
0xb2a2:  25 38          jrc $b2dc  (offset=56)                   cycles=1-2
0xb2a4:  3f f8          clr $f8                                  cycles=1
0xb2a6:  72 0f 00 8a 1d btjf $8a, #7, $b2c8  (offset=29)         cycles=2-3
0xb2ab:  72 0a 00 8a 0e btjt $8a, #5, $b2be  (offset=14)         cycles=2-3
0xb2b0:  b6 de          ld A, $de                                cycles=1
0xb2b2:  a1 02          cp A, #$02                               cycles=1
0xb2b4:  25 12          jrc $b2c8  (offset=18)                   cycles=1-2
0xb2b6:  b1 dd          cp A, $dd                                cycles=1
0xb2b8:  24 14          jrnc $b2ce  (offset=20)                  cycles=1-2
0xb2ba:  3c de          inc $de                                  cycles=1
0xb2bc:  20 1e          jra $b2dc  (offset=30)                   cycles=2
0xb2be:  3d de          tnz $de                                  cycles=1
0xb2c0:  27 06          jreq $b2c8  (offset=6)                   cycles=1-2
0xb2c2:  3a de          dec $de                                  cycles=1
0xb2c4:  20 16          jra $b2dc  (offset=22)                   cycles=2
0xb2c6:  20 60          jra $b328  (offset=96)                   cycles=2
0xb2c8:  a6 02          ld A, #$02                               cycles=1
0xb2ca:  b7 de          ld $de,A                                 cycles=1
0xb2cc:  20 0e          jra $b2dc  (offset=14)                   cycles=2
0xb2ce:  b6 dd          ld A, $dd                                cycles=1
0xb2d0:  b7 de          ld $de,A                                 cycles=1
0xb2d2:  20 08          jra $b2dc  (offset=8)                    cycles=2
0xb2d4:  3f de          clr $de                                  cycles=1
0xb2d6:  a1 0a          cp A, #$0a                               cycles=1
0xb2d8:  25 4e          jrc $b328  (offset=78)                   cycles=1-2
0xb2da:  3f 11          clr $11                                  cycles=1
0xb2dc:  72 05 00 8a 1a btjf $8a, #2, $b2fb  (offset=26)         cycles=2-3
0xb2e1:  b6 a8          ld A, $a8                                cycles=1
0xb2e3:  a1 01          cp A, #$01                               cycles=1
0xb2e5:  25 0e          jrc $b2f5  (offset=14)                   cycles=1-2
0xb2e7:  a1 02          cp A, #$02                               cycles=1
0xb2e9:  25 0c          jrc $b2f7  (offset=12)                   cycles=1-2
0xb2eb:  a1 03          cp A, #$03                               cycles=1
0xb2ed:  25 0a          jrc $b2f9  (offset=10)                   cycles=1-2
0xb2ef:  a1 04          cp A, #$04                               cycles=1
0xb2f1:  25 08          jrc $b2fb  (offset=8)                    cycles=1-2
0xb2f3:  20 06          jra $b2fb  (offset=6)                    cycles=2
0xb2f5:  20 04          jra $b2fb  (offset=4)                    cycles=2
0xb2f7:  20 02          jra $b2fb  (offset=2)                    cycles=2
0xb2f9:  20 00          jra $b2fb  (offset=0)                    cycles=2
0xb2fb:  72 07 00 8a 08 btjf $8a, #3, $b308  (offset=8)          cycles=2-3
0xb300:  3c 12          inc $12                                  cycles=1
0xb302:  b6 12          ld A, $12                                cycles=1
0xb304:  b1 16          cp A, $16                                cycles=1
0xb306:  25 20          jrc $b328  (offset=32)                   cycles=1-2
0xb308:  a6 96          ld A, #$96                               cycles=1
0xb30a:  b7 16          ld $16,A                                 cycles=1
0xb30c:  72 15 00 8a    bres $8a, #2                             cycles=1
0xb310:  72 17 00 98    bres $98, #3                             cycles=1
0xb314:  72 17 00 8a    bres $8a, #3                             cycles=1
0xb318:  3f 12          clr $12                                  cycles=1
0xb31a:  3f 15          clr $15                                  cycles=1
0xb31c:  3f 13          clr $13                                  cycles=1
0xb31e:  3f 14          clr $14                                  cycles=1
0xb320:  3f 17          clr $17                                  cycles=1
0xb322:  3f 18          clr $18                                  cycles=1
0xb324:  3f 19          clr $19                                  cycles=1
0xb326:  3f a8          clr $a8                                  cycles=1
0xb328:  72 0e 50 10 13 btjt $5010, #7, $b340  (offset=19)       cycles=2-3
0xb32d:  72 07 00 8c 0b btjf $8c, #3, $b33d  (offset=11)         cycles=2-3
0xb332:  72 17 00 8c    bres $8c, #3                             cycles=1
0xb336:  72 01 50 15 4e btjf $5015, #0, $b389  (offset=78)       cycles=2-3
0xb33b:  20 11          jra $b34e  (offset=17)                   cycles=2
0xb33d:  cc b3 f9       jp $b3f9                                 cycles=1
0xb340:  72 06 00 8c f8 btjt $8c, #3, $b33d  (offset=-8)         cycles=2-3
0xb345:  72 16 00 8c    bset $8c, #3                             cycles=1
0xb349:  72 00 50 15 3b btjt $5015, #0, $b389  (offset=59)       cycles=2-3
0xb34e:  72 17 00 98    bres $98, #3                             cycles=1
0xb352:  72 04 00 8b 43 btjt $8b, #2, $b39a  (offset=67)         cycles=2-3
0xb357:  72 06 00 8a 08 btjt $8a, #3, $b364  (offset=8)          cycles=2-3
0xb35c:  72 16 00 8a    bset $8a, #3                             cycles=1
0xb360:  3f 12          clr $12                                  cycles=1
0xb362:  20 d9          jra $b33d  (offset=-39)                  cycles=2
0xb364:  72 0e 50 10 06 btjt $5010, #7, $b36f  (offset=6)        cycles=2-3
0xb369:  b6 12          ld A, $12                                cycles=1
0xb36b:  b7 13          ld $13,A                                 cycles=1
0xb36d:  20 04          jra $b373  (offset=4)                    cycles=2
0xb36f:  b6 12          ld A, $12                                cycles=1
0xb371:  b7 14          ld $14,A                                 cycles=1
0xb373:  3f 12          clr $12                                  cycles=1
0xb375:  3d 13          tnz $13                                  cycles=1
0xb377:  27 c4          jreq $b33d  (offset=-60)                 cycles=1-2
0xb379:  3d 14          tnz $14                                  cycles=1
0xb37b:  27 c0          jreq $b33d  (offset=-64)                 cycles=1-2
0xb37d:  b6 14          ld A, $14                                cycles=1
0xb37f:  b7 01          ld $01,A                                 cycles=1
0xb381:  b6 13          ld A, $13                                cycles=1
0xb383:  3f 13          clr $13                                  cycles=1
0xb385:  3f 14          clr $14                                  cycles=1
0xb387:  20 2f          jra $b3b8  (offset=47)                   cycles=2
0xb389:  72 16 00 98    bset $98, #3                             cycles=1
0xb38d:  72 05 00 8a 08 btjf $8a, #2, $b39a  (offset=8)          cycles=2-3
0xb392:  3c 19          inc $19                                  cycles=1
0xb394:  b6 19          ld A, $19                                cycles=1
0xb396:  a1 0d          cp A, #$0d                               cycles=1
0xb398:  25 5f          jrc $b3f9  (offset=95)                   cycles=1-2
0xb39a:  72 15 00 8a    bres $8a, #2                             cycles=1
0xb39e:  72 17 00 8a    bres $8a, #3                             cycles=1
0xb3a2:  3f 12          clr $12                                  cycles=1
0xb3a4:  3f 15          clr $15                                  cycles=1
0xb3a6:  3f 13          clr $13                                  cycles=1
0xb3a8:  3f 14          clr $14                                  cycles=1
0xb3aa:  3f 17          clr $17                                  cycles=1
0xb3ac:  3f 18          clr $18                                  cycles=1
0xb3ae:  3f 19          clr $19                                  cycles=1
0xb3b0:  3f a8          clr $a8                                  cycles=1
0xb3b2:  20 45          jra $b3f9  (offset=69)                   cycles=2
0xb3b4:  3f 19          clr $19                                  cycles=1
0xb3b6:  20 41          jra $b3f9  (offset=65)                   cycles=2
0xb3b8:  b7 01          ld $01,A                                 cycles=1
0xb3ba:  b6 2e          ld A, $2e                                cycles=1
0xb3bc:  b7 2f          ld $2f,A                                 cycles=1
0xb3be:  b6 01          ld A, $01                                cycles=1
0xb3c0:  b7 2e          ld $2e,A                                 cycles=1
0xb3c2:  b6 2e          ld A, $2e                                cycles=1
0xb3c4:  bb 2f          add A, $2f                               cycles=1
0xb3c6:  24 02          jrnc $b3ca  (offset=2)                   cycles=1-2
0xb3c8:  a6 ff          ld A, #$ff                               cycles=1
0xb3ca:  44             srl A                                    cycles=1
0xb3cb:  b7 17          ld $17,A                                 cycles=1
0xb3cd:  3f 19          clr $19                                  cycles=1
0xb3cf:  72 04 00 8a 14 btjt $8a, #2, $b3e8  (offset=20)         cycles=2-3
0xb3d4:  3c 15          inc $15                                  cycles=1
0xb3d6:  b6 15          ld A, $15                                cycles=1
0xb3d8:  a1 02          cp A, #$02                               cycles=1
0xb3da:  25 1d          jrc $b3f9  (offset=29)                   cycles=1-2
0xb3dc:  3f 15          clr $15                                  cycles=1
0xb3de:  72 14 00 8a    bset $8a, #2                             cycles=1
0xb3e2:  20 15          jra $b3f9  (offset=21)                   cycles=2
0xb3e4:  3f a8          clr $a8                                  cycles=1
0xb3e6:  20 11          jra $b3f9  (offset=17)                   cycles=2
0xb3e8:  72 0f 00 8a f7 btjf $8a, #7, $b3e4  (offset=-9)         cycles=2-3
0xb3ed:  3c a8          inc $a8                                  cycles=1
0xb3ef:  b6 a8          ld A, $a8                                cycles=1
0xb3f1:  a1 0c          cp A, #$0c                               cycles=1
0xb3f3:  25 04          jrc $b3f9  (offset=4)                    cycles=1-2
0xb3f5:  a6 0c          ld A, #$0c                               cycles=1
0xb3f7:  b7 a8          ld $a8,A                                 cycles=1
0xb3f9:  3f 01          clr $01                                  cycles=1
0xb3fb:  72 02 00 8f 0b btjt $8f, #1, $b40b  (offset=11)         cycles=2-3
0xb400:  72 02 00 8a 06 btjt $8a, #1, $b40b  (offset=6)          cycles=2-3
0xb405:  b6 a0          ld A, $a0                                cycles=1
0xb407:  b0 01          sub A, $01                               cycles=1
0xb409:  20 06          jra $b411  (offset=6)                    cycles=2
0xb40b:  b6 a0          ld A, $a0                                cycles=1
0xb40d:  b0 01          sub A, $01                               cycles=1
0xb40f:  20 00          jra $b411  (offset=0)                    cycles=2
0xb411:  b7 38          ld $38,A                                 cycles=1
0xb413:  72 0a 50 01 10 btjt $5001, #5, $b428  (offset=16)       cycles=2-3
0xb418:  b6 99          ld A, $99                                cycles=1
0xb41a:  a1 c8          cp A, #$c8                               cycles=1
0xb41c:  27 04          jreq $b422  (offset=4)                   cycles=1-2
0xb41e:  3c 99          inc $99                                  cycles=1
0xb420:  20 16          jra $b438  (offset=22)                   cycles=2
0xb422:  72 10 00 96    bset $96, #0                             cycles=1
0xb426:  20 10          jra $b438  (offset=16)                   cycles=2
0xb428:  3d 99          tnz $99                                  cycles=1
0xb42a:  27 04          jreq $b430  (offset=4)                   cycles=1-2
0xb42c:  3a 99          dec $99                                  cycles=1
0xb42e:  20 08          jra $b438  (offset=8)                    cycles=2
0xb430:  72 11 00 96    bres $96, #0                             cycles=1
0xb434:  72 13 00 96    bres $96, #1                             cycles=1
0xb438:  72 01 00 96 55 btjf $96, #0, $b492  (offset=85)         cycles=2-3
0xb43d:  72 02 00 96 10 btjt $96, #1, $b452  (offset=16)         cycles=2-3
0xb442:  b6 1f          ld A, $1f                                cycles=1
0xb444:  4c             inc A                                    cycles=1
0xb445:  b7 36          ld $36,A                                 cycles=1
0xb447:  a6 01          ld A, #$01                               cycles=1
0xb449:  b7 37          ld $37,A                                 cycles=1
0xb44b:  cd 9e 63       call $9e63                               cycles=4
0xb44e:  72 12 00 96    bset $96, #1                             cycles=1
0xb452:  72 04 00 96 3b btjt $96, #2, $b492  (offset=59)         cycles=2-3
0xb457:  b6 68          ld A, $68                                cycles=1
0xb459:  b7 36          ld $36,A                                 cycles=1
0xb45b:  a6 00          ld A, #$00                               cycles=1
0xb45d:  b7 37          ld $37,A                                 cycles=1
0xb45f:  cd 9e 63       call $9e63                               cycles=4
0xb462:  9d             nop                                      cycles=1
0xb463:  9d             nop                                      cycles=1
0xb464:  9d             nop                                      cycles=1
0xb465:  a6 12          ld A, #$12                               cycles=1
0xb467:  b7 31          ld $31,A                                 cycles=1
0xb469:  cd b9 90       call $b990                               cycles=4
0xb46c:  a6 05          ld A, #$05                               cycles=1
0xb46e:  cd b9 83       call $b983                               cycles=4
0xb471:  c6 54 04       ld A, $5404                              cycles=1
0xb474:  97             ld XL, A                                 cycles=1
0xb475:  b6 31          ld A, $31                                cycles=1
0xb477:  a1 12          cp A, #$12                               cycles=1
0xb479:  26 ea          jrne $b465  (offset=-22)                 cycles=1-2
0xb47b:  9f             ld A, XL                                 cycles=1
0xb47c:  b1 d1          cp A, $d1                                cycles=1
0xb47e:  25 04          jrc $b484  (offset=4)                    cycles=1-2
0xb480:  b0 d1          sub A, $d1                               cycles=1
0xb482:  20 01          jra $b485  (offset=1)                    cycles=2
0xb484:  4f             clr A                                    cycles=1
0xb485:  b7 36          ld $36,A                                 cycles=1
0xb487:  a6 02          ld A, #$02                               cycles=1
0xb489:  b7 37          ld $37,A                                 cycles=1
0xb48b:  cd 9e 63       call $9e63                               cycles=4
0xb48e:  72 14 00 96    bset $96, #2                             cycles=1
0xb492:  72 0b 00 97 27 btjf $97, #5, $b4be  (offset=39)         cycles=2-3
0xb497:  72 0e 00 97 06 btjt $97, #7, $b4a2  (offset=6)          cycles=2-3
0xb49c:  72 1e 00 97    bset $97, #7                             cycles=1
0xb4a0:  20 28          jra $b4ca  (offset=40)                   cycles=2
0xb4a2:  72 1f 00 97    bres $97, #7                             cycles=1
0xb4a6:  3c 39          inc $39                                  cycles=1
0xb4a8:  26 02          jrne $b4ac  (offset=2)                   cycles=1-2
0xb4aa:  3c aa          inc $aa                                  cycles=1
0xb4ac:  b6 aa          ld A, $aa                                cycles=1
0xb4ae:  a1 07          cp A, #$07                               cycles=1
0xb4b0:  24 0c          jrnc $b4be  (offset=12)                  cycles=1-2
0xb4b2:  b6 aa          ld A, $aa                                cycles=1
0xb4b4:  a1 08          cp A, #$08                               cycles=1
0xb4b6:  25 12          jrc $b4ca  (offset=18)                   cycles=1-2
0xb4b8:  b7 7c          ld $7c,A                                 cycles=1
0xb4ba:  3c 7c          inc $7c                                  cycles=1
0xb4bc:  20 0c          jra $b4ca  (offset=12)                   cycles=2
0xb4be:  3f 39          clr $39                                  cycles=1
0xb4c0:  a6 07          ld A, #$07                               cycles=1
0xb4c2:  b7 aa          ld $aa,A                                 cycles=1
0xb4c4:  b7 7c          ld $7c,A                                 cycles=1
0xb4c6:  b7 39          ld $39,A                                 cycles=1
0xb4c8:  b7 7b          ld $7b,A                                 cycles=1
0xb4ca:  72 02 50 01 12 btjt $5001, #1, $b4e1  (offset=18)       cycles=2-3
0xb4cf:  72 03 00 97 0a btjf $97, #1, $b4de  (offset=10)         cycles=2-3
0xb4d4:  72 13 00 97    bres $97, #1                             cycles=1
0xb4d8:  3f b3          clr $b3                                  cycles=1
0xb4da:  72 1d 00 97    bres $97, #6                             cycles=1
0xb4de:  cc b5 18       jp $b518                                 cycles=1
0xb4e1:  72 0c 00 97 f8 btjt $97, #6, $b4de  (offset=-8)         cycles=2-3
0xb4e6:  72 1c 00 97    bset $97, #6                             cycles=1
0xb4ea:  72 0a 00 97 06 btjt $97, #5, $b4f5  (offset=6)          cycles=2-3
0xb4ef:  72 1a 00 97    bset $97, #5                             cycles=1
0xb4f3:  20 e9          jra $b4de  (offset=-23)                  cycles=2
0xb4f5:  3d aa          tnz $aa                                  cycles=1
0xb4f7:  26 06          jrne $b4ff  (offset=6)                   cycles=1-2
0xb4f9:  b6 39          ld A, $39                                cycles=1
0xb4fb:  a1 0a          cp A, #$0a                               cycles=1
0xb4fd:  25 19          jrc $b518  (offset=25)                   cycles=1-2
0xb4ff:  b6 39          ld A, $39                                cycles=1
0xb501:  b7 7b          ld $7b,A                                 cycles=1
0xb503:  b6 aa          ld A, $aa                                cycles=1
0xb505:  b7 7c          ld $7c,A                                 cycles=1
0xb507:  b6 7b          ld A, $7b                                cycles=1
0xb509:  97             ld XL, A                                 cycles=1
0xb50a:  b6 7c          ld A, $7c                                cycles=1
0xb50c:  95             ld XH, A                                 cycles=1
0xb50d:  54             srlw X                                   cycles=2
0xb50e:  9f             ld A, XL                                 cycles=1
0xb50f:  b7 7b          ld $7b,A                                 cycles=1
0xb511:  9e             ld A, XH                                 cycles=1
0xb512:  b7 7c          ld $7c,A                                 cycles=1
0xb514:  3f 39          clr $39                                  cycles=1
0xb516:  3f aa          clr $aa                                  cycles=1
0xb518:  72 0d 00 97 10 btjf $97, #6, $b52d  (offset=16)         cycles=2-3
0xb51d:  3c b3          inc $b3                                  cycles=1
0xb51f:  b6 b3          ld A, $b3                                cycles=1
0xb521:  a1 05          cp A, #$05                               cycles=1
0xb523:  25 08          jrc $b52d  (offset=8)                    cycles=1-2
0xb525:  a6 05          ld A, #$05                               cycles=1
0xb527:  b7 b3          ld $b3,A                                 cycles=1
0xb529:  72 12 00 97    bset $97, #1                             cycles=1
0xb52d:  72 00 50 1f 10 btjt $501f, #0, $b542  (offset=16)       cycles=2-3
0xb532:  b6 3b          ld A, $3b                                cycles=1
0xb534:  a1 32          cp A, #$32                               cycles=1
0xb536:  27 04          jreq $b53c  (offset=4)                   cycles=1-2
0xb538:  3c 3b          inc $3b                                  cycles=1
0xb53a:  20 12          jra $b54e  (offset=18)                   cycles=2
0xb53c:  72 16 00 91    bset $91, #3                             cycles=1
0xb540:  20 0c          jra $b54e  (offset=12)                   cycles=2
0xb542:  3d 3b          tnz $3b                                  cycles=1
0xb544:  27 04          jreq $b54a  (offset=4)                   cycles=1-2
0xb546:  3a 3b          dec $3b                                  cycles=1
0xb548:  20 04          jra $b54e  (offset=4)                    cycles=2
0xb54a:  72 17 00 91    bres $91, #3                             cycles=1
0xb54e:  72 02 50 1f 0c btjt $501f, #1, $b55f  (offset=12)       cycles=2-3
0xb553:  a1 32          cp A, #$32                               cycles=1
0xb555:  27 02          jreq $b559  (offset=2)                   cycles=1-2
0xb557:  20 0e          jra $b567  (offset=14)                   cycles=2
0xb559:  72 12 00 91    bset $91, #1                             cycles=1
0xb55d:  20 08          jra $b567  (offset=8)                    cycles=2
0xb55f:  27 02          jreq $b563  (offset=2)                   cycles=1-2
0xb561:  20 04          jra $b567  (offset=4)                    cycles=2
0xb563:  72 13 00 91    bres $91, #1                             cycles=1
0xb567:  72 08 00 85 2d btjt $85, #4, $b599  (offset=45)         cycles=2-3
0xb56c:  72 08 00 8d 26 btjt $8d, #4, $b597  (offset=38)         cycles=2-3
0xb571:  72 0a 00 89 5c btjt $89, #5, $b5d2  (offset=92)         cycles=2-3
0xb576:  72 04 00 8f 1c btjt $8f, #2, $b597  (offset=28)         cycles=2-3
0xb57b:  72 04 00 8b 14 btjt $8b, #2, $b594  (offset=20)         cycles=2-3
0xb580:  72 08 00 8a 12 btjt $8a, #4, $b597  (offset=18)         cycles=2-3
0xb585:  72 00 00 89 5d btjt $89, #0, $b5e7  (offset=93)         cycles=2-3
0xb58a:  72 0b 00 85 0c btjf $85, #5, $b59b  (offset=12)         cycles=2-3
0xb58f:  72 0a 00 85 07 btjt $85, #5, $b59b  (offset=7)          cycles=2-3
0xb594:  cc b6 61       jp $b661                                 cycles=1
0xb597:  20 4b          jra $b5e4  (offset=75)                   cycles=2
0xb599:  20 37          jra $b5d2  (offset=55)                   cycles=2
0xb59b:  72 0f 00 96 0a btjf $96, #7, $b5aa  (offset=10)         cycles=2-3
0xb5a0:  72 00 00 8c 05 btjt $8c, #0, $b5aa  (offset=5)          cycles=2-3
0xb5a5:  a6 64          ld A, #$64                               cycles=1
0xb5a7:  cc b6 5f       jp $b65f                                 cycles=1
0xb5aa:  72 0e 00 8c 35 btjt $8c, #7, $b5e4  (offset=53)         cycles=2-3
0xb5af:  b6 1c          ld A, $1c                                cycles=1
0xb5b1:  a1 55          cp A, #$55                               cycles=1
0xb5b3:  24 37          jrnc $b5ec  (offset=55)                  cycles=1-2
0xb5b5:  72 0f 00 8a 04 btjf $8a, #7, $b5be  (offset=4)          cycles=2-3
0xb5ba:  a1 4e          cp A, #$4e                               cycles=1
0xb5bc:  24 2e          jrnc $b5ec  (offset=46)                  cycles=1-2
0xb5be:  3f 02          clr $02                                  cycles=1
0xb5c0:  72 04 00 8a 3c btjt $8a, #2, $b601  (offset=60)         cycles=2-3
0xb5c5:  72 07 00 97 08 btjf $97, #3, $b5d2  (offset=8)          cycles=2-3
0xb5ca:  cd 9a f3       call $9af3                               cycles=4
0xb5cd:  72 04 00 8a 2f btjt $8a, #2, $b601  (offset=47)         cycles=2-3
0xb5d2:  3f ef          clr $ef                                  cycles=1
0xb5d4:  3f f0          clr $f0                                  cycles=1
0xb5d6:  72 0f 00 8a 0c btjf $8a, #7, $b5e7  (offset=12)         cycles=2-3
0xb5db:  72 0d 00 96 06 btjf $96, #6, $b5e6  (offset=6)          cycles=2-3
0xb5e0:  72 1a 00 8a    bset $8a, #5                             cycles=1
0xb5e4:  20 01          jra $b5e7  (offset=1)                    cycles=2
0xb5e6:  83             trap                                     cycles=9
0xb5e7:  cc b7 04       jp $b704                                 cycles=1
0xb5ea:  a6 7b          ld A, #$7b                               cycles=1
0xb5ec:  a0 49          sub A, #$49                              cycles=1
0xb5ee:  72 1b 00 8a    bres $8a, #5                             cycles=1
0xb5f2:  72 07 00 91 06 btjf $91, #3, $b5fd  (offset=6)          cycles=2-3
0xb5f7:  a1 5a          cp A, #$5a                               cycles=1
0xb5f9:  25 02          jrc $b5fd  (offset=2)                    cycles=1-2
0xb5fb:  a6 5a          ld A, #$5a                               cycles=1
0xb5fd:  b7 02          ld $02,A                                 cycles=1
0xb5ff:  20 60          jra $b661  (offset=96)                   cycles=2
0xb601:  72 06 00 97 08 btjt $97, #3, $b60e  (offset=8)          cycles=2-3
0xb606:  72 1b 00 8a    bres $8a, #5                             cycles=1
0xb60a:  b6 04          ld A, $04                                cycles=1
0xb60c:  20 51          jra $b65f  (offset=81)                   cycles=2
0xb60e:  b6 20          ld A, $20                                cycles=1
0xb610:  b1 d9          cp A, $d9                                cycles=1
0xb612:  25 27          jrc $b63b  (offset=39)                   cycles=1-2
0xb614:  72 1b 00 8a    bres $8a, #5                             cycles=1
0xb618:  3f ef          clr $ef                                  cycles=1
0xb61a:  3f f0          clr $f0                                  cycles=1
0xb61c:  72 0e 00 8a 06 btjt $8a, #7, $b627  (offset=6)          cycles=2-3
0xb621:  b0 d9          sub A, $d9                               cycles=1
0xb623:  a1 00          cp A, #$00                               cycles=1
0xb625:  25 ab          jrc $b5d2  (offset=-85)                  cycles=1-2
0xb627:  cd 9b 2c       call $9b2c                               cycles=4
0xb62a:  b6 a8          ld A, $a8                                cycles=1
0xb62c:  a1 03          cp A, #$03                               cycles=1
0xb62e:  25 04          jrc $b634  (offset=4)                    cycles=1-2
0xb630:  b6 de          ld A, $de                                cycles=1
0xb632:  20 29          jra $b65d  (offset=41)                   cycles=2
0xb634:  a6 1e          ld A, #$1e                               cycles=1
0xb636:  20 25          jra $b65d  (offset=37)                   cycles=2
0xb638:  cc b5 d2       jp $b5d2                                 cycles=1
0xb63b:  72 0a 00 8a 4c btjt $8a, #5, $b68c  (offset=76)         cycles=2-3
0xb640:  72 0f 00 8a 8d btjf $8a, #7, $b5d2  (offset=-115)       cycles=2-3
0xb645:  3c ef          inc $ef                                  cycles=1
0xb647:  b6 ef          ld A, $ef                                cycles=1
0xb649:  a1 0a          cp A, #$0a                               cycles=1
0xb64b:  25 0a          jrc $b657  (offset=10)                   cycles=1-2
0xb64d:  3f ef          clr $ef                                  cycles=1
0xb64f:  3c f0          inc $f0                                  cycles=1
0xb651:  b6 f0          ld A, $f0                                cycles=1
0xb653:  a1 19          cp A, #$19                               cycles=1
0xb655:  24 e1          jrnc $b638  (offset=-31)                 cycles=1-2
0xb657:  a6 01          ld A, #$01                               cycles=1
0xb659:  b7 d6          ld $d6,A                                 cycles=1
0xb65b:  a6 14          ld A, #$14                               cycles=1
0xb65d:  b6 de          ld A, $de                                cycles=1
0xb65f:  b7 02          ld $02,A                                 cycles=1
0xb661:  b6 02          ld A, $02                                cycles=1
0xb663:  b1 a4          cp A, $a4                                cycles=1
0xb665:  25 04          jrc $b66b  (offset=4)                    cycles=1-2
0xb667:  b6 a4          ld A, $a4                                cycles=1
0xb669:  b7 02          ld $02,A                                 cycles=1
0xb66b:  b6 02          ld A, $02                                cycles=1
0xb66d:  b1 a5          cp A, $a5                                cycles=1
0xb66f:  25 04          jrc $b675  (offset=4)                    cycles=1-2
0xb671:  b6 a5          ld A, $a5                                cycles=1
0xb673:  b7 02          ld $02,A                                 cycles=1
0xb675:  b6 03          ld A, $03                                cycles=1
0xb677:  b1 02          cp A, $02                                cycles=1
0xb679:  27 1f          jreq $b69a  (offset=31)                  cycles=1-2
0xb67b:  25 12          jrc $b68f  (offset=18)                   cycles=1-2
0xb67d:  72 04 00 8a 06 btjt $8a, #2, $b688  (offset=6)          cycles=2-3
0xb682:  b6 02          ld A, $02                                cycles=1
0xb684:  b7 03          ld $03,A                                 cycles=1
0xb686:  20 12          jra $b69a  (offset=18)                   cycles=2
0xb688:  3a 03          dec $03                                  cycles=1
0xb68a:  20 0e          jra $b69a  (offset=14)                   cycles=2
0xb68c:  cc b7 04       jp $b704                                 cycles=1
0xb68f:  72 05 00 89 f8 btjf $89, #2, $b68c  (offset=-8)         cycles=2-3
0xb694:  72 15 00 89    bres $89, #2                             cycles=1
0xb698:  3c 03          inc $03                                  cycles=1
0xb69a:  72 0c 00 96 10 btjt $96, #6, $b6af  (offset=16)         cycles=2-3
0xb69f:  b6 03          ld A, $03                                cycles=1
0xb6a1:  a1 64          cp A, #$64                               cycles=1
0xb6a3:  25 20          jrc $b6c5  (offset=32)                   cycles=1-2
0xb6a5:  a6 01          ld A, #$01                               cycles=1
0xb6a7:  b7 24          ld $24,A                                 cycles=1
0xb6a9:  a6 f4          ld A, #$f4                               cycles=1
0xb6ab:  b7 25          ld $25,A                                 cycles=1
0xb6ad:  20 39          jra $b6e8  (offset=57)                   cycles=2
0xb6af:  b6 03          ld A, $03                                cycles=1
0xb6b1:  a1 64          cp A, #$64                               cycles=1
0xb6b3:  24 07          jrnc $b6bc  (offset=7)                   cycles=1-2
0xb6b5:  b7 c2          ld $c2,A                                 cycles=1
0xb6b7:  cd 86 dc       call $86dc                               cycles=4
0xb6ba:  20 44          jra $b700  (offset=68)                   cycles=2
0xb6bc:  a6 64          ld A, #$64                               cycles=1
0xb6be:  b7 c2          ld $c2,A                                 cycles=1
0xb6c0:  cd 86 dc       call $86dc                               cycles=4
0xb6c3:  20 3b          jra $b700  (offset=59)                   cycles=2
0xb6c5:  72 06 00 89 0f btjt $89, #3, $b6d9  (offset=15)         cycles=2-3
0xb6ca:  72 04 00 8a 00 btjt $8a, #2, $b6cf  (offset=0)          cycles=2-3
0xb6cf:  ae 00 05       ldw X, #$5                               cycles=2
0xb6d2:  20 0d          jra $b6e1  (offset=13)                   cycles=2
0xb6d4:  ae 00 08       ldw X, #$8                               cycles=2
0xb6d7:  20 08          jra $b6e1  (offset=8)                    cycles=2
0xb6d9:  ae 00 06       ldw X, #$6                               cycles=2
0xb6dc:  20 03          jra $b6e1  (offset=3)                    cycles=2
0xb6de:  ae 00 05       ldw X, #$5                               cycles=2
0xb6e1:  42             mul X, A                                 cycles=4
0xb6e2:  9f             ld A, XL                                 cycles=1
0xb6e3:  b7 25          ld $25,A                                 cycles=1
0xb6e5:  9e             ld A, XH                                 cycles=1
0xb6e6:  b7 24          ld $24,A                                 cycles=1
0xb6e8:  72 0d 00 8a 13 btjf $8a, #6, $b700  (offset=19)         cycles=2-3
0xb6ed:  72 02 00 89 0e btjt $89, #1, $b700  (offset=14)         cycles=2-3
0xb6f2:  72 02 00 8f 05 btjt $8f, #1, $b6fc  (offset=5)          cycles=2-3
0xb6f7:  cd 9c 16       call $9c16                               cycles=4
0xb6fa:  20 04          jra $b700  (offset=4)                    cycles=2
0xb6fc:  72 1e 00 93    bset $93, #7                             cycles=1
0xb700:  72 1e 00 8a    bset $8a, #7                             cycles=1
0xb704:  72 07 00 92 10 btjf $92, #3, $b719  (offset=16)         cycles=2-3
0xb709:  3a 4e          dec $4e                                  cycles=1
0xb70b:  26 0c          jrne $b719  (offset=12)                  cycles=1-2
0xb70d:  a6 64          ld A, #$64                               cycles=1
0xb70f:  b7 4e          ld $4e,A                                 cycles=1
0xb711:  3a 4f          dec $4f                                  cycles=1
0xb713:  26 04          jrne $b719  (offset=4)                   cycles=1-2
0xb715:  72 17 00 92    bres $92, #3                             cycles=1
0xb719:  72 08 00 8d 1b btjt $8d, #4, $b739  (offset=27)         cycles=2-3
0xb71e:  72 03 00 93 16 btjf $93, #1, $b739  (offset=22)         cycles=2-3
0xb723:  3c 63          inc $63                                  cycles=1
0xb725:  b6 63          ld A, $63                                cycles=1
0xb727:  a1 fa          cp A, #$fa                               cycles=1
0xb729:  25 12          jrc $b73d  (offset=18)                   cycles=1-2
0xb72b:  3f 63          clr $63                                  cycles=1
0xb72d:  3c 64          inc $64                                  cycles=1
0xb72f:  b6 64          ld A, $64                                cycles=1
0xb731:  a1 04          cp A, #$04                               cycles=1
0xb733:  25 08          jrc $b73d  (offset=8)                    cycles=1-2
0xb735:  72 13 00 93    bres $93, #1                             cycles=1
0xb739:  3f 63          clr $63                                  cycles=1
0xb73b:  3f 64          clr $64                                  cycles=1
0xb73d:  72 03 00 8f 0f btjf $8f, #1, $b751  (offset=15)         cycles=2-3
0xb742:  72 0c 00 96 0a btjt $96, #6, $b751  (offset=10)         cycles=2-3
0xb747:  72 0f 00 8a 05 btjf $8a, #7, $b751  (offset=5)          cycles=2-3
0xb74c:  72 0c 00 8a 03 btjt $8a, #6, $b754  (offset=3)          cycles=2-3
0xb751:  cc b7 c5       jp $b7c5                                 cycles=1
0xb754:  9b             sim                                      cycles=1
0xb755:  cd ab 28       call $ab28                               cycles=4
0xb758:  b6 08          ld A, $08                                cycles=1
0xb75a:  b1 07          cp A, $07                                cycles=1
0xb75c:  26 03          jrne $b761  (offset=3)                   cycles=1-2
0xb75e:  cc b7 90       jp $b790                                 cycles=1
0xb761:  b6 08          ld A, $08                                cycles=1
0xb763:  b1 05          cp A, $05                                cycles=1
0xb765:  27 12          jreq $b779  (offset=18)                  cycles=1-2
0xb767:  b1 06          cp A, $06                                cycles=1
0xb769:  27 0e          jreq $b779  (offset=14)                  cycles=1-2
0xb76b:  72 11 50 05    bres $5005, #0                           cycles=1
0xb76f:  72 13 50 05    bres $5005, #1                           cycles=1
0xb773:  72 15 50 05    bres $5005, #2                           cycles=1
0xb777:  20 17          jra $b790  (offset=23)                   cycles=2
0xb779:  cd b9 97       call $b997                               cycles=4
0xb77c:  72 00 00 90 05 btjt $90, #0, $b786  (offset=5)          cycles=2-3
0xb781:  72 0f 00 8a 04 btjf $8a, #7, $b78a  (offset=4)          cycles=2-3
0xb786:  72 1e 00 93    bset $93, #7                             cycles=1
0xb78a:  cd ab 98       call $ab98                               cycles=4
0xb78d:  cd b9 f9       call $b9f9                               cycles=4
0xb790:  9a             rim                                      cycles=1
0xb791:  72 0e 00 8a 06 btjt $8a, #7, $b79c  (offset=6)          cycles=2-3
0xb796:  72 1f 00 93    bres $93, #7                             cycles=1
0xb79a:  20 29          jra $b7c5  (offset=41)                   cycles=2
0xb79c:  72 0f 00 93 24 btjf $93, #7, $b7c5  (offset=36)         cycles=2-3
0xb7a1:  3d 65          tnz $65                                  cycles=1
0xb7a3:  26 20          jrne $b7c5  (offset=32)                  cycles=1-2
0xb7a5:  a6 05          ld A, #$05                               cycles=1
0xb7a7:  b7 65          ld $65,A                                 cycles=1
0xb7a9:  72 1f 00 93    bres $93, #7                             cycles=1
0xb7ad:  9b             sim                                      cycles=1
0xb7ae:  cd ab 28       call $ab28                               cycles=4
0xb7b1:  cd ab 98       call $ab98                               cycles=4
0xb7b4:  72 00 00 90 05 btjt $90, #0, $b7be  (offset=5)          cycles=2-3
0xb7b9:  cd a9 9b       call $a99b                               cycles=4
0xb7bc:  20 03          jra $b7c1  (offset=3)                    cycles=2
0xb7be:  cd a3 8b       call $a38b                               cycles=4
0xb7c1:  cd a0 dd       call $a0dd                               cycles=4
0xb7c4:  9a             rim                                      cycles=1
0xb7c5:  3d 65          tnz $65                                  cycles=1
0xb7c7:  27 02          jreq $b7cb  (offset=2)                   cycles=1-2
0xb7c9:  3a 65          dec $65                                  cycles=1
0xb7cb:  72 01 00 90 21 btjf $90, #0, $b7f1  (offset=33)         cycles=2-3
0xb7d0:  3c 69          inc $69                                  cycles=1
0xb7d2:  b6 69          ld A, $69                                cycles=1
0xb7d4:  a1 64          cp A, #$64                               cycles=1
0xb7d6:  25 19          jrc $b7f1  (offset=25)                   cycles=1-2
0xb7d8:  3f 69          clr $69                                  cycles=1
0xb7da:  72 0d 00 8a 12 btjf $8a, #6, $b7f1  (offset=18)         cycles=2-3
0xb7df:  b6 24          ld A, $24                                cycles=1
0xb7e1:  95             ld XH, A                                 cycles=1
0xb7e2:  b6 25          ld A, $25                                cycles=1
0xb7e4:  97             ld XL, A                                 cycles=1
0xb7e5:  a3 00 00       cpw X, #$0                               cycles=2
0xb7e8:  27 07          jreq $b7f1  (offset=7)                   cycles=1-2
0xb7ea:  5a             decw X                                   cycles=1
0xb7eb:  9e             ld A, XH                                 cycles=1
0xb7ec:  b7 24          ld $24,A                                 cycles=1
0xb7ee:  9f             ld A, XL                                 cycles=1
0xb7ef:  b7 25          ld $25,A                                 cycles=1
0xb7f1:  72 0c 50 0b 10 btjt $500b, #6, $b806  (offset=16)       cycles=2-3
0xb7f6:  b6 6b          ld A, $6b                                cycles=1
0xb7f8:  a1 32          cp A, #$32                               cycles=1
0xb7fa:  27 04          jreq $b800  (offset=4)                   cycles=1-2
0xb7fc:  3c 6b          inc $6b                                  cycles=1
0xb7fe:  20 12          jra $b812  (offset=18)                   cycles=2
0xb800:  72 18 00 92    bset $92, #4                             cycles=1
0xb804:  20 0c          jra $b812  (offset=12)                   cycles=2
0xb806:  3d 6b          tnz $6b                                  cycles=1
0xb808:  27 04          jreq $b80e  (offset=4)                   cycles=1-2
0xb80a:  3a 6b          dec $6b                                  cycles=1
0xb80c:  20 04          jra $b812  (offset=4)                    cycles=2
0xb80e:  72 19 00 92    bres $92, #4                             cycles=1
0xb812:  72 06 52 45 10 btjt $5245, #3, $b827  (offset=16)       cycles=2-3
0xb817:  3c 6e          inc $6e                                  cycles=1
0xb819:  b6 6e          ld A, $6e                                cycles=1
0xb81b:  a1 64          cp A, #$64                               cycles=1
0xb81d:  25 08          jrc $b827  (offset=8)                    cycles=1-2
0xb81f:  3f 6e          clr $6e                                  cycles=1
0xb821:  72 16 52 45    bset $5245, #3                           cycles=1
0xb825:  3f 6f          clr $6f                                  cycles=1
0xb827:  72 0a 00 85 47 btjt $85, #5, $b873  (offset=71)         cycles=2-3
0xb82c:  a6 10          ld A, #$10                               cycles=1
0xb82e:  b7 31          ld $31,A                                 cycles=1
0xb830:  cd b9 90       call $b990                               cycles=4
0xb833:  a6 07          ld A, #$07                               cycles=1
0xb835:  cd b9 83       call $b983                               cycles=4
0xb838:  c6 54 04       ld A, $5404                              cycles=1
0xb83b:  97             ld XL, A                                 cycles=1
0xb83c:  b6 31          ld A, $31                                cycles=1
0xb83e:  a1 10          cp A, #$10                               cycles=1
0xb840:  26 e5          jrne $b827  (offset=-27)                 cycles=1-2
0xb842:  9f             ld A, XL                                 cycles=1
0xb843:  b1 5b          cp A, $5b                                cycles=1
0xb845:  27 04          jreq $b84b  (offset=4)                   cycles=1-2
0xb847:  b7 5b          ld $5b,A                                 cycles=1
0xb849:  20 02          jra $b84d  (offset=2)                    cycles=2
0xb84b:  b7 5a          ld $5a,A                                 cycles=1
0xb84d:  b1 1c          cp A, $1c                                cycles=1
0xb84f:  27 26          jreq $b877  (offset=38)                  cycles=1-2
0xb851:  25 1c          jrc $b86f  (offset=28)                   cycles=1-2
0xb853:  b6 1c          ld A, $1c                                cycles=1
0xb855:  a1 55          cp A, #$55                               cycles=1
0xb857:  24 06          jrnc $b85f  (offset=6)                   cycles=1-2
0xb859:  3c 1c          inc $1c                                  cycles=1
0xb85b:  3f ff          clr $ff                                  cycles=1
0xb85d:  20 18          jra $b877  (offset=24)                   cycles=2
0xb85f:  b6 ff          ld A, $ff                                cycles=1
0xb861:  a1 1e          cp A, #$1e                               cycles=1
0xb863:  25 06          jrc $b86b  (offset=6)                    cycles=1-2
0xb865:  3c 1c          inc $1c                                  cycles=1
0xb867:  3f ff          clr $ff                                  cycles=1
0xb869:  20 0c          jra $b877  (offset=12)                   cycles=2
0xb86b:  3c ff          inc $ff                                  cycles=1
0xb86d:  20 08          jra $b877  (offset=8)                    cycles=2
0xb86f:  3a 1c          dec $1c                                  cycles=1
0xb871:  20 04          jra $b877  (offset=4)                    cycles=2
0xb873:  a6 64          ld A, #$64                               cycles=1
0xb875:  b7 1c          ld $1c,A                                 cycles=1
0xb877:  a6 11          ld A, #$11                               cycles=1
0xb879:  b7 31          ld $31,A                                 cycles=1
0xb87b:  cd b9 90       call $b990                               cycles=4
0xb87e:  a6 06          ld A, #$06                               cycles=1
0xb880:  cd b9 83       call $b983                               cycles=4
0xb883:  c6 54 04       ld A, $5404                              cycles=1
0xb886:  97             ld XL, A                                 cycles=1
0xb887:  b6 31          ld A, $31                                cycles=1
0xb889:  a1 11          cp A, #$11                               cycles=1
0xb88b:  26 ea          jrne $b877  (offset=-22)                 cycles=1-2
0xb88d:  9f             ld A, XL                                 cycles=1
0xb88e:  b7 66          ld $66,A                                 cycles=1
0xb890:  b1 1f          cp A, $1f                                cycles=1
0xb892:  27 08          jreq $b89c  (offset=8)                   cycles=1-2
0xb894:  25 04          jrc $b89a  (offset=4)                    cycles=1-2
0xb896:  3c 1f          inc $1f                                  cycles=1
0xb898:  20 02          jra $b89c  (offset=2)                    cycles=2
0xb89a:  3a 1f          dec $1f                                  cycles=1
0xb89c:  a6 12          ld A, #$12                               cycles=1
0xb89e:  b7 31          ld $31,A                                 cycles=1
0xb8a0:  cd b9 90       call $b990                               cycles=4
0xb8a3:  a6 05          ld A, #$05                               cycles=1
0xb8a5:  cd b9 83       call $b983                               cycles=4
0xb8a8:  c6 54 04       ld A, $5404                              cycles=1
0xb8ab:  a1 3f          cp A, #$3f                               cycles=1
0xb8ad:  24 07          jrnc $b8b6  (offset=7)                   cycles=1-2
0xb8af:  48             sll A                                    cycles=1
0xb8b0:  48             sll A                                    cycles=1
0xb8b1:  ca 54 05       or A, $5405                              cycles=1
0xb8b4:  20 02          jra $b8b8  (offset=2)                    cycles=2
0xb8b6:  a6 ff          ld A, #$ff                               cycles=1
0xb8b8:  97             ld XL, A                                 cycles=1
0xb8b9:  b6 31          ld A, $31                                cycles=1
0xb8bb:  a1 12          cp A, #$12                               cycles=1
0xb8bd:  26 dd          jrne $b89c  (offset=-35)                 cycles=1-2
0xb8bf:  72 00 00 96 12 btjt $96, #0, $b8d6  (offset=18)         cycles=2-3
0xb8c4:  72 0e 00 8a 0d btjt $8a, #7, $b8d6  (offset=13)         cycles=2-3
0xb8c9:  3c d2          inc $d2                                  cycles=1
0xb8cb:  b6 d2          ld A, $d2                                cycles=1
0xb8cd:  a1 fa          cp A, #$fa                               cycles=1
0xb8cf:  25 07          jrc $b8d8  (offset=7)                    cycles=1-2
0xb8d1:  3f d2          clr $d2                                  cycles=1
0xb8d3:  9f             ld A, XL                                 cycles=1
0xb8d4:  b7 d0          ld $d0,A                                 cycles=1
0xb8d6:  3f d2          clr $d2                                  cycles=1
0xb8d8:  9f             ld A, XL                                 cycles=1
0xb8d9:  b1 d0          cp A, $d0                                cycles=1
0xb8db:  27 09          jreq $b8e6  (offset=9)                   cycles=1-2
0xb8dd:  25 07          jrc $b8e6  (offset=7)                    cycles=1-2
0xb8df:  3f cf          clr $cf                                  cycles=1
0xb8e1:  b0 d0          sub A, $d0                               cycles=1
0xb8e3:  97             ld XL, A                                 cycles=1
0xb8e4:  20 05          jra $b8eb  (offset=5)                    cycles=2
0xb8e6:  a6 01          ld A, #$01                               cycles=1
0xb8e8:  b7 cf          ld $cf,A                                 cycles=1
0xb8ea:  4f             clr A                                    cycles=1
0xb8eb:  bb 1d          add A, $1d                               cycles=1
0xb8ed:  25 05          jrc $b8f4  (offset=5)                    cycles=1-2
0xb8ef:  44             srl A                                    cycles=1
0xb8f0:  b7 1d          ld $1d,A                                 cycles=1
0xb8f2:  20 03          jra $b8f7  (offset=3)                    cycles=2
0xb8f4:  9f             ld A, XL                                 cycles=1
0xb8f5:  b7 1d          ld $1d,A                                 cycles=1
0xb8f7:  a6 13          ld A, #$13                               cycles=1
0xb8f9:  b7 31          ld $31,A                                 cycles=1
0xb8fb:  cd b9 90       call $b990                               cycles=4
0xb8fe:  a6 04          ld A, #$04                               cycles=1
0xb900:  cd b9 83       call $b983                               cycles=4
0xb903:  c6 54 04       ld A, $5404                              cycles=1
0xb906:  a1 7f          cp A, #$7f                               cycles=1
0xb908:  24 0b          jrnc $b915  (offset=11)                  cycles=1-2
0xb90a:  48             sll A                                    cycles=1
0xb90b:  b7 df          ld $df,A                                 cycles=1
0xb90d:  c6 54 05       ld A, $5405                              cycles=1
0xb910:  44             srl A                                    cycles=1
0xb911:  ba df          or A, $df                                cycles=1
0xb913:  20 02          jra $b917  (offset=2)                    cycles=2
0xb915:  a6 ff          ld A, #$ff                               cycles=1
0xb917:  97             ld XL, A                                 cycles=1
0xb918:  b6 31          ld A, $31                                cycles=1
0xb91a:  a1 13          cp A, #$13                               cycles=1
0xb91c:  26 d9          jrne $b8f7  (offset=-39)                 cycles=1-2
0xb91e:  9f             ld A, XL                                 cycles=1
0xb91f:  b7 22          ld $22,A                                 cycles=1
0xb921:  b1 20          cp A, $20                                cycles=1
0xb923:  27 44          jreq $b969  (offset=68)                  cycles=1-2
0xb925:  25 21          jrc $b948  (offset=33)                   cycles=1-2
0xb927:  72 09 00 97 10 btjf $97, #4, $b93c  (offset=16)         cycles=2-3
0xb92c:  b0 20          sub A, $20                               cycles=1
0xb92e:  b7 21          ld $21,A                                 cycles=1
0xb930:  3c f6          inc $f6                                  cycles=1
0xb932:  b6 f6          ld A, $f6                                cycles=1
0xb934:  a1 14          cp A, #$14                               cycles=1
0xb936:  25 31          jrc $b969  (offset=49)                   cycles=1-2
0xb938:  3f f6          clr $f6                                  cycles=1
0xb93a:  20 04          jra $b940  (offset=4)                    cycles=2
0xb93c:  3c 20          inc $20                                  cycles=1
0xb93e:  20 29          jra $b969  (offset=41)                   cycles=2
0xb940:  b6 20          ld A, $20                                cycles=1
0xb942:  bb 21          add A, $21                               cycles=1
0xb944:  b7 20          ld $20,A                                 cycles=1
0xb946:  20 21          jra $b969  (offset=33)                   cycles=2
0xb948:  72 09 00 97 12 btjf $97, #4, $b95f  (offset=18)         cycles=2-3
0xb94d:  b6 20          ld A, $20                                cycles=1
0xb94f:  b0 22          sub A, $22                               cycles=1
0xb951:  b7 21          ld $21,A                                 cycles=1
0xb953:  3c f7          inc $f7                                  cycles=1
0xb955:  b6 f7          ld A, $f7                                cycles=1
0xb957:  a1 10          cp A, #$10                               cycles=1
0xb959:  25 0e          jrc $b969  (offset=14)                   cycles=1-2
0xb95b:  3f f7          clr $f7                                  cycles=1
0xb95d:  20 04          jra $b963  (offset=4)                    cycles=2
0xb95f:  3a 20          dec $20                                  cycles=1
0xb961:  20 06          jra $b969  (offset=6)                    cycles=2
0xb963:  b6 20          ld A, $20                                cycles=1
0xb965:  b0 21          sub A, $21                               cycles=1
0xb967:  b7 20          ld $20,A                                 cycles=1
0xb969:  20 00          jra $b96b  (offset=0)                    cycles=2
0xb96b:  72 18 50 00    bset $5000, #4                           cycles=1
0xb96f:  cd 83 e4       call $83e4                               cycles=4
0xb972:  72 04 00 8a 06 btjt $8a, #2, $b97d  (offset=6)          cycles=2-3
0xb977:  72 17 00 77    bres $77, #3                             cycles=1
0xb97b:  20 04          jra $b981  (offset=4)                    cycles=2
0xb97d:  72 16 00 77    bset $77, #3                             cycles=1
0xb981:  80             iret                                     cycles=11
0xb982:  81             ret                                      cycles=4
0xb983:  c7 54 00       ld $5400,A                               cycles=1
0xb986:  72 10 54 01    bset $5401, #0                           cycles=1
0xb98a:  72 0f 54 00 fb btjf $5400, #7, $b98a  (offset=-5)       cycles=2-3
0xb98f:  81             ret                                      cycles=4
0xb990:  ae 00 08       ldw X, #$8                               cycles=2
0xb993:  5a             decw X                                   cycles=1
0xb994:  26 fd          jrne $b993  (offset=-3)                  cycles=1-2
0xb996:  81             ret                                      cycles=4
0xb997:  cd ba ca       call $baca                               cycles=4
0xb99a:  a3 9c 40       cpw X, #$9c40                            cycles=2
0xb99d:  24 25          jrnc $b9c4  (offset=37)                  cycles=1-2
0xb99f:  b6 08          ld A, $08                                cycles=1
0xb9a1:  b1 29          cp A, $29                                cycles=1
0xb9a3:  27 1f          jreq $b9c4  (offset=31)                  cycles=1-2
0xb9a5:  b1 2a          cp A, $2a                                cycles=1
0xb9a7:  27 1b          jreq $b9c4  (offset=27)                  cycles=1-2
0xb9a9:  b1 2b          cp A, $2b                                cycles=1
0xb9ab:  27 17          jreq $b9c4  (offset=23)                  cycles=1-2
0xb9ad:  b6 29          ld A, $29                                cycles=1
0xb9af:  b1 2a          cp A, $2a                                cycles=1
0xb9b1:  27 11          jreq $b9c4  (offset=17)                  cycles=1-2
0xb9b3:  b1 2b          cp A, $2b                                cycles=1
0xb9b5:  27 0d          jreq $b9c4  (offset=13)                  cycles=1-2
0xb9b7:  b6 2a          ld A, $2a                                cycles=1
0xb9b9:  b1 2b          cp A, $2b                                cycles=1
0xb9bb:  27 07          jreq $b9c4  (offset=7)                   cycles=1-2
0xb9bd:  72 15 00 93    bres $93, #2                             cycles=1
0xb9c1:  cd 9c 06       call $9c06                               cycles=4
0xb9c4:  81             ret                                      cycles=4
0xb9c5:  cd ba ca       call $baca                               cycles=4
0xb9c8:  72 02 00 8f 17 btjt $8f, #1, $b9e4  (offset=23)         cycles=2-3
0xb9cd:  72 0f 00 8a 0b btjf $8a, #7, $b9dd  (offset=11)         cycles=2-3
0xb9d2:  a3 00 19       cpw X, #$19                              cycles=2
0xb9d5:  24 06          jrnc $b9dd  (offset=6)                   cycles=1-2
0xb9d7:  a6 1e          ld A, #$1e                               cycles=1
0xb9d9:  b7 50          ld $50,A                                 cycles=1
0xb9db:  20 0c          jra $b9e9  (offset=12)                   cycles=2
0xb9dd:  a3 09 c4       cpw X, #$9c4                             cycles=2
0xb9e0:  25 16          jrc $b9f8  (offset=22)                   cycles=1-2
0xb9e2:  20 05          jra $b9e9  (offset=5)                    cycles=2
0xb9e4:  a3 fd e8       cpw X, #$fde8                            cycles=2
0xb9e7:  25 0f          jrc $b9f8  (offset=15)                   cycles=1-2
0xb9e9:  72 02 00 8d 0a btjt $8d, #1, $b9f8  (offset=10)         cycles=2-3
0xb9ee:  72 0f 00 8a 05 btjf $8a, #7, $b9f8  (offset=5)          cycles=2-3
0xb9f3:  72 0c 00 96 00 btjt $96, #6, $b9f8  (offset=0)          cycles=2-3
0xb9f8:  81             ret                                      cycles=4
0xb9f9:  72 1d 00 8a    bres $8a, #6                             cycles=1
0xb9fd:  3f 0e          clr $0e                                  cycles=1
0xb9ff:  b6 2a          ld A, $2a                                cycles=1
0xba01:  b7 2b          ld $2b,A                                 cycles=1
0xba03:  b6 29          ld A, $29                                cycles=1
0xba05:  b7 2a          ld $2a,A                                 cycles=1
0xba07:  b6 08          ld A, $08                                cycles=1
0xba09:  b7 29          ld $29,A                                 cycles=1
0xba0b:  b1 2a          cp A, $2a                                cycles=1
0xba0d:  27 10          jreq $ba1f  (offset=16)                  cycles=1-2
0xba0f:  b1 2b          cp A, $2b                                cycles=1
0xba11:  27 0c          jreq $ba1f  (offset=12)                  cycles=1-2
0xba13:  b6 2a          ld A, $2a                                cycles=1
0xba15:  b1 2b          cp A, $2b                                cycles=1
0xba17:  27 06          jreq $ba1f  (offset=6)                   cycles=1-2
0xba19:  3c 0a          inc $0a                                  cycles=1
0xba1b:  3f 0f          clr $0f                                  cycles=1
0xba1d:  3f 10          clr $10                                  cycles=1
0xba1f:  81             ret                                      cycles=4
0xba20:  00 00          neg ($00,SP)                             cycles=1
0xba22:  00 00          neg ($00,SP)                             cycles=1
0xba24:  00 00          neg ($00,SP)                             cycles=1
0xba26:  00 00          neg ($00,SP)                             cycles=1
0xba28:  00 00          neg ($00,SP)                             cycles=1
0xba2a:  00 00          neg ($00,SP)                             cycles=1
0xba2c:  00 00          neg ($00,SP)                             cycles=1
0xba2e:  00 00          neg ($00,SP)                             cycles=1
0xba30:  01             rrwa X, A                                cycles=1
0xba31:  01             rrwa X, A                                cycles=1
0xba32:  01             rrwa X, A                                cycles=1
0xba33:  01             rrwa X, A                                cycles=1
0xba34:  01             rrwa X, A                                cycles=1
0xba35:  01             rrwa X, A                                cycles=1
0xba36:  01             rrwa X, A                                cycles=1
0xba37:  01             rrwa X, A                                cycles=1
0xba38:  01             rrwa X, A                                cycles=1
0xba39:  01             rrwa X, A                                cycles=1
0xba3a:  01             rrwa X, A                                cycles=1
0xba3b:  01             rrwa X, A                                cycles=1
0xba3c:  01             rrwa X, A                                cycles=1
0xba3d:  01             rrwa X, A                                cycles=1
0xba3e:  01             rrwa X, A                                cycles=1
0xba3f:  02             rlwa X, A                                cycles=1
0xba40:  02             rlwa X, A                                cycles=1
0xba41:  02             rlwa X, A                                cycles=1
0xba42:  02             rlwa X, A                                cycles=1
0xba43:  02             rlwa X, A                                cycles=1
0xba44:  02             rlwa X, A                                cycles=1
0xba45:  02             rlwa X, A                                cycles=1
0xba46:  02             rlwa X, A                                cycles=1
0xba47:  03 03          cpl ($03,SP)                             cycles=1
0xba49:  03 03          cpl ($03,SP)                             cycles=1
0xba4b:  03 04          cpl ($04,SP)                             cycles=1
0xba4d:  04 04          srl ($04,SP)                             cycles=1
0xba4f:  04 05          srl ($05,SP)                             cycles=1
0xba51:  05             ???                                      cycles=?
0xba52:  05             ???                                      cycles=?
0xba53:  06 06          rrc ($06,SP)                             cycles=1
0xba55:  06 06          rrc ($06,SP)                             cycles=1
0xba57:  07 07          sra ($07,SP)                             cycles=1
0xba59:  07 08          sra ($08,SP)                             cycles=1
0xba5b:  08 08          sll ($08,SP)                             cycles=1
0xba5d:  09 09          rlc ($09,SP)                             cycles=1
0xba5f:  09 0a          rlc ($0a,SP)                             cycles=1
0xba61:  0a 0a          dec ($0a,SP)                             cycles=1
0xba63:  0b             ???                                      cycles=?
0xba64:  0b             ???                                      cycles=?
0xba65:  0c 0c          inc ($0c,SP)                             cycles=1
0xba67:  0d 0d          tnz ($0d,SP)                             cycles=1
0xba69:  0e 0e          swap ($0e,SP)                            cycles=1
0xba6b:  0f 10          clr ($10,SP)                             cycles=1
0xba6d:  11 12          cp A, ($12,SP)                           cycles=1
0xba6f:  13 14          cpw X, ($14,SP)                          cycles=2
0xba71:  15 16          bcp A, ($16,SP)                          cycles=1
0xba73:  17 18          ldw ($18,SP),Y                           cycles=2
0xba75:  19 1a          adc A, ($1a,SP)                          cycles=1
0xba77:  1b 1c          add A, ($1c,SP)                          cycles=1
0xba79:  1d 1e 1f       subw X, #$1e1f                           cycles=2
0xba7c:  20 21          jra $ba9f  (offset=33)                   cycles=2
0xba7e:  22 23          jrugt $baa3  (offset=35)                 cycles=1-2
0xba80:  24 25          jrnc $baa7  (offset=37)                  cycles=1-2
0xba82:  26 27          jrne $baab  (offset=39)                  cycles=1-2
0xba84:  28 2a          jrnv $bab0  (offset=42)                  cycles=1-2
0xba86:  2c 2e          jrsgt $bab6  (offset=46)                 cycles=1-2
0xba88:  30 32          neg $32                                  cycles=1
0xba8a:  3c 46          inc $46                                  cycles=1
0xba8c:  50             negw X                                   cycles=1
0xba8d:  5a             decw X                                   cycles=1
0xba8e:  64 6e          srl ($6e,X)                              cycles=1
0xba90:  78             sll (X)                                  cycles=1
0xba91:  7d             tnz (X)                                  cycles=1
0xba92:  7d             tnz (X)                                  cycles=1
0xba93:  7d             tnz (X)                                  cycles=1
0xba94:  7d             tnz (X)                                  cycles=1
0xba95:  7d             tnz (X)                                  cycles=1
0xba96:  7d             tnz (X)                                  cycles=1
0xba97:  7d             tnz (X)                                  cycles=1
0xba98:  7d             tnz (X)                                  cycles=1
0xba99:  7d             tnz (X)                                  cycles=1
0xba9a:  7d             tnz (X)                                  cycles=1
0xba9b:  7d             tnz (X)                                  cycles=1
0xba9c:  7d             tnz (X)                                  cycles=1
0xba9d:  7d             tnz (X)                                  cycles=1
0xba9e:  7d             tnz (X)                                  cycles=1
0xba9f:  7d             tnz (X)                                  cycles=1
0xbaa0:  7d             tnz (X)                                  cycles=1
0xbaa1:  7d             tnz (X)                                  cycles=1
0xbaa2:  00 00          neg ($00,SP)                             cycles=1
0xbaa4:  00 00          neg ($00,SP)                             cycles=1
0xbaa6:  00 01          neg ($01,SP)                             cycles=1
0xbaa8:  02             rlwa X, A                                cycles=1
0xbaa9:  03 04          cpl ($04,SP)                             cycles=1
0xbaab:  05             ???                                      cycles=?
0xbaac:  06 06          rrc ($06,SP)                             cycles=1
0xbaae:  06 06          rrc ($06,SP)                             cycles=1
0xbab0:  06 06          rrc ($06,SP)                             cycles=1
0xbab2:  06 06          rrc ($06,SP)                             cycles=1
0xbab4:  06 06          rrc ($06,SP)                             cycles=1
0xbab6:  06 06          rrc ($06,SP)                             cycles=1
0xbab8:  06 06          rrc ($06,SP)                             cycles=1
0xbaba:  06 06          rrc ($06,SP)                             cycles=1
0xbabc:  06 06          rrc ($06,SP)                             cycles=1
0xbabe:  06 06          rrc ($06,SP)                             cycles=1
0xbac0:  06 06          rrc ($06,SP)                             cycles=1
0xbac2:  06 06          rrc ($06,SP)                             cycles=1
0xbac4:  06 06          rrc ($06,SP)                             cycles=1
0xbac6:  06 06          rrc ($06,SP)                             cycles=1
0xbac8:  06 06          rrc ($06,SP)                             cycles=1
0xbaca:  c6 53 28       ld A, $5328                              cycles=1
0xbacd:  b7 3e          ld $3e,A                                 cycles=1
0xbacf:  95             ld XH, A                                 cycles=1
0xbad0:  c6 53 29       ld A, $5329                              cycles=1
0xbad3:  b7 3f          ld $3f,A                                 cycles=1
0xbad5:  97             ld XL, A                                 cycles=1
0xbad6:  72 10 53 24    bset $5324, #0                           cycles=1
0xbada:  54             srlw X                                   cycles=2
0xbadb:  54             srlw X                                   cycles=2
0xbadc:  9e             ld A, XH                                 cycles=1
0xbadd:  b7 40          ld $40,A                                 cycles=1
0xbadf:  9f             ld A, XL                                 cycles=1
0xbae0:  b7 41          ld $41,A                                 cycles=1
0xbae2:  cd 88 bc       call $88bc                               cycles=4
0xbae5:  b6 40          ld A, $40                                cycles=1
0xbae7:  b1 a9          cp A, $a9                                cycles=1
0xbae9:  27 05          jreq $baf0  (offset=5)                   cycles=1-2
0xbaeb:  25 03          jrc $baf0  (offset=3)                    cycles=1-2
0xbaed:  3c a9          inc $a9                                  cycles=1
0xbaef:  81             ret                                      cycles=4
0xbaf0:  b7 a9          ld $a9,A                                 cycles=1
0xbaf2:  81             ret                                      cycles=4
0xbaf3:  5f             clrw X                                   cycles=1
0xbaf4:  b6 70          ld A, $70                                cycles=1
0xbaf6:  97             ld XL, A                                 cycles=1
0xbaf7:  c6 52 41       ld A, $5241                              cycles=1
0xbafa:  e7 7e          ld ($7e,X),A                             cycles=1
0xbafc:  3c 70          inc $70                                  cycles=1
0xbafe:  b6 70          ld A, $70                                cycles=1
0xbb00:  a1 07          cp A, #$07                               cycles=1
0xbb02:  24 0e          jrnc $bb12  (offset=14)                  cycles=1-2
0xbb04:  a1 01          cp A, #$01                               cycles=1
0xbb06:  26 08          jrne $bb10  (offset=8)                   cycles=1-2
0xbb08:  b6 7e          ld A, $7e                                cycles=1
0xbb0a:  a1 59          cp A, #$59                               cycles=1
0xbb0c:  27 02          jreq $bb10  (offset=2)                   cycles=1-2
0xbb0e:  3f 70          clr $70                                  cycles=1
0xbb10:  20 36          jra $bb48  (offset=54)                   cycles=2
0xbb12:  3f 70          clr $70                                  cycles=1
0xbb14:  b6 7e          ld A, $7e                                cycles=1
0xbb16:  bb 7f          add A, $7f                               cycles=1
0xbb18:  bb 80          add A, $80                               cycles=1
0xbb1a:  bb 81          add A, $81                               cycles=1
0xbb1c:  bb 82          add A, $82                               cycles=1
0xbb1e:  bb 83          add A, $83                               cycles=1
0xbb20:  b1 84          cp A, $84                                cycles=1
0xbb22:  27 02          jreq $bb26  (offset=2)                   cycles=1-2
0xbb24:  20 22          jra $bb48  (offset=34)                   cycles=2
0xbb26:  72 1b 00 8d    bres $8d, #5                             cycles=1
0xbb2a:  3f 71          clr $71                                  cycles=1
0xbb2c:  b6 7f          ld A, $7f                                cycles=1
0xbb2e:  b7 85          ld $85,A                                 cycles=1
0xbb30:  b6 82          ld A, $82                                cycles=1
0xbb32:  b7 87          ld $87,A                                 cycles=1
0xbb34:  b6 81          ld A, $81                                cycles=1
0xbb36:  27 10          jreq $bb48  (offset=16)                  cycles=1-2
0xbb38:  b7 f1          ld $f1,A                                 cycles=1
0xbb3a:  b6 83          ld A, $83                                cycles=1
0xbb3c:  a1 0e          cp A, #$0e                               cycles=1
0xbb3e:  25 04          jrc $bb44  (offset=4)                    cycles=1-2
0xbb40:  b7 f2          ld $f2,A                                 cycles=1
0xbb42:  20 04          jra $bb48  (offset=4)                    cycles=2
0xbb44:  a6 19          ld A, #$19                               cycles=1
0xbb46:  b7 f2          ld $f2,A                                 cycles=1
0xbb48:  80             iret                                     cycles=11
0xbb49:  3f de          clr $de                                  cycles=1
0xbb4b:  72 15 52 5c    bres $525c, #2                           cycles=1
0xbb4f:  72 1d 52 5c    bres $525c, #6                           cycles=1
0xbb53:  72 15 52 5d    bres $525d, #2                           cycles=1
0xbb57:  72 1a 52 57    bset $5257, #5                           cycles=1
0xbb5b:  72 5f 52 65    clr $5265                                cycles=1
0xbb5f:  72 5f 52 67    clr $5267                                cycles=1
0xbb63:  72 5f 52 69    clr $5269                                cycles=1
0xbb67:  72 5f 52 6b    clr $526b                                cycles=1
0xbb6b:  3f 24          clr $24                                  cycles=1
0xbb6d:  72 5f 52 66    clr $5266                                cycles=1
0xbb71:  72 5f 52 68    clr $5268                                cycles=1
0xbb75:  72 5f 52 6a    clr $526a                                cycles=1
0xbb79:  72 5f 52 6c    clr $526c                                cycles=1
0xbb7d:  3f 25          clr $25                                  cycles=1
0xbb7f:  72 10 52 57    bset $5257, #0                           cycles=1
0xbb83:  72 11 52 5c    bres $525c, #0                           cycles=1
0xbb87:  72 19 52 5c    bres $525c, #4                           cycles=1
0xbb8b:  72 11 52 5d    bres $525d, #0                           cycles=1
0xbb8f:  72 15 52 5c    bres $525c, #2                           cycles=1
0xbb93:  72 1d 52 5c    bres $525c, #6                           cycles=1
0xbb97:  72 15 52 5d    bres $525d, #2                           cycles=1
0xbb9b:  72 1a 52 57    bset $5257, #5                           cycles=1
0xbb9f:  72 11 50 05    bres $5005, #0                           cycles=1
0xbba3:  72 13 50 05    bres $5005, #1                           cycles=1
0xbba7:  72 15 50 05    bres $5005, #2                           cycles=1
0xbbab:  72 1d 00 98    bres $98, #6                             cycles=1
0xbbaf:  72 1f 00 98    bres $98, #7                             cycles=1
0xbbb3:  72 1d 00 96    bres $96, #6                             cycles=1
0xbbb7:  72 1b 52 50    bres $5250, #5                           cycles=1
0xbbbb:  72 17 52 53    bres $5253, #3                           cycles=1
0xbbbf:  72 15 52 53    bres $5253, #2                           cycles=1
0xbbc3:  72 13 52 53    bres $5253, #1                           cycles=1
0xbbc7:  3f c4          clr $c4                                  cycles=1
0xbbc9:  3f c7          clr $c7                                  cycles=1
0xbbcb:  72 1f 00 8a    bres $8a, #7                             cycles=1
0xbbcf:  72 13 53 21    bres $5321, #1                           cycles=1
0xbbd3:  72 15 53 21    bres $5321, #2                           cycles=1
0xbbd7:  72 15 00 8b    bres $8b, #2                             cycles=1
0xbbdb:  72 13 00 89    bres $89, #1                             cycles=1
0xbbdf:  3f 02          clr $02                                  cycles=1
0xbbe1:  3f 03          clr $03                                  cycles=1
0xbbe3:  3f c2          clr $c2                                  cycles=1
0xbbe5:  3f c0          clr $c0                                  cycles=1
0xbbe7:  a6 96          ld A, #$96                               cycles=1
0xbbe9:  b7 16          ld $16,A                                 cycles=1
0xbbeb:  3f a8          clr $a8                                  cycles=1
0xbbed:  3f ab          clr $ab                                  cycles=1
0xbbef:  3f ac          clr $ac                                  cycles=1
0xbbf1:  b7 9b          ld $9b,A                                 cycles=1
0xbbf3:  3f ef          clr $ef                                  cycles=1
0xbbf5:  3f f0          clr $f0                                  cycles=1
0xbbf7:  72 1b 00 8a    bres $8a, #5                             cycles=1
0xbbfb:  72 1f 00 8c    bres $8c, #7                             cycles=1
0xbbff:  80             iret                                     cycles=11
0xbc00:  80             iret                                     cycles=11
0xbc01:  00 00          neg ($00,SP)                             cycles=1
0xbc03:  00 00          neg ($00,SP)                             cycles=1
0xbc05:  00 00          neg ($00,SP)                             cycles=1
0xbc07:  00 00          neg ($00,SP)                             cycles=1
0xbc09:  00 00          neg ($00,SP)                             cycles=1
0xbc0b:  00 00          neg ($00,SP)                             cycles=1
0xbc0d:  00 00          neg ($00,SP)                             cycles=1
0xbc0f:  00 00          neg ($00,SP)                             cycles=1
0xbc11:  00 00          neg ($00,SP)                             cycles=1
0xbc13:  00 00          neg ($00,SP)                             cycles=1
0xbc15:  00 00          neg ($00,SP)                             cycles=1
0xbc17:  00 00          neg ($00,SP)                             cycles=1
0xbc19:  00 00          neg ($00,SP)                             cycles=1
0xbc1b:  00 00          neg ($00,SP)                             cycles=1
0xbc1d:  00 00          neg ($00,SP)                             cycles=1
0xbc1f:  00 00          neg ($00,SP)                             cycles=1
0xbc21:  00 00          neg ($00,SP)                             cycles=1
0xbc23:  00 00          neg ($00,SP)                             cycles=1
0xbc25:  00 00          neg ($00,SP)                             cycles=1
0xbc27:  00 00          neg ($00,SP)                             cycles=1
0xbc29:  00 00          neg ($00,SP)                             cycles=1
0xbc2b:  00 00          neg ($00,SP)                             cycles=1
0xbc2d:  00 00          neg ($00,SP)                             cycles=1
0xbc2f:  00 00          neg ($00,SP)                             cycles=1
0xbc31:  00 00          neg ($00,SP)                             cycles=1
0xbc33:  00 00          neg ($00,SP)                             cycles=1
0xbc35:  00 00          neg ($00,SP)                             cycles=1
0xbc37:  00 00          neg ($00,SP)                             cycles=1
0xbc39:  00 00          neg ($00,SP)                             cycles=1
0xbc3b:  00 00          neg ($00,SP)                             cycles=1
0xbc3d:  00 00          neg ($00,SP)                             cycles=1
0xbc3f:  00 00          neg ($00,SP)                             cycles=1
0xbc41:  00 00          neg ($00,SP)                             cycles=1
0xbc43:  00 00          neg ($00,SP)                             cycles=1
0xbc45:  00 00          neg ($00,SP)                             cycles=1
0xbc47:  00 00          neg ($00,SP)                             cycles=1
0xbc49:  00 00          neg ($00,SP)                             cycles=1
0xbc4b:  00 00          neg ($00,SP)                             cycles=1
0xbc4d:  00 00          neg ($00,SP)                             cycles=1
0xbc4f:  00 00          neg ($00,SP)                             cycles=1
0xbc51:  00 00          neg ($00,SP)                             cycles=1
0xbc53:  00 00          neg ($00,SP)                             cycles=1
0xbc55:  00 00          neg ($00,SP)                             cycles=1
0xbc57:  00 00          neg ($00,SP)                             cycles=1
0xbc59:  00 00          neg ($00,SP)                             cycles=1
0xbc5b:  00 00          neg ($00,SP)                             cycles=1
0xbc5d:  00 00          neg ($00,SP)                             cycles=1
0xbc5f:  00 00          neg ($00,SP)                             cycles=1
0xbc61:  00 00          neg ($00,SP)                             cycles=1
0xbc63:  00 00          neg ($00,SP)                             cycles=1
0xbc65:  00 00          neg ($00,SP)                             cycles=1
0xbc67:  00 00          neg ($00,SP)                             cycles=1
0xbc69:  00 00          neg ($00,SP)                             cycles=1
0xbc6b:  00 00          neg ($00,SP)                             cycles=1
0xbc6d:  00 00          neg ($00,SP)                             cycles=1
0xbc6f:  00 00          neg ($00,SP)                             cycles=1
0xbc71:  00 00          neg ($00,SP)                             cycles=1
0xbc73:  00 00          neg ($00,SP)                             cycles=1
0xbc75:  00 00          neg ($00,SP)                             cycles=1
0xbc77:  00 00          neg ($00,SP)                             cycles=1
0xbc79:  00 00          neg ($00,SP)                             cycles=1
0xbc7b:  00 00          neg ($00,SP)                             cycles=1
0xbc7d:  00 00          neg ($00,SP)                             cycles=1
0xbc7f:  00 00          neg ($00,SP)                             cycles=1
0xbc81:  00 00          neg ($00,SP)                             cycles=1
0xbc83:  00 00          neg ($00,SP)                             cycles=1
0xbc85:  00 00          neg ($00,SP)                             cycles=1
0xbc87:  00 00          neg ($00,SP)                             cycles=1
0xbc89:  00 00          neg ($00,SP)                             cycles=1
0xbc8b:  00 00          neg ($00,SP)                             cycles=1
0xbc8d:  00 00          neg ($00,SP)                             cycles=1
0xbc8f:  00 00          neg ($00,SP)                             cycles=1
0xbc91:  00 00          neg ($00,SP)                             cycles=1
0xbc93:  00 00          neg ($00,SP)                             cycles=1
0xbc95:  00 00          neg ($00,SP)                             cycles=1
0xbc97:  00 00          neg ($00,SP)                             cycles=1
0xbc99:  00 00          neg ($00,SP)                             cycles=1
0xbc9b:  00 00          neg ($00,SP)                             cycles=1
0xbc9d:  00 00          neg ($00,SP)                             cycles=1
0xbc9f:  00 00          neg ($00,SP)                             cycles=1
0xbca1:  00 00          neg ($00,SP)                             cycles=1
0xbca3:  00 00          neg ($00,SP)                             cycles=1
0xbca5:  00 00          neg ($00,SP)                             cycles=1
0xbca7:  00 00          neg ($00,SP)                             cycles=1
0xbca9:  00 00          neg ($00,SP)                             cycles=1
0xbcab:  00 00          neg ($00,SP)                             cycles=1
0xbcad:  00 00          neg ($00,SP)                             cycles=1
0xbcaf:  00 00          neg ($00,SP)                             cycles=1
0xbcb1:  00 00          neg ($00,SP)                             cycles=1
0xbcb3:  00 00          neg ($00,SP)                             cycles=1
0xbcb5:  00 00          neg ($00,SP)                             cycles=1
0xbcb7:  00 00          neg ($00,SP)                             cycles=1
0xbcb9:  00 00          neg ($00,SP)                             cycles=1
0xbcbb:  00 00          neg ($00,SP)                             cycles=1
0xbcbd:  00 00          neg ($00,SP)                             cycles=1
0xbcbf:  00 00          neg ($00,SP)                             cycles=1
0xbcc1:  00 00          neg ($00,SP)                             cycles=1
0xbcc3:  00 00          neg ($00,SP)                             cycles=1
0xbcc5:  00 00          neg ($00,SP)                             cycles=1
0xbcc7:  00 00          neg ($00,SP)                             cycles=1
0xbcc9:  00 00          neg ($00,SP)                             cycles=1
0xbccb:  00 00          neg ($00,SP)                             cycles=1
0xbccd:  00 00          neg ($00,SP)                             cycles=1
0xbccf:  00 00          neg ($00,SP)                             cycles=1
0xbcd1:  00 00          neg ($00,SP)                             cycles=1
0xbcd3:  00 00          neg ($00,SP)                             cycles=1
0xbcd5:  00 00          neg ($00,SP)                             cycles=1
0xbcd7:  00 00          neg ($00,SP)                             cycles=1
0xbcd9:  00 00          neg ($00,SP)                             cycles=1
0xbcdb:  00 00          neg ($00,SP)                             cycles=1
0xbcdd:  00 00          neg ($00,SP)                             cycles=1
0xbcdf:  00 00          neg ($00,SP)                             cycles=1
0xbce1:  00 00          neg ($00,SP)                             cycles=1
0xbce3:  00 00          neg ($00,SP)                             cycles=1
0xbce5:  00 00          neg ($00,SP)                             cycles=1
0xbce7:  00 00          neg ($00,SP)                             cycles=1
0xbce9:  00 00          neg ($00,SP)                             cycles=1
0xbceb:  00 00          neg ($00,SP)                             cycles=1
0xbced:  00 00          neg ($00,SP)                             cycles=1
0xbcef:  00 00          neg ($00,SP)                             cycles=1
0xbcf1:  00 00          neg ($00,SP)                             cycles=1
0xbcf3:  00 00          neg ($00,SP)                             cycles=1
0xbcf5:  00 00          neg ($00,SP)                             cycles=1
0xbcf7:  00 00          neg ($00,SP)                             cycles=1
0xbcf9:  00 00          neg ($00,SP)                             cycles=1
0xbcfb:  00 00          neg ($00,SP)                             cycles=1
0xbcfd:  00 00          neg ($00,SP)                             cycles=1
0xbcff:  00 00          neg ($00,SP)                             cycles=1
0xbd01:  00 00          neg ($00,SP)                             cycles=1
0xbd03:  00 00          neg ($00,SP)                             cycles=1
0xbd05:  00 00          neg ($00,SP)                             cycles=1
0xbd07:  00 00          neg ($00,SP)                             cycles=1
0xbd09:  00 00          neg ($00,SP)                             cycles=1
0xbd0b:  00 00          neg ($00,SP)                             cycles=1
0xbd0d:  00 00          neg ($00,SP)                             cycles=1
0xbd0f:  00 00          neg ($00,SP)                             cycles=1
0xbd11:  00 00          neg ($00,SP)                             cycles=1
0xbd13:  00 00          neg ($00,SP)                             cycles=1
0xbd15:  00 00          neg ($00,SP)                             cycles=1
0xbd17:  00 00          neg ($00,SP)                             cycles=1
0xbd19:  00 00          neg ($00,SP)                             cycles=1
0xbd1b:  00 00          neg ($00,SP)                             cycles=1
0xbd1d:  00 00          neg ($00,SP)                             cycles=1
0xbd1f:  00 00          neg ($00,SP)                             cycles=1
0xbd21:  00 00          neg ($00,SP)                             cycles=1
0xbd23:  00 00          neg ($00,SP)                             cycles=1
0xbd25:  00 00          neg ($00,SP)                             cycles=1
0xbd27:  00 00          neg ($00,SP)                             cycles=1
0xbd29:  00 00          neg ($00,SP)                             cycles=1
0xbd2b:  00 00          neg ($00,SP)                             cycles=1
0xbd2d:  00 00          neg ($00,SP)                             cycles=1
0xbd2f:  00 00          neg ($00,SP)                             cycles=1
0xbd31:  00 00          neg ($00,SP)                             cycles=1
0xbd33:  00 00          neg ($00,SP)                             cycles=1
0xbd35:  00 00          neg ($00,SP)                             cycles=1
0xbd37:  00 00          neg ($00,SP)                             cycles=1
0xbd39:  00 00          neg ($00,SP)                             cycles=1
0xbd3b:  00 00          neg ($00,SP)                             cycles=1
0xbd3d:  00 00          neg ($00,SP)                             cycles=1
0xbd3f:  00 00          neg ($00,SP)                             cycles=1
0xbd41:  00 00          neg ($00,SP)                             cycles=1
0xbd43:  00 00          neg ($00,SP)                             cycles=1
0xbd45:  00 00          neg ($00,SP)                             cycles=1
0xbd47:  00 00          neg ($00,SP)                             cycles=1
0xbd49:  00 00          neg ($00,SP)                             cycles=1
0xbd4b:  00 00          neg ($00,SP)                             cycles=1
0xbd4d:  00 00          neg ($00,SP)                             cycles=1
0xbd4f:  00 00          neg ($00,SP)                             cycles=1
0xbd51:  00 00          neg ($00,SP)                             cycles=1
0xbd53:  00 00          neg ($00,SP)                             cycles=1
0xbd55:  00 00          neg ($00,SP)                             cycles=1
0xbd57:  00 00          neg ($00,SP)                             cycles=1
0xbd59:  00 00          neg ($00,SP)                             cycles=1
0xbd5b:  00 00          neg ($00,SP)                             cycles=1
0xbd5d:  00 00          neg ($00,SP)                             cycles=1
0xbd5f:  00 00          neg ($00,SP)                             cycles=1
0xbd61:  00 00          neg ($00,SP)                             cycles=1
0xbd63:  00 00          neg ($00,SP)                             cycles=1
0xbd65:  00 00          neg ($00,SP)                             cycles=1
0xbd67:  00 00          neg ($00,SP)                             cycles=1
0xbd69:  00 00          neg ($00,SP)                             cycles=1
0xbd6b:  00 00          neg ($00,SP)                             cycles=1
0xbd6d:  00 00          neg ($00,SP)                             cycles=1
0xbd6f:  00 00          neg ($00,SP)                             cycles=1
0xbd71:  00 00          neg ($00,SP)                             cycles=1
0xbd73:  00 00          neg ($00,SP)                             cycles=1
0xbd75:  00 00          neg ($00,SP)                             cycles=1
0xbd77:  00 00          neg ($00,SP)                             cycles=1
0xbd79:  00 00          neg ($00,SP)                             cycles=1
0xbd7b:  00 00          neg ($00,SP)                             cycles=1
0xbd7d:  00 00          neg ($00,SP)                             cycles=1
0xbd7f:  00 00          neg ($00,SP)                             cycles=1
0xbd81:  00 00          neg ($00,SP)                             cycles=1
0xbd83:  00 00          neg ($00,SP)                             cycles=1
0xbd85:  00 00          neg ($00,SP)                             cycles=1
0xbd87:  00 00          neg ($00,SP)                             cycles=1
0xbd89:  00 00          neg ($00,SP)                             cycles=1
0xbd8b:  00 00          neg ($00,SP)                             cycles=1
0xbd8d:  00 00          neg ($00,SP)                             cycles=1
0xbd8f:  00 00          neg ($00,SP)                             cycles=1
0xbd91:  00 00          neg ($00,SP)                             cycles=1
0xbd93:  00 00          neg ($00,SP)                             cycles=1
0xbd95:  00 00          neg ($00,SP)                             cycles=1
0xbd97:  00 00          neg ($00,SP)                             cycles=1
0xbd99:  00 00          neg ($00,SP)                             cycles=1
0xbd9b:  00 00          neg ($00,SP)                             cycles=1
0xbd9d:  00 00          neg ($00,SP)                             cycles=1
0xbd9f:  00 00          neg ($00,SP)                             cycles=1
0xbda1:  00 00          neg ($00,SP)                             cycles=1
0xbda3:  00 00          neg ($00,SP)                             cycles=1
0xbda5:  00 00          neg ($00,SP)                             cycles=1
0xbda7:  00 00          neg ($00,SP)                             cycles=1
0xbda9:  00 00          neg ($00,SP)                             cycles=1
0xbdab:  00 00          neg ($00,SP)                             cycles=1
0xbdad:  00 00          neg ($00,SP)                             cycles=1
0xbdaf:  00 00          neg ($00,SP)                             cycles=1
0xbdb1:  00 00          neg ($00,SP)                             cycles=1
0xbdb3:  00 00          neg ($00,SP)                             cycles=1
0xbdb5:  00 00          neg ($00,SP)                             cycles=1
0xbdb7:  00 00          neg ($00,SP)                             cycles=1
0xbdb9:  00 00          neg ($00,SP)                             cycles=1
0xbdbb:  00 00          neg ($00,SP)                             cycles=1
0xbdbd:  00 00          neg ($00,SP)                             cycles=1
0xbdbf:  00 00          neg ($00,SP)                             cycles=1
0xbdc1:  00 00          neg ($00,SP)                             cycles=1
0xbdc3:  00 00          neg ($00,SP)                             cycles=1
0xbdc5:  00 00          neg ($00,SP)                             cycles=1
0xbdc7:  00 00          neg ($00,SP)                             cycles=1
0xbdc9:  00 00          neg ($00,SP)                             cycles=1
0xbdcb:  00 00          neg ($00,SP)                             cycles=1
0xbdcd:  00 00          neg ($00,SP)                             cycles=1
0xbdcf:  00 00          neg ($00,SP)                             cycles=1
0xbdd1:  00 00          neg ($00,SP)                             cycles=1
0xbdd3:  00 00          neg ($00,SP)                             cycles=1
0xbdd5:  00 00          neg ($00,SP)                             cycles=1
0xbdd7:  00 00          neg ($00,SP)                             cycles=1
0xbdd9:  00 00          neg ($00,SP)                             cycles=1
0xbddb:  00 00          neg ($00,SP)                             cycles=1
0xbddd:  00 00          neg ($00,SP)                             cycles=1
0xbddf:  00 00          neg ($00,SP)                             cycles=1
0xbde1:  00 00          neg ($00,SP)                             cycles=1
0xbde3:  00 00          neg ($00,SP)                             cycles=1
0xbde5:  00 00          neg ($00,SP)                             cycles=1
0xbde7:  00 00          neg ($00,SP)                             cycles=1
0xbde9:  00 00          neg ($00,SP)                             cycles=1
0xbdeb:  00 00          neg ($00,SP)                             cycles=1
0xbded:  00 00          neg ($00,SP)                             cycles=1
0xbdef:  00 00          neg ($00,SP)                             cycles=1
0xbdf1:  00 00          neg ($00,SP)                             cycles=1
0xbdf3:  00 00          neg ($00,SP)                             cycles=1
0xbdf5:  00 00          neg ($00,SP)                             cycles=1
0xbdf7:  00 00          neg ($00,SP)                             cycles=1
0xbdf9:  00 00          neg ($00,SP)                             cycles=1
0xbdfb:  00 00          neg ($00,SP)                             cycles=1
0xbdfd:  00 00          neg ($00,SP)                             cycles=1
0xbdff:  00 00          neg ($00,SP)                             cycles=1
0xbe01:  00 00          neg ($00,SP)                             cycles=1
0xbe03:  00 00          neg ($00,SP)                             cycles=1
0xbe05:  00 00          neg ($00,SP)                             cycles=1
0xbe07:  00 00          neg ($00,SP)                             cycles=1
0xbe09:  00 00          neg ($00,SP)                             cycles=1
0xbe0b:  00 00          neg ($00,SP)                             cycles=1
0xbe0d:  00 00          neg ($00,SP)                             cycles=1
0xbe0f:  00 00          neg ($00,SP)                             cycles=1
0xbe11:  00 00          neg ($00,SP)                             cycles=1
0xbe13:  00 00          neg ($00,SP)                             cycles=1
0xbe15:  00 00          neg ($00,SP)                             cycles=1
0xbe17:  00 00          neg ($00,SP)                             cycles=1
0xbe19:  00 00          neg ($00,SP)                             cycles=1
0xbe1b:  00 00          neg ($00,SP)                             cycles=1
0xbe1d:  00 00          neg ($00,SP)                             cycles=1
0xbe1f:  00 00          neg ($00,SP)                             cycles=1
0xbe21:  00 00          neg ($00,SP)                             cycles=1
0xbe23:  00 00          neg ($00,SP)                             cycles=1
0xbe25:  00 00          neg ($00,SP)                             cycles=1
0xbe27:  00 00          neg ($00,SP)                             cycles=1
0xbe29:  00 00          neg ($00,SP)                             cycles=1
0xbe2b:  00 00          neg ($00,SP)                             cycles=1
0xbe2d:  00 00          neg ($00,SP)                             cycles=1
0xbe2f:  00 00          neg ($00,SP)                             cycles=1
0xbe31:  00 00          neg ($00,SP)                             cycles=1
0xbe33:  00 00          neg ($00,SP)                             cycles=1
0xbe35:  00 00          neg ($00,SP)                             cycles=1
0xbe37:  00 00          neg ($00,SP)                             cycles=1
0xbe39:  00 00          neg ($00,SP)                             cycles=1
0xbe3b:  00 00          neg ($00,SP)                             cycles=1
0xbe3d:  00 00          neg ($00,SP)                             cycles=1
0xbe3f:  00 00          neg ($00,SP)                             cycles=1
0xbe41:  00 00          neg ($00,SP)                             cycles=1
0xbe43:  00 00          neg ($00,SP)                             cycles=1
0xbe45:  00 00          neg ($00,SP)                             cycles=1
0xbe47:  00 00          neg ($00,SP)                             cycles=1
0xbe49:  00 00          neg ($00,SP)                             cycles=1
0xbe4b:  00 00          neg ($00,SP)                             cycles=1
0xbe4d:  00 00          neg ($00,SP)                             cycles=1
0xbe4f:  00 00          neg ($00,SP)                             cycles=1
0xbe51:  00 00          neg ($00,SP)                             cycles=1
0xbe53:  00 00          neg ($00,SP)                             cycles=1
0xbe55:  00 00          neg ($00,SP)                             cycles=1
0xbe57:  00 00          neg ($00,SP)                             cycles=1
0xbe59:  00 00          neg ($00,SP)                             cycles=1
0xbe5b:  00 00          neg ($00,SP)                             cycles=1
0xbe5d:  00 00          neg ($00,SP)                             cycles=1
0xbe5f:  00 00          neg ($00,SP)                             cycles=1
0xbe61:  00 00          neg ($00,SP)                             cycles=1
0xbe63:  00 00          neg ($00,SP)                             cycles=1
0xbe65:  00 00          neg ($00,SP)                             cycles=1
0xbe67:  00 00          neg ($00,SP)                             cycles=1
0xbe69:  00 00          neg ($00,SP)                             cycles=1
0xbe6b:  00 00          neg ($00,SP)                             cycles=1
0xbe6d:  00 00          neg ($00,SP)                             cycles=1
0xbe6f:  00 00          neg ($00,SP)                             cycles=1
0xbe71:  00 00          neg ($00,SP)                             cycles=1
0xbe73:  00 00          neg ($00,SP)                             cycles=1
0xbe75:  00 00          neg ($00,SP)                             cycles=1
0xbe77:  00 00          neg ($00,SP)                             cycles=1
0xbe79:  00 00          neg ($00,SP)                             cycles=1
0xbe7b:  00 00          neg ($00,SP)                             cycles=1
0xbe7d:  00 00          neg ($00,SP)                             cycles=1
0xbe7f:  00 00          neg ($00,SP)                             cycles=1
0xbe81:  00 00          neg ($00,SP)                             cycles=1
0xbe83:  00 00          neg ($00,SP)                             cycles=1
0xbe85:  00 00          neg ($00,SP)                             cycles=1
0xbe87:  00 00          neg ($00,SP)                             cycles=1
0xbe89:  00 00          neg ($00,SP)                             cycles=1
0xbe8b:  00 00          neg ($00,SP)                             cycles=1
0xbe8d:  00 00          neg ($00,SP)                             cycles=1
0xbe8f:  00 00          neg ($00,SP)                             cycles=1
0xbe91:  00 00          neg ($00,SP)                             cycles=1
0xbe93:  00 00          neg ($00,SP)                             cycles=1
0xbe95:  00 00          neg ($00,SP)                             cycles=1
0xbe97:  00 00          neg ($00,SP)                             cycles=1
0xbe99:  00 00          neg ($00,SP)                             cycles=1
0xbe9b:  00 00          neg ($00,SP)                             cycles=1
0xbe9d:  00 00          neg ($00,SP)                             cycles=1
0xbe9f:  00 00          neg ($00,SP)                             cycles=1
0xbea1:  00 00          neg ($00,SP)                             cycles=1
0xbea3:  00 00          neg ($00,SP)                             cycles=1
0xbea5:  00 00          neg ($00,SP)                             cycles=1
0xbea7:  00 00          neg ($00,SP)                             cycles=1
0xbea9:  00 00          neg ($00,SP)                             cycles=1
0xbeab:  00 00          neg ($00,SP)                             cycles=1
0xbead:  00 00          neg ($00,SP)                             cycles=1
0xbeaf:  00 00          neg ($00,SP)                             cycles=1
0xbeb1:  00 00          neg ($00,SP)                             cycles=1
0xbeb3:  00 00          neg ($00,SP)                             cycles=1
0xbeb5:  00 00          neg ($00,SP)                             cycles=1
0xbeb7:  00 00          neg ($00,SP)                             cycles=1
0xbeb9:  00 00          neg ($00,SP)                             cycles=1
0xbebb:  00 00          neg ($00,SP)                             cycles=1
0xbebd:  00 00          neg ($00,SP)                             cycles=1
0xbebf:  00 00          neg ($00,SP)                             cycles=1
0xbec1:  00 00          neg ($00,SP)                             cycles=1
0xbec3:  00 00          neg ($00,SP)                             cycles=1
0xbec5:  00 00          neg ($00,SP)                             cycles=1
0xbec7:  00 00          neg ($00,SP)                             cycles=1
0xbec9:  00 00          neg ($00,SP)                             cycles=1
0xbecb:  00 00          neg ($00,SP)                             cycles=1
0xbecd:  00 00          neg ($00,SP)                             cycles=1
0xbecf:  00 00          neg ($00,SP)                             cycles=1
0xbed1:  00 00          neg ($00,SP)                             cycles=1
0xbed3:  00 00          neg ($00,SP)                             cycles=1
0xbed5:  00 00          neg ($00,SP)                             cycles=1
0xbed7:  00 00          neg ($00,SP)                             cycles=1
0xbed9:  00 00          neg ($00,SP)                             cycles=1
0xbedb:  00 00          neg ($00,SP)                             cycles=1
0xbedd:  00 00          neg ($00,SP)                             cycles=1
0xbedf:  00 00          neg ($00,SP)                             cycles=1
0xbee1:  00 00          neg ($00,SP)                             cycles=1
0xbee3:  00 00          neg ($00,SP)                             cycles=1
0xbee5:  00 00          neg ($00,SP)                             cycles=1
0xbee7:  00 00          neg ($00,SP)                             cycles=1
0xbee9:  00 00          neg ($00,SP)                             cycles=1
0xbeeb:  00 00          neg ($00,SP)                             cycles=1
0xbeed:  00 00          neg ($00,SP)                             cycles=1
0xbeef:  00 00          neg ($00,SP)                             cycles=1
0xbef1:  00 00          neg ($00,SP)                             cycles=1
0xbef3:  00 00          neg ($00,SP)                             cycles=1
0xbef5:  00 00          neg ($00,SP)                             cycles=1
0xbef7:  00 00          neg ($00,SP)                             cycles=1
0xbef9:  00 00          neg ($00,SP)                             cycles=1
0xbefb:  00 00          neg ($00,SP)                             cycles=1
0xbefd:  00 00          neg ($00,SP)                             cycles=1
0xbeff:  00 00          neg ($00,SP)                             cycles=1
0xbf01:  00 00          neg ($00,SP)                             cycles=1
0xbf03:  00 00          neg ($00,SP)                             cycles=1
0xbf05:  00 00          neg ($00,SP)                             cycles=1
0xbf07:  00 00          neg ($00,SP)                             cycles=1
0xbf09:  00 00          neg ($00,SP)                             cycles=1
0xbf0b:  00 00          neg ($00,SP)                             cycles=1
0xbf0d:  00 00          neg ($00,SP)                             cycles=1
0xbf0f:  00 00          neg ($00,SP)                             cycles=1
0xbf11:  00 00          neg ($00,SP)                             cycles=1
0xbf13:  00 00          neg ($00,SP)                             cycles=1
0xbf15:  00 00          neg ($00,SP)                             cycles=1
0xbf17:  00 00          neg ($00,SP)                             cycles=1
0xbf19:  00 00          neg ($00,SP)                             cycles=1
0xbf1b:  00 00          neg ($00,SP)                             cycles=1
0xbf1d:  00 00          neg ($00,SP)                             cycles=1
0xbf1f:  00 00          neg ($00,SP)                             cycles=1
0xbf21:  00 00          neg ($00,SP)                             cycles=1
0xbf23:  00 00          neg ($00,SP)                             cycles=1
0xbf25:  00 00          neg ($00,SP)                             cycles=1
0xbf27:  00 00          neg ($00,SP)                             cycles=1
0xbf29:  00 00          neg ($00,SP)                             cycles=1
0xbf2b:  00 00          neg ($00,SP)                             cycles=1
0xbf2d:  00 00          neg ($00,SP)                             cycles=1
0xbf2f:  00 00          neg ($00,SP)                             cycles=1
0xbf31:  00 00          neg ($00,SP)                             cycles=1
0xbf33:  00 00          neg ($00,SP)                             cycles=1
0xbf35:  00 00          neg ($00,SP)                             cycles=1
0xbf37:  00 00          neg ($00,SP)                             cycles=1
0xbf39:  00 00          neg ($00,SP)                             cycles=1
0xbf3b:  00 00          neg ($00,SP)                             cycles=1
0xbf3d:  00 00          neg ($00,SP)                             cycles=1
0xbf3f:  00 00          neg ($00,SP)                             cycles=1
0xbf41:  00 00          neg ($00,SP)                             cycles=1
0xbf43:  00 00          neg ($00,SP)                             cycles=1
0xbf45:  00 00          neg ($00,SP)                             cycles=1
0xbf47:  00 00          neg ($00,SP)                             cycles=1
0xbf49:  00 00          neg ($00,SP)                             cycles=1
0xbf4b:  00 00          neg ($00,SP)                             cycles=1
0xbf4d:  00 00          neg ($00,SP)                             cycles=1
0xbf4f:  00 00          neg ($00,SP)                             cycles=1
0xbf51:  00 00          neg ($00,SP)                             cycles=1
0xbf53:  00 00          neg ($00,SP)                             cycles=1
0xbf55:  00 00          neg ($00,SP)                             cycles=1
0xbf57:  00 00          neg ($00,SP)                             cycles=1
0xbf59:  00 00          neg ($00,SP)                             cycles=1
0xbf5b:  00 00          neg ($00,SP)                             cycles=1
0xbf5d:  00 00          neg ($00,SP)                             cycles=1
0xbf5f:  00 00          neg ($00,SP)                             cycles=1
0xbf61:  00 00          neg ($00,SP)                             cycles=1
0xbf63:  00 00          neg ($00,SP)                             cycles=1
0xbf65:  00 00          neg ($00,SP)                             cycles=1
0xbf67:  00 00          neg ($00,SP)                             cycles=1
0xbf69:  00 00          neg ($00,SP)                             cycles=1
0xbf6b:  00 00          neg ($00,SP)                             cycles=1
0xbf6d:  00 00          neg ($00,SP)                             cycles=1
0xbf6f:  00 00          neg ($00,SP)                             cycles=1
0xbf71:  00 00          neg ($00,SP)                             cycles=1
0xbf73:  00 00          neg ($00,SP)                             cycles=1
0xbf75:  00 00          neg ($00,SP)                             cycles=1
0xbf77:  00 00          neg ($00,SP)                             cycles=1
0xbf79:  00 00          neg ($00,SP)                             cycles=1
0xbf7b:  00 00          neg ($00,SP)                             cycles=1
0xbf7d:  00 00          neg ($00,SP)                             cycles=1
0xbf7f:  00 00          neg ($00,SP)                             cycles=1
0xbf81:  00 00          neg ($00,SP)                             cycles=1
0xbf83:  00 00          neg ($00,SP)                             cycles=1
0xbf85:  00 00          neg ($00,SP)                             cycles=1
0xbf87:  00 00          neg ($00,SP)                             cycles=1
0xbf89:  00 00          neg ($00,SP)                             cycles=1
0xbf8b:  00 00          neg ($00,SP)                             cycles=1
0xbf8d:  00 00          neg ($00,SP)                             cycles=1
0xbf8f:  00 00          neg ($00,SP)                             cycles=1
0xbf91:  00 00          neg ($00,SP)                             cycles=1
0xbf93:  00 00          neg ($00,SP)                             cycles=1
0xbf95:  00 00          neg ($00,SP)                             cycles=1
0xbf97:  00 00          neg ($00,SP)                             cycles=1
0xbf99:  00 00          neg ($00,SP)                             cycles=1
0xbf9b:  00 00          neg ($00,SP)                             cycles=1
0xbf9d:  00 00          neg ($00,SP)                             cycles=1
0xbf9f:  00 00          neg ($00,SP)                             cycles=1
0xbfa1:  00 00          neg ($00,SP)                             cycles=1
0xbfa3:  00 00          neg ($00,SP)                             cycles=1
0xbfa5:  00 00          neg ($00,SP)                             cycles=1
0xbfa7:  00 00          neg ($00,SP)                             cycles=1
0xbfa9:  00 00          neg ($00,SP)                             cycles=1
0xbfab:  00 00          neg ($00,SP)                             cycles=1
0xbfad:  00 00          neg ($00,SP)                             cycles=1
0xbfaf:  00 00          neg ($00,SP)                             cycles=1
0xbfb1:  00 00          neg ($00,SP)                             cycles=1
0xbfb3:  00 00          neg ($00,SP)                             cycles=1
0xbfb5:  00 00          neg ($00,SP)                             cycles=1
0xbfb7:  00 00          neg ($00,SP)                             cycles=1
0xbfb9:  00 00          neg ($00,SP)                             cycles=1
0xbfbb:  00 00          neg ($00,SP)                             cycles=1
0xbfbd:  00 00          neg ($00,SP)                             cycles=1
0xbfbf:  00 00          neg ($00,SP)                             cycles=1
0xbfc1:  00 00          neg ($00,SP)                             cycles=1
0xbfc3:  00 00          neg ($00,SP)                             cycles=1
0xbfc5:  00 00          neg ($00,SP)                             cycles=1
0xbfc7:  00 00          neg ($00,SP)                             cycles=1
0xbfc9:  00 00          neg ($00,SP)                             cycles=1
0xbfcb:  00 00          neg ($00,SP)                             cycles=1
0xbfcd:  00 00          neg ($00,SP)                             cycles=1
0xbfcf:  00 00          neg ($00,SP)                             cycles=1
0xbfd1:  00 00          neg ($00,SP)                             cycles=1
0xbfd3:  00 00          neg ($00,SP)                             cycles=1
0xbfd5:  00 00          neg ($00,SP)                             cycles=1
0xbfd7:  00 00          neg ($00,SP)                             cycles=1
0xbfd9:  00 00          neg ($00,SP)                             cycles=1
0xbfdb:  00 00          neg ($00,SP)                             cycles=1
0xbfdd:  00 00          neg ($00,SP)                             cycles=1
0xbfdf:  00 00          neg ($00,SP)                             cycles=1
0xbfe1:  00 00          neg ($00,SP)                             cycles=1
0xbfe3:  00 00          neg ($00,SP)                             cycles=1
0xbfe5:  00 00          neg ($00,SP)                             cycles=1
0xbfe7:  00 00          neg ($00,SP)                             cycles=1
0xbfe9:  00 00          neg ($00,SP)                             cycles=1
0xbfeb:  00 00          neg ($00,SP)                             cycles=1
0xbfed:  00 00          neg ($00,SP)                             cycles=1
0xbfef:  00 00          neg ($00,SP)                             cycles=1
0xbff1:  00 00          neg ($00,SP)                             cycles=1
0xbff3:  00 00          neg ($00,SP)                             cycles=1
0xbff5:  00 00          neg ($00,SP)                             cycles=1
0xbff7:  00 00          neg ($00,SP)                             cycles=1
0xbff9:  00 00          neg ($00,SP)                             cycles=1
0xbffb:  00 00          neg ($00,SP)                             cycles=1
0xbffd:  00 00          neg ($00,SP)                             cycles=1
0xbfff:  00 00          neg ($00,SP)                             cycles=1
